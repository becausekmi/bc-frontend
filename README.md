[HIGH_PRIORITY]

aggiungere parola 'started' alla prima position
NAVIGATION TREE: window on the discussion tree
NAVIGATION TREE: riallineare claims this
FEATURE: render URL on claims
FEATURE: upload audio on claims
CODING: claim component optimization
BUGFIX: message error on login
FEATURE: update debate info from user dashboard (cover, text, description)
FEATURE: password recovery
PUT LOADER WHEN THE POSITION IS NOT LOADED
FEATURE: discussion map -> aggiungere tasto per accoppiare lo scroll della discussion map alla parte centrale
FEATURE: discussion map -> main points icons

quando espando arguments scrollare all'inizio

PREVIEW URL type, search meta tags,

quando non sono nella debate page dopo login mi porta alla pagina profilo

anche se non è consentito mostrare icona cestino con tooltip
@ANGELO - "reflect" evidenziare icona quando lasciamo dei feedback

[LOW_PRIORITY]

improve data loading on scrolling
analytics
user data on profile dashboard

[MEETING_22_02]

Timeline -> 06/05 -> nine month ago with tooltip with the date
Timeline -> add popup with specific dates
Timeline -> try to add the sensemaking nuggets
Timeline -> try withough

-the slider feedback comparing you with the group mean 

# HIGH PRIORITY

- aggiungere dot dot dot nella timeline sulla sx

LAYOUT: aggiungere '...' sia sulla timeline che sulla discussion map (quando inizia togliere linea che esce dal box)
@todo
- check platformData and remove from claims model

- quando clicco su summary non evidenza argument o position
- drag/scroll 
- Quando clicco and other 1 people deve apparire la lista pls

# URGENT
- Analytics
- Explore
- Reflect

# RELEASE NOTE - 19/07/2022 - https://alpha.bcause.app/home

- version: v 1.7.19 gamma
- change the summary endpoint
- update the way sensemaking nuggets are rendered

# RELEASE NOT - 18/07/2022 - https://alpha.bcause.app/home

Hotfixing on the previous release:
- replicate the "join the group" functionlaity when inside the /discuss/landing page as is when inside the group landing page
- members list with (random) avatars or user photo 
- discussions header appears only if debates to be shown
- enlarge a bit the image in discussion landing page

# RELEASE NOTE - 16/07/2022 - https://alpha.bcause.app/home

- Auth fixes: save email on signup + realtime subscription fixes
- Group Landing Page: show members only to users who are part of the group

# RELEASE NOTE - 15/07/2022 - https://alpha.bcause.app/home

- Mobile Layout Hidden + Message to use desktop devices
- Responsive Debate Grid on Discussion Page

# RELEASE NOTE - 15/07/2022 - https://alpha.bcause.app/home

- SINGLE DISCUSSION LANDING -> GO TO THE DISCUSSION (in 'closed' debates) -> if you are not logged in will open login/signup modal, after login/signup will check if you are already part of the group you will go to the discussion otherwise will show you JOIN GROUP modal

- GROUP LANDING -> JOIN THE GROUP -> if you are not logged in will open login/signup modal, after login/signup if you are not part of the group will show you JOIN GROUP MODAL

- signup url -> https://alpha.bcause.app/auth/signup

- login url -> https://alpha.bcause.app/auth/login

On signup and login URL if you are already logged in you will see 'YOU'RE ALREADY LOGGED IN' message

# RELEASE NOTE - 20/05/2022 - https://alpha.bcause.app/home

- fix on collapse arguments
- fix delete icon 

# RELEASE NOTE - 20/05/2022 - https://alpha.bcause.app/home

- fix on url recognition on positions and arguments
- new data alignment on reflections
- code optimization on CLAIMS BOXES
- check discussion map highilight on claims
- data fix on argument reflection chart
- layout fix on CLAIMS BOXES
- contributor popup with data about the position

# RELEASE NOTE - 11/05/2022 - https://alpha.bcause.app/home

- fix on discussion map
- fix on empty analytics data
- restore of add pros / add cons buttons

# RELEASE NOTE - 11/05/2022 - https://alpha.bcause.app/home

- right padding on add claims text boxes
- today point on the left timeline
- "it is CLEAR I can trust this" fixed card and logic
- "prioritization" label fix
- reduce padding on position toolbar
- BUGFIX: bin icon is not shown

# RELEASE NOTE - 07/05/2022 - https://alpha.bcause.app/home

- add avatar close to the author
- remove 'thank you' interaction from my arguments
- remove 'thank you' count from contributors and avatars

# RELEASE NOTE - 07/05/2022 - https://alpha.bcause.app/home

- updating of the analytics views
- "reply to" popup layout improvement
- Why I cannot reply to a reply? Look at the reply below, there is only a “heart icon” (which is overlapped weirdly btw) but there is not a reply icon to its left. Please add.
- add tooltips to each of the action icons (pls change the look and feel of tooltips ;))


# RELEASE NOTE - 06/05/2022 - https://alpha.bcause.app/home

- Change give your opinion and reflect colours and try to have a button like style
- Change give your opinion icon
- Fix timeline (have to add dotted line)
- Add icon +/- to today and make it clickable (add claims popup)

# RELEASE NOTE - 06/05/2022 - https://alpha.bcause.app/home

- move "enter new position" box to the top
- no highlight on new position
- change highlights colors (orange -> arguments and positions, light blue -> my contributions)
- add ":" to position and argument author
- remove 'give your pros' / 'give your cons' buttons from arguments list bottom
- make 'add your overall opinion more evident'

# RELEASE NOTE - 05/05/2022 - https://alpha.bcause.app/home

- add debate description
- fix debate creation
- add delete button to debates
- create new debate creates a clone several times
- fix new position refresh in a new debate
- ask angelo to create new reply button with the arrow going onward the left (not right)
- description of a debate is gone, not shown anywhere (now shown on debate landing page)
- reply icon looks like forward icon
- MY CONTRIBUTIONS are still coloured in light blue, please change it to a more saturated colour, possibly on the green tones.
- edit the “give your opinion” pop up. Please follow consider.it model:
    change the “neutral” to “Slide Your Overall Opinion”
    change the text “add an opposing” to “Give your Cons”
    change the text “add a supporting” to “Give your Pros”
- change radar chart group average color
- add labels on reflection cards sliders
- add labels on popup left bar (position, question, arguments)
- on reflect input form is confusing what is what - maybe just label it!(be explicit) on the two extreme cases e.g. 0% = this is the least polarising,  needs more evidence,


# RELEASE NOTE 1.0.1- 04/03/2022 - https://alpha.bcause.app/home

- FEATURE: align argument reflection workflow to mturk003 (3 steps: agreement slider -> reflection values -> recap)
- FEATURE: highlights icons on the discussion map ()


# RELEASE NOTE 1.0.0- 01/03/2022 - https://alpha.bcause.app/home

- REFACTORING on fetching debate data and summary data 
- LAYOUT/FEATURE: Debate landing page
- LAYOUT: "have your opinion" -> "give your opinion" 
- LAYOUT: on agreement slider use "neutral" as default
- LAYOUT: "have your opinion" modal -> change buttons in plus and minus
- FEATURE: "have your opinion" modal -> keep input box texts and if you press save push whats' inside
- BUG: if you're not logged in show login modal


# RELEASE NOTE 0.0.11- 08/02/2022 - https://alpha.bcause.app/home

- LAYOUT: remove border from my contributions
- LAYOUT: position circle on timeline
- LAYOUT: make the claims central line less thick
- LAYOUT: use same font for input box placeholders 
- LAYOUT: remove blue line from summary
- BUGFIX: there is a bug in the faces showing when the faces are many: look at this debate~: https://bcause-alpha01.web.app/discuss/debate/-M_1YNLl74hj1-KvS_vL
- "have your opinion" su unico rigo su safari
- FEATURE: reflection counter on my contributions
- FEATURE: hearts counter on my contributions


# RELEASE NOTE 0.0.10- 04/02/2022 - https://alpha.bcause.app/home

- FEATURE: fullwidth scroll
- FEATURE: discussion map -> coupled scrolling between the discussion map and the central part
- FEATURE: navigation bar
- BUGFIX: focus on input box after click
- BUGFIX: "have your opinion" save button
- remove second step on "have your opinion"
- FEATURE: couple discussion map to central scroll when clicking on summary or single position/claims
- FEATURE: agreement slider on argument reflection
- RANDOM AVATARS on contributions
- FEATURE: render URL on positions

# RELEASE NOTE 0.0.9 - 31/01/2022 - https://alpha.bcause.app/home

- LAYOUT: review of box border radius
- LAYOUT: hightlight my contributions in a different color (bg + border) 
- LAYOUT: icon colors on hightlighting

# RELEASE NOTE 0.0.8 - 28/01/2022 - https://alpha.bcause.app/home

- summary review
- labels on most ... positions and arguments from Lucas Data

# RELEASE NOTE 0.0.7 - 25/01/2022 - https://alpha.bcause.app/home

- FEATURE: audio message on a single position
- BUGFIX: scroll position to from discussion map
- BUGFIX: slow transition on scroll
- LAYOUT: discussion map -> keep fixed question mark
- LAYOUT: discussion map -> on first load go to the end (today)

# RELEASE NOTE 0.0.6 - 18/01/2021 - https://bcause-alpha01.web.app/

- LAYOUT FIX: circle on claims timeline
- LAYOUT FIX: move action bar buttons to new positions (reply, have_your_opinion, reflect, heart)
- LAYOUT FIX: use 2 buttons on the popups (CTA or CLOSE)
- BUGFIX: pseudo is not initiliazed when signup
- BUGFIX: show correct date on agreement chart bar when sliding
- BUGFIX: when I click on a position is not highlighting
- FEATURE: position/claim author cannot delete when there are some contributions linked (reflection, agreements, hearts, claims)
- FEATURE: position/claim author cannot update when there are some contributions linked (reflection, agreements, hearts, claims)
- FEATURE: audio recording (@todo: save data on DB)

# RELEASE NOTE 0.0.5 - 12/01/2021 - https://bcause-alpha01.web.app/

- User Contributions Stats on profile page
- Date format on positions and arguments
- Discuss infographic image fix
- DiscussionMap: check link hitbox on the position square (light bulb)

# RELEASE NOTE 0.0.4 - 11/01/2021 - https://bcause-alpha01.web.app/

- Position RTDB Triggers
- Arguments RTDB Triggers
- Position Component Optimization
- Set Fixed width on central debate flow
- Debate header with timeline bar fixing
- Login/Logout refresh bugfix
- Data subscriptions bugfix
- Claim Reflections
- Delete Argument bugfix
- First layout review

# RELEASE NOTE 0.0.2 - 24/10/2021 - https://bcause-alpha01.web.app/

- Bugfix on push participant (lurker / user)

# RELEASE NOTE 0.0.1 - 24/10/2021 - https://bcause-alpha01.web.app/

- Push new participant if he never left arguments or position
- "Heart" interactions enabled
- Stop event propagation on Position Clicked


# BcFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Firebase configuration

Copy the firebaseConfig in the proper dir

Follow these steps
firebase login
firebase init
firebase deploy

