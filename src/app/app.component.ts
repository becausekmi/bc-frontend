import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from './core/services/api.service';

@Component({
  selector: 'app-root',
  template: `
    <app-layout *ngIf="ready$ | async">
      <router-outlet></router-outlet>
    </app-layout>
  `,
  styles: []
})
export class AppComponent {
  title = 'bc-frontend';

  ready$: Observable<boolean>;

  constructor(private apiService: ApiService) {
    this.ready$ = this.apiService.getIdentity()
      .pipe(
        map(() => true)
      );
  }

}
