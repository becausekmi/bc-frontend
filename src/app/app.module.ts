import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreModule } from './core/core.module';
import { ProfileModule } from './profile/profile.module';
import { DiscussModule } from './discuss/discuss.module';
import { ExploreModule } from './explore/explore.module';
import { ReflectModule } from './reflect/reflect.module';
import { DecideModule } from './decide/decide.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [ AppComponent ],
  imports: [
    RouterModule,
    ProfileModule,
    DiscussModule,
    ExploreModule,
    ReflectModule,
    DecideModule,
    CoreModule,
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
