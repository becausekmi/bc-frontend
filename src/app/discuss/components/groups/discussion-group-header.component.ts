import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { tap, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DiscussionGroupModalComponent } from './discussion-group-modal.component';
import { DiscussionGroupComponent } from './create-discussion-group.component';
import { GeneralDialogComponent } from 'src/app/shared/components/general-dialog.component';
import { MemberModalComponent } from './member-modal.component';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';

@Component({
  selector: 'app-discussion-group-header',
  template: `
        <div fxLayout="row" fxLayoutAlign="space-between end" style="padding-bottom:10px;border-bottom:1px solid rgb(216, 216, 216);width:100%">

            <div *ngIf="checkIfUserInGroup()" style="width:fit-content;background:orange;padding:2px 7px;color:white;font-size:14px;border-radius:4px">Member</div>

            <div fxLayout="row" fxLayoutAlign="start end" style="color:#0073b6;cursor:pointer" (click)="showMembers()">
            <mat-icon style="font-size:20px;height:20px">person</mat-icon>
            {{groupData.users.length}} members
            </div>

            <button (click)="join()" *ngIf="!checkIfUserInGroup()" class="sea-button" fxLayout="row" fxLayoutAlign="center center">Join the group</button>
        </div>
  `,
  styles: [`

      .hide {
        display:none;
      }


  `]
})
export class DiscussionGroupHeaderComponent {
  @Input() groupData!:any;
  @Input() userData!: any;
  @Output() updateData = new EventEmitter();
  userGroups:any;

  constructor(
    public dialog: MatDialog,
    private apiService:ApiService
  ) {
    
  }

  async ngOnChanges() {
    
  }

  

  join() {
    if (this.userData) {
      const dialogRef = this.dialog.open(DiscussionGroupModalComponent, {
        data:{
          userData: this.userData,
          discussionGroup: this.groupData
        }
      });
  
      dialogRef.afterClosed().subscribe(async newGroup => {
      })
    } else {
      this.openLoginModal();
    }
  }

  
  openLoginModal() {
    const dialogRef = this.dialog.open(SigninOrSignupComponent);
    dialogRef.afterClosed().subscribe(async newGroup => {
      /* this.identity$ = this.apiService.getIdentity();
      this.identity$.subscribe(data =>{
        this.userData = data; */
        if (this.userData) {
          this.join();
        }
      /* }) */
    })
  }

  showMembers() {
    if (this.checkIfUserInGroup()){
        
      const dialogRef = this.dialog.open(MemberModalComponent, {
        width:'400px',
        panelClass: 'no-padding',
        data:{
          userData: this.userData,
          discussionGroup: this.groupData
        }
      });
    } else {
      this.dialog.open(GeneralDialogComponent, {
        data:{
          title: 'Hidden informations',
          text: 'You can\'t see members if you are not part of the group'
        }
      });
    }
    
  } 
  
  checkIfUserInGroup() {
    if (this.userData && this.groupData && this.groupData.users.find((u:any)=>u === this.userData.email)) {
        return true;
    }

    return false;
  }

}
