import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { tap, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-member-detail',
  template: `
      <div fxLayout="row" style="margin-bottom:15px" fxLayoutAlign="start start" *ngIf="memberData">

      <img *ngIf="memberData.photoURL" style="margin-left:10px" class="avatar"  [src]="memberData.photoURL" />

        <img *ngIf="!memberData.photoURL && memberData.avatar" style="margin-left:10px" class="avatar"  [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+memberData.avatar+'.svg?alt=media'" />
        <img *ngIf="!memberData.photoURL && !memberData.avatar" style="margin-left:10px" class="avatar"  [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+ getRandomAvatarIndex() +'.svg?alt=media'" />

        <!-- <div *ngIf="!memberData.avatar" style="margin-left:10px" class="avatar"   ></div> -->
        <div fxFlex="10px"></div>
        <div fxLayout="column" >
            <div class="member" *ngIf="!isAdmin(memberData.email)">MEMBER</div>
            <div class="admin" *ngIf="isAdmin(memberData.email)">ADMIN</div>
            <div fxFlex="5px"></div>
            <div>{{ memberData.firstName}} {{ memberData.lastName}} </div>
            <b>{{ memberData.email}}  </b>
        </div>
      </div>
  `,
  styles: [`

      .member {
        width:fit-content;background:orange;padding:2px 7px;color:white;font-size:12px;border-radius:4px;
      }
      .admin {
        width:fit-content;background:green;padding:2px 7px;color:white;font-size:12px;border-radius:4px;
      }
      .avatar {
            width: 50px;
            border: 1px solid #919191;
            border-radius: 50px;
            height: 50px;
            background:#9e9e9e;
      }
  `]
})
export class MemberDetailComponent {
  @Input() group!:any;
  @Input() userData!: any;
  @Input() memberEmail!:any;
  user$ = new BehaviorSubject<any>(null);
  memberData:any = null;

  constructor(
    public dialog: MatDialog,
    private apiService:ApiService
  ) {
  }

  async ngOnChanges() {
    if (this.memberEmail) {
      //this.user$ = this.apiService.fetchUserByEmail(this.memberEmail);
      let data = await (this.apiService.fetchUserByEmail(this.memberEmail).pipe(take(1)).toPromise());
      console.log(data);
      if (data && data[0]) {
        this.memberData = data[0];
      }
    }

    console.log(this.memberData);
  }

  isMember(email:string) {

    if (this.group.users.indexOf(email) > -1) {
      return true;
    }

    return false;
  }

  isAdmin(email:string) {
    if (this.group.admins.indexOf(email) > -1) {
      return true;
    }

    return false;
  }

  getRandomAvatarIndex() {
    return Math.floor(Math.random() * (20)) + 1;
  }

}
