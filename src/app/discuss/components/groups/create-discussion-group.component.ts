import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { ApiService } from 'src/app/core/services/api.service';
import { MatChipInputEvent } from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { EmailService } from 'src/app/core/services/email.service';

@Component({
  selector: 'app-position-reply',
  template: `
    <div mat-dialog-content  fxLayout="column">
        <div fxLayout="row" fxLayoutAlign="space-between center">
          <div style="font-size:1rem;font-weight:bold" *ngIf="!alreadyExist">New Discussion Group</div>
          <div style="font-size:1rem;font-weight:bold" *ngIf="alreadyExist">Edit Discussion Group</div>
          <i class="material-icons" style="cursor:pointer" (click)="close(false)">close</i>
        </div>
        <div fxFlex="10px"></div>
        <div fxLayout="row"  class="margin-t-20 width-100">

          <div *ngIf="image && image!== ''" [style.backgroundImage]="'url('+image+')'" style="height:200px;width:200px;background-size:cover;border-radius:5px">

          </div>
          <div *ngIf="image && image!== ''" fxFlex="20px"></div>
          <div fxLayout="column" fxFlex>
            <b style="font-size:1rem;" [ngClass]="{'red':alert}">Name*</b>
            <input class="input-box" rows="5" [ngClass]="{'red-border':alert}" [(ngModel)]="title" placeholder="Write the discussion group name" required/>
            <div fxFlex="20px"></div>
            <b style="font-size:1rem;"> Write more here</b>
            <textarea rows="5" class="input-box" [(ngModel)]="text" placeholder="Write more info here" required></textarea>
          </div>
        </div>
        
        <div fxFlex="20px"></div>
        <div style="font-size:0.8rem;" [ngClass]="{'red':alert}"> Is the discussion group joining open?</div>
        <mat-radio-group aria-label="Select an option" [(ngModel)]="open" fxLayout="row" fxLayoutAlign="start center">
            <mat-radio-button [value]="true">YES</mat-radio-button>
            <div fxFlex="20px"></div>
            <mat-radio-button [value]="false">NO</mat-radio-button>
        </mat-radio-group>
        <div fxFlex="20px"></div>
        <div fxLayout="column" class="margin-t-20">
            <div style="font-size:0.8rem;">Discussion Group Cover</div>
            <div fxLayout="column" fxLayoutAlign="center start" fxFlex>
                <label *ngIf="!uploadCoverPercent" [for]="'file-upload-cover'" class="label-button" fxLayout="row" fxLayoutAlign="center center">

                </label>
                <div *ngIf="uploadCoverPercent" class="label-button margin-t-10 width-100"  fxLayout="row" fxLayoutAlign="center center">
                    {{ uploadCoverPercent| async  | number:'1.2-2'}} %
                </div>
                <input [id]="'file-upload-cover'" accept=".jpg,.jpeg,.png" type="file" (change)="uploadCover($event)" />
            </div>
        </div>
        <div fxFlex="20px"></div>
        <div fxLayout="row">
            <div fxFlex fxLayout="column">
              <mat-form-field class="example-chip-list" appearance="outline">
                <mat-label>Admins</mat-label>
                <mat-chip-list #chipListAdmins aria-label="Admins list">
                  <mat-chip *ngFor="let user of admins; let i = index" (removed)="removeAdmin(user)">
                    {{user}}
                    <button matChipRemove *ngIf="i > 0">
                      <mat-icon>cancel</mat-icon>
                    </button>
                  </mat-chip>
                  <input placeholder="New admin..."
                        [matChipInputFor]="chipListAdmins"
                        [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                        [matChipInputAddOnBlur]="addOnBlur"
                        (matChipInputTokenEnd)="addAdmin($event)">
                </mat-chip-list>
              </mat-form-field>
              <mat-form-field class="example-chip-list" appearance="outline">
                <mat-label>Users</mat-label>
                <mat-chip-list #chipListUsers aria-label="Admins list">
                  <mat-chip *ngFor="let user of users; let i = index" (removed)="removeUser(user)">
                    {{user}}
                    <button matChipRemove *ngIf="i > 0">
                      <mat-icon>cancel</mat-icon>
                    </button>
                  </mat-chip>
                  <input placeholder="New user..."
                        [matChipInputFor]="chipListUsers"
                        [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                        [matChipInputAddOnBlur]="addOnBlur"
                        (matChipInputTokenEnd)="addUser($event)">
                </mat-chip-list>
              </mat-form-field>
            </div>
            <div fxFlex="20px" fxLayout="column" *ngIf="data.group && data.group.standbyRequests && data.group.standbyRequests.length"></div>
            <div fxFlex="40" fxLayout="column" *ngIf="data.group && data.group.standbyRequests && data.group.standbyRequests.length">
                <b>Pending Requests</b>
                <div *ngFor="let request of data.group.standbyRequests;let i = index" fxLayout="row" fxLayoutAlign="space-between center">
                  <div> {{request.email}}</div>
                  <button style="background:orange;color:white" mat-button (click)="acceptRequest(request,i)">Accept</button>
                </div>
            </div>
        </div>
        <button mat-button class="button-blue-negative" [disabled]="admins.length == 0" (click)="save()" *ngIf="!alreadyExist ">Create New Discussion Group</button>
        <button mat-button class="button-blue-negative" [disabled]="admins.length == 0" (click)="save()" *ngIf="alreadyExist">Update Discussion Group</button>
        <div fxFlex="10px"></div>
        <div style="color:red" *ngIf="admins.length == 0">Please enter at least 1 admin</div>
        <div style="color:red" *ngIf="users.length == 0">Please enter at least 1 user</div>
        <div style="color:red" *ngIf="title === ''">Please enter a title</div>
        <div fxLayout="column" *ngIf="showConfirm" style="color:#1b5a0e">
            The request has been accepted and the user received an email.
        </div>
    </div>
  `,
  styles: [
    `
      .red{
        color:red;
      }

      .red-border {
        border-color:red!important;
      }

      .mat-chip-remove {
        min-width:0px!important;
      }

      .input-box {
        border-radius:5px;border:1px solid rgba(0, 0, 0, 0.87);padding:10px;
      }

      .mat-button-disabled {
        opacity:0.4;
      }

    `
  ]
})
export class DiscussionGroupComponent implements OnInit {

  title:string = '';
  text:string = '';
  image:string = '';
  showConfirm:boolean = false;
  addOnBlur = true;
  alert:boolean = false;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  uploadCoverPercent:any;
  alreadyExist:boolean = false;
  open:boolean = false;
  users:string[] = [];
  admins:string[] = [];
  id:any = null;

  constructor(
    private storage: AngularFireStorage, // @todo replace this
    private apiService: ApiService,
    public dialogRef: MatDialogRef<DiscussionGroupComponent>,
    private emailService: EmailService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    if (this.data && this.data.group) {
      this.alreadyExist = true;
      this.text = this.data.group.text;
      this.title = this.data.group.title;
      this.image = this.data.group.image;
      this.open = this.data.group.open;
      this.admins = this.data.group.admins;
      this.users = this.data.group.users;
      this.id = this.data.group.id;
    } else {
      
      this.admins.push(this.data.userData.email);
      this.users.push(this.data.userData.email);
    }
  }

  save() {
    console.log(this.users,this.admins);
    this.close({text:this.text,title:this.title,image:this.image,open:this.open,users:this.users,admins:this.admins, id:this.id});
  }


  close(save:any): void {
    let data = JSON.parse(JSON.stringify(save))
    console.log(data);
    if (data) {
      if (this.title !== '') {
        this.dialogRef.close(data);
      } else {
        this.alert = true;
      }
    } else {
      this.dialogRef.close(null);
    }
  }

  async uploadCover(event:any) {
    const file = event.target.files[0];
    const filePath = 'groups/covers/' + file.name;

    // @todo replace firebase storage
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    // observe percentage changes
    this.uploadCoverPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(async () => {
          this.uploadCoverPercent = null;
          this.image = await fileRef.getDownloadURL().toPromise();
        })
     )
    .subscribe();
  }

  removeAdmin(email: string): void {
    if (email !==this.data.userData.email ) {
      const index = this.admins.indexOf(email);

      if (index >= 0) {
        this.admins.splice(index, 1);
      }
    }
  }

  removeUser(email: string): void {
    if (email !==this.data.userData.email ) {
      const index = this.users.indexOf(email);

      if (index >= 0) {
        this.users.splice(index, 1);
      }
    }
  }

  addAdmin(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.admins.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();
    console.log(this.users,this.admins)
  }

  addUser(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.users.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();
    console.log(this.users,this.admins)
  }

  async acceptRequest(userData:any, index:number) {
    if (this.users.indexOf(userData.email) === -1) {
      this.users.push(userData.email);
      this.data.group.standbyRequests.splice(index,1)

      // SAVE DATA
      await this.apiService.updateDiscussionGroup(this.data.group);

      // SEND EMAIL
      let text = this.emailService.getDiscussionGroupRequestConfirmEmailTemplate(userData,this.data.group)
      await this.emailService.sendEmail(userData.email,'Bcause App - '+this.data.group.title+' Joining Request Approved',text);

      // SHOW CONFIRM MESSAGE
      this.showConfirm = true;
      setTimeout(()=>{
        this.showConfirm = false;
      },6000)
    }
  }

}
