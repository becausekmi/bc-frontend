import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { ApiService } from 'src/app/core/services/api.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { EmailService } from 'src/app/core/services/email.service';

@Component({
  selector: 'app-create-debate',
  template: `
    <div fxLayout="row" style="position:relative;height:100%">
        <i class="material-icons" style="cursor:pointer;position:absolute;right:20px;top:20px" (click)="close(false)">close</i>
        <!-- <div fxFlex="200px" style="background:#E8E8E8;height:100%;position:relative" fxLayout="column">
            <div class="arrow-left"></div>
        </div> -->

        <div  style="padding:20px" class="box-container" fxLayout="column" >
            <div style="font-weight:bold;" >Group Members</div>
            <div fxFlex="20px"></div>
            <div *ngFor="let user of users">
              <app-member-detail [group]="data.discussionGroup" [memberEmail]="user" [userData]="userData"  ></app-member-detail>
            </div>
            <!-- 

            </app-member-detail> -->
            <!-- <div fxLayout="row" fxLayoutAlign="start start">
                <img style="margin-left:10px" class="avatar"  [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+contributor.avatar+'.svg?alt=media'" />
                <div fxFlex="10px"></div>
                <div fxLayout="column" >
                   
                    
                </div>
                <div fxFlex="10px"></div>
            </div> -->
        </div>

    </div>
  `,
  styles: [
    `
    `
  ]
})
export class MemberModalComponent implements OnInit {

  text:string = '';
  debate:any;
  userData:any;
  selectedGroup:any = null;
  loader:boolean = false;
  users:any[] = [];

  constructor(
    private router:Router,
    private apiService: ApiService,
    public dialogRef: MatDialogRef<MemberModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    console.log(data);
    this.users = data.discussionGroup.users;
  }

  ngOnInit(): void {
  }


  async close(save:any) {
      this.dialogRef.close();
  }
}
