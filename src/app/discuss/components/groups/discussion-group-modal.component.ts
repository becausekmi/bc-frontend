import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { ApiService } from 'src/app/core/services/api.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { EmailService } from 'src/app/core/services/email.service';

@Component({
  selector: 'app-create-debate',
  template: `
    <div mat-dialog-content  fxLayout="column" class="scroll-bar" style="position:relative">
        <div *ngIf="loader" style="left:0px;position:absolute;width:100%;height:100%;background:rgba(255,255,255,0.9);z-index:10000" fxLayout="column" fxLayoutAlign="center center">
          <img src="assets/logo_blue.svg" height="80px" />
          <div>SENDING THE REQUEST</div>
        </div>
        <div fxLayout="row" fxLayoutAlign="space-between center">
          <div style="font-size:1rem;font-weight:bold">Join the discussion group</div>
          <!-- <i class="material-icons" style="cursor:pointer" (click)="close(false)">close</i> -->
        </div>
        <div  fxLayout="column" fxLayoutAlign="start start" *ngIf="!showEmailConfirm">
            <div *ngIf="debate">This discussion is accessible only to the <b>{{ selectedGroup.title }}</b> discussion group</div>
            Please click on the button here to send a request.
        </div>
        
        <div  fxLayout="column" fxLayoutAlign="start start" *ngIf="showEmailConfirm">
          <div fxFlex="10px"></div>
          The request has been sent to the admins.<br>You will receive an email as soon as the admins have approved your request.<br><b>Please check the SPAM</b>
        </div>

        <div fxFlex="10px"></div>
        <div fxLayout="row" fxLayoutAlign="space-between start">
          <div fxLayout="row" fxFlex fxLayoutAlign="start start" *ngIf="userData && !selectedGroup.open  && !showEmailConfirm">
              <button mat-button class="button-blue" (click)="askJoin()" style="width:100%">Ask to join the group</button>
          </div>
          <div fxFlex="20px"  *ngIf="userData && !selectedGroup.open  && !showEmailConfirm"></div>
          <div fxLayout="row" fxFlex fxLayoutAlign="start start" *ngIf="selectedGroup.open && userData && !showEmailConfirm ">
              <button mat-button class="button-blue" (click)="close(true)" style="width:100%">Join the group</button>
          </div>
          <div fxFlex="20px" *ngIf="selectedGroup.open && userData && !showEmailConfirm "></div>
          <div fxLayout="row" fxFlex fxLayoutAlign="start start" *ngIf="debate && (!selectedGroup.open || !userData)">
              <button mat-button class="button-blue-negative" (click)="close(false)" style="width:100%">Go Back</button>
          </div>
        </div>
    </div>
  `,
  styles: [
    `
    `
  ]
})
export class DiscussionGroupModalComponent implements OnInit {

  text:string = '';
  summary$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  debate:any;
  userData:any;
  selectedGroup:any = null;
  showEmailConfirm:boolean = false;
  loader:boolean = false;

  constructor(
    private router:Router,
    private apiService: ApiService,
    private emailService: EmailService,
    public dialogRef: MatDialogRef<DiscussionGroupModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    console.log(data);
    this.selectedGroup = data.discussionGroup;
    this.debate = data.debateData;
    this.userData = data.userData;
  }

  ngOnInit(): void {
  }

  async askJoin() {
    this.loader = true;
    let text = this.emailService.getDiscussionGroupRequestEmailTemplate(this.userData,this.selectedGroup);
    await this.emailService.sendEmail(this.selectedGroup.admins.join(),this.selectedGroup.title+' joining request',text);
    if (!this.selectedGroup.standbyRequests) {
      this.selectedGroup.standbyRequests = [];
    }
    if (!this.selectedGroup.standbyRequests.find((u:any)=>(u.email == this.userData.email))) {
      this.selectedGroup.standbyRequests.push(this.userData);
    }
    await this.apiService.updateDiscussionGroup(this.selectedGroup);
    this.showEmailConfirm = true;
    this.loader = false;
  }

  async close(save:any) {
    if (save) {
      this.selectedGroup.users.push(this.userData.email);
      await this.apiService.updateDiscussionGroup(this.selectedGroup);
      this.dialogRef.close(save);
    } else {
      this.dialogRef.close();
      this.router.navigate(['/discuss']);
    }
  }
}
