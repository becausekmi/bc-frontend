import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { tap, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DiscussionGroupModalComponent } from './discussion-group-modal.component';
import { DiscussionGroupComponent } from './create-discussion-group.component';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';

@Component({
  selector: 'app-group-grid',
  template: `
        <div *ngFor="let group of groups"style="margin-bottom:30px;cursor:pointer"  fxLayout="row"  fxLayoutAlign="start stretch" [routerLink]="'/discuss/group/'+group.id">
        
          <div fxLayout="column" >
            
              <div *ngIf="group.image && group.image!== ''" [style.backgroundImage]="'url('+group.image+')'" style="height:70px;width:70px;background-size:cover;border-radius:5px">

              </div>
              <!-- <div fxFlex="10px"></div>
              <div> {{group.users.length}} <mat-icon>person</mat-icon></div> -->
          </div>
          <div fxFlex="20px"></div>
          <div  fxLayout="column"  fxLayoutAlign="space-between start" fxFlex>
            <div style="font-size:1rem;font-weight:bold"> {{ group.title}}</div>
            <div fxFlex="10px"></div>
            <div fxLayout="row" fxLayoutAlign="space-between stretch" style="width:100%" >
              <div *ngIf="!checkIfUserInGroup(group.id)" (click)="join($event,group)" style="cursor:pointer;text-decoration:underline;color:#0073b6;font-weight:500" mat-button>Join the group</div>
              <div style="" *ngIf="checkIfUserInGroup(group.id)" style="background:orange;padding:2px 7px;color:white;font-size:14px;border-radius:4px">Member</div>
              <mat-icon *ngIf="userData && group.admins.indexOf(userData.email) > -1"  style="color:#0073b6;cursor:pointer" (click)="updateDiscussionGroup($event,group)">edit</mat-icon>
            </div>
          </div>
        </div>
  `,
  styles: [`

      .hide {
        display:none;
      }


  `]
})
export class GroupsGridListComponent {
  @Input() groups!:any;
  @Input() userData!: any;
  @Output() updateData = new EventEmitter();
  userGroups:any;

  constructor(
    public dialog: MatDialog,
    private apiService:ApiService
  ) {
    
  }

  async ngOnChanges() {
    if (this.userData) {
      this.userGroups = await this.apiService.fetchGroupsByUser(this.userData).pipe(take(1)).toPromise();
      console.log(this.userGroups);
    }
  }

  checkIfUserInGroup(id:any) {
    if (this.userGroups && this.userGroups.find((g:any)=> g.id === id)) {
      return true;
    } else {
      return false;
    }
  }

  emitNewData() {   
    this.updateData.emit(true);
  }

  join(event:any,group:any) {
    event.stopPropagation();

    if (this.userData) {
    const dialogRef = this.dialog.open(DiscussionGroupModalComponent, {
      data:{
        userData: this.userData,
        discussionGroup: group
      }
    });

    dialogRef.afterClosed().subscribe(async newGroup => {
    })
   } else { 
      this.openLoginModal(group);
    }
  }

  openLoginModal(group:any) {
    const dialogRef = this.dialog.open(SigninOrSignupComponent);
    dialogRef.afterClosed().subscribe(async newGroup => {
      /* this.identity$ = this.apiService.getIdentity();
      this.identity$.subscribe(data =>{
        this.userData = data; */
        if (this.userData) {
          this.join(null,group);
        }
      /* }) */
    })
  }

  
  updateDiscussionGroup(event:any,group:any) {
    event.stopPropagation();
    const dialogRef = this.dialog.open(DiscussionGroupComponent, {
        width: '800px',
        data: {
            group
        }
    });

    dialogRef.afterClosed().subscribe(async save => {
        if (save) {
          await this.apiService.updateDiscussionGroup(save);
          this.userGroups = await this.apiService.fetchGroupsByUser(this.userData).pipe(take(1)).toPromise();
        }
      });
  }

}
