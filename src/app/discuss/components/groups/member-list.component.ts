import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { tap, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-group-grid',
  template: `
        
  `,
  styles: [`

      .hide {
        display:none;
      }


  `]
})
export class MemberListComponent {
  @Input() group!:any;
  @Input() user!: any;

  constructor(
    public dialog: MatDialog,
    private apiService:ApiService
  ) {
    
  }

  async ngOnChanges() {
  }


}
