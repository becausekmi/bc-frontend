import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-parent-box',
  template: `
   <div class="parent-box" (click)="goToParent($event, content.id)" >
    <div fxLayout="row" fxLayoutAlign="start center">
        <img src="assets/reply-black.svg"  height="15px"/>
        <div fxFlex="10px"></div>
        <div style="font-size:1rem;font-weight: bold;" *ngIf="(participant$ | async) && (participant$ | async).pseudo ">{{ (participant$ | async).pseudo }}</div>
        <div style="font-size:1rem;font-weight: bold;" *ngIf="!(participant$ | async) || !(participant$ | async).pseudo ">---</div>
    </div>
    <div style="font-size:1rem;">{{content.text}}</div>
   </div>
  `,
  styles: [
    `
      .parent-box {
        margin:0px 20px;
        background:#eaeaea;
        padding:20px;
        border-radius:10px;
        margin-bottom:10px;
      }
      `
  ]
})
export class ParentBoxComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() contentId!:any;
  @Input() type!:any;
  @Input() positions!:any;
  @Input() debateKey!:any;
  @Input() arguments!:any;

  content:any;
  participant$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
    if (this.type === 'position' && this.contentId) {
      this.content = this.positions.find((p:Position)=>p.id === this.contentId);
    }

    if (this.content) {
      this.participant$ = this.apiService.fetchParticipant(this.debateKey, this.content.metadata.createdBy);
    }
  }

  goToParent(event:any, contentId:string) {
    if (this.type === 'position') {
      let position = this.positions.find((p:Position)=>p.id === contentId);
      //this.positionClicked(event, position);
    }
    let element = document.getElementById(contentId);
    if (element) {
      element.scrollIntoView({block:'center'});
    }
  }
}
