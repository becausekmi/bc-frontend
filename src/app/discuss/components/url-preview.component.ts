import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-url-preview',
  template: `
    <div *ngIf="temp === true" fxLayout="row" fxLayoutAlign="start start" class="url-box">
      <mat-icon style="position:absolute;right:10px;top:10px;cursor:pointer" (click)="resetMeta()">delete_outline</mat-icon>
      <div class="url-image" *ngIf="meta.image" [style.backgroundImage]="'url('+meta.image+')'"></div>

      <div fxLayout="column" style="padding:20px">
        <div style="font-size:20px;font-weight: bold">{{meta.title}}</div>
        <div fxFlex="5px"></div>
        <div>{{meta.description}}</div>
      </div>
    </div>
    <a [href]="meta.url" target="blank" *ngIf="temp === false" fxLayout="row" fxLayoutAlign="start start" class="url-box">
      <div class="url-image" *ngIf="meta.image" [style.backgroundImage]="'url('+meta.image+')'"></div>
      <div fxLayout="column" style="padding:20px">
        <div style="font-size:20px;font-weight: bold">{{meta.title}}</div>
        <div fxFlex="5px"></div>
        <div>{{meta.description}}</div>
      </div>
    </a>
  `,
  styles: [
    `
      .url-box{
        border-radius:5px;
        background:#fffdee;
        width:100%;
        margin-top:10px;
        text-decoration: none;
        color:black;
        padding:10px;
        position:relative;
      }

      .url-image {
        width:100px;height:100px;background-size:cover;border-radius:5px
      }

    `
  ]
})
export class UrlPreviewComponent implements OnInit {

    @Input() meta: any;
    @Input() temp: any;
    @Output() resetMetaEmitter = new EventEmitter();

    constructor() {
    }

    ngOnInit(): void {
    }

    resetMeta() {
      this.resetMetaEmitter.emit(true);
    }
}
