import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { PositionService } from 'src/app/core/services/position.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';
import { ContributorsComponent } from '../contributors.component';
import { ReflectionArgumentComponent } from './claim-reflection.component';
import { ClaimReplyComponent } from './claim-reply.component';
import { RecommendationSelectorComponent } from './recommendation-selection.component';

@Component({
  selector: 'app-claim-box',
  template: `
    
  <div *ngIf="claim && claim.type === 'OPPOSING'" class="claim" fxLayout="column" >
    <div class="date" fxLayout="row" fxLayoutAlign="end center">
        <div class="pin opposing"></div>
        {{ dateSince }} ago
    </div>

    <!-- CLAIM BODY -->

    <div [ngClass]="{'highlighted-argument-con':highlighted, 'my-contribution':!highlighted && userData && claim.metadata.createdBy === userData.uid}" [id]="claim.id" class="claim-box opposing" fxLayout="column" (click)="claimClicked($event,claim)">

      <app-claim-highlights [claim]="claim" [summary]="summary"></app-claim-highlights>

      <!-- DELETE BUTTON -->
      <div class="delete-button-claim-opposing" *ngIf="userData && claim.metadata.createdBy === userData.uid " >
        <mat-icon (click)="deleteClaim(claim)">delete_outline</mat-icon>
      </div>

      <!-- IF IS A REPLY TO -->
      <div *ngIf="claim.parentId" class="parent-box" (click)="goToParentClaim($event,claim.parentId)">
          <img src="assets/reply-black.svg"  height="15px"/>
          <div fxLayout="row" fxLayoutAlign="start center">
          <div fxFlex="10px"></div>
          <div class="author" *ngIf="debateService.participantsDict[debateService.argumentsDict[claim.parentId].metadata.createdBy]">{{ debateService.participantsDict[debateService.argumentsDict[claim.parentId].metadata.createdBy].pseudo }}</div>
          <div class="author" *ngIf="!debateService.participantsDict[debateService.argumentsDict[claim.parentId].metadata.createdBy]">---</div>
          </div>
          <div class="text">{{debateService.argumentsDict[claim.parentId].text}}</div>
      </div>
      <div [id]="claim.id+'-anchor'" style="position:absolute;top:-300px;z-index:0"></div>

      <!-- ROW AUTHOR + REPLY/HEART -->
      <div fxLayout="row" fxLayoutAlign="space-between center">

          <div class="author" *ngIf="debateService.participantsDict && debateService.participantsDict[claim.metadata.createdBy]">{{ debateService.participantsDict[claim.metadata.createdBy].pseudo }}</div>
          <div class="author" *ngIf="!debateService.participantsDict ||!debateService.participantsDict[claim.metadata.createdBy]">---</div>
          <div fxLayout="row" fxLayoutAlign="end center">
              <!-- REPLY -->
              <img *ngIf="!claim.parentId && !highlighted" (click)="replyClaim(claim)" src="assets/reply.svg" style="cursor:pointer" height="15px"/>
              <img *ngIf="!claim.parentId && highlighted" (click)="replyClaim(claim)" src="assets/reply_white.svg" style="cursor:pointer" height="15px"/>
              <div fxFlex="10px"></div>
              <!-- LIKE -->
              <app-argument-heart  fxLayout="row" fxLayoutAlign="start center" [highlighted]="highlighted" [userData]="userData" [argument]="claim"></app-argument-heart>
          </div>
      </div>

      <div fxFlex="5px"></div>

      <!-- CLAIM TEXT -->
      <div class="text"> {{claim.text}}</div>

      <app-url-preview *ngIf="claim.attachedUrl" [temp]="false" [meta]="claim.attachedUrl"></app-url-preview>
      <!-- TOOLBAR -->
      <div style="width:100%;height:0.5px;background: #E2E2E2;margin-top:10px;margin-bottom: 10px;" ></div>
      <div fxLayout="row" fxLayoutAlign="space-between center">
          <div fxLayout="row" fxLayoutAlign="space-between center" class="width-100">

              
              <div fxLayout="row" fxLayoutAlign="start center"  style="cursor:pointer" (click)="reflectionArgument(claim)">
                  <!-- REFLECT -->    
                  <mat-icon *ngIf="highlighted" style="color:white">psychology</mat-icon>
                  <mat-icon *ngIf="!highlighted">psychology</mat-icon>
                  <div fxFlex="5px"></div>
                  <div style="font-size:0.7rem" >Reflect</div>
                  <!-- <div style="margin-left:5px" >({{claim.platformData.reflectionsCounter}})</div> -->
              </div>
              <!-- Recommendation -->
              <!--
              <div fxLayout="row" fxLayoutAlign="start end"  style="cursor:pointer" (click)="fetchRecommendations(claim)">
                  <mat-icon>attach_file</mat-icon>
                  <div fxFlex="5px"></div>
                  <div style="font-size:0.7rem">Fetch args</div>
              </div>
              -->

              

              <div fxFlex fxLayout="row" fxLayoutAlign="end center" >
                  <!--  <div *ngFor="let contributor of claim.platformData.contributors; let i = index" style="margin-left: -10px;">
                      <div *ngIf="i < 10" (click)="showContributors(claim.platformData.contributors,i)">
                          <img class="avatar" *ngIf="debateService.participantsDict[contributor] && i%2 === 0" [matTooltip]="debateService.participantsDict[contributor].pseudo" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+(i+1)%30+'.svg?alt=media'" height="30px"/>
                          <img class="avatar"  *ngIf="debateService.participantsDict[contributor] && i%2 !== 0" [matTooltip]="debateService.participantsDict[contributor].pseudo" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+(i+1)%30+'.svg?alt=media'" height="30px"/>
                      </div>
                      <div *ngIf="i === 10" style="text-decoration: underline;margin-left:5px;font-size:12px" (click)="showContributors(claim.platformData.contributors,null)">and other {{position.platformData.contributors.length - 10}} people</div>
                  </div> -->
              </div>

          </div>
      </div>
    </div>
  </div>


  <div *ngIf="claim && claim.type === 'SUPPORTING'" class="claim" fxLayout="column">
    <div class="date" fxLayout="row" fxLayoutAlign="start center">
      <div class="pin supporting"></div>
      {{ dateSince }} ago
    </div>
    <div [ngClass]="{'highlighted-argument-pro':highlighted, 'my-contribution':!highlighted && userData && claim.metadata.createdBy === userData.uid}" [id]="claim.id" class="claim-box support" fxLayout="column" (click)="claimClicked($event,claim)">

      <app-claim-highlights [claim]="claim" [summary]="summary"></app-claim-highlights>

      <!-- DELETE BUTTON -->
      <div class="delete-button-claim-supporting" *ngIf="userData && claim.metadata.createdBy === userData.uid" >
        <mat-icon (click)="deleteClaim(claim)">delete_outline</mat-icon>
      </div>

      <!-- IF IS A REPLY TO -->
      <div *ngIf="claim.parentId && debateService.argumentsDict && debateService.argumentsDict[claim.parentId]" class="parent-box" (click)="goToParentClaim($event,claim.parentId)">
          <div fxLayout="row" fxLayoutAlign="start center">
          <img src="assets/reply-black.svg" height="15px"/>
          <div fxFlex="10px"></div>
          <div class="author" *ngIf="debateService.participantsDict && debateService.participantsDict[debateService.argumentsDict[claim.id].metadata.createdBy]">{{ debateService.participantsDict[debateService.argumentsDict[claim.id].metadata.createdBy].pseudo }}</div>
          <div class="author" *ngIf="!debateService.participantsDict || !debateService.participantsDict[debateService.argumentsDict[claim.id].metadata.createdBy]">---</div>
          </div>
          <div class="text">{{debateService.argumentsDict[claim.parentId].text}}</div>
      </div>

      <div [id]="claim.id+'-anchor'" style="position:absolute;top:-300px;z-index:0"></div>

      <!-- ROW AUTHOR + REPLY/HEART -->
      <div fxLayout="row" fxLayoutAlign="space-between center">

          <div class="author" *ngIf="debateService.participantsDict && debateService.participantsDict[claim.metadata.createdBy]">{{ debateService.participantsDict[claim.metadata.createdBy].pseudo }}</div>
          <div class="author" *ngIf="!debateService.participantsDict ||!debateService.participantsDict[claim.metadata.createdBy]">---</div>
          <div fxLayout="row" fxLayoutAlign="end center">
              <!-- REPLY -->
              <img *ngIf="!claim.parentId && !highlighted" (click)="replyClaim(claim)" src="assets/reply.svg" style="cursor:pointer" height="15px"/>
              <img *ngIf="!claim.parentId && highlighted" (click)="replyClaim(claim)" src="assets/reply_white.svg" style="cursor:pointer" height="15px"/>
              <div fxFlex="10px"></div>
              <!-- LIKE -->
              <app-argument-heart  fxLayout="row" fxLayoutAlign="start center" [highlighted]="highlighted" [userData]="userData" [argument]="claim"></app-argument-heart>
          </div>
      </div>

      <div fxFlex="5px"></div>

      <!-- CLAIM TEXT -->
      <div class="text"> {{claim.text}}</div>
      <app-url-preview *ngIf="claim.attachedUrl" [temp]="false" [meta]="claim.attachedUrl"></app-url-preview>

      <!-- TOOLBAR -->
      <div style="width:100%;height:0.5px;background: #E2E2E2;margin-top:10px;margin-bottom: 10px;" ></div>

      <div fxLayout="row" fxLayoutAlign="space-between center">
        <div fxLayout="row" fxLayoutAlign="space-between center" class="width-100">

          <!-- REFLECT -->
          <div fxLayout="row" fxLayoutAlign="start center" (click)="reflectionArgument(claim)" style="cursor:pointer">
              
              <mat-icon *ngIf="highlighted" style="color:white">psychology</mat-icon>
              <mat-icon *ngIf="!highlighted">psychology</mat-icon>
              <div fxFlex="5px"></div>
              <div style="font-size:0.7rem">Reflect</div>
              <!--  <div style="margin-left:5px" >({{claim.platformData.reflectionsCounter}})</div> -->
          </div>
          <!-- Recommendation -->
          <!--
          <div fxLayout="row" fxLayoutAlign="start end"  style="cursor:pointer" (click)="fetchRecommendations(claim)">
                <mat-icon>attach_file</mat-icon>
                <div fxFlex="5px"></div>
                <div style="font-size:0.7rem">Fetch args</div>
            </div>
            -->
          
          <div fxFlex fxLayout="row" fxLayoutAlign="end center" >
              <!-- <div *ngFor="let contributor of claim.platformData.contributors; let i = index" >
              <div *ngIf="i < 10" (click)="showContributors(claim.platformData.contributors,i)">
                  <img class="avatar" *ngIf="debateService.participantsDict[contributor] && i%2 === 0" [matTooltip]="debateService.participantsDict[contributor].pseudo" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+(i+1)%30+'.svg?alt=media'" height="30px"/>
                  <img class="avatar"  *ngIf="debateService.participantsDict[contributor] && i%2 !== 0" [matTooltip]="debateService.participantsDict[contributor].pseudo" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+(i+1)%30+'.svg?alt=media'" height="30px"/>
              </div>
              <div *ngIf="i === 10" style="text-decoration: underline;margin-left:5px;font-size:12px" (click)="showContributors(claim.platformData.contributors,null)">and other {{position.platformData.contributors.length - 10}} people</div>
              </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>

  `,
  styleUrls: ['./claim-box.component.scss']
})
export class ClaimBoxComponent implements OnChanges {

  @Input() claim!:Argument;
  @Input() summary:any;
  @Input() position!:Position;
  @Input() userData: Identity | null = null;
  highlighted: boolean = false;
  dateSince:string = '';
  contributors$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  
  constructor(
      private apiService: ApiService,
      private dialog: MatDialog,
      public debateService: DebateService,
      public statsService: StatsService,
      public positionService: PositionService,
  ) { }

  ngOnChanges(): void {
    this.dateSince  = this.statsService.getTimeSince(this.claim.metadata.createdDate)

    this.positionService.selectedClaimObservable$.subscribe(id => {
      if(this.claim && id !== '' && id === this.claim.id){
        this.highlighted = true;
      } else {
        this.highlighted = false;
      }
    })

    
  }

  
  async deleteClaim(claim:Argument) {
    const dialogRef = this.dialog.open(DialogYesNoComponent, {
      data:{
        title: 'Delete argument',
        text: 'Are you sure you want to delete the argument and all his data (reflections)?'
      }
    });

    dialogRef.afterClosed().subscribe(async yes => {
        if (yes) {
          await this.apiService.deleteArgument(claim);
        }
    });
  }

  reflectionArgument(argument:Argument) {

    if (this.userData) {

      const dialogRef = this.dialog.open(ReflectionArgumentComponent, {
        data: {argument, userData:this.userData},
        panelClass: 'no-padding',
        width:"850px"
      });

      dialogRef.afterClosed().subscribe(saveArgument => {
        /* if (saveArgument && this.userData) {
          console.log(saveArgument);
          //
        } */
      });
    } else {
      this.dialog.open(SigninOrSignupComponent)
    }
  }

  fetchRecommendations(argument:Argument){
    if (this.userData) {



      const dialogRef = this.dialog.open(RecommendationSelectorComponent, {
        data: {argument, userData:this.userData},
        panelClass: 'no-padding',
        width:"850px"
      });

      dialogRef.afterClosed().subscribe(saveArgument => {
        /* if (saveArgument && this.userData) {
          console.log(saveArgument);
          //
        } */
      });
    } else {
      this.dialog.open(SigninOrSignupComponent)
    }
  }

  showContributors(contributors:any, index:number | null) {
    console.log(contributors);

    const dialogRef = this.dialog.open(ContributorsComponent, {
      data:{
        contributors: contributors,
        index: index
      },
      panelClass: 'no-padding',
      width:"600px",
      height:"600px"
    });

    dialogRef.afterClosed().subscribe(async newClaims => {
    })
  }

  replyClaim(claim:Argument) {
    
    if (this.userData) {
      if (!claim.parentId && this.position) {
        const dialogRef = this.dialog.open(ClaimReplyComponent, {
          width:'600px',
          data: {claim, position: this.position, participantsDict: this.debateService.participantsDict, userData:this.userData}
        });

        dialogRef.afterClosed().subscribe(saveClaim => {
          if (saveClaim) {
            console.log(saveClaim)
            this.pushReplyClaim(saveClaim);
          }
        });
      }
    } else {
      this.dialog.open(SigninOrSignupComponent)
    }
  }

  async pushReplyClaim(claim:Argument) {
    if (this.userData) {

      let newId = await this.apiService.createArgument(claim);

      if (newId) {
        claim.id = newId;
      }

      setTimeout(()=>{

        let element = document.getElementById(claim.id+'-anchor');
        if (element) {
          element.scrollIntoView();
        }

      },100);
    } else {
      this.dialog.open(SigninOrSignupComponent)
    }
  }

  goToParentClaim(event:any, claimId:string) {
    let claim:any = this.debateService.argumentsArray.find(a => a.id === claimId);
    this.claimClicked(event, claim)
    let element = document.getElementById(claimId);
    if (element) {
      element.scrollIntoView({block:'center'});
    }
  }
  

  claimClicked(event:any, claim:Argument) {
    console.log(claim);
    event.stopPropagation();
    this.highlighted =  !this.highlighted;
    if (this.highlighted) {
      let element = document.getElementById('discussion-map-'+claim.positionId);
      if (element) {
        element.scrollIntoView({block:'center'});
        /* this.positionService.notifySelectedPosition(this.position.id); */
      }
    }

    if (this.highlighted) {
      this.positionService.notifySelectedClaim(this.claim.id);
    } else {
      this.positionService.notifySelectedClaim('');
    }

  }
}