import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DebateService } from 'src/app/core/services/debate.service';

@Component({
  selector: 'app-claim-reply',
  template: `
    <div mat-dialog-content *ngIf="data.claim" fxLayout="column">
      <div fxLayout="row" fxLayoutAlign="space-between center">
        <div style="font-size:1rem;font-weight:bold">Reply to</div>
        <i class="material-icons" style="cursor:pointer" (click)="close(false)">close</i>
      </div>
        <div fxFlex="10px"></div>
        <div [ngClass]="{'contributions-labels-supporting': data.claim.type === 'SUPPORTING', 'contributions-labels-opposing':data.claim.type === 'OPPOSING'}" fxLayout="row" fxLayoutAlign="center center">
            <mat-icon style="font-size:16px;height:16px;width:16px" *ngIf="data.claim.type === 'OPPOSING'">remove_circle</mat-icon>
            <mat-icon style="font-size:16px;height:16px;width:16px" *ngIf="data.claim.type === 'SUPPORTING'">add_circle</mat-icon>
            <div fxFlex="5px"></div>
            <div>ARGUMENT</div>
        </div>
        <div fxFlex="5px"></div>
        <div class="position-info-box" fxLayout="column" >
              <div style="font-size:1rem;font-weight: bold;" *ngIf="debateService.participantsDict && debateService.participantsDict[data.claim.metadata.createdBy]">{{ debateService.participantsDict[data.position.metadata.createdBy].pseudo }}:</div>
              <div style="font-size:1rem;font-weight: bold;" *ngIf="!debateService.participantsDict || !debateService.participantsDict[data.claim.metadata.createdBy]">---</div>
              <div style="line-height: 1.5;font-size:1rem;">{{data.claim.text}}</div>
        </div>
        <div fxFlex="20px"></div>
        <textarea rows="5" style="border-radius:5px;border:1px solid rgba(0, 0, 0, 0.87);padding:10px;" [(ngModel)]="text" placeholder="Elaborate on your reply" ></textarea>
        <div fxFlex="20px"></div>

        <div style="background:#EFEFEF;padding:10px;border-radius:5px;" fxLayout="column" >
          <div style="font-size:0.7rem;">choose one of the following options</div>
          <div fxFlex="5px"></div>
          <div style="font-size:1rem;font-weight:bold">This argument is in</div>
          <div fxFlex="5px"></div>
          <mat-radio-group  [(ngModel)]="type" aria-label="Select an option">
            <mat-radio-button class="support-radio" value="SUPPORTING" style="margin-right:10px">support of</mat-radio-button>
            <mat-radio-button class="opposing-radio" value="OPPOSING">opposition to</mat-radio-button>
          </mat-radio-group>
          <div fxFlex="10px"></div>
          <div style="font-size:0.85rem;font-weight:bold"  *ngIf="data.participantsDict[data.position.metadata.createdBy]">{{ data.participantsDict[data.position.metadata.createdBy].pseudo }}</div>
          <div style="font-size:0.85rem;font-weight:bold" *ngIf="!data.participantsDict[data.position.metadata.createdBy]">---</div>
          <div style="font-size:0.8rem;"> {{data.position.text}}</div>
        </div>
        <div fxFlex="20px"></div>


        <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">
              
          <button fxFlex mat-button class="button-blue-negative" (click)="close(false)" >Close</button>
          <div fxFlex="20px"></div>
          <button fxFlex mat-button class="button-blue" [ngClass]="{'opacity-5':text === '' || !type}" (click)="close(true)" [disabled]="text === '' || !type">Reply</button>
        </div>
        <div fxFlex="10px"></div>
    </div>
  `,
  styles:  ['']
})
export class ClaimReplyComponent implements OnInit {

  text:string = '';
  type:string = '';

  constructor(public dialogRef: MatDialogRef<ClaimReplyComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public debateService:DebateService) { 
    console.log(JSON.parse(JSON.stringify(data)))
  }

  ngOnInit(): void {
  }

  
  close(save:boolean): void {
      if (save) {
        let newClaim = {
          debateId: this.data.claim.debateId,
          id: '',
          metadata: {
            createdDate: new Date().getTime(),
            createdBy: this.data.userData.uid
          },
          parentId: this.data.claim.id,
          positionAgreement: null,
          positionId: this.data.position.id,
          text: this.text,
          type: this.type,
          platformData: {
            highlighted:true,
            currentReplyClaim: null,
          }
        }

        this.dialogRef.close(newClaim);
      } else {
        this.dialogRef.close();
      }
  }
}
