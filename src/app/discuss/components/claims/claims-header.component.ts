import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-claims-header',
  template: `
    <div fxLayout="row" style="padding:0px 20px;" >

        <div fxFlex class="opposing-header" fxLayout="row" fxLayoutAlign="center center">
          <div> CONS</div>
        </div>

        <div fxFlex="20px"></div>

        <div style="width:2px;" ></div>

        <div fxFlex="20px"></div>

        <div fxFlex class="support-header" fxLayout="row" fxLayoutAlign="center center">
           <div> PROS</div>
        </div>

    </div>
  `,
  styles: [
    `

    .support-header {
        color:#007FC1;
        border-bottom:2px solid #007FC1;
    }

    .opposing-header {
        color:#FE9329;
        border-bottom:2px solid #FE9329;
    }

    `
  ]
})
export class ClaimsHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


}
