import { Component, OnInit, Inject, Input } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { StatsService } from 'src/app/core/services/stats.service';

@Component({
  selector: 'app-add-claims',
  template: `
    <div fxLayout="row" style="position:relative;height:100%">
        <i class="material-icons" style="cursor:pointer;position:absolute;right:20px;top:20px" (click)="close(false)">close</i>
        <div fxFlex="200px" style="background:#E8E8E8;height:100%;position:relative" fxLayout="column">
          <div style="padding:20px" fxLayout="column" fxLayoutAlign="start start">
              <div class="contributions-labels-question " fxLayout="row" fxLayoutAlign="center center">
                  <mat-icon style="font-size:16px;height:16px;width:16px">help</mat-icon>
                  <div fxFlex="5px"></div>
                  <div>QUESTION</div>
              </div>
              <div fxFlex="10px"></div>
            <span style="font-weight:bold;text-align:left;padding:0px;"> {{ debate?.title }}</span>
          </div>
          <div class="arrow-left"></div>
        </div>

        <div fxFlex="650px" style="padding:20px" class="box-container" fxLayout="column" *ngIf="step === 1">

          <div class="position-info-box" fxLayout="column" >
              <div style="font-size:1rem;font-weight: bold;" *ngIf="debateService.participantsDict && debateService.participantsDict[position.metadata.createdBy]">{{ debateService.participantsDict[position.metadata.createdBy].pseudo }}:</div>
              <div style="font-size:1rem;font-weight: bold;" *ngIf="!debateService.participantsDict || !debateService.participantsDict[position.metadata.createdBy]">---</div>
              <div style="line-height: 1.5;font-size:1rem;">{{position.text}}</div>
          </div>
          <div fxFlex="50px"></div>
          <div  fxLayout="row" fxLayoutAlign="center center" style="width:100%">
            <app-agreement-slider [lastUserAgreement]="(agreementLastValue)" [position]="position" [userData]="userData!" (setNewAgreementData)="saveSliderData($event)" style="width:80%"></app-agreement-slider>
          </div>
          <div fxFlex="20px"></div>

          <div fxFlex style="overflow-y:scroll;background:white" class="scroll-bar claims" >
            <div fxLayout="column" style="min-height:100%">
              <!-- INPUT TEXT BAR -->

              <div fxLayout="row" fxLayoutAlign="center center" style="width:100%;padding:10px 20px">

                <!-- OPPOSING -->

                <div *ngIf="!currentOpposingNewClaim" fxFlex class="big-input-box" fxLayout="column" (click)="addOpposing()">
                  <textarea rows="1" class="grey-border border-orange scroll-textarea"  id="input-box" type="text" [placeholder]="'Give your Cons'" ></textarea>
                  <button class="button-icon" fxLayout="row" fxLayoutAlign="end center" style="color:#fe9329">
                    <!-- <img src="assets/send-orange.svg"  height="15px"/> -->
                    <mat-icon>add_circle</mat-icon>
                  </button>
                </div>

                <div fxFlex *ngIf="currentOpposingNewClaim && currentOpposingNewClaim.type === 'OPPOSING'" class="big-input-box" fxLayout="column" >
                    <textarea rows="1" cdkFocusInitial   (input)="autoGrow($event.target)" class="grey-border border-orange scroll-textarea"  id="input-box-opposing" type="text" [placeholder]="claimPlaceholder" [(ngModel)]="currentOpposingNewClaim.text" (keyup.enter)="pushOpposingClaim()" ></textarea>
                    <button class="button-icon" [disabled]="currentOpposingNewClaim.text === ''" (click)="pushOpposingClaim()" fxLayout="row" fxLayoutAlign="end center" style="color:#fe9329">
                      <!-- <img src="assets/send-orange.svg"  height="15px"/> -->
                      <mat-icon>add_circle</mat-icon>
                    </button>
                </div>

                <div fxFlex="40px"></div>

                <!-- SUPPORTING -->
                <div fxFlex *ngIf="!currentSupportingNewClaim"  class="big-input-box" fxLayout="column" (click)="addSupporting()">
                  <textarea rows="1" class="grey-border border-blue scroll-textarea"  id="input-box" type="text" [placeholder]="'Give your Pros'" ></textarea>
                  <button class="button-icon" fxLayout="row" fxLayoutAlign="end center" style="color:#007fc1">
                    <!-- <img src="assets/send-light-blue.svg"  height="15px"/> -->
                    <mat-icon>add_circle</mat-icon>
                  </button>
                </div>

                <div fxFlex class="big-input-box" fxLayout="column" *ngIf="currentSupportingNewClaim && currentSupportingNewClaim.type === 'SUPPORTING'">

                  <textarea rows="1" (input)="autoGrow($event.target)" class="grey-border border-blue scroll-textarea"  id="input-box-supporting" type="text" [placeholder]="claimPlaceholder" [(ngModel)]="currentSupportingNewClaim.text" (keyup.enter)="pushSupportingClaim()" ></textarea>
                  <button class="button-icon" [disabled]="currentSupportingNewClaim.text === ''" (click)="pushSupportingClaim()" fxLayout="row" fxLayoutAlign="end center" style="color:#007fc1">
                    <!-- <img src="assets/send-light-blue.svg"  height="15px"/> -->
                    <mat-icon>add_circle</mat-icon>
                  </button>
                </div>
              </div>
              <div fxLayout="row" style="width:100%;padding:20px;" fxFlex>
                <div fxLayout="column" fxFlex>
                  <div *ngFor="let claim of opposingClaims" class="claim-box">
                    {{claim.text}}
                  </div>
                </div>
                <div fxFlex="20px"></div>

                <div style="width:3px;" class="line"></div>

                <div fxFlex="20px"></div>
                <div fxLayout="column" fxFlex>
                  <div *ngFor="let claim of supportingClaims" class="claim-box">
                      {{claim.text}}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div fxFlex="20px"></div>

          <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">
              <button fxFlex mat-button class="button-blue-negative" (click)="close(false)" >Close</button>
              <div fxFlex="20px"></div>
              <button fxFlex mat-button class="button-blue" (click)="close(true)" >Save</button>
          </div>
        </div>

        <div fxFlex="650px" style="padding:20px" class="box-container" fxLayout="column" *ngIf="step === 2">
          <div style="font-weight:normal;padding-left:0px;padding-right:40px" >See how you and other users react to this position</div>
          <div fxFlex="10px"></div>
          <div style="font-weight:normal;background:rgb(232, 232, 232);padding:20px;border-radius:10px" >{{ data.position.text }}</div>
          <div fxFlex="20px"></div>
          <div fxLayout="column" fxLayoutAlign="space-between start" fxFlex="200px" >

            <div style="color:#007FC1;font-size:14px;font-weight: 600;">Agreement Chart ({{position.platformData.agreements.length}})</div>
            <div fxFlex="20px" ></div>
            <div fxLayout="row"  fxLayoutAlign="space-between start"  style="width:100%;font-size:10px;position:relative;height:100%;padding:0px 20px">
              <div style="position:absolute;top:0px;left:0px;width:100%;height:100%">
                <div style="z-index:10000;position:absolute;" [style.left]="(agreement.positionAgreement+100)/200*100+'%'" [style.top.px]="agreement.top" *ngFor="let agreement of position.platformData.agreements" >
                  <div (mouseenter)="agreement.showName= true" (mouseleave)="agreement.showName= false" style="background:white;border:1px solid black;width:20px;height:20px;border-radius:15px" fxLayout="row" fxLayoutAlign="center center">
                    {{ agreement.name}}
                  </div>
                  <div *ngIf="agreement.showName" style="position:absolute;top:-20px;right:0px;white-space: nowrap;background:rgba(255,255,255,1);border:1px solid grey; border-radius:5px;padding-left:3px;padding-right:3px;z-index:10000">
                      {{ agreement.fullName}}
                  </div>
                </div>
              </div>



              <!-- AGREEMENT BARS -->
              <div fxLayout="column" fxLayoutAlign="start center" style="position:relative;height:100%">
                  <div style="border:0.5px solid rgba(0,0,0,0.2);z-index:1" fxFlex></div>
                  <div fxFlex="10px"></div>
                  <img fxFlex="20px" src="assets/disagree01.svg" height="20px" style="cursor:pointer" />
                  <div  style="position:absolute;top:170px;text-align: center;line-height: 1;">strongly<br>disagree</div>
              </div>

              <div fxLayout="column" fxLayoutAlign="start center" style="position:relative;height:100%" >
                <div style="border:0.5px solid rgba(0,0,0,0.2);z-index:1" fxFlex></div>
                <div fxFlex="10px"></div>
                <img src="assets/disagree01.svg" style="cursor:pointer"  height="20px"/>
                <div  style="position:absolute;top:170px;text-align: center;line-height: 1;">disagree</div>
              </div>


              <div fxLayout="column" fxLayoutAlign="start center" style="position:relative;height:100%">
                <div style="border:0.5px solid rgba(0,0,0,0.2);z-index:1;line-height: 1;" fxFlex></div>
                <div fxFlex="10px"></div>
                <img src="assets/disagree02.svg" style="cursor:pointer" height="20px"/>
                <div style="position:absolute;top:170px;text-align: center;line-height: 1;">somewhat<br>disagree</div>
              </div>


              <div fxLayout="column" fxLayoutAlign="start center" style="position:relative;height:100%">
                <div style="border:0.5px solid rgba(0,0,0,0.2);z-index:1" fxFlex></div>
                <div fxFlex="10px"></div>
                <img src="assets/disagree02.svg" style="cursor:pointer" height="20px"/>
                <div style="position:absolute;top:170px;text-align: center;line-height: 1;">Slide your<br>Overall Opinion</div>
              </div>


              <div fxLayout="column" fxLayoutAlign="start center" style="position:relative;height:100%">
                <div style="border:0.5px solid rgba(0,0,0,0.2);z-index:1" fxFlex></div>
                <div fxFlex="10px"></div>
                <img src="assets/agree01.svg" style="cursor:pointer"  height="20px"/>
                <div style="position:absolute;top:170px;text-align: center;line-height: 1;">somewhat<br>agree</div>
              </div>


              <div fxLayout="column" fxLayoutAlign="start center" style="position:relative;height:100%">
                <div style="border:0.5px solid rgba(0,0,0,0.2);z-index:1" fxFlex></div>
                <div fxFlex="10px"></div>
                <img src="assets/agree01.svg" style="cursor:pointer" height="20px"/>
                <div  style="position:absolute;top:170px;text-align: center;line-height: 1;">agree</div>
              </div>


              <div fxLayout="column" fxLayoutAlign="start center" style="position:relative;height:100%">
                <div style="border:0.5px solid rgba(0,0,0,0.2);z-index:1" fxFlex></div>
                <div fxFlex="10px"></div>
                <div  style="position:absolute;top:170px;text-align: center;line-height: 1;">strongly<br>agree</div>
                <img src="assets/agree02.svg" style="cursor:pointer"  height="20px"/>
              </div>

            </div>
          </div>
          <div fxFlex></div>

          <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">

            <button fxFlex mat-button class="button-blue-negative" (click)="close(false)" >Close</button>
            <div fxFlex="20px"></div>
            <button fxFlex mat-button class="button-blue" (click)="close(true)" >Save</button>

          </div>

        </div>
    </div>
  `,
  styles:  [`
    /* .box-container { display: flex; flex-direction: column; } */
    .pro {background:#007FC1;color:white;}
    .con {background:#FE9329;color:white;}
    span {  padding:10px;text-align:center;font-size:1rem }

    .arrow-left {
      width: 0;
      height: 0;
      border-top: 10px solid transparent;
      border-bottom: 10px solid transparent;
      border-right:10px solid white;
      position:absolute;
      right:0px;
      top:20px;
    }


    .claims {
      border:1px solid #dedede;
      padding-top:20px;
      border-radius:5px;
      margin-top:20px;
    }

    .line {
      background:#dedede;
    }



    .claim-box {
      background:white;
      color:black;
      padding:10px;
      margin-bottom:20px;
      position:relative;
      border-radius:5px;
      background:#dedede;
    }

    .big-input-box {
      position:relative;
    }

    .grey-border {
      border:1px solid #CBCBCB;
    }

    input {
      background:white;
      border-radius:25px;
      width:100%;
      box-sizing: border-box;
      border:none;
      outline: none;
      padding:15px;
    }

    textarea {
      background:white;
      border-radius:25px;
      width:100%;
      box-sizing: border-box;
      border:none;
      outline: none;
      padding:15px;
      resize: none;
    }

    .scroll-textarea {
      padding-right:40px;
    }

    .scroll-textarea::-webkit-scrollbar {
      display: none;
    }

    img {
      cursor:pointer;
    }

    .button-icon {
      position:absolute;
      right:15px;
      top:0px;
      height:47px;
      min-width:30px;
      background:none;
      border:none;
      font-size:0.8rem;
      padding:0px;
    }
  `]
})
export class AddClaimsComponent implements OnInit {

  debate:Debate;
  position!:Position;
  userData!:Identity;
  claimPlaceholder:string = '';
  newClaims:Argument[] = [];
  currentOpposingNewClaim:Argument | null = null;
  currentSupportingNewClaim:Argument | null = null;
  supportingClaims:Argument[] = [];
  opposingClaims: Argument[] = [];
  step:number = 1;

  agreementLastValue$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  agreementLastValue:any = null;

  constructor(
    public dialogRef: MatDialogRef<AddClaimsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public debateService: DebateService,
    private statsService: StatsService,
    private apiService: ApiService
    ) {

    console.log(JSON.parse(JSON.stringify(data)))
    this.position = data.position;
    this.userData = data.userData;
    this.debate = data.debate;
    this.agreementLastValue$ = this.apiService.fetchLastUserAgreementByPosition(this.position, this.userData.uid);
    this.agreementLastValue$.subscribe(data =>{
      this.agreementLastValue = data;
    })
  }

  ngOnInit(): void {
    

  }


  close(save:boolean): void {
    if (save) {

      if (!this.position.platformData.agreementLastValue) {
        this.position.platformData.agreementLastValue = 0;
      } else {
        this.debateService.pushPositionAgreement(this.position,this.userData);
      }
      this.pushOpposingClaim();
      this.pushSupportingClaim();
      this.dialogRef.close(this.newClaims);
    } else {
      this.dialogRef.close();
    }
  }


  saveSliderData(value:number) {
    this.position.platformData.agreementLastTS = new Date().getTime();
    this.position.platformData.agreementLastValue = value;

    console.log(this.position.platformData);
  }

  autoGrow(element:any) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
  }


  addSupporting() {

    this.currentSupportingNewClaim = {
      type: 'SUPPORTING',
      parentId:null,
      attachedUrl:null,
      debateId: this.position.debateId,
      id: '',
      metadata: {
        createdBy: this.userData.uid,
        createdDate: new Date().getTime()
      },
      positionAgreement: null,
      positionId: this.position.id,
      text: '',
      platformData: {
        agreements: [],
        agreementLastValue: 0,
        agreementAverageValue: 0,
        agreementLastTS: new Date().getTime(),
        currentReplyClaim: null,
        dateSince: this.statsService.getTimeSince(new Date().getTime()),
        state:'Neutral',
        reflectionsCounter:0,
        heartsCounter:0,
        contributors:[]
      },
      externalClaimsAttached:null
    }

    this.claimPlaceholder = 'Tell us why you agree';
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      let element = document.getElementById("input-box-supporting")
      if (element) {
        element.focus();
      }
    },200);
  }

  addOpposing() {
    this.currentOpposingNewClaim =  {
        type: 'OPPOSING',
        parentId:null,
        attachedUrl:null,
        debateId: this.position.debateId,
        id: '',
        metadata: {
          createdBy: this.userData.uid,
          createdDate: new Date().getTime()
        },
        positionAgreement: null,
        positionId: this.position.id,
        text: '',
        platformData: {
          agreements: [],
          agreementLastValue: 0,
          agreementAverageValue: 0,
          agreementLastTS: new Date().getTime(),
          currentReplyClaim: null,
          dateSince: this.statsService.getTimeSince(new Date().getTime()),
          state:'Neutral',
          reflectionsCounter:0,
          heartsCounter:0,
          contributors:[]
        },
        externalClaimsAttached:null
    }

    this.claimPlaceholder = 'Tell us why you disagree';

    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      let element = document.getElementById("input-box-opposing")
      if (element) {
        element.focus();
      }
    },200);

  }

  pushSupportingClaim() {
    if (this.currentSupportingNewClaim) {
      this.newClaims.push(this.currentSupportingNewClaim);
      this.supportingClaims = this.newClaims.filter((claim)=>{return claim.type === 'SUPPORTING'});
      this.opposingClaims = this.newClaims.filter((claim)=>{return  claim.type === 'OPPOSING'});
      console.log(this.newClaims, this.opposingClaims, this.supportingClaims);
      this.currentSupportingNewClaim = null;
    }
  }

  pushOpposingClaim() {
    console.log(this.currentSupportingNewClaim, this.currentOpposingNewClaim);
    if (this.currentOpposingNewClaim) {
      this.newClaims.push(this.currentOpposingNewClaim);
      this.supportingClaims = this.newClaims.filter((claim)=>{return claim.type === 'SUPPORTING'});
      this.opposingClaims = this.newClaims.filter((claim)=>{return  claim.type === 'OPPOSING'});
      console.log(this.newClaims, this.opposingClaims, this.supportingClaims);
      this.currentOpposingNewClaim = null;
    }
  }
}
