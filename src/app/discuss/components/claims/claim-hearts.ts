import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-argument-heart',
  template: `
    <img src="assets/heart.svg" (click)="popArgumentHeart($event,argument)" *ngIf="userData && (heart$ | async)" height="15px"/>
    <img src="assets/heart-empty.svg" (click)="pushArgumentHeart($event,argument)"  *ngIf="!highlighted && !(heart$ | async)" height="15px"/>
    <img src="assets/heart_empty_white.svg" (click)="pushArgumentHeart($event,argument)"  *ngIf="highlighted && !(heart$ | async)" height="15px"/>
    <div style="margin-left:5px" *ngIf="userData && argument.metadata.createdBy === userData.uid && (heartCounter$ | async)">({{(heartCounter$ | async)}})</div>
  `,
  styles: [

  ]
})
export class HeartArgumentComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() argument!:Argument;
  @Input() highlighted: boolean = false;

  heart$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  heartCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
    if (this.argument && this.userData)  {
      this.heart$ = this.apiService.fetchHeartsByArgumentAndUser(this.argument, this.userData);
      this.heartCounter$ = this.apiService.fetchHeartsByArgument(this.argument);
    }
  }

  async pushArgumentHeart(event:any, argument:Argument) {
      event.stopPropagation();
      if (this.userData) {
        let heart = {
          debateId: argument.debateId,
          metadata: {
            createdBy: this.userData.uid,
            createdDate: new Date().getTime()
          },
          claimId: argument.id,
          positionId: argument.positionId,
          value: 1
        }
        await this.apiService.createHeartOnArgument(heart, argument, this.userData);
      } else {
        this.dialog.open(SigninOrSignupComponent)
      }
  }

  async popArgumentHeart(event:any, argument:Argument) {
    
      event.stopPropagation();
      if (this.userData) {
        this.apiService.deleteHeartOnArgument(argument, this.userData)
      } else {
        this.dialog.open(SigninOrSignupComponent)
      }
  }
}