import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DebateService } from 'src/app/core/services/debate.service';

/* HIGHCHARTS */

import * as Highcharts from 'highcharts';
import Item from 'highcharts/modules/item-series';
import HighchartsMore from 'highcharts/highcharts-more';
import { ReflectionArgument } from 'src/app/core/models/reflection';
import { Argument } from 'src/app/core/models/argument';
import { Identity } from 'src/app/auth/models/Identity';
import { Position } from 'src/app/core/models/position';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';

/* SLIDER */


Item(Highcharts);
HighchartsMore(Highcharts)


@Component({
  selector: 'app-reflection-argument',
  template: `
  <div fxLayout="row" style="position:relative">
  <i class="material-icons" style="cursor:pointer;position:absolute;right:20px;top:20px" (click)="close(null)">close</i>
    <div fxFlex="200px" style="background:#E8E8E8;" fxLayout="column">
        
        <div style="padding:20px" *ngIf="selectedPosition" fxLayout="column" fxLayoutAlign="start start">
            <div class="contributions-labels-position " fxLayout="row" fxLayoutAlign="center center">
                <mat-icon style="font-size:16px;height:16px;width:16px">lightbulb</mat-icon>
                <div fxFlex="5px"></div>
                <div>POSITION</div>
            </div>
            <div fxFlex="10px"></div>
           <span style="font-weight:bold;text-align:left;padding:0px;"> {{ selectedPosition.text }}</span>
        </div>
        <div style="border-top:white solid 1px;padding:20px" *ngIf="selectedPosition" fxLayout="column" fxLayoutAlign="start start">
            <div [ngClass]="{'contributions-labels-supporting': data.argument.type === 'SUPPORTING', 'contributions-labels-opposing':data.argument.type === 'OPPOSING'}" fxLayout="row" fxLayoutAlign="center center">
                <mat-icon style="font-size:16px;height:16px;width:16px" *ngIf="data.argument.type === 'OPPOSING'">remove_circle</mat-icon>
                <mat-icon style="font-size:16px;height:16px;width:16px" *ngIf="data.argument.type === 'SUPPORTING'">add_circle</mat-icon>
                <div fxFlex="5px"></div>
                <div>ARGUMENT</div>
            </div>
            <div fxFlex="10px"></div>
            <div style="font-weight:normal;text-align:left;padding:0px;overflow-wrap: break-word;">{{ data.argument.text }}</div>
        </div>
    </div>

    <app-loader fxFlex="650px"  *ngIf="loading"></app-loader>
    <div fxFlex="650px" style="padding:20px" *ngIf="!loading" class="chart-container">

        <!-- STEP 1 -->

        <div *ngIf="userData && step === 1" fxLayout="column" style="width:100%">
            <div style="font-size:1rem;font-weight:bold;margin-bottom:10px">How do you feel about this argument?</div>
            <div fxFlex="50px"></div>
            <div  fxLayout="row" fxLayoutAlign="center center" style="width:100%">
                <app-agreement-argument-slider [lastUserAgreement]="(agreementLastValue)" [disabled]="false" [argument]="selectedArgument" [userData]="userData!" (setNewAgreementData)="saveSliderData($event)" style="width:80%"></app-agreement-argument-slider >
            </div>
            <div fxFlex="50px"></div>
            <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">
                <button fxFlex mat-button disabled><mat-icon>chevron_left</mat-icon><span >BACK</span></button>
                <div fxFlex="20px"></div>
                <button fxFlex mat-button (click)="step = 2;checkAgreementLevel()"><span >NEXT</span> <mat-icon>chevron_right</mat-icon></button>
            </div>
        </div>

        <!-- STEP 2 -->

        <div *ngIf="userData && step === 2" fxLayout="column" style="width:100%">
            <div style="font-size:1rem;font-weight:bold;margin-bottom:10px">To what extend do you agree that</div>
            <!-- HAS HE NEVER LEFT REFLECTIONS -->

            <div class="slider-box" >
                <div class="card-box pro">
                    <span>This is a very <b>polarized</b> argument</span>
                </div>
                <div class="range-slider">
                    <div class="thumb-slider" [style.left.%]="userLevels.polarization" >{{userLevels.polarization}} %</div>
                    <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.polarization">
                    <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                        <div fxLayout="row" fxLayoutAlign="start center" >
                            <div>low polarization</div>
                            <!-- <div fxFlex="10px"></div>
                            <div>low polarization</div> -->
                        </div>
                        <div fxLayout="row" fxLayoutAlign="end center">
                            <div>high polarization</div>
                            <!-- <div fxFlex="10px"></div>
                            <mat-icon>east</mat-icon> -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="slider-box">
                <div class="card-box pro" >
                    <span>It is <b>clear</b> I can <b>trust</b> this</span>
                </div>
                <div class="range-slider">
                    <div class="thumb-slider" [style.left.%]="userLevels.trust" >{{userLevels.trust}} %</div>
                    <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.trust">
                    <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                        <div fxLayout="row" fxLayoutAlign="start center" >
                            <!-- <mat-icon>west</mat-icon>
                            <div fxFlex="10px"></div> -->
                            <div>low trust</div>
                        </div>
                        <div fxLayout="row" fxLayoutAlign="end center">
                            <div>high trust</div>
                            <!-- <div fxFlex="10px"></div>
                            <mat-icon>east</mat-icon> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-box" >
                <div class="card-box con">
                    <span>This needs more <b>evidences</b></span>
                </div>
                <div class="range-slider">
                    <div class="thumb-slider" [style.left.%]="userLevels.evidences" >{{userLevels.evidences}} %</div>
                    <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.evidences">
                    <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                        <div fxLayout="row" fxLayoutAlign="start center" >
                            <!-- <mat-icon>west</mat-icon>
                            <div fxFlex="10px"></div> -->
                            <div>enough evidence</div>
                        </div>
                        <div fxLayout="row" fxLayoutAlign="end center">
                            <div>need evidence</div>
                            <!-- <div fxFlex="10px"></div>
                            <mat-icon>east</mat-icon> -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="slider-box">
                <div class="card-box con">
                    <span>Most people would <b>agree</b> with this</span>
                </div>
                <div class="range-slider">
                    <div class="thumb-slider" [style.left.%]="userLevels.agree" >{{userLevels.agree}} %</div>
                    <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.agree">
                    <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                        <div fxLayout="row" fxLayoutAlign="start center" >
                            <!-- <mat-icon>west</mat-icon>
                            <div fxFlex="10px"></div> -->
                            <div>low group agreement</div>
                        </div>
                        <div fxLayout="row" fxLayoutAlign="end center">
                            <div>high group agreement</div>
                            <!-- <div fxFlex="10px"></div>
                            <mat-icon>east</mat-icon> -->
                        </div>
                    </div>
                </div>
            </div>

            <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">

                <button fxFlex mat-button (click)="step = 1"><mat-icon>chevron_left</mat-icon><span >BACK</span></button>
                <div fxFlex="20px"></div>
                <button fxFlex mat-button class="button-blue"  (click)="saveFeedbacks()">SAVE</button>

            </div>
        </div>

        <!-- STEP 3 -->

        <div *ngIf="!userData || step === 3" style="width:100%" fxLayout="column">
            <div fxFlex="50px"></div>
            <div  fxLayout="row" fxLayoutAlign="center center" style="width:100%">
                <app-agreement-argument-slider [lastUserAgreement]="agreementLastValue" [disabled]="true" [argument]="data.argument" [userData]="userData!" (setNewAgreementData)="saveSliderData($event)" style="width:80%"></app-agreement-argument-slider>
            </div>
            <div fxFlex="50px"></div>
            <div style="font-size:1rem;font-weight:bold">Reflections feedback</div>
            <!-- <div>«{{data.argument.text}}»</div> -->
            <!-- SHOW RADAR CHART -->
            <highcharts-chart
                [Highcharts]="Highcharts"
                [options]="chartRadarOptions"
                *ngIf="showReflectionsChart && chartRadarOptions"
            ></highcharts-chart>
            <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">

                <button fxFlex mat-button class="button-blue-negative" (click)="close(null)" >Close</button>
                <div fxFlex="20px"></div>
                <button  fxFlex mat-button class="button-blue"  (click)="step = 1">RETHINK IT THROUGH</button>

            </div>
        </div>
    </div>
    </div>
  `,
  styles:  [`
        .chart-container { display: flex; flex-direction: column; }
        .slider-box { display:flex; flex-direction:row; align-items: center}
        .card-box {
            margin-bottom: 10px;
            display: flex;
            flex-direction: column;
            align-content:center;
            justify-content:center;
            border-radius:5px;
            min-width:110px;
            max-width:110px;
            height:110px;
            margin-right:10px;
        }
        .pro {background:#007FC1;color:white;}
        .con {background:#FE9329;color:white;}
        span {  padding:10px;text-align:center;font-size:1rem }
        .range-slider { width:470px!important}
        .slider-reflection { flex-grow:1;margin-bottom:10px}
      `]
})
export class ReflectionArgumentComponent implements OnInit {

  Highcharts: typeof Highcharts = Highcharts;
  text:string = '';
  type:string = '';
  userData:Identity|null = null;
  step: number = 1;
  chartRadarOptions: Highcharts.Options| null = null;
  radarChartData:any;
  showReflectionsChart:boolean = false;
  loading:boolean = true;
  selectedPosition: Position | null = null;
  selectedArgument: Argument | null = null;

  
  agreementLastValue$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  agreementLastValue:any = null;

  userLevels = {
    agree: 50,
    evidences: 50,
    trust: 50,
    polarization: 50
  }

  constructor(
      public dialogRef: MatDialogRef<ReflectionArgumentComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public debateService:DebateService,
      public apiService: ApiService
    ) {
    console.log(JSON.parse(JSON.stringify(data)))
    this.userData = data.userData;
    this.selectedPosition = this.debateService.positionsDict[data.argument.positionId];
    let claim = this.debateService.argumentsArray.find(a => a.id === data.argument.id);

    if (claim) {
        console.log(claim);
        this.selectedArgument = claim;
            
        if (this.userData) {
            this.agreementLastValue$ = this.apiService.fetchLastUserAgreementByArgument(this.selectedArgument, this.userData.uid);
            this.agreementLastValue$.subscribe(data =>{
                console.log(data);
                this.agreementLastValue = data;
            })
        }
    }
  }

  checkAgreementLevel() {
      
    console.log(JSON.parse(JSON.stringify(this.selectedArgument)));
    if (this.selectedArgument && !this.selectedArgument.platformData.agreementLastValue) {
     this.selectedArgument.platformData.agreementLastValue = 0;
    }
  }

  ngOnInit(): void {
    this.generateRadarChart();
  }

  saveSliderData(value:any) {
    if (this.selectedArgument) {
        this.selectedArgument.platformData.agreementLastTS = new Date().getTime();
        this.selectedArgument.platformData.agreementLastValue = value;
    }
    console.log(JSON.parse(JSON.stringify(this.selectedArgument)));
  }

  close(save:Argument|null): void {

    this.dialogRef.close(save);
  }

  generateRadarChart() {

    let yourReflectionsArray: any[] = [];
    let otherReflectionsArray: any[] = [];

    if (this.data.argument) {

        if (this.userData && this.debateService.reflectionsByArgumentsDict && this.debateService.reflectionsByArgumentsDict[this.data.argument.id] && this.debateService.reflectionsByArgumentsDict[this.data.argument.id][this.userData.uid]) {
            let levels = this.debateService.reflectionsByArgumentsDict[this.data.argument.id][this.userData.uid].levels;

            yourReflectionsArray[0]=levels.agree
            yourReflectionsArray[1]=levels.evidences
            yourReflectionsArray[2]=levels.polarization
            yourReflectionsArray[3]=levels.trust
            this.userLevels = {
                agree: levels.agree,
                polarization: levels.polarization,
                trust: levels.trust,
                evidences: levels.evidences
            }
            this.step = 3;
        }

        let argumentReflections = this.debateService.reflectionsByArgumentsArray.filter(a => a.claimId === this.data.argument.id );
        console.log(argumentReflections);
        if (argumentReflections.length) {
            otherReflectionsArray = [0,0,0,0];
            let counter = 1;
            for (let reflection of argumentReflections) {
                otherReflectionsArray[0]+=reflection.levels.agree
                otherReflectionsArray[1]+=reflection.levels.evidences
                otherReflectionsArray[2]+=reflection.levels.polarization
                otherReflectionsArray[3]+=reflection.levels.trust

                otherReflectionsArray[0]= otherReflectionsArray[0]/counter
                otherReflectionsArray[1]= otherReflectionsArray[1]/counter
                otherReflectionsArray[2]= otherReflectionsArray[2]/counter
                otherReflectionsArray[3]= otherReflectionsArray[3]/counter

                counter++;
            }
        }

    }

    console.log(yourReflectionsArray, otherReflectionsArray);

    this.radarChartData = [yourReflectionsArray,otherReflectionsArray]

    this.chartRadarOptions = {
      chart: {
          polar: true,
          type: 'line'
      },

      accessibility: {
          description: ''
      },

      title: {
          text: '',
          x: -80
      },

      pane: {
          size: '80%'
      },

      xAxis: {
          categories: ['Group Agreement', 'Need of evidence', 'Polarization', 'Trust'],
          tickmarkPlacement: 'on',
          lineWidth: 0,
          labels: {
            style:{
                width:300,
                whiteSpace:'normal'//set to normal
            },
            formatter: function() {
                return this.value.toString()
            }
          }
      },

      yAxis: {
          gridLineInterpolation: 'polygon',
          lineWidth: 0,
          min: 0
      },

      tooltip: {
          shared: true,
          pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
      },

      legend: {
          align: 'center',
          verticalAlign: 'top',
          layout: 'horizontal'
      },


      series: [{
        type: 'area',
        name: 'Me',
        color:'#ffd968',
        fillOpacity:0.2,
        data: this.radarChartData[0],
        pointPlacement: 'on'
      }, {
        type: 'area',
        color:'#ffbfa4',
        name: 'Group average',
        fillOpacity:0.1,
        data: this.radarChartData[1],
        pointPlacement: 'on'
      }],

      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      align: 'center',
                      verticalAlign: 'bottom',
                      layout: 'horizontal'
                  },
                  pane: {
                      size: '70%'
                  }
              }
          }]
      }
    }

    setTimeout(()=>{
      this.showReflectionsChart = true;
      this.loading = false;
    }, 100)

  }

  async saveFeedbacks() {
    if (this.userData && this.debateService.debateKey && this.selectedArgument) {

        let reflectionObj:ReflectionArgument = {
            debateId: this.debateService.debateKey,
            levels: this.userLevels,
            metadata: {
                createdBy: this.userData.uid,
                createdDate: new Date().getTime()
            },
            positionId: this.data.argument.positionId,
            claimId: this.data.argument.id,
        }
        console.log(this.userLevels);

        console.log(JSON.parse(JSON.stringify(this.selectedArgument)));
        await this.debateService.pushArgumentAgreement(this.selectedArgument,this.userData);
        this.agreementLastValue$ = this.apiService.fetchLastUserAgreementByArgument(this.selectedArgument, this.userData.uid);
        this.agreementLastValue$.subscribe(data =>{
            console.log(data);
            this.agreementLastValue = data;
        })
        console.log(JSON.parse(JSON.stringify(this.selectedArgument)));
        await this.debateService.pushReflectionArgument(reflectionObj);
        
        
        this.generateRadarChart();
    }

    this.step = 3;
  }

  updateSlider(event:any, id:string) {
  }
}
