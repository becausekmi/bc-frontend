import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DebateService } from 'src/app/core/services/debate.service';

import { ReflectionArgument } from 'src/app/core/models/reflection';
import { Argument } from 'src/app/core/models/argument';
import { Identity } from 'src/app/auth/models/Identity';
import { Position } from 'src/app/core/models/position';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';

import { RecommenderService } from 'src/app/core/services/apis/recommender.service';
import { RecommendationRoot, ArticleAuthor, Data, Query, Recommendation, Response } from 'src/app/core/models/recommendation-query';

import { RecommendationItem} from 'src/app/discuss/components/claims/recommendation-item.component'


@Component({
    selector: 'app-recommendation-selector',
    templateUrl: 'recommendation-selection.component.html',
    styleUrls: ['./recommendation-selection.component.scss']
})
export class RecommendationSelectorComponent implements OnInit {


    recommendationResponse: any = null;
    areRecommendationsReady = false;
    selectedPosition: Position | null = null;
    selectedArgument: Argument | null = null;
    
    constructor(
        public dialogRef: MatDialogRef<RecommendationSelectorComponent>,
        public recommenderService: RecommenderService,
        public debateService:DebateService,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.selectedPosition = this.debateService.positionsDict[data.argument.positionId];
        let tempArg = this.debateService.argumentsArray.find(a => a.id === data.argument.id);
        if (tempArg) {
            this.selectedArgument = tempArg;
        }

        this.fetchRecommendations();

    }

    ngOnInit(): void {
    
    }

    fetchRecommendations() {
        this.areRecommendationsReady = false;
        
        let recommendationPromise = this.recommenderService.query("bcause-alpha",
            this.data.argument.debateId,
            this.data.argument.positionId,
            this.data.argument.id);
            
            recommendationPromise.then(
                (val) => {
                    this.recommendationResponse = val;
                    this.areRecommendationsReady = true;
                },
                (err) => {
                    console.log(err)
                    this.areRecommendationsReady = false;
                }
            );
        // recommendationPromise.then((response) => {//success

        //         console.log(response);

        //         this.recommendationResponse = response;
        //         this.isRecommendationsPromisePending = false;
        //     },
        //         (msg) => {//error
        //             // ?
        //         })
        //     .catch(() => {
        //         this.isRecommendationsPromisePending = false;
        //         //Error handler below
        //     });
    }

    close(save: Argument | null): void {
        this.dialogRef.close(save);
    }

    logIt() {
        console.log(this.recommendationResponse);
    }

    externalEvidenceSelected(recommendation:Recommendation) {
        console.log("adding evidence in argument, about to store in DB");
        this.debateService.pushExternalClaimAttached(recommendation, this.selectedArgument!);
    }

}
