import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-claim-highlights',
  template: `
      <div style="position:absolute;font-size:12px;padding:5px 10px; border-radius:10px;color:white; font-weight: 500;" [ngClass]="{'most-contested-bg-claim-support':this.claim.type === 'SUPPORTING','most-contested-bg-claim-oppose':this.claim.type === 'OPPOSING'}" *ngIf="this.summary && this.summary.generic.current.Contested_position.target_node === this.claim.id">
          <div fxLayout="row" fxLayoutAlign="start center"  >
              <mat-icon style="font-size:16px;height:16px">thumbs_up_downs</mat-icon>
              <div fxFlex="2px"></div>
              <div>Most Contested Point</div>
          </div>
      </div>
      <div style="position:absolute;font-size:12px;padding:5px 10px; border-radius:10px;color:white; font-weight: 500;" [ngClass]="{'most-opposed-bg-claim-support': this.claim.type === 'SUPPORTING', 'most-opposed-bg-claim-oppose': this.claim.type === 'OPPOSING'}" *ngIf="this.summary && this.summary.generic.current.Opposed_position.target_node === this.claim.id">
          
          <div fxLayout="row" fxLayoutAlign="start center"  >
              <mat-icon style="font-size:16px;height:16px">pan_tool</mat-icon>
              <div fxFlex="2px"></div>
              <div>Most Opposed Point</div>
          </div>
      </div>
      <div style="position:absolute;font-size:12px;padding:5px 10px; border-radius:10px;color:white; font-weight: 500;" [ngClass]="{'relevant-bg-claim-support':this.claim.type === 'SUPPORTING', 'relevant-bg-claim-oppose':this.claim.type === 'OPPOSING'}" *ngIf="this.summary && this.summary.generic.current.Needs_attention.target_node === this.claim.id">
          
      <div fxLayout="row" fxLayoutAlign="start center"  >
              <mat-icon style="font-size:16px;height:16px">visibility</mat-icon>
              <div fxFlex="2px"></div>
              <div>Needs Attention</div>
          </div>
      </div>
    `,
  styles: [
    `
    
    
    .most-contested-bg-claim {
    width:fit-content!important;
    right: 10px!important;
    top:-20px;
    }
    
    .relevant-bg-claim-support {
    width:fit-content!important;
    right: 10px!important;
    top:-20px!important;
    background:#1EC1AB;
    }
    
    .most-contested-bg-claim-support {
    width:fit-content!important;
    right: 10px!important;
    top:-20px!important;
    background:#DECF00;
    }
    
    .most-opposed-bg-claim-support {
    width:fit-content!important;
    right: 10px!important;
    top:-20px!important;
    background:#F28222;
    }
    
    .relevant-bg-claim-oppose {
    width:fit-content!important;
    left: 10px!important;
    top:-20px!important;
    background:#1EC1AB;
    }
    
    .most-contested-bg-claim-oppose {
    width:fit-content!important;
    left: 10px!important;
    top:-20px!important;
    background:#DECF00;
    }
    
    .most-opposed-bg-claim-oppose {
    width:fit-content!important;
    left: 10px!important;
    top:-20px!important;
    background:#F28222;
    }`
    
  ]
})
export class ClaimHighlightsComponent implements OnChanges {
  @Input() summary!:any;
  @Input() claim!:Argument;


  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
  
  }
}
