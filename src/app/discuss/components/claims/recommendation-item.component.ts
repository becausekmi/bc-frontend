import { Component, EventEmitter, Input, Output } from '@angular/core';

import { RecommendationRoot, ArticleAuthor, Data, Query, Recommendation, Response } from 'src/app/core/models/recommendation-query';

@Component({
    selector: 'recommendation-item',
    template: `
    <div class="recommendation-item-card">
    <div class="recommendation-item-card-content">
      <p class="recommendation-item-sentence">{{ recommendationItem.sentence }}</p>
      <p>Extracted as
      <span [ngClass]="{
        'own_claim':recommendationItem.type=='own_claim',
        'background_claim':recommendationItem.type=='background_claim',
        'data_claim':recommendationItem.type=='data'
      }"> {{ recommendationItem.type }}  </span> from <a href="http://core.ac.uk/works/{{recommendationItem.article_id}}" target="_blank">
            <span *ngFor="let aa of recommendationItem.article_authors"> {{ aa.name}}, </span>
            {{recommendationItem.article_title}},             
            {{recommendationItem.article_yearPublished}}
        </a>
      </p>
      </div>
      <div class="recommendation-item-card-attach-btn" fxLayout="row" fxLayoutAlign="end end" style="cursor:pointer" (click)="saveSelectedRecommendation(recommendationItem)">
        
        <mat-icon>attach_file</mat-icon>
        <div fxFlex="5px"></div>
        <div style="font-size:0.7rem">attach as evidence</div>
    </div>
    </div>
  `,
  styles: [`
    .recommendation-item-card {     
        border: 1px dashed grey;   
        padding:3px;
        margin:2px;
    }
    .recommendation-item-card-content {
        
    }
    .recommendation-item-card-attach-btn {

    }
    .own_claim {
        background-color : #f18320;
        color: white;
    }
    .background_claim {
        background-color : #9005C1;
        color: white;
    }
    .data_claim {
        background-color:#1fc1ab;
        color: white;
    }
    .recommendation-item-sentence {
        font-family: 'Libre Baskerville', serif;
    }
  `]
})
export class RecommendationItem {
    @Input() recommendationItem!: Recommendation;
    @Output() externalEvidenceSelected = new EventEmitter<Recommendation>();

    saveSelectedRecommendation(recommendationItem: Recommendation) {
        this.externalEvidenceSelected.emit(recommendationItem);
    }
}