import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { DebateService } from 'src/app/core/services/debate.service';
import { StatsService } from 'src/app/core/services/stats.service';

@Component({
  selector: 'app-contributors',
  template: `
     <div fxLayout="row" style="position:relative;height:100%">
        <i class="material-icons" style="cursor:pointer;position:absolute;right:20px;top:20px" (click)="close(false)">close</i>
        <!-- <div fxFlex="200px" style="background:#E8E8E8;height:100%;position:relative" fxLayout="column">
            <div class="arrow-left"></div>
        </div> -->

        <div  style="padding:20px" class="box-container" fxLayout="column" *ngIf="contributor">
            <div style="font-weight:bold;" >Contributor Details</div>
            <div fxFlex="20px"></div>
            <div fxLayout="row" fxLayoutAlign="start start">
                <img style="margin-left:10px" class="avatar"  [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+contributor.avatar+'.svg?alt=media'" />
                <div fxFlex="10px"></div>
                <div fxLayout="column" >
                   <div> <b>{{contributor.pseudo}}</b> contributed to this position with <span *ngIf="reflectionContributions.length">the follow reflection</span></div>
                   <div fxLayout="row" *ngIf="data.data.type === 'POSITION' && showReflectionChart">
                        <app-reflection-radar-chart-position [userData]="contributor" [userLevels]="userLevels" [position]="data.data"></app-reflection-radar-chart-position>
                    </div>

                    <div *ngFor="let claim of claimsContributions" fxLayout="column" fxLayoutAlign="start start" style="width:400px">
                        <div fxFlex="10px"></div>
                        <div [ngClass]="{'contributions-labels-supporting': claim.type === 'SUPPORTING', 'contributions-labels-opposing':claim.type === 'OPPOSING'}" fxLayout="row" fxLayoutAlign="center center">
                            <mat-icon style="font-size:16px;height:16px;width:16px" *ngIf="claim.type === 'OPPOSING'">remove_circle</mat-icon>
                            <mat-icon style="font-size:16px;height:16px;width:16px" *ngIf="claim.type === 'SUPPORTING'">add_circle</mat-icon>
                            <div fxFlex="5px"></div>
                            <div>ARGUMENT</div>
                        </div>
                        <div fxFlex="10px"></div>
                        <div style="font-weight:normal;text-align:left;padding:0px;overflow-wrap: break-word;">{{ claim.text }}</div>
                    </div>
                    
                </div>
                <div fxFlex="10px"></div>
            </div>
            <!-- <div fxFlex="20px"></div>
            <div fxLayout="row" *ngIf="userContributionsStats">
                <div class="box" fxLayout="column" fxLayoutAlign="center center">
                    <div class="box-title">{{userContributionsStats.positions}}</div>
                    <div class="box-text">Positions</div>
                </div>
                <div fxFlex="20px"></div>
                <div class="box" fxLayout="column" fxLayoutAlign="center center">
                    <div class="box-title">{{userContributionsStats.pros}}</div>
                    <div class="box-text">PRO</div>
                </div>
                <div fxFlex="20px"></div>
                <div class="box" fxLayout="column" fxLayoutAlign="center center">
                    <div class="box-title">{{userContributionsStats.cons}}</div>
                    <div class="box-text">CON</div>
                </div>
            </div> -->
        </div>

    </div>
  `,
  styles:  [`

        .avatar {
            
                width: 50px;
                border: 1px solid #919191;
                border-radius: 50px;
                height: 50px;
                background:white;
        }
        .box {
            width:100px;
            height:100px;
            border:1px solid #CBCBCB;
            border-radius:3px;
        }

        .box-title {
            font-size:50px;
            font-weight:bold;
            line-height:1;
        }

        .box-text {
            font-size:12px;
        }
  `]
})
export class ContributorsComponent implements OnInit {
    contributor!:any;
    public userContributionsStats:any = null;
    contributorId!:string;
    debateId!:string;
    contributions:any[] = [];
    reflectionContributions:any[] = [];
    claimsContributions:any[] = [];
    showReflectionChart:boolean = false;
    userLevels = {
        agree: 50,
        polarization: 50,
        trust: 50,
        prioritization: 50
    }

    constructor(
        public dialogRef: MatDialogRef<ContributorsComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public debateService: DebateService,
        public statsService: StatsService
    ) { 
        console.log(data);
        if (data.index === null) {
            this.initListData();
        } else {
            let contributorId = data.contributors[data.index];
            this.contributor = this.debateService.participantsDict[contributorId];
            this.contributor.uid = this.contributor.metadata.createdBy;
            this.contributorId = contributorId;
            console.log(this.contributor);

            this.initContributorData();
        }

        this.debateId = data.debate.id;
    }

    async ngOnInit() {
        
    }

    initContributorData() {
        if (this.data.data.type === 'POSITION') {
            let reflections = this.debateService.reflectionsByPositionsDict[this.data.data.id];
            let claims = this.debateService.argumentsArray.filter(a => a.positionId === this.data.data.id);
            let positionsReflections = this.debateService.reflectionsByArgumentsArray.filter(p => p.positionId === this.data.data.id && p.metadata.createdBy === this.contributorId);


            if (reflections && reflections[this.contributor.id]) {
                this.reflectionContributions.push({
                    type: 'REFLECTION',
                    data: reflections[this.contributor.id]
                });


                this.showReflectionChart = true;
            } 

            this.claimsContributions = claims.filter(c => c.metadata.createdBy === this.contributorId);

            
            console.log(this.claimsContributions, this.reflectionContributions);
            
        } else {


            /* if (claims ) {
                this.contributions.push({
                    type: 'ARGUMENTS',
                    data: claims[this.contributor.id]
                    });
            } */
        }

        console.log(this.contributions);

        this.statsService.getUserContributionsByDebate(this.contributorId, this.debateId).then(ok=>{
            console.log(ok);
            if (ok) {
                this.userContributionsStats = ok;
            } else {
                this.userContributionsStats = {
                    positions:0,
                    pros:0,
                    cons:0
                }
            }
        })
    }

    initListData() {
        console.log("INIT LIST DATA")
    }
    
    close(save:boolean): void {
        this.dialogRef.close();
    }
}
