import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable } from 'rxjs';
import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';
import { NewDebateComponent } from './create-debate.component';

@Component({
  selector: 'app-debate-stats',
  template: `
  <div>
    </div>
    <div fxLayout="row" fxLayoutAlign="space-between center" class="width-100">

        <a [routerLink]="'/discuss/debate/'+debate.id+'/preview'" style="font-weight:bold;color:#007FC1;padding:0px" >READ MORE</a>
        <div *ngIf="(identity$ | async) && debate.metadata.createdBy === (identity$ | async)?.uid" fxLayout="row">
            <div class="update-button"  >
                <mat-icon (click)="updateDebate(debate)">edit</mat-icon>
            </div>
            <div fxFlex="5px"></div>
            <!-- DELETE BUTTON -->
            <div class="delete" *ngIf="!(positions$ | async)?.length && !(arguments$ | async)?.length && !(agreements$ | async)?.length" >
                <mat-icon (click)="deleteDebate(debate)">delete_outline</mat-icon>
            </div>

        </div>
    </div>

  `,
  styles: [`

        .delete {
            flex-direction: row;
            cursor:pointer
        }

        .update-button {
            flex-direction: row;
            cursor:pointer
        }
  `]
})
export class DebateStatsComponent {
  @Input() debate!: Debate;
  @Input() userData!: any;
  @Output() updateDebates = new EventEmitter();

  positions$!: BehaviorSubject<any[]>;
  arguments$!: BehaviorSubject<any[]>;
  agreements$!:BehaviorSubject<any[]>;
  identity$!: Observable<Identity | null>;

  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private debateService: DebateService
    ) {
  }

  ngOnChanges() {

    this.identity$ = this.apiService.getIdentity();
    this.positions$ = this.apiService.fetchPositionsByDebate(this.debate.id);
    this.arguments$ = this.apiService.fetchArgumentsByDebate(this.debate.id);
    this.agreements$ = this.apiService.fetchAgreementsByDebate(this.debate.id);
  }


  deleteDebate(debate:any) {

    const dialogRef = this.dialog.open(DialogYesNoComponent, {
      width: '600px',
      data: {
        title:'DELETE DISCUSSION',
        text:'Are you sure you want to delete this discussion?'
      }
    });

    let subscription = dialogRef.afterClosed().subscribe(async yes => {
      subscription.unsubscribe();
      if (yes) {

        await this.debateService.deleteDebate(this.debate.id);
        this.updateDebates.emit(true);
      }
    })
  }

  updateDebate(debate:any) {
    const dialogRef = this.dialog.open(NewDebateComponent, {
        width:'800px',
        data: {
            debate,
            userData: this.userData
        }
    });

    dialogRef.afterClosed().subscribe(async save => {

        if (save) {
            this.debate.text = save.text;
            this.debate.title = save.title;
            this.debate.image = save.image;
            this.debate.interactions = save.interactions;
            this.debate.visibility = save.visibility;
            this.debate.discussionGroup = save.selectedGroup;
            await this.debateService.updateDebate(this.debate);
            this.updateDebates.emit(true);
        }
      });
  }

}
