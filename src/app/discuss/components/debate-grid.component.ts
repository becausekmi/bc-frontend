import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { tap, take } from 'rxjs/operators';

@Component({
  selector: 'app-debate-grid-list',
  template: `
            
        <mat-grid-list [cols]="breakpoint" rowHeight="300px" style="width:100%" gutterSize="40px" (window:resize)="onResize($event)">

            <mat-grid-tile *ngFor="let debate of filteredDebates" >
                <app-debate-cell (updateData)="emitNewData()" [debatesContributedByUser]="debatesContributedByUser" [debate]="debate" [userData]="userData" fxFill></app-debate-cell>
            </mat-grid-tile>

        </mat-grid-list>
  `,
  styles: [`

      .hide {
        display:none;
      }


  `]
})
export class DebateGridListComponent {
  @Input() debatesContributedByUser!:any;
  @Input() debates!:any;
  @Input() userData!: any;
  @Output() updateData = new EventEmitter();
  filteredDebates!:any;
  userGroups:any;
  breakpoint:number=3;

  constructor(

    private apiService:ApiService
  ) {
    
  }

  async ngOnChanges() {
    if (this.debates) {
      this.filteredDebates = this.debates.filter((d:Debate)=> !this.hide(d));
    }
    if (this.userData) {
      this.userGroups = await this.apiService.fetchGroupsByUser(this.userData).pipe(take(1)).toPromise();
    }
    this.setBreakpoint(window.innerWidth);
  }


  onResize(event:any) {
    this.setBreakpoint(event.target.innerWidth);
  }


  checkIfUserInGroup(id:any) {
    if (this.userGroups && this.userGroups.find((g:any)=> g.id === id)) {
      return true;
    } else {
      return false;
    }
  }

  emitNewData() {   
    this.updateData.emit(true);
  }

  setBreakpoint(width:number) {
    if (width <=400) {
      this.breakpoint= 1;
    } else if (width > 400 && width <= 800) {
      this.breakpoint= 2;
    } else if (width > 800 && width < 1700) {
      this.breakpoint= 3;
    } else {
      this.breakpoint= 4;
    }
  }

  
  hide(debate:Debate) {
    if (debate.visibility === 'public') {
      return false;
    } else if (debate.visibility === 'private') {
      if ((this.userData && debate.metadata.createdBy === this.userData.uid) || (this.debatesContributedByUser && this.debatesContributedByUser[debate.id]) || (debate.discussionGroup && this.checkIfUserInGroup(debate.discussionGroup))) {
        return false;
      } else {
        return true;
      }
    } 

    return false;
  }
}
