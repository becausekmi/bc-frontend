import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Identity } from 'src/app/auth/models/Identity';
import { Position } from 'src/app/core/models/position';

@Component({
  selector: 'app-agreement-slider',
  template: `
    <div fxLayout="column" style="position:relative;width:100%" fxLayoutAlign="center center" *ngIf="position"  >

        <div class="range-slider" *ngIf="userData">
            <div class="agreement-label" fxLayout="row" fxLayoutAlign="center center" [style.left.%]="((agreementLastValue+100)/2)*0.96" *ngIf="agreementLastValue !== null"> {{state}} </div>

            <div class="agreement-label-neutral" fxLayout="row" fxLayoutAlign="center center" [style.left.%]="((agreementLastValue+100)/2)*0.96" *ngIf="this.agreementLastValue === null"> Slide Your Overall Opinion </div>

            <div fxLayout="row" fxLayoutAlign="center center" [style.left.%]="((agreementLastValue+100)/2)*0.93"  class="agreement-date" *ngIf="agreementLastValue !== null">
                {{ agreementLastTS | date: 'dd/MM/yyyy - HH:mm' }}
            </div>
            <input (click)="stopPropagation($event)" [id]="'slider-'+position.id" class="slider-agreement" type="range" min="-100" max="100" [(ngModel)]="agreementLastValue" (change)="saveSliderData()" (input)="setSliderThumb(position)" >

        </div>

        <div class="range-slider" *ngIf="!userData" (click)="openAuth($event)">

            <div class="agreement-label" fxLayout="row" fxLayoutAlign="center center" [style.left.%]="((agreementLastValue+100)/2)*0.96" *ngIf="agreementLastValue === null"> Slide Your Overall Opinion </div>

            <input  [(ngModel)]="agreementLastValue"  disabled [id]="'slider-'+position.id" class="slider-agreement" type="range" min="-100" max="100"  >

        </div>
      <div class="arrow-top" [style.left.%]="((agreementLastValue+100)/2)*0.95" ></div>
    </div>

  `,
  styles:  [`

    .slider {
      position:relative
    }

    .arrow-top {
      width: 0;
      height: 0;
      border-right: 15px solid transparent;
      border-left: 15px solid transparent;
      border-bottom:15px solid #dedede;
      position:absolute;
      bottom:-40px;
    }


    .slider-agreement::-webkit-slider-thumb {
      -webkit-appearance: none;
      appearance: none;
      width:35px;
      height:35px;
      margin-top:-3px;
      background-image:url('/assets/face_default.svg')!important;
      background-size:contain;
      background-repeat:no-repeat;
      cursor: pointer;
    }

    .slider-agreement {
      -webkit-appearance: none;
      width: 100%;
      height: 4px;
      border-radius: 5px;
      background: #d3d3d3;
      outline: none;
      z-index:1000;
      padding:0px!important;
    }


    .range-slider {
      width:100%;
    }


    .agreement-label {
      position:absolute;
      top:-40px;
      background:white;
      color:#007fc1;
      width:150px;
      margin-left:-60px;
      height:25px;
      border: 2px solid #007fc1;
      font-weight: bold;
      font-size: 0.8rem;
      border-radius:5px;
    }

    .agreement-date {
      position:absolute;
      top:25px;
      color:#007fc1;
      width:150px;
      margin-left:-60px;
      height:25px;
      font-weight: bold;
      font-size: 0.6rem;
    }

    .agreement-label-neutral {

      position:absolute;
      top:-40px;
      color:#007fc1;
      width:170px;
      margin-left:-70px;
      height:25px;
      border: 2px solid #007fc1;
      font-size: 0.8rem;
    }


  `]
})
export class AgreementSliderComponent implements OnInit {

    @Input() position: any;
    @Input() userData!: Identity;
    @Input() lastUserAgreement!:any;
    @Output() setNewAgreementData = new EventEmitter();

    agreementLastValue:any = null;
    agreementLastTS:any = null;
    state:string = '';

    constructor() {
    }

    ngOnInit(): void {
        if (this.position ) {
           this.setAgreementFace(this.position)
        }
    }

    ngOnChanges()  {
      console.log(this.lastUserAgreement)
      if (this.lastUserAgreement) {
        this.agreementLastValue = this.lastUserAgreement.level;
        this.agreementLastTS = this.lastUserAgreement.metadata.createdDate;
        
      }
      if (this.position) {
        this.setAgreementFace(this.position)
      }
    }

    saveSliderData() {
      this.setNewAgreementData.emit(this.agreementLastValue);
    }

    async setSliderThumb(position:Position) {
      /* if (this.agreementLastValue) { */
        this.setAgreementFace(position)
     /*  } */
    }

    async setAgreementFace(position:Position){

        let x = this.agreementLastValue;
        let id = 'slider-'+position.id;
        var style = document.querySelector('[data="test"]');
        if (style && x !== null) {
          if (x >= -100 && x < -66 ) {
            style.innerHTML += "#"+id+"::-webkit-slider-thumb { background-image:url('assets/face_001.svg')!important;}";
            this.state = 'I strongly disagree';
          } else if (x >= -66 && x < -33 ) {
            style.innerHTML += "#"+id+"::-webkit-slider-thumb { background-image:url('assets/face_002.svg')!important; }";
            this.state = 'I disagree';
          } else if (x >= -33 && x < -5 ) {
            style.innerHTML += "#"+id+"::-webkit-slider-thumb { background-image:url('assets/face_003.svg')!important;}";
            this.state = 'I somewhat disagree';
          } else if (x > -5 && x < 5) {
            style.innerHTML += "#"+id+"::-webkit-slider-thumb { background-image:url('assets/face_004.svg')!important;}";
            this.state = 'Neutral';
          } else if (x > 5 && x <= 33) {
            style.innerHTML += "#"+id+"::-webkit-slider-thumb { background-image:url('assets/face_005.svg')!important; }";
            this.state = 'I somewhat agree';
          } else if (x > 33 && x <= 66) {
            style.innerHTML += "#"+id+"::-webkit-slider-thumb { background-image:url('assets/face_006.svg')!important;}";
            this.state = 'I agree';
          } else if (x > 66 && x <= 100) {
            style.innerHTML +="#"+id+"::-webkit-slider-thumb { background-image:url('assets/face_007.svg')!important; }";
            this.state = 'I strongly agree';
          }
        }
    }

    stopPropagation(event:any) {
      event.stopPropagation();
    }

    openAuth(evt:any) {

    }
}
