
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, EventEmitter,Inject, Input, Output, OnChanges, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { DebateService } from 'src/app/core/services/debate.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { ReflectionArgumentComponent } from '../claims/claim-reflection.component';
import { BehaviorSubject } from 'rxjs';
import { PositionService } from 'src/app/core/services/position.service';
import { ApiService } from 'src/app/core/services/api.service';


  @Component({
    selector: 'app-position-box',
    templateUrl: './position-box.component.html',
    styleUrls: ['./position-box.component.scss'],
    encapsulation: ViewEncapsulation.None
  })
  export class PositionBoxComponent implements OnChanges {
    @Input() position!:Position;
    @Input() debate!:Debate;
    @Input() summary!:any;
    @Input() authenticated: boolean | null = null;
    @Input() userData: Identity | null = null;

    @Input() claimPlaceholder!:string;
    public nowTS:number = new Date().getTime();
    currentNewClaim!:Argument | null;
    currentMetaTags:any = null;
    text:string = '';
    index:number = 0;
    highlighted:boolean = false;
    collapsed:boolean = false;
    claims$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
    dateSince:string = '';
    author$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

    constructor(
      private positionService: PositionService,
      private domSanitizer: DomSanitizer,
      public dialog: MatDialog,
      public debateService:DebateService,
      private statsService:StatsService,
      private http: HttpClient,
      private authService: AuthService,
      private apiService: ApiService) { }

    ngOnInit(): void {

      this.index = this.debateService.positionsArray.findIndex((p:Position)=>p.id === this.position.id);

      this.dateSince = this.statsService.getTimeSince(this.position.metadata.createdDate)
      this.claims$ = this.apiService.fetchArgumentsByPosition(this.position.debateId, this.position.id);

      this.positionService.selectedPositionObservable$.subscribe(id => {
        if(this.position && id !== '' && id === this.position.id){
          this.highlighted = true;
        } else {
          this.highlighted = false;
        }
      })

      this.positionService.collapsedPositionObservable$.subscribe(id => {
        if(this.position && id !== '' && id === this.position.id){
          this.collapsed = true;
        } else {
          this.collapsed = false;
        }
      })

      if (this.debate && this.position) {
        this.author$ = this.apiService.fetchParticipant(this.debate.id, this.position.metadata.createdBy);
      }
    }

    claimInputClick(event:any, position: Position) {
        event.stopPropagation();
    }

    ngOnChanges() {
    }

    expandPosition(value:any) {
      console.log(this.collapsed);
      if (this.collapsed) {
        this.toggleCollapse();
      }
    }


    async checkIfUrl(text:any) {

      const regex = new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?");

      if(regex.test(text)) {
        let urlFound = text.match(regex)[0].split(" ")[0];

        let url = btoa(urlFound);
        console.log("URL FOUND", urlFound);
        console.log("URL BTOA", url);
        if (!this.currentMetaTags && urlFound) {
          let meta:any = await this.debateService.getMetaInformation(url);
          console.log(meta);
          this.currentMetaTags = {
            url: urlFound,
            image: meta['og:image'],
            title: meta['title'],
            description: meta['description']
          }
          console.log(this.currentMetaTags);
        }
      }
    }

    apiCallbackFn(route:string) {
      console.log(route);
      return this.http.get(route);
    };

    addOpposing(event:any,position:Position) {

      this.claimPlaceholder = 'Tell us why you disagree';
      if (event) {
        event.stopPropagation();
      }

      if (this.userData) {

        this.resetHighlightining(position.id);
        this.currentNewClaim = {
          type: 'OPPOSING',
          debateId: position.debateId,
          id: '',
          metadata: {
            createdBy: this.userData.uid,
            createdDate: new Date().getTime()
          },
          parentId:null,
          attachedUrl:null,
          positionAgreement: null,
          positionId: position.id,
          text: '',
          platformData: {
            currentReplyClaim:null,
            dateSince:null,
            state: '',
            agreements: [],
            agreementLastValue:null,
            agreementAverageValue: 0,
            agreementLastTS: null,
            reflectionsCounter:0,
            heartsCounter:0,
            contributors:[],
          },
          externalClaimsAttached:null
        }

        setTimeout(()=>{
          let element = document.getElementById("input-opposing-"+position.id)

          if (element) {
            element.focus();
          }
        },200);
      } else {
        this.dialog.open(SigninOrSignupComponent)
      }
    }


  addSupporting(event:any,position: Position) {
    this.claimPlaceholder = 'Tell us why you agree';
    if (event) {
      event.stopPropagation();
    }

    if (this.userData) {


      this.resetHighlightining(position.id);
      this.currentNewClaim = {
        type: 'SUPPORTING',
        debateId: position.debateId,
        id: '',
        metadata: {
          createdBy: this.userData.uid,
          createdDate: new Date().getTime()
        },
        parentId:null,
        attachedUrl:null,
        positionAgreement: null,
        positionId: position.id,
        text: '',
        platformData: {
          currentReplyClaim:null,
          dateSince:null,
          state: '',
          agreements: [],
          agreementLastValue:null,
          agreementAverageValue: 0,
          agreementLastTS: null,
          reflectionsCounter:0,
          heartsCounter:0,
          contributors:[],
        },
        externalClaimsAttached:null
      }

      setTimeout(()=>{
        let element = document.getElementById("input-supporting-"+position.id)
        console.log(element)
        if (element) {
          element.focus();
        }
      },200);
    } else {
      this.dialog.open(SigninOrSignupComponent)
    }
  }

  positionClicked(position:Position) {
    this.resetHighlightining(position.id);
    //event.stopPropagation();
    console.log(position);
    this.highlighted = !this.highlighted;

    setTimeout(()=>{
      let element2 = document.getElementById('discussion-map-'+position.id);
        if (element2) {
          element2.scrollIntoView({behavior: "smooth"});
        }
    },200);


    if (this.highlighted) {
      this.positionService.notifySelectedPosition(this.position.id);
    } else {
      this.positionService.notifySelectedPosition('');
    }
  }

  toggleCollapse() {
      this.collapsed = !this.collapsed

      /* if (!this.collapsed ) { */
        setTimeout(()=>{
          if (this.position.id) {
            let element = document.getElementById(this.position.id);
            if (element) {
              element.scrollIntoView();
            }
          }
        },10)
     /*  } */
  }

  autoGrow(element:any) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
  }

  reflectionArgument(argument:Argument) {

    if (this.userData) {

      this.resetHighlightining(null);

      const dialogRef = this.dialog.open(ReflectionArgumentComponent, {
        data: {argument, userData:this.userData},
        panelClass: 'no-padding',
        width:"850px"
      });

      dialogRef.afterClosed().subscribe(saveArgument => {
        /* if (saveArgument && this.userData) {
          console.log(saveArgument);
          //
        } */
      });
    } else {
      this.dialog.open(SigninOrSignupComponent)
    }
  }

  resetHighlightining(positionId:string | null) {
      this.highlighted = false;
  }

  async pushClaim(claim:any) {
    if (claim) {
      this.currentNewClaim = claim;
    }

    if (this.userData) {

      if (this.currentNewClaim && this.currentNewClaim.text !== '') {
        this.position.platformData.showAgreementAlert = true;
        let newClaim = JSON.parse(JSON.stringify(this.currentNewClaim));
        let newId = await this.debateService.pushArgument(newClaim, this.userData);

        if (newId) {
          newClaim.id = newId;
        }

        this.collapsed = false;
        this.currentNewClaim = null;

        this.resetHighlightining(null);

        this.claims$ = this.apiService.fetchArgumentsByPosition(this.position.debateId, this.position.id);
        setTimeout(()=>{

          let element = document.getElementById(newClaim.id+'-anchor');
          if (element) {
            element.scrollIntoView();
          }

        },100);
      } else {
        this.claimPlaceholder = 'Please write something';
      }

    } else {
      this.dialog.open(SigninOrSignupComponent)
    }

  }
}
