import { Component, OnInit, Inject, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AnyNaptrRecord } from 'dns';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Position } from 'src/app/core/models/position';
import { Argument } from 'src/app/core/models/argument';
import { DebateService } from 'src/app/core/services/debate.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';
import { PositionReplyComponent } from './position-reply.component';
import { DomSanitizer } from '@angular/platform-browser';
import { AddClaimsComponent } from '../claims/add-claims.component';
import { ReflectionPositionComponent } from './reflection-position.component';
import { ContributorsComponent } from '../contributors.component';

@Component({
  selector: 'app-position-content',
  template: `<div [ngClass]="{'highlighted-position':highlighted,'my-contribution':!highlighted && userData && position.metadata.createdBy  === userData.uid,'no-bottom-radius-borders':!collapsed }" class="position-box"  fxLayout="column" (click)="positionClicked($event,position)" >
        <!-- IF IS A REPLY TO -->
        <div *ngIf="position.platformData.parentPosition" class="parent-box" (click)="goToParentPosition($event,position.platformData.parentPosition)">
            <div fxLayout="row" fxLayoutAlign="start center">
                <mat-icon>reply</mat-icon>
                <div fxFlex="10px"></div>
                <div style="font-size:1rem;font-weight: bold;" *ngIf="debateService.participantsDict && debateService.participantsDict[position.platformData.parentPosition.metadata.createdBy] &&  debateService.participantsDict[position.platformData.parentPosition.metadata.createdBy].pseudo !== ''">{{ debateService.participantsDict[position.platformData.parentPosition.metadata.createdBy].pseudo }}:</div>
                <div style="font-size:1rem;font-weight: bold;" *ngIf="!debateService.participantsDict || !debateService.participantsDict[position.platformData.parentPosition.metadata.createdBy] || debateService.participantsDict[position.platformData.parentPosition.metadata.createdBy].pseudo === ''">---</div>
            </div>
            <div style="font-size:1rem;">{{position.platformData.parentPosition.text}}</div>
        </div>

        <!-- POSITION INFOs -->
        <div style="padding:0px 20px;position:relative">


            <!-- HEART/REPLY BUTTON -->
            <div class="heart-reply" [ngClass]="{'parent':position.platformData.parentPosition}" fxLayout="row" fxLayoutAlign="end center" >
                <div *ngIf="!userData || (userData && position.metadata.createdBy !== userData.uid)" fxLayout="row" fxLayoutAlign="start center"  style="cursor:pointer">
                    <mat-icon matTooltipClass="custom-tooltip" [matTooltip]="'reply to the author'" *ngIf="!highlighted " (click)="replyPosition(position)" >reply</mat-icon>
                    <mat-icon matTooltipClass="custom-tooltip"  [matTooltip]="'reply to the author'" *ngIf="highlighted " (click)="replyPosition(position)" style="color:white">reply</mat-icon>
                    <div fxFlex="3px"></div>
                    <img src="assets/heart.svg" matTooltipClass="custom-tooltip" matTooltip="You already thanked this author" (click)="popPositionHeart($event,position)" *ngIf="userData && debateService.heartsByPositionsDict[position.id] && debateService.heartsByPositionsDict[position.id][userData.uid]" height="15px"/>
                    <img src="assets/heart-empty.svg" matTooltipClass="custom-tooltip" matTooltip="Thanks the author for his contribution"  (click)="pushPositionHeart($event,position)"  *ngIf="!highlighted && !(userData && debateService.heartsByPositionsDict[position.id] && debateService.heartsByPositionsDict[position.id][userData.uid])" height="15px"/>
                    <img src="assets/heart_empty_white.svg" matTooltipClass="custom-tooltip" matTooltip="Thanks the author for his contribution"  (click)="pushPositionHeart($event,position)"  *ngIf="highlighted && !(userData && debateService.heartsByPositionsDict[position.id] && debateService.heartsByPositionsDict[position.id][userData.uid])" height="15px"/>
                    <div style="margin-left:5px" matTooltipClass="custom-tooltip" [matTooltip]="position.platformData.heartsCounter+' participants thanked you so far'" *ngIf="userData && position.metadata.createdBy === userData.uid">({{position.platformData.heartsCounter}})</div>
                    <div fxFlex="5px"></div>
                </div>
            </div>
            <div fxLayout="row" fxLayoutAlign="start center" >
                <img style="margin-left:0px;margin-right:5px" class="avatar" *ngIf="debateService.participantsDict && debateService.participantsDict[position.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+debateService.participantsDict[position.metadata.createdBy].avatar+'.svg?alt=media'" height="30px"/>
                <div style="font-size:1rem;font-weight: bold;" *ngIf="debateService.participantsDict && debateService.participantsDict[position.metadata.createdBy] &&  debateService.participantsDict[position.metadata.createdBy] !== ''">{{ debateService.participantsDict[position.metadata.createdBy].pseudo }}:</div>
                <div style="font-size:1rem;font-weight: bold;" *ngIf="!debateService.participantsDict || !debateService.participantsDict[position.metadata.createdBy] || debateService.participantsDict[position.metadata.createdBy] === ''">---</div>
            </div>
            <div style="line-height: 1.5;font-size:1rem;">{{position.text}}</div>

            <app-url-preview *ngIf="position.attachedUrl" [temp]="false" [meta]="position.attachedUrl"></app-url-preview>

            <div *ngIf="position.audio && position.audio !== ''">
                <audio controls="" style="width:100%">
                    <source [src]="sanitize(position.audio)" type="audio/wav">
                </audio>
            </div>
        </div>
        <div fxFlex="10px"></div>
        <div style="width:100%;height:0.5px;background: #E2E2E2;"></div>
        <!-- TOOLBAR -->
        <div fxLayout="row" fxLayoutAlign="space-between center" style="padding:12px 20px 10px 20px">
            <div fxLayout="row" fxLayoutAlign="start center">

                <!-- REPLY -->
                <!-- <div fxLayout="row" fxLayoutAlign="start center" *ngIf="!position.parentId" (click)="replyPosition(position)" style="cursor:pointer">
                    <img src="assets/reply.svg" height="15px"/>
                    <div fxFlex="5px"></div>
                    <div style="font-size:0.7rem;" >Reply</div>
                    <div fxFlex="20px"></div>
                </div> -->

                <div fxLayout="row" fxLayoutAlign="start center" (click)="addClaims(position)" class="position-bar" [ngClass]="{'highlighted':highlighted}" >
                    <!-- <img src="assets/have_your_opinion_light_blue.svg" style="height:20px;width:20px"/> -->
                    <mat-icon   >iso</mat-icon>
                    <div fxFlex="5px"></div>
                    <div style="font-size:0.8rem;font-weight: 500;width:110px" >Give your opinion</div>
                </div>

                <div fxFlex="20px"></div>

                <!-- REFLECT -->
                <div fxLayout="row" fxLayoutAlign="start center" (click)="reflectionPosition(position)" class="position-bar" [ngClass]="{'highlighted':highlighted}" >
                    <mat-icon   >psychology</mat-icon>
                    <div fxFlex="5px"></div>
                    <div style="font-size:0.8rem;font-weight: 500;" >Reflect</div>
                    <div style="margin-left:5px">({{position.platformData.reflectionsCounter}})</div>
                </div>
                <!-- <div style="font-size:0.7rem">Analytics</div> -->

                <div fxFlex="40px"></div>
                <div fxFlex fxLayout="row" fxLayoutAlign="start center" >
                <div *ngFor="let contributor of position.platformData.contributors; let i = index" >
                    <div *ngIf="i < 10" (click)="showContributors(position,position.platformData.contributors,i)">
                        <img class="avatar" matTooltipClass="custom-tooltip" *ngIf="debateService.participantsDict && debateService.participantsDict[contributor]" [matTooltip]="debateService.participantsDict[contributor].pseudo" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+debateService.participantsDict[contributor].avatar+'.svg?alt=media'" height="30px"/>
                    </div>
                </div>
                <!--<div  style="text-decoration: underline;margin-left:5px;font-size:12px"  matTooltipClass="custom-tooltip"  matTooltip="Show all contributors" (click)="showContributors(position,position.platformData.contributors,null)" >and other {{position.platformData.contributors.length - 10}} people</div>-->
                </div>
            </div>

            <div fxFlex="20px"></div>

            <div fxLayout="row"  fxLayoutAlign="end center" style="position:relative">
                <div fxFlex="10px"></div>
                <div fxLayout="row" fxLayoutAlign="end center" matTooltipClass="custom-tooltip"  matTooltip="Collapse arguments"  (click)="toggleCollapse($event);" style="cursor:pointer" *ngIf="claims?.length">
                <div style="font-size:0.7rem;font-weight: bold;">{{claims?.length}} Arguments</div>
                <div fxFlex="5px"></div>
                <img src="assets/chevron-top.svg" *ngIf="!collapsed && (!highlighted)" height="8px"/>
                <img src="assets/chevron-down.svg" *ngIf="collapsed && (!highlighted)" height="8px"/>
                <img src="assets/chevron-top_white.svg" *ngIf="!collapsed && highlighted" height="8px"/>
                <img src="assets/chevron-down_white.svg" *ngIf="collapsed && highlighted" height="8px"/>
                </div>
            </div>

        </div>
        <!-- <div fxFlex="10px"></div>
        <div style="width:100%;height:0.5px;background: #E2E2E2;"></div> -->
    </div>
    `,
    styles: [
        `
    
        .support-header {
            color:#007FC1;
            border-bottom:2px solid #007FC1;
        }
    
        .opposing-header {
            color:#FE9329;
            border-bottom:2px solid #FE9329;
        }
    
        `
    ]
})
export class PositionContentComponent implements OnChanges {
      
    @Input() position!:any;
    @Input() collapsed!:any;
    @Input() highlighted!:any;
    @Input() debate!:any;
    @Input() claimPlaceholder!:any;
    @Input() summary!:any;
    @Input() userData!:any;
    @Input() claims!:Argument[];
    @Output() resetEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() clickEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() collapseEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() pushClaimEmitter:EventEmitter<any> = new EventEmitter<any>();
    
    labels:any[] = [];

    constructor(
        public dialog: MatDialog,
        public debateService:DebateService,
        public statsService:StatsService,
        public domSanitizer:DomSanitizer) { }

    ngOnChanges(): void {

    }

    toggleCollapse(event:any) {
        event.stopPropagation();
        this.collapseEmitter.emit();
    }
      
        
      
    positionClicked(event:any,position:Position) {
        event.stopPropagation();
        this.clickEmitter.emit(position);
    }

            
    goToParentPosition(event:any, position:Position) {
        this.positionClicked(event, position);
        let element = document.getElementById(position.id);
        if (element) {
            element.scrollIntoView({block:'center'});
        }
    }
      
        
    replyPosition(position:Position) {
        if (this.userData) {

        this.resetEmitter.emit(null);
        position.platformData.currentReplyPosition = '';
        const dialogRef = this.dialog.open(PositionReplyComponent, {
            width:'600px',
            data: {position}
        });

        dialogRef.afterClosed().subscribe(savePosition => {
            if (savePosition) {
                position.platformData.currentReplyPosition = savePosition;
                this.pushReplyPosition(position);
            }
        });
        }  else {
            this.dialog.open(SigninOrSignupComponent)
        }
    }

        
    async pushReplyPosition(position:Position)  {

        if (this.debateService.debateKey && this.userData) {
  
          let newPosition:Position = {
            debateId: this.debateService.debateKey,
            id: '' ,
            metadata: {
              createdBy: this.userData.uid,
              createdDate: new Date().getTime()
            },
            audio:'',
            text: position.platformData.currentReplyPosition,
            type: "POSITION",
            parentId: position.id,
            attachedUrl:null,
            platformData: {
              agreements: [],
              agreementLastValue: 0,
              agreementAverageValue: 0,
              agreementLastTS: null,
              claims: [],
              contributors:[],
              currentReplyPosition: '',
              opposingClaims: [],
              parentPosition: position,
              showAgreementAlert: false,
              showOnScroll:false,
              state:'',
              supportingClaims: [],
              treeCollapsed:true,
              dateSince: this.statsService.getTimeSince(new Date().getTime()),
              reflectionsCounter:0,
              heartsCounter:0,
            }
          }
  
          let newId = await this.debateService.pushPosition(newPosition, this.userData);
  
          if (newId) {
            newPosition.id = newId;
          }
  
          console.log(newPosition);
          position.platformData.currentReplyPosition = '';
  
          setTimeout(()=>{
            let positionId = this.debateService.positionsArray[this.debateService.positionsArray.length-1].id
  
            if (positionId) {
              let element = document.getElementById(positionId);
              console.log(element);
              if (element) {
                element.scrollIntoView();
              }
            }
          }, 300)
  
        } else {
          this.dialog.open(SigninOrSignupComponent)
        }
      }

      

    async pushPositionHeart(event:any, position:Position) {
        event.stopPropagation();
        console.log("PUSH PUSH")
        if (this.userData) {
        await this.debateService.pushPositionHeart(position, this.userData);
        } else {
        this.dialog.open(SigninOrSignupComponent)
        }
    }

    async popPositionHeart(event:any,position:Position) {
        event.stopPropagation();
        if (this.userData) {
        await this.debateService.popPositionHeart(position, this.userData);
        } else {
        this.dialog.open(SigninOrSignupComponent)
        }
    }

    sanitize(url: string) {
      return this.domSanitizer.bypassSecurityTrustUrl(url);
    }

    
    async addClaims(position:Position) {
        if (this.userData) {

            const dialogRef = this.dialog.open(AddClaimsComponent, {
                data:{
                position: position,
                userData: this.userData,
                debate:this.debate
                },
                panelClass: 'no-padding',
                width:"850px",
                height:"600px"
            });

            dialogRef.afterClosed().subscribe(async newClaims => {
                if (newClaims) {
                    for (let claim of newClaims) {
                        this.pushClaimEmitter.emit(claim);
                    }
                }
            });
        } else {
        this.dialog.open(SigninOrSignupComponent)
        }
    }

    

      
    reflectionPosition(position:Position) {

        if (this.userData) {
          this.resetEmitter.emit(null);
  
          const dialogRef = this.dialog.open(ReflectionPositionComponent, {
            data: {position, userData:this.userData, debate:this.debate},
            panelClass: 'no-padding',
            width:"850px"
          });
  
          dialogRef.afterClosed().subscribe(savePosition => {
  
          });
        }else {
          this.dialog.open(SigninOrSignupComponent)
        }
    }

      
    showContributors(data:any, contributors:any, index:number | null) {

        const dialogRef = this.dialog.open(ContributorsComponent, {
        data:{
            contributors: contributors,
            data:data,
            index: index,
            debate: this.debate
        },
        panelClass: 'no-padding',
        });

        dialogRef.afterClosed().subscribe(async newClaims => {
        })
    }
      
}