import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-position-heart',
  template: `
    <img src="assets/heart.svg" (click)="popPositionHeart($event,position)" *ngIf="userData && (heart$ | async)" height="15px"/>
    <img src="assets/heart-empty.svg" (click)="pushPositionHeart($event,position)"  *ngIf="!highlighted && !(heart$ | async)" height="15px"/>
    <img src="assets/heart_empty_white.svg" (click)="pushPositionHeart($event,position)"  *ngIf="highlighted && !(heart$ | async)" height="15px"/>
    <div style="margin-left:5px" *ngIf="userData && position.metadata.createdBy === userData.uid && (heartCounter$ | async)">({{(heartCounter$ | async)}})</div>
  `,
  styles: [

  ]
})
export class HeartPositionComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() position!:Position;
  @Input() highlighted!:boolean;

  heart$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  heartCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
    if (this.position && this.userData)  {
      this.heart$ = this.apiService.fetchHeartsByPositionAndUser(this.position, this.userData);
      this.heartCounter$ = this.apiService.fetchHeartsByPosition(this.position);
      //heartsByPositionsDict[position.id] && debateService.heartsByPositionsDict[position.id][userData.uid]
    }
  }

  async pushPositionHeart(event:any, position:Position) {
      event.stopPropagation();
      console.log("PUSH PUSH")
      if (this.userData) {
        let heart = {
          debateId: position.debateId,
          metadata: {
            createdBy: this.userData.uid,
            createdDate: new Date().getTime()
          },
          positionId: position.id,
          value: 1
        }
        await this.apiService.createHeartOnPosition(heart, position, this.userData);
      } else {
        this.dialog.open(SigninOrSignupComponent)
      }
  }

  async popPositionHeart(event:any,position:Position) {
      event.stopPropagation();
      if (this.userData) {
        await this.apiService.deleteHeartOnPosition(position, this.userData);
      } else {
        this.dialog.open(SigninOrSignupComponent)
      }
  }
}
