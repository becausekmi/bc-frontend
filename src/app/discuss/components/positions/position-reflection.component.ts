import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DebateService } from 'src/app/core/services/debate.service';

/* HIGHCHARTS */

import { Position } from 'src/app/core/models/position';
import { ReflectionArgument, ReflectionPosition } from 'src/app/core/models/reflection';
import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from 'src/app/core/models/debate';

/* SLIDER */

@Component({
  selector: 'app-reflection-position',
  template: `
    <div fxLayout="row" style="position:relative">
         <i class="material-icons" style="cursor:pointer;position:absolute;right:20px;top:20px" (click)="close(false)">close</i>
        <div fxFlex="200px" style="background:#E8E8E8;" fxLayout="column">
            <div style="padding:20px" fxLayout="column" fxLayoutAlign="start start">
                <div class="contributions-labels-question " fxLayout="row" fxLayoutAlign="center center">
                    <mat-icon style="font-size:16px;height:16px;width:16px">help</mat-icon>
                    <div fxFlex="5px"></div>
                    <div>QUESTION</div>
                </div>
                <div fxFlex="10px"></div>
                <span style="font-weight:bold;text-align:left;padding:0px;"> {{ debate?.title }}</span>
            </div>
            <div style="border-top:white solid 1px;padding:20px" fxLayout="column" fxLayoutAlign="start start">
                <div class="contributions-labels-position " fxLayout="row" fxLayoutAlign="center center">
                    <mat-icon style="font-size:16px;height:16px;width:16px">lightbulb</mat-icon>
                    <div fxFlex="5px"></div>
                    <div>POSITION</div>
                </div>
                <div fxFlex="10px"></div>
                <span style="font-weight:bold;text-align:left;overflow-wrap: break-word;padding:0px;"> {{ data.position.text }}</span>
            </div>
        </div>

        <!-- <app-loader fxFlex="650px"  *ngIf="loading"></app-loader> -->
        <div fxFlex="650px" style="padding:20px"  class="chart-container">
            
            <div *ngIf="userData && step === 1" fxLayout="column" style="width:100%">
                <div style="font-size:1rem;font-weight:bold;margin-bottom:10px">To what extend do you agree that</div>
                <!-- HAS HE NEVER LEFT REFLECTIONS -->
                <div class="slider-box" >
                    <div class="card-box pro">
                        <span>This is a very <b>polarized</b> position</span>
                    </div>
                    <div class="range-slider">
                        <div class="thumb-slider" [style.left.%]="userLevels.polarization" >{{userLevels.polarization}} %</div>
                        <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.polarization">
                        <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                            <div fxLayout="row" fxLayoutAlign="start center" >
                                <!-- <mat-icon>west</mat-icon>
                                <div fxFlex="10px"></div> -->
                                <div>low polarization</div>
                            </div>
                            <div fxLayout="row" fxLayoutAlign="end center">
                                <div>high polarization</div>
                                <!-- <div fxFlex="10px"></div>
                                <mat-icon>east</mat-icon> -->
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="slider-box">
                    <div class="card-box pro" >
                        <span>It is <b>clear</b> I can <b>trust</b> this</span>
                    </div>
                    <div class="range-slider">
                        <div class="thumb-slider" [style.left.%]="userLevels.trust" >{{userLevels.trust}} %</div>
                        <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.trust">
                        <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                            <div fxLayout="row" fxLayoutAlign="start center" >
                                <!-- <mat-icon>west</mat-icon>
                                <div fxFlex="10px"></div> -->
                                <div>low trust</div>
                            </div>
                            <div fxLayout="row" fxLayoutAlign="end center">
                                <div>high trust</div>
                                <!-- <div fxFlex="10px"></div>
                                <mat-icon>east</mat-icon> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider-box">
                    <div class="card-box con" >
                        <span>This should be <b>prioritized</b></span>
                    </div>
                    
                    <div class="range-slider">
                        <div class="thumb-slider" [style.left.%]="userLevels.prioritization" >{{userLevels.prioritization}} %</div>
                        <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.prioritization">
                        <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                            <div fxLayout="row" fxLayoutAlign="start center" >
                                <!-- <mat-icon>west</mat-icon>
                                <div fxFlex="10px"></div> -->
                                <div>low prioritization</div>
                            </div>
                            <div fxLayout="row" fxLayoutAlign="end center">
                                <div>high prioritization</div>
                                <!-- <div fxFlex="10px"></div>
                                <mat-icon>east</mat-icon> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider-box">
                    <div class="card-box con">
                        <span>Most people would <b>agree</b> with this</span>
                    </div>
                    <div class="range-slider">
                        <div class="thumb-slider" [style.left.%]="userLevels.agree" >{{userLevels.agree}} %</div>
                        <input class="slider-reflection" type="range" min="1" max="100" [(ngModel)]="userLevels.agree">
                        <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%;color:#9a9a9a">
                            <div fxLayout="row" fxLayoutAlign="start center" >
                                <!-- <mat-icon>west</mat-icon>
                                <div fxFlex="10px"></div> -->
                                <div>low group agreement</div>
                            </div>
                            <div fxLayout="row" fxLayoutAlign="end center">
                                <div>high group agreement</div>
                                <!-- <div fxFlex="10px"></div>
                                <mat-icon>east</mat-icon> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">
                    <button fxFlex mat-button disabled><mat-icon>chevron_left</mat-icon><span >BACK</span></button>
                    <div fxFlex="20px"></div>
                    <button fxFlex mat-button (click)="saveFeedbacks()"><span >NEXT</span><mat-icon>chevron_right</mat-icon></button>
                </div>
            </div>
            <div *ngIf="!userData || step === 2" style="width:100%">
                <div style="font-size:1rem;font-weight:bold">Reflections feedback</div>
                <div>«{{data.position.text}}»</div>
                <!-- SHOW RADAR CHART -->
                <app-reflection-radar-chart-position [userData]="userData" [userLevels]="userLevels" [position]="data.position"></app-reflection-radar-chart-position>
                <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">
                    <button fxFlex mat-button class="button-blue-negative" (click)="close(false)" >Close</button>
                    <div fxFlex="20px"></div>
                    <button fxFlex mat-button class="button-blue"  (click)="step = 1">RETHINK IT THROUGH</button>
                </div>
            </div>
        </div>
    </div>
  `,
  styles:  [`
        .chart-container { display: flex; flex-direction: column; }
        .slider-box { display:flex; flex-direction:row; align-items: center}
        .card-box { 
            margin-bottom: 10px;
            display: flex;
            flex-direction: column;
            align-content:center;
            justify-content:center;
            border-radius:5px;
            min-width:110px;
            max-width:110px;
            height:110px;
            margin-right:10px;
        }
        .pro {background:#007FC1;color:white;}
        .con {background:#FE9329;color:white;}
        span {  padding:10px;text-align:center;font-size:1rem }
        .range-slider { width:470px!important}
        .slider-reflection { flex-grow:1;margin-bottom:10px}
    `]
})
export class ReflectionPositionComponent implements OnInit {

  debate: Debate;
  text:string = '';
  type:string = '';
  userData:Identity|null = null;
  step: number = 1;
  loading:boolean = true;
  userLevels = {
      agree: 50,
      polarization: 50,
      trust: 50,
      prioritization: 50
  }


  constructor(public dialogRef: MatDialogRef<ReflectionPositionComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public debateService: DebateService) { 
    console.log(JSON.parse(JSON.stringify(data)))
    this.userData = data.userData;
    this.debate = data.debate;

    if (data.position && this.userData && this.debateService.reflectionsByPositionsDict && this.debateService.reflectionsByPositionsDict[data.position.id] && this.debateService.reflectionsByPositionsDict[data.position.id][this.userData.uid]) {
        this.step = 2;
    }
  }

  ngOnInit(): void {
  }

  
  close(save:boolean): void {
      
    this.dialogRef.close();
  }

  

  async saveFeedbacks() {
    if (this.userData && this.debateService.debateKey) {

        let reflectionObj:ReflectionPosition = {
            debateId: this.debateService.debateKey,
            levels: this.userLevels,
            metadata: {
                createdBy: this.userData.uid,
                createdDate: new Date().getTime()
            },
            positionId: this.data.position.id,
        }
        console.log(this.userLevels);

        await this.debateService.pushReflectionPosition(reflectionObj);
        //this.generateRadarChart();
    }

    this.step = 2;
  } 

  updateSlider(event:any, id:string) {
  }
}
