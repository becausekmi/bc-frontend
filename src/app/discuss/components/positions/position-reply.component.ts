import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DebateService } from 'src/app/core/services/debate.service';

@Component({
  selector: 'app-position-reply',
  template: `
    <div mat-dialog-content *ngIf="data.position" fxLayout="column">
        <div fxLayout="row" fxLayoutAlign="space-between center">
          <div style="font-size:1rem;font-weight:bold">Reply to</div>
          <i class="material-icons" style="cursor:pointer" (click)="close(false)">close</i>
        </div>
        <div fxFlex="10px"></div>
        <div class="contributions-labels-position " fxLayout="row" fxLayoutAlign="center center">
            <mat-icon style="font-size:16px;height:16px;width:16px">lightbulb</mat-icon>
            <div fxFlex="5px"></div>
            <div>POSITION</div>
        </div>
        <div fxFlex="5px"></div>
        <div class="position-info-box" fxLayout="column" >
              <div style="font-size:1rem;font-weight: bold;" *ngIf="debateService.participantsDict && debateService.participantsDict[data.position.metadata.createdBy]">{{ debateService.participantsDict[data.position.metadata.createdBy].pseudo }}:</div>
              <div style="font-size:1rem;font-weight: bold;" *ngIf="!debateService.participantsDict || !debateService.participantsDict[data.position.metadata.createdBy]">---</div>
              <div style="line-height: 1.5;font-size:1rem;">{{data.position.text}}</div>
        </div>
        <div fxFlex="20px"></div>
        <textarea rows="5" style="border-radius:5px;border:1px solid rgba(0, 0, 0, 0.87);padding:10px;" [(ngModel)]="text" placeholder="Elaborate on your reply" ></textarea>
        <div fxFlex="20px"></div>
        
        <div fxLayout="row" fxLayoutAlign="space-between center" style="width:100%">
                        
            <button fxFlex mat-button class="button-blue-negative" (click)="close(false)" >Close</button>
            <div fxFlex="20px"></div>
            <button mat-button fxFlex class="button-blue" (click)="close(true)" [disabled]="text === ''">Reply</button>
            
        </div>
        <div fxFlex="10px"></div>
    </div>
  `,
  styles: [

  ]
})
export class PositionReplyComponent implements OnInit {

  text:string = '';

  constructor(
    public dialogRef: MatDialogRef<PositionReplyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public debateService:DebateService
    ) { }

  ngOnInit(): void {
  }

  
  close(save:boolean): void {
      if (save) {
        this.dialogRef.close(this.text);
      } else {
        this.dialogRef.close();
      }
  }
}
