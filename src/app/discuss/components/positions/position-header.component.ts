import { Component, OnInit, Inject, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AnyNaptrRecord } from 'dns';
import { Position } from 'src/app/core/models/position';
import { DebateService } from 'src/app/core/services/debate.service';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';

@Component({
  selector: 'app-position-header',
  template: `
    <div style="position:absolute;right:100px;top:30px;font-size:12px;padding:5px 10px; border-radius:10px;color:white; font-weight: 500;" [ngClass]="{'most-contested-bg': label === 'most-contested', 'most-opposed-bg': label === 'most-opposed', 'relevant-bg': label === 'needs-attention'}" fxLayout="row" fxLayoutAlign="end center" *ngFor="let label of labels">
        <div fxLayout="row" fxLayoutAlign="start center"  *ngIf="label === 'most-contested'">
        <mat-icon style="font-size:16px;height:16px">thumbs_up_downs</mat-icon>
        <div fxFlex="2px"></div>
        <div>Most Contested Point</div>
        </div>
        <div fxLayout="row" fxLayoutAlign="start center"  *ngIf="label === 'most-opposed'">
        <mat-icon style="font-size:16px;height:16px">pan_tool</mat-icon>
        <div fxFlex="2px"></div>
        <div>Most Opposed Point</div>
        </div>
        <div fxLayout="row" fxLayoutAlign="start center" *ngIf="label === 'needs-attention'" >
        <mat-icon style="font-size:16px;height:16px">visibility</mat-icon>
        <div fxFlex="2px"></div>
        <div>Needs Attention</div>
        </div>
    </div>

    <!-- DELETE BUTTON -->
    <div class="delete-button" *ngIf="userData && position.metadata.createdBy === userData.uid && !position.platformData.contributors.length" >
        <mat-icon (click)="deletePosition(position)" style="color:black">delete_outline</mat-icon>
    </div>

    <!-- TIMELINE -->
    <div style="font-size:0.8rem;padding-top:20px;color:grey; padding-bottom:5px;position:relative" (click)="positionClicked($event,position)" >
        <div class="timeline-dot"></div>
        {{ position.platformData.dateSince }} ago

    </div>
  `,
  styles: [
    `

    .support-header {
        color:#007FC1;
        border-bottom:2px solid #007FC1;
    }

    .opposing-header {
        color:#FE9329;
        border-bottom:2px solid #FE9329;
    }

    `
  ]
})
export class PositionHeaderComponent implements OnChanges {

    @Input() position!:any;
    @Input() summary!:any;
    @Input() userData!:any;
    @Output() resetEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() clickEmitter:EventEmitter<any> = new EventEmitter<any>();

    labels:any[] = [];

    constructor(
        public dialog: MatDialog,
        public debateService:DebateService) { }

    ngOnChanges(): void {

        let mostContested = null;
        let needsAttention = null;
        let mostOpposed = null;

        if (this.summary) {
        if (this.summary.generic.current.Contested_position)
            mostContested = this.summary.generic.current.Contested_position;

        if (this.summary.generic.current.Needs_attention)
            needsAttention = this.summary.generic.current.Needs_attention;

        if (this.summary.generic.current.Opposed_position)
            mostOpposed = this.summary.generic.current.Opposed_position;
        }

        
        if (mostContested && mostContested.target_node === this.position.id) {
            this.labels.push('most-contested');
        }

        if (needsAttention && needsAttention.target_node === this.position.id) {
            this.labels.push('needs-attention');
        }

        if (mostOpposed && mostOpposed.target_node === this.position.id) {
            this.labels.push('most-opposed');
        } 
    }

  
    async deletePosition(position:Position) {

        const dialogRef = this.dialog.open(DialogYesNoComponent, {
        data:{
            title: 'Delete position',
            text: 'Are you sure you want to delete the position and all his data (arguments, reflections)?'
        }
        });

        dialogRef.afterClosed().subscribe(async yes => {
            if (yes) {
                await this.debateService.deletePosition(position.id);
            }

            this.resetEmitter.emit(null);
        });
    }

    positionClicked(event:any,position:Position) {
        event.stopPropagation();
        this.clickEmitter.emit(position);
    }


}
