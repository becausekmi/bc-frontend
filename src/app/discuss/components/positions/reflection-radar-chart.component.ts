import { Component, OnInit, Inject, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Identity } from 'src/app/auth/models/Identity';
import { Position } from 'src/app/core/models/position';
import * as Highcharts from 'highcharts';
import Item from 'highcharts/modules/item-series';
import HighchartsMore from 'highcharts/highcharts-more';
import { DebateService } from 'src/app/core/services/debate.service';


Item(Highcharts);
HighchartsMore(Highcharts)

@Component({
  selector: 'app-reflection-radar-chart-position',
  template: `
            <highcharts-chart
                    [Highcharts]="Highcharts"
                    [options]="chartRadarOptions"
                    *ngIf="showReflectionsChart && chartRadarOptions"
                ></highcharts-chart>
  `,
  styles:  [`



  `]
})
export class ReflectionRadarChartPositionComponent implements OnChanges {

    @Input() userData: any;
    @Input() position!: Position;
    @Input() userLevels:any;
    @Output() loaderEmitter = new EventEmitter();
        
    radarChartData:any;
    userName:any

    Highcharts: typeof Highcharts = Highcharts;
    chartRadarOptions: Highcharts.Options| null = null;
    showReflectionsChart:boolean = false;
    constructor(private debateService: DebateService) {
    }

    ngOnInit() {
    }

    ngOnChanges()  {
     
        console.log("GENERATE CHART")
        this.generateRadarChart();
    }

    generateRadarChart() {

        let yourReflectionsArray: any[] = [];
        let otherReflectionsArray: any[] = [];
    
        if (this.position) {
            console.log(this.position);
            console.log(this.userData)
    
            if (this.userData && this.debateService.reflectionsByPositionsDict && this.debateService.reflectionsByPositionsDict[this.position.id] && this.debateService.reflectionsByPositionsDict[this.position.id][this.userData.uid]) {
                let levels = this.debateService.reflectionsByPositionsDict[this.position.id][this.userData.uid].levels;
                
                yourReflectionsArray[0]=levels.agree
                yourReflectionsArray[1]=levels.polarization
                yourReflectionsArray[2]=levels.trust
                yourReflectionsArray[3]=levels.prioritization
                this.userLevels = {
                    agree: levels.agree,
                    polarization: levels.polarization,
                    trust: levels.trust,
                    prioritization: levels.prioritization
                }
                //this.step = 2;
            }
    
            let positionReflections = this.debateService.reflectionsByPositionsArray.filter(p => p.positionId === this.position.id );
    
            if (positionReflections.length) {
                otherReflectionsArray = [0,0,0,0];
                let counter = 1;
                for (let reflection of positionReflections) {
                    otherReflectionsArray[0]+=reflection.levels.agree
                    otherReflectionsArray[1]+=reflection.levels.polarization
                    otherReflectionsArray[2]+=reflection.levels.trust
                    otherReflectionsArray[3]+=reflection.levels.prioritization
    
                    
                    otherReflectionsArray[0]= otherReflectionsArray[0]/counter
                    otherReflectionsArray[1]= otherReflectionsArray[1]/counter
                    otherReflectionsArray[2]= otherReflectionsArray[2]/counter
                    otherReflectionsArray[3]= otherReflectionsArray[3]/counter
    
                    counter++;
                }
            }
    
        }
    
        console.log(yourReflectionsArray, otherReflectionsArray);
    
    
        this.radarChartData = [yourReflectionsArray,otherReflectionsArray]

        let pseudo = '';

        if (this.userData.defaultPseudo) {
            pseudo = this.userData.defaultPseudo
        } else {
            pseudo = this.userData.pseudo;
        }
    
        this.chartRadarOptions = {
          chart: {
              polar: true,
              type: 'line'
          },
    
          accessibility: {
              description: ''
          },
    
          title: {
              text: '',
              x: -80
          },
    
          pane: {
              size: '80%'
          },
    
          xAxis: {
              categories: ['Group Agreement Level', 'Polarization Level', 'Trust Level', 'Prioritazion Level'],
              tickmarkPlacement: 'on',
              lineWidth: 0,
              labels: {
                style:{
                    width:300,
                    whiteSpace:'normal'//set to normal
                },
                formatter: function() {
                    return this.value.toString()
                }
              }
          },
    
          yAxis: {
              gridLineInterpolation: 'polygon',
              lineWidth: 0,
              min: 0
          },
    
          tooltip: {
              shared: true,
              pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
          },
    
          legend: {
              align: 'center',
              verticalAlign: 'top',
              layout: 'horizontal'
          },
    
          series: [ {
            type: 'area',
            color:'#ffbfa4',
            name: 'Group average',
            fillOpacity:0.1,
            data: this.radarChartData[1],
            pointPlacement: 'on'
          },{
            type: 'area',
            name: pseudo,
            color:'#ffd968',
            fillOpacity:0.2,
            data: this.radarChartData[0],
            pointPlacement: 'on'
          }],
    
          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          align: 'center',
                          verticalAlign: 'bottom',
                          layout: 'horizontal'
                      },
                      pane: {
                          size: '70%'
                      }
                  }
              }]
          }
        }
    
        setTimeout(()=>{
          this.showReflectionsChart = true;
          //this.loading = false;
        }, 100)
    
      }
}
