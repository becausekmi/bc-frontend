import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { PositionService } from 'src/app/core/services/position.service';
import { AddClaimsComponent } from '../claims/claim-multi-add.component';
import { ContributorsComponent } from '../contributors.component';
import { ReflectionPositionComponent } from './position-reflection.component';

@Component({
  selector: 'app-position-toolbar',
  template: `
   <div fxLayout="row" fxLayoutAlign="space-between center" style="padding:20px 20px">
    <div fxLayout="row" fxLayoutAlign="start center">
        
        <div fxLayout="row" fxLayoutAlign="start center" (click)="addClaims(position)" class="position-bar" [ngClass]="{'highlighted':highlighted}" >
            <!-- <img src="assets/have_your_opinion.svg" *ngIf="!highlighted" height="15px"/>
            <img src="assets/have_your_opinion_white.svg" *ngIf="highlighted" height="15px"/> -->
            <!-- <mat-icon style="color:black;font-size:20px;height:20px;width:20px" *ngIf="!highlighted" >thumbs_up_downs</mat-icon>
            <mat-icon style="color:white;font-size:20px;height:20px;width:20px" *ngIf="highlighted" >thumbs_up_downs</mat-icon> -->
            <mat-icon style="font-size:20px;height:20px;width:20px"  >pan_tool</mat-icon>
            <div fxFlex="5px"></div>
            <div style="font-size:0.8rem;font-weight: 500;width:110px" >Give your opinion</div>
        </div>

        <div fxFlex="20px"></div>

        <!-- REFLECT -->
        <div fxLayout="row" fxLayoutAlign="start center" (click)="reflectionPosition(position)" class="position-bar" [ngClass]="{'highlighted':highlighted}" >
            <mat-icon   >psychology</mat-icon>
            <div fxFlex="5px"></div>
            <div style="font-size:0.8rem;font-weight: 500;" >Reflect</div>
            <div style="margin-left:5px" *ngIf="(reflectionCounter$| async)">({{(reflectionCounter$| async)}})</div>
        </div>
        <!-- <div style="font-size:0.7rem">Analytics</div> -->
        
        <div fxFlex="40px"></div>
        <div fxFlex fxLayout="row" fxLayoutAlign="start center" >
        <div *ngFor="let contributor of position.platformData.contributors; let i = index" >
            <div *ngIf="i < 10" (click)="showContributors(position.platformData.contributors,i)">
                <img class="avatar" *ngIf="debateService.participantsDict[contributor] && i%2 === 0" [matTooltip]="debateService.participantsDict[contributor].pseudo" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+(i+1)%30+'.svg?alt=media'" height="30px"/>
                <img class="avatar" *ngIf="debateService.participantsDict[contributor] && i%2 !== 0" [matTooltip]="debateService.participantsDict[contributor].pseudo" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+(i+1)%30+'.svg?alt=media'" height="30px"/>
            </div>
            <div *ngIf="i === 10" style="text-decoration: underline;margin-left:5px;font-size:12px" (click)="showContributors(position.platformData.contributors,null)">and other {{position.platformData.contributors.length - 10}} people</div>
        </div>
        </div>
    </div>

    <div fxFlex="20px"></div>

    <div fxLayout="row"  fxLayoutAlign="end center" style="position:relative">
        <div fxFlex="10px"></div>
        <div fxLayout="row" fxLayoutAlign="end center" (click)="toggleCollapse($event,position);" style="cursor:pointer" *ngIf="position.platformData.claims?.length">
        <div style="font-size:0.7rem;font-weight: bold;">{{position.platformData.claims?.length}} Arguments</div>
        <div fxFlex="5px"></div>
        <img src="assets/chevron-top.svg" *ngIf="!collapsed && (!highlighted)" height="8px"/>
        <img src="assets/chevron-down.svg" *ngIf="collapsed && (!highlighted)" height="8px"/>
        <img src="assets/chevron-top_white.svg" *ngIf="!collapsed && highlighted" height="8px"/>
        <img src="assets/chevron-down_white.svg" *ngIf="collapsed && highlighted" height="8px"/>
        </div>
    </div>

</div>
  `,
  styles: [
    `
        .avatar {
            margin-left: -15px;
            width: 25px;
            border: 1px solid #919191;
            border-radius: 50px;
            height: 25px;
            background:white;
        }

        .position-bar {
            color:#007fc1;
            cursor:pointer;

        }
        .highlighted {
            color:white!important;
        }
    
    `
  ]
})
export class ToolbarPositionComponent implements OnChanges {
    @Input() userData!:Identity | null;
    @Input() position!:Position;
    @Input() debate!:Debate;
    @Input() highlighted!:boolean;
    @Input() collapsed!:boolean;

    reflectionCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

    constructor(
        private apiService:ApiService,
        private dialog: MatDialog,
        public debateService: DebateService,
        public positionService: PositionService
    ) { }

    async ngOnChanges(changes: SimpleChanges) {
        if (this.position && this.userData)  {
            this.reflectionCounter$ = this.apiService.fetchReflectionsCounterByPosition(this.position);
        }
    }

    
    async addClaims(position:Position) {
        if (this.userData) {

        const dialogRef = this.dialog.open(AddClaimsComponent, {
            data:{
            position: position,
            userData: this.userData,
            debate:this.debate
            },
            panelClass: 'no-padding',
            width:"850px",
            height:"600px"
        });
    
        dialogRef.afterClosed().subscribe(async newClaims => {
            if (newClaims) {
                for (let claim of newClaims) {
                    position.platformData.currentNewClaim = claim;
                    this.pushClaim(position);
                }
            }
        });
        } else {
            this.dialog.open(SigninOrSignupComponent)
        }
    }

    async pushClaim(position:Position) {
        //position.chooseClaimsType = false;
        if (this.userData) {

        if (position.platformData.currentNewClaim.text !== '') {
            position.platformData.showAgreementAlert = true;
            let newClaim = JSON.parse(JSON.stringify(position.platformData.currentNewClaim));
            let newId = await this.apiService.createArgument(newClaim);

            if (newId) {
            newClaim.id = newId;
            }

            this.positionService.notifyCollapsedPosition('');
            position.platformData.currentNewClaim = null;
            position.platformData.chooseClaimsType = false;
            position.platformData.showClaimsButtons = true;

            //this.resetHighlightining(null);

            setTimeout(()=>{

            let element = document.getElementById(newClaim.id+'-anchor');
            if (element) {
                element.scrollIntoView();
            }

            },100);
        } else {
            //this.claimPlaceholder = 'Please write something';
        }

        } else {
        this.dialog.open(SigninOrSignupComponent)
        }

    }

  
    toggleCollapse(event:any, position:Position) {
        event.stopPropagation();

        if (!this.collapsed ) {
            this.collapsed = true;
            this.positionService.notifyCollapsedPosition(position.id);
                setTimeout(()=>{
                if (position.id) {
                    let element = document.getElementById(position.id);
                    if (element) {
                        element.scrollIntoView();
                    }
                }
            },100)
        } else {
            this.collapsed = false;
            this.positionService.notifyCollapsedPosition('');
        }
    }

        
    showContributors(contributors:any, index:number | null) {
        console.log(contributors);

        const dialogRef = this.dialog.open(ContributorsComponent, {
        data:{
            contributors: contributors,
            index: index
        },
        panelClass: 'no-padding',
        width:"600px",
        height:"600px"
        });

        dialogRef.afterClosed().subscribe(async newClaims => {
        })
    }

    
    reflectionPosition(position:Position) {

        if (this.userData) {
          //this.resetHighlightining(null);
  
          const dialogRef = this.dialog.open(ReflectionPositionComponent, {
            data: {position, userData:this.userData, debate:this.debate},
            panelClass: 'no-padding',
            width:"850px"
          });
  
          dialogRef.afterClosed().subscribe(savePosition => {
  
          });
        }else {
          this.dialog.open(SigninOrSignupComponent)
        }
      }
}
