import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';
import { PositionReplyComponent } from './position-reply.component';

@Component({
  selector: 'app-position-top-bar',
  template: `
  <!-- HEART BUTTON -->
  <div style="position:absolute;right:20px;top:60px" fxLayout="row" fxLayoutAlign="end center" >
            <!-- *ngIf="!userData || (userData && position.metadata.createdBy !== userData.uid)"  -->
            <div fxLayout="row" fxLayoutAlign="start center"  style="cursor:pointer">
                <img  *ngIf="!highlighted && !position.parentId" (click)="replyPosition(position)" src="assets/reply.svg" height="15px"/>
                <img  *ngIf="highlighted && !position.parentId" (click)="replyPosition(position)" src="assets/reply_white.svg" height="15px"/>
                <div fxFlex="10px"></div>
                <app-position-heart  fxLayout="row" fxLayoutAlign="start center" [highlighted]="highlighted" [userData]="userData" [position]="position"></app-position-heart>
                <div fxFlex="5px"></div>
            </div>
        </div>

        <div style="position:absolute;right:100px;top:30px;font-size:12px;padding:5px 10px; border-radius:10px;color:white; font-weight: 500;" class="most-contested-bg"  *ngIf="this.summary && this.summary?.generic.current.Contested_position.target_node === this.position.id" fxLayout="row" fxLayoutAlign="end center">
          <div fxLayout="row" fxLayoutAlign="start center" >
            <mat-icon style="font-size:16px;height:16px">thumbs_up_downs</mat-icon>
            <div fxFlex="2px"></div>
            <div>Most Contested Point</div>
          </div>
        </div>

        <div style="position:absolute;right:100px;top:30px;font-size:12px;padding:5px 10px; border-radius:10px;color:white; font-weight: 500;" class="most-opposed-bg"  *ngIf="this.summary && this.summary?.generic.current.Opposed_position.target_node === this.position.id" fxLayout="row" fxLayoutAlign="end center">
          <div fxLayout="row" fxLayoutAlign="start center" >
            <mat-icon style="font-size:16px;height:16px">pan_tool</mat-icon>
            <div fxFlex="2px"></div>
            <div>Most Opposed Point</div>
          </div>
        </div>

        <div style="position:absolute;right:100px;top:30px;font-size:12px;padding:5px 10px; border-radius:10px;color:white; font-weight: 500;" class="relevant-bg"   *ngIf="this.summary && this.summary.generic.current.Needs_attention.target_node === this.position.id" fxLayout="row" fxLayoutAlign="end center">
          <div fxLayout="row" fxLayoutAlign="start center" >
            <mat-icon style="font-size:16px;height:16px">visibility</mat-icon>
            <div fxFlex="2px"></div>
            <div>Needs Attention</div>
          </div>
        </div>

        <!-- DELETE BUTTON -->
        <div class="delete-button" *ngIf="userData && position.metadata.createdBy === userData.uid && !hasContributions" >
            <mat-icon (click)="deletePosition(position)">delete_outline</mat-icon>
        </div>
  `,
  styles: [
    `
    .most-contested-bg {
    right: 260px!important;
    }
    
    .relevant-bg {
    right:430px!important;
    }
    `
  ]
})
export class TopbarPositionComponent implements OnChanges {
    @Input() userData!:Identity | null;
    @Input() position!:Position;
    @Input() highlighted!:any;
    @Input() summary!:any;
    @Input() currentReplyPosition!:string;

    hasContributions: boolean = false;
    heart$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
    heartCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

    constructor(
        private apiService:ApiService,
        private dialog: MatDialog,
        private statsService: StatsService,
        private debateService: DebateService
    ) { }

    async ngOnChanges(changes: SimpleChanges) {
        if (this.position && this.userData)  {
        }
    }

    replyPosition(position:Position) {
        if (this.userData) {

        if (!position.parentId) {
            //this.resetHighlightining(null);
            this.currentReplyPosition = '';
            const dialogRef = this.dialog.open(PositionReplyComponent, {
            width:'600px',
            data: {position}
            });

            dialogRef.afterClosed().subscribe(savePosition => {
                if (savePosition) {
                    this.currentReplyPosition = savePosition;
                    this.pushReplyPosition(position);
                }
            });
        }
        }  else {
        this.dialog.open(SigninOrSignupComponent)
        }
    }

    async pushReplyPosition(position:Position)  {

        if (this.debateService.debateKey && this.userData) {

        let newPosition:Position = {
            debateId: this.debateService.debateKey,
            id: '' ,
            metadata: {
                createdBy: this.userData.uid,
                createdDate: new Date().getTime()
            },
            audio:'',
            text: this.currentReplyPosition,
            type: "POSITION",
            parentId: position.id,
            attachedUrl:null,
            platformData: {
                agreements: [],
                agreementLastValue: 0,
                agreementAverageValue: 0,
                agreementLastTS: null,
                chooseClaimsType: false,
                claims: [],
                contributors:[],
                currentNewClaim: null,
                currentShown:false,
                opposingClaims: [],
                parentPosition: position,
                showAgreementAlert: false,
                showClaimsButtons: true,
                showOnScroll:false,
                state:'',
                supportingClaims: [],
                treeCollapsed:true
            }
        }

        let newId = await this.apiService.createPosition(newPosition);

        if (newId) {
            newPosition.id = newId;
        }

        console.log(newPosition);
        this.currentReplyPosition = '';

        setTimeout(()=>{
            let positionId = this.debateService.positionsArray[this.debateService.positionsArray.length-1].id

            if (positionId) {
            let element = document.getElementById(positionId);
            /* this.debateService.positionsArray[this.debateService.positionsArray.length-1].platformData.highlighted = true; */
            console.log(element);
            if (element) {
                element.scrollIntoView();
            }
            }
        }, 300)

        } else {
        this.dialog.open(SigninOrSignupComponent)
        }
    }

  
    async deletePosition(position:Position) {

        const dialogRef = this.dialog.open(DialogYesNoComponent, {
            data:{
            title: 'Delete position',
            text: 'Are you sure you want to delete the position and all his data (arguments, reflections)?'
            }
        });

        dialogRef.afterClosed().subscribe(async yes => {
            if (yes) {
                await this.apiService.deletePosition(position);
            }
            //this.resetHighlightining(null);
        });
    }
}
