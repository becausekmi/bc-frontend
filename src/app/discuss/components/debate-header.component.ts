import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-debate-header',
  template: `

    <div class="debate-title" fxLayout="row" fxLayoutAlign="start center" style="position:relative">
      <img src="assets/question-mark.svg" />
      <div fxFlex="20px"></div>
      <div fxFlex fxLayout="row" fxLayoutAlign="start center">{{ title}}</div>
    </div>
  `,
  styles: [`


    .debate-title {
        background:#003A5A;
        border-radius:10px;
        font-size:25px;
        font-weight: bold;
        color:white;
        padding:20px;
        line-height: 1.2;
        width:100%;
    }


  `]
})
export class DebateHeaderComponent {
  @Input() title: string | undefined= '';
  @Input() text: string | undefined= '';
  @Input() image: string | undefined= '';

  constructor() {

      console.log(this.title);
  }

}
