import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { AnalyticsService } from 'src/app/core/services/analytics.service';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-agreements-widget',
  template: `
    <div fxLayout="row" fxLayoutAlign="start end">
          <div style="font-size:40px;line-height:1" >{{ allAgreements.length}}</div>
          <div fxFlex="5px"></div>
          <div>agreements<br>left so far</div>
    </div> 
    <div fxLayout="row" fxLayoutAlign="start end" *ngIf="positions">
          <div style="font-size:40px;line-height:1" >{{positions.length}}</div>
          <div fxFlex="5px"></div>
          <div>positions <br>judged</div>
    </div> 
    <div fxLayout="row" fxLayoutAlign="start start">
      <img [src]="getAgreementFace(averageLevel)" height="45px" style="cursor:pointer;" />
      <div fxFlex="5px"></div>
      <div>Users feeling<br>is 
        <span *ngIf="averageLevel < -50">STRONGLY DISAGREE</span>
        <span *ngIf="averageLevel >= -50 && averageLevel < 0">DISAGREE</span>
        <span *ngIf="averageLevel >= 0 && averageLevel < 50">AGREE</span>
        <span *ngIf="averageLevel >= 50">STRONGLY AGREE</span>
      </div>
    </div> 
  `,
  styles: [

  ]
})
export class AgreementWidgetComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() debate!:Debate;
  @Input() positions!:Position[];
  @Input() averageLevel:number = 0;
  @Input() participants!:any;
  @Input() arguments!:any;

  agreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  positionsAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  argumentsAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  allAgreements:any[] = [];

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog,
    private analyticsService: AnalyticsService
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
    if (this.debate) {
      this.positionsAgreements$ = this.apiService.fetchPositionsAgreementsByDebate(this.debate);
      this.argumentsAgreements$ = this.apiService.fetchArgumentsAgreementsByDebate(this.debate);

      combineLatest([this.positionsAgreements$, this.argumentsAgreements$]).subscribe(results => {
        // results[0] is positions
        // results[1] is arguments
        this.allAgreements = results[0].concat(results[1]);
        
      })
    }
  }

  
  getAgreementFace(level:any) {
    return './assets/'+this.analyticsService.getAgreementFace(level);
  }
}
