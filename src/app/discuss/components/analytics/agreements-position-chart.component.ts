import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { AnalyticsService } from 'src/app/core/services/analytics.service';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-agreements-positions-chart',
  template: `
  <div class="label">Positions Agreements across the debate</div>
  <div fxLayout="row" fxLayoutAlign="start end" style="width:100%;height:350px;" class="noselect"  >
    <div fxLayout="row" fxLayoutAlign="center center" style="width:100%;height:350px;" class="noselect"  *ngIf="!agreementsByLevels || !maxLevel">
        No Data about Agreements
    </div>
    <div fxLayout="row" fxLayoutAlign="start end" style="height:100%;padding:60px 30px 30px 30px" fxFlex class="noselect"  *ngIf="agreementsByLevels && maxLevel ">
      <!-- STRONGLY DISAGREE -->
      <div fxLayout="column" fxFlex fxLayoutAlign="end start" style="position:relative;height:100%">


        <div style="font-size:12px;text-align:left;padding-bottom:15px">
          {{ (agreementsByLevels['strongly_disagree'].length/totalAggregatedByPosition)*100 | number:'1.2-2'}}% of {{totalAggregatedByPosition}} ideas have been strongly not supported
        </div>
        <div class="chart-block" fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['strongly_disagree'].length/maxLevel)*100">
          <div fxLayout="row" style="position:relative;width:90%;height:100%">
            <div *ngFor="let agreement of agreementsByLevels['strongly_disagree']; let i = index" class="avatar-container" (click)="togglePosition(agreement.positionId)" [ngClass]="{'hide': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === false}" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" >
                <img class="avatar" [ngClass]="{'red-circle': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === true}" src="./assets/idea.png"/>
            </div>
            <div *ngFor="let agreement of selectedPositionsAgremeentsByLevels['strongly_disagree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" (click)="openParticipant(agreement.metadata.createdBy)" [matTooltip]="participantsDict[agreement.metadata.createdBy].pseudo" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" >
              <img class="avatar" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'"/>
            </div>
          </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start start" style="height:40px">
          <img fxFlex="20px" src="assets/disagree01.svg" height="20px" style="cursor:pointer" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;">strongly<br>disagree</div>
        </div>
      </div>


      <!-- DISAGREE -->

      <div fxLayout="column" fxFlex fxLayoutAlign="end start" style="position:relative;height:100%" >

        <div style="font-size:12px;text-align:left;padding-bottom:15px">
          {{ (agreementsByLevels['disagree'].length/totalAggregatedByPosition)*100 | number:'1.2-2'}}% of {{totalAggregatedByPosition}} ideas have been not supported
        </div>
        <div class="chart-block" fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['disagree'].length/maxLevel)*100">
          <div fxLayout="row" style="position:relative;width:90%;height:100%">
            <div *ngFor="let agreement of agreementsByLevels['disagree']; let i = index" class="avatar-container" (click)="togglePosition(agreement.positionId)" [ngClass]="{'hide': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === false}"  [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center">
                <img class="avatar" [ngClass]="{'red-circle': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === true}" src="./assets/idea.png"/>
            </div>
            <div *ngFor="let agreement of selectedPositionsAgremeentsByLevels['disagree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" (click)="openParticipant(agreement.metadata.createdBy)" [matTooltip]="participantsDict[agreement.metadata.createdBy].pseudo"  [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" >
              <img class="avatar"  [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'"/>
            </div>
          </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start start" style="height:40px">
          <img fxFlex="20px" src="assets/disagree01.svg" height="20px" style="cursor:pointer;margin-left:-10px" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center;margin-left:-25px">disagree</div>
        </div>
      </div>


      <!-- NEUTRAL  -->

      <div fxLayout="column" fxFlex="1px" fxLayoutAlign="end center" style="position:relative;height:100%">

        <div fxLayout="column" fxLayoutAlign="start center" style="height:40px">
          <img fxFlex="20px" src="assets/disagree02.svg" height="20px" style="cursor:pointer" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center">neutral</div>
        </div>
      </div>


      <!-- AGREE  -->

      <div fxLayout="column" fxFlex fxLayoutAlign="end end" style="position:relative;height:100%">

      <div style="font-size:12px;text-align:right;padding-bottom:15px">
          {{ (agreementsByLevels['agree'].length/totalAggregatedByPosition)*100 | number:'1.2-2'}}% of {{totalAggregatedByPosition}} ideas have been supported
        </div>
        <div class="chart-block"  fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['agree'].length/maxLevel)*100">
          <div fxLayout="row" style="position:relative;width:90%;height:100%">
              <div *ngFor="let agreement of agreementsByLevels['agree']; let i = index" class="avatar-container" (click)="togglePosition(agreement.positionId)" [ngClass]="{'hide': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === false}" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center">
                <img [ngClass]="{'red-circle': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === true}" class="avatar"  src="./assets/idea.png"/>
              </div>
              <div *ngFor="let agreement of selectedPositionsAgremeentsByLevels['agree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" (click)="openParticipant(agreement.metadata.createdBy)" [matTooltip]="participantsDict[agreement.metadata.createdBy].pseudo"  [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" >
                <img class="avatar" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'"/>
              </div>
          </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start center" style="height:40px;margin-right:-18px">
          <img fxFlex="20px" src="assets/agree01.svg" height="20px" style="cursor:pointer;" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center">agree</div>
        </div>
      </div>

      <!-- STRONGLY AGREE  -->

      <div fxLayout="column" fxFlex fxLayoutAlign="end end" style="position:relative;height:100%">
        <div style="font-size:12px;text-align:right;padding-bottom:15px">
          {{ (agreementsByLevels['strongly_agree'].length/totalAggregatedByPosition)*100 | number:'1.2-2'}}% of {{totalAggregatedByPosition}} ideas have been strongly supported
        </div>
        <div class="chart-block" fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['strongly_agree'].length/maxLevel)*100">
            <div fxLayout="row" style="position:relative;width:90%;height:100%">
              <div *ngFor="let agreement of agreementsByLevels['strongly_agree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" (click)="togglePosition(agreement.positionId)" [matTooltip]="'Click here to see how this participant felt during the debate '" [ngClass]="{'hide': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === false}" >
                <img [ngClass]="{'red-circle': positionsDict[agreement.positionId] && positionsDict[agreement.positionId].selected === true}" class="avatar"  src="./assets/idea.png" />
              </div>

              <div *ngFor="let agreement of selectedPositionsAgremeentsByLevels['strongly_agree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" (click)="openParticipant(agreement.metadata.createdBy)" [matTooltip]="participantsDict[agreement.metadata.createdBy].pseudo" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" >
                <img class="avatar" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'"/>
              </div>
            </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start end" style="height:40px">
          <img fxFlex="20px" src="assets/agree02.svg" height="20px" style="cursor:pointer;" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center;">strongly<br>agree</div>
        </div>
      </div>
    </div>
    <div fxFlex="30px"></div>
    <app-agreements-position-detail-box class="detail-box"  fxFlex="300px" style="height:100%" [position]="positionsDict[selectedPosition]" [agreements]="positionAgreements" *ngIf="selectedPosition !== ''" [positions]="positions" [arguments]="arguments"></app-agreements-position-detail-box>
    <div  fxFlex="300px" class="detail-box" *ngIf="selectedPosition === ''" fxLayout="column" fxLayoutAlign="start start">
      <div class="detail-title">POSITION DETAILS</div>
      <div fxFlex="5px"></div>
      <div class="detail-text">  Click on one of the positions to see more informations about that</div>
    </div>
    </div>
  `,
  styles: [
    `
    .detail-box {
      border-left:solid 1px #D8D8D8;
      padding:20px;
      height:100%;
    }

    .detail-title {
      color:#003A5A;
      font-size:16px;
      font-weight:bold;
    }

    .detail-text {
      color:#838383;
      font-size:14px;
    }

    .chart-block {
        width:100%;
        position:relative;
        border-top:2px solid #cecece;
        background:#f0faff;
      }

      .avatar {
          width: 25px;
          border: 1px solid #919191;
          border-radius: 50px;
          height: 25px;
          background:white;
      }

      .avatar-container {
        position:absolute;
        width:25px;
        z-index:1000;
      }

      .hide {
        opacity:0.1;
      }

      .red-circle {
        border:3px solid red;

      }


    .label {
        position:absolute;
        background:#003A5A;
        padding:5px 10px;
        color:white;
        top:20px;
        left:0px;
        text-transform:uppercase;
      }
    `
  ]
})
export class AgreementPositionChartComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() debate!:Debate;
  @Input() participants!:any;
  @Input() arguments!:any;
  @Input() positions!:any;
  @Input() positionTriggered!:any;
  @Output() toggleParticipantEmitter:EventEmitter<string> = new EventEmitter<string>();

  argumentsAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  positionsAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  debateAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  allAgreements = <any>[];
  positionsDict = <any>{};

  maxLevel = 0;
  totalAggregatedByPosition:number = 0;
  agreementsByLevels:any;
  selectedPositionsAgremeentsByLevels:any;
  selectedPosition:string = '';
  positionAgreements:any[] = [];
  allAgreementsByPositions = <any>[];
  participantsDict = <any>{};


  constructor(
    private apiService:ApiService,
    private dialog: MatDialog,
    private analyticsService: AnalyticsService
  ) {

  }


  async ngOnChanges(changes: SimpleChanges) {
    if (this.positionTriggered) {
      this.togglePosition(this.positionTriggered)
    } else if (this.debate) {
      this.positionsAgreements$ = this.apiService.fetchPositionsAgreementsByDebate(this.debate);
      this.argumentsAgreements$ = this.apiService.fetchArgumentsAgreementsByDebate(this.debate);
      this.processData();
    }

  }

  getRandomNumber(base:number, offset:number) {
    return Math.floor(Math.random() * base)+offset;
  }

  processData() {
    combineLatest([this.positionsAgreements$, this.argumentsAgreements$]).subscribe(results => {
      // results[0] is positions
      // results[1] is arguments
      this.totalAggregatedByPosition = 0;

      for (let position of this.positions) {
        position.selected = null;
        this.positionsDict[position.id] = position;
      }





      this.allAgreements = this.analyticsService.getAllAgreements(results[0], results[1], this.arguments)
      this.allAgreementsByPositions = this.analyticsService.groupByPositions(this.allAgreements, this.positionsDict);
      this.agreementsByLevels = this.analyticsService.getAgreementsByLevel(this.allAgreementsByPositions);
      this.selectedPositionsAgremeentsByLevels = this.analyticsService.getAgreementsByLevel([]);

      this.participants = this.analyticsService.checkParticipants(this.participants,this.allAgreements);
      let counter = 0;
      for (let participant of this.participants) {
        if (!participant.avatar) {
          participant.avatar = counter%70;
          participant.background = counter%255;
        }
        participant.selected = null;
        this.participantsDict[participant.id] = participant;
        counter++;
      }

      console.log("ALL AGREEMENTS ", this.allAgreements);
      console.log("ALL AGREEMENTS GROUP BY POSITIONS", this.allAgreementsByPositions);
      console.log("POSITIONS BY LEVELS", this.agreementsByLevels);



      this.maxLevel = Math.max(this.agreementsByLevels['strongly_disagree'].length, this.agreementsByLevels['disagree'].length,  this.agreementsByLevels['neutral'].length,  this.agreementsByLevels['agree'].length, this.agreementsByLevels['strongly_agree'].length     )


      this.totalAggregatedByPosition = this.agreementsByLevels['strongly_disagree'].length + this.agreementsByLevels['disagree'].length + this.agreementsByLevels['neutral'].length + this.agreementsByLevels['agree'].length + this.agreementsByLevels['strongly_agree'].length

      console.log(this.agreementsByLevels);
      console.log(this.totalAggregatedByPosition);
    });
  }


  getNumberFromIndex(base:number, offset:number) {
    return Math.floor(Math.random() * base)+offset;
  }


  getBottom(agreement:any, index:number) {
    return ((Math.abs(agreement.level)+ index*10)%80 + 20);
  }

  getAvatar(agreement:any, index:number) {
    return (Math.trunc((agreement.level+ index*10)%25) +1);
  }


  openParticipant(participantId:string) {
    console.log(participantId);
    this.toggleParticipantEmitter.emit(participantId);

    let element = document.getElementById('people-chart');
    if (element) {
      element.scrollIntoView({behavior:'smooth', block:'center'});
    }
  }


  togglePosition(id:string) {
    if (this.positionsDict[id] && !this.positionsDict[id].selected) {
      this.positionsDict[id].selected = true;

      for (let positionId in this.positionsDict) {
        if (positionId !== id) {
          this.positionsDict[positionId].selected = false;
        }
      }

      // EXPLODE DATA ABOUT PARTICIPANT

      this.explodePositiontData(id);
    } else {
      this.resetData();
    }
  }

  explodePositiontData(id: string) {
    this.positionAgreements = this.allAgreements.filter((a:any) =>{
      if (a.positionId === id) {
        return a;
      } else {
        return null;
      }
    })


    this.selectedPositionsAgremeentsByLevels = this.analyticsService.getAgreementsByLevel(this.positionAgreements);
    this.selectedPosition = id;
    console.log(this.selectedPositionsAgremeentsByLevels);
  }

  resetData() {
    console.log("RESET DATA")
    this.positionAgreements = [];
    this.selectedPositionsAgremeentsByLevels = this.analyticsService.getAgreementsByLevel([]);
    for (let positionId in this.positionsDict) {
      this.positionsDict[positionId].selected = null;
    }

    this.selectedPosition = '';
  }


}
