import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-reflections-widget',
  template: `
    <div fxLayout="row" fxLayoutAlign="start end">
        <div style="font-size:40px;line-height:1">10</div>
        <div fxFlex="5px"></div>
        <div>positions<br>reflections</div>
    </div> 
    <div fxFlex="10px"></div>
    <div style="height:9px;width:100%;background: #f7f9fc;position:relative">
        <div style="position: absolute;height:100%;width:70%;background:#012e46"></div>
    </div>
    <div style="font-size:10px;color:#8f9bb3">Most people would agree with this</div>
    <div style="height:9px;width:100%;background: #f7f9fc;position:relative">
        <div style="position: absolute;height:100%;width:80%;background:#012e46"></div>
    </div>
    <div style="font-size:10px;color:#8f9bb3">This is a very polarized position</div>
    
    <div style="height:9px;width:100%;background: #f7f9fc;position:relative">
        <div style="position: absolute;height:100%;width:30%;background:#012e46"></div>
    </div>
    <div style="font-size:10px;color:#8f9bb3">It is clear I can trust this</div>
    
    <div style="height:9px;width:100%;background: #f7f9fc;position:relative">
        <div style="position: absolute;height:100%;width:20%;background:#012e46"></div>
    </div>
    <div style="font-size:10px;color:#8f9bb3">This should be prioritized</div>


  `,
  styles: [

  ]
})
export class ReflectionWidgetComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() argument!:Argument;
  @Input() highlighted: boolean = false;
  @Input() participants!:any;
  @Input() arguments!:any;
  @Input() positions!:any;
  @Input() debate!:Debate;

  reflectionsCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
  }
}
