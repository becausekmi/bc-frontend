import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-participants-widget',
  template: `
    <div fxLayout="row" fxLayoutAlign="start end">
    <div style="font-size:40px;line-height:1">20</div>
    <div fxFlex="5px"></div>
    <div>participants</div>
    </div> 

    
    <div style="height:9px;width:100%;background: #f7f9fc;position:relative">
        <div style="left:0px;position: absolute;height:100%;width:30%;background:#d4d4d4"></div>
        <div style="right:0px;position: absolute;height:100%;width:70%;background:#012e46"></div>
    </div>
    <div style="font-size:12px;color:#8f9bb3;width:100%" fxLayout="row" fxLayoutAlign="space-between center">
    <div>Lurkers (30%)</div>
    <div>Active (70%)</div>
    </div>
  `,
  styles: [

  ]
})
export class ParticipantWidgetComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() argument!:Argument;
  @Input() highlighted: boolean = false;
  @Input() participants!:any;
  @Input() arguments!:any;
  @Input() positions!:any;
  @Input() debate!:Debate;

  participantsCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
  }
}
