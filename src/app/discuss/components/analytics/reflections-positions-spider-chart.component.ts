import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, combineLatest, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { AnalyticsService } from 'src/app/core/services/analytics.service';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-reflections-positions-spider-chart',
  template: `
  <div class="label">Reflections on Positions across the debate</div>

  `,
  styles: [`

    .detail-box {
      border-left:solid 1px #D8D8D8;
      padding:20px;
      height:100%;
    }

    .detail-title {
      color:#003A5A;
      font-size:16px;
      font-weight:bold;
    }

    .detail-text {
      color:#838383;
      font-size:14px;
    }

    .chart-block {
        width:100%;
        position:relative;
        border-top:2px solid #cecece;
        background:#f0faff;
      }

      .avatar {
          width: 25px;
          border: 1px solid #919191;
          border-radius: 50px;
          height: 25px;
          background:white;
      }

      .avatar-container {
        position:absolute;
        width:25px;
        z-index:1000;
      }

      .hide {
        opacity:0.1;
      }

      .red-circle {
        border:3px solid red;

      }

      .label {
        position:absolute;
        background:#003A5A;
        padding:5px 10px;
        color:white;
        top:20px;
        left:0px;
        text-transform:uppercase;
      }
    `
  ]
})
export class ReflectionsPositionsSpiderChartComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() debate!:Debate;
  @Input() participants!:any;
  @Input() arguments!:any;
  @Input() positions!:any;
  @Input() participantTriggered!:any;
  @Output() averageLevelEmit:EventEmitter<number> = new EventEmitter<number>();
  @Output() togglePositionEmitter:EventEmitter<string> = new EventEmitter<string>();

  loading:boolean = true;

  positionsReflections$: BehaviorSubject<any> = new BehaviorSubject<null>(null);


  constructor(
    private apiService:ApiService,
    private dialog: MatDialog,
    private analyticsService: AnalyticsService
  ) {

  }

  async ngOnChanges(changes: SimpleChanges) {
    this.loading = true;
    if (this.participantTriggered) {
    } else if (this.debate) {
      //this.positionsReflections$ = this.apiService.fetchRefl
      console.log(this.positions);
      this.processData();
    }

  }

  openPosition(positionId:string) {
    this.togglePositionEmitter.emit(positionId);
  }


  processData() {
   
  }

}
