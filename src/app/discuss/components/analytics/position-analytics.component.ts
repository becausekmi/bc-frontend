import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-agreements-widget',
  template: `
   <div fxLayout="row" fxLayoutAlign="start end">
        <div style="font-size:40px;line-height:1">20</div>
        <div fxFlex="5px"></div>
        <div>agreements<br>left so far</div>
    </div> 
  `,
  styles: [

  ]
})
export class PositionAnalyticsComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() debate!:Debate;
  @Input() participants!:any;
  @Input() arguments!:any;
  @Input() positions!:any;

  heart$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  agreementsCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {
  }
}
