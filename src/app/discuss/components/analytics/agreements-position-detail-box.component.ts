import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, combineLatest, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { AnalyticsService } from 'src/app/core/services/analytics.service';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-agreements-position-detail-box',
  template: `
    <div fxLayout="column" fxLayoutAlign="start start" class="scroll-bar" style="overflow-y:scroll;height:100%;width:100%;padding-right:10px" >


        <!-- <div class="detail-title" *ngIf="participant.pseudo !== ''">{{participant.pseudo}} DETAILS</div>
        <div class="detail-title" *ngIf="participant.pseudo === ''">PARTICIPANT DETAILS</div> -->
        <div fxFlex="5px"></div>
        <div class="detail-title" >"{{getPosition(position).text }}" </div>
        <div *ngFor="let agreement of explicitAgreements" fxLayout="row" fxLayoutAlign="start center" style="margin-top:10px;width:100%">
          <img class="avatar" [src]="getAgreementFace(agreement)" />
          <div fxFlex="10px"></div>
          <div fxFlex class="detail-text">in {{getAgreementLabel(agreement)}} with </div>
          <div fxFlex="5px"></div>
        </div>
        <div fxFlex="10px" *ngIf="explicitAgreements.length"></div>
        <div *ngFor="let agreement of implicitAgreements" fxLayout="row" fxLayoutAlign="start center" style="margin-top:10px;width:100%">
          <img *ngIf="agreement.mode === 'opposing'" src="assets/minus-active.svg" class="avatar"/>
          <img *ngIf="agreement.mode === 'supporting'" src="assets/plus-active.svg" class="avatar"/>
          <div fxFlex="10px"></div>
          <div class="detail-text" fxFlex [matTooltip]="getArgument(agreement.argumentId).text">{{getArgument(agreement.argumentId).text | slice:0:40 }} ...</div>
          <div fxFlex="5px"></div>

        </div>
    </div>
   `,
  styles: [`
    .detail-box {
      border-left:solid 1px #D8D8D8;
      padding:20px;
      height:100%;
    }

    .detail-title {
      color:#003A5A;
      font-size:15px;
      font-weight:500;
      font-style:italic;
    }

    .chart-block {
        width:100%;
        position:relative;
        border-top:2px solid #cecece;
        background:#f0faff;
      }

      .avatar {
          width: 25px;
          border: 1px solid #919191;
          border-radius: 50px;
          height: 25px;
          background:white;
          margin-left:0px;
      }

      .avatar-container {
        position:absolute;
        width:25px;
        z-index:1000;
      }

      .hide {
        opacity:0.1;
      }

      .red-circle {
        border:3px solid red;

      }
    `
  ]
})
export class AgreementPositionDetailComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() debate!:Debate;
  @Input() position!:any;
  @Input() arguments!:any;
  @Input() positions!:any;
  @Input() agreements!:any;
  @Output() toggleParticipantEmitter = new EventEmitter();


  explicitAgreements: any[] = [];
  implicitAgreements: any[] = [];

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog,
    private analyticsService: AnalyticsService
  ) {

  }

  async ngOnChanges(changes: SimpleChanges) {

    this.explicitAgreements = this.agreements.filter((a:any) => a.type === 'explicit');
    this.implicitAgreements = this.agreements.filter((a:any) => a.type === 'implicit');
    console.log(this.agreements, this.implicitAgreements, this.explicitAgreements);
  }

  getPosition(position:any) {
    return this.positions.find((p:any)=>{
      return p.id === position.id;
    })
  }

  getArgument(argumentId:string) {
    return this.arguments.find((a:any)=>{
      return a.id === argumentId;
    })
  }

  getAgreementLabel(agreement:any) {
    return this.analyticsService.getAgreementsLabel(agreement.level);
  }

  getAgreementFace(agreement:any) {
    return './assets/'+this.analyticsService.getAgreementFace(agreement.level);
  }
}
