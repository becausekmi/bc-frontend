import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, combineLatest, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { AnalyticsService } from 'src/app/core/services/analytics.service';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-agreements-people-chart',
  template: `
  <div class="label">Participants Agreements across the debate</div>

  <div fxLayout="row" fxLayoutAlign="center center" style="width:100%;height:350px;" class="noselect"  *ngIf="!agreementsByLevels || !maxLevel">
      No Data about Agreements
  </div>
  <div fxLayout="row" fxLayoutAlign="start end" style="width:100%;height:350px;" class="noselect"  *ngIf="agreementsByLevels && maxLevel">
    <!-- CHART -->
    <div fxLayout="row" fxLayoutAlign="start end" style="height:100%;padding:60px 30px 30px 30px" fxFlex class="noselect"  *ngIf="agreementsByLevels && maxLevel">

      <!-- STRONGLY DISAGREE -->
      <div fxLayout="column" fxFlex fxLayoutAlign="end start" style="position:relative;height:100%" *ngIf="agreementsByLevels['strongly_disagree']">
        <div style="font-size:12px;text-align:left;padding-bottom:15px">
          <b>{{ (agreementsByLevels['strongly_disagree'].length/totalAggregatedByPeople)*100 | number:'1.2-2'}}%</b> of <b>{{totalAggregatedByPeople}}</b> participants<br> <b>strongly disagreed</b>
        </div>
        <div class="chart-block" fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['strongly_disagree'].length/maxLevel)*100">
          <div fxLayout="row" style="position:relative;width:90%;height:100%">
            <div *ngFor="let agreement of agreementsByLevels['strongly_disagree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" [matTooltip]="'Click here to see how this participant felt during the debate '"  [ngClass]="{'hide': participantsDict[agreement.metadata.createdBy].selected === false}" (click)="toggleParticipant(agreement.metadata.createdBy)">

              <img class="avatar" [ngClass]="{'red-circle': participantsDict[agreement.metadata.createdBy].selected === true}" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'" [style.background]="'rgba('+participantsDict[agreement.metadata.createdBy].background+','+(255-participantsDict[agreement.metadata.createdBy].background)+','+((participantsDict[agreement.metadata.createdBy].background+255)/2)+',0.5)'"/>
            </div>
              <div *ngFor="let agreement of selectedParticipantAgremeentsByLevels['strongly_disagree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center">
                <img class="avatar" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'"/>
              </div>
          </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start start" style="height:40px">
          <img fxFlex="20px" src="assets/disagree01.svg" height="20px" style="cursor:pointer" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;">strongly<br>disagree</div>
        </div>
      </div>


      <!-- DISAGREE -->

      <div fxLayout="column" fxFlex fxLayoutAlign="end start" style="position:relative;height:100%" *ngIf="agreementsByLevels['disagree']">
        <div style="font-size:12px;text-align:left;padding-bottom:15px">
          <b>{{ (agreementsByLevels['disagree'].length/totalAggregatedByPeople)*100 | number:'1.2-2'}}%</b> of <b>{{totalAggregatedByPeople}}</b> participants<br> <b>disagreed</b>
        </div>
        <div class="chart-block" fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['disagree'].length/maxLevel)*100">
          <div fxLayout="row" style="position:relative;width:90%;height:100%">
            <div *ngFor="let agreement of agreementsByLevels['disagree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" [matTooltip]="'Click here to see how this participant felt during the debate '" [ngClass]="{'hide': participantsDict[agreement.metadata.createdBy].selected === false}" (click)="toggleParticipant(agreement.metadata.createdBy)">

              <img class="avatar" [ngClass]="{'red-circle': participantsDict[agreement.metadata.createdBy].selected === true}" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'" [style.background]="'rgba('+participantsDict[agreement.metadata.createdBy].background+','+(255-participantsDict[agreement.metadata.createdBy].background)+','+((participantsDict[agreement.metadata.createdBy].background+255)/2)+',0.5)'"/>
            </div>
              <div *ngFor="let agreement of selectedParticipantAgremeentsByLevels['disagree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" >
                <img class="avatar" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'" />
              </div>
          </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start start" style="height:40px">
          <img fxFlex="20px" src="assets/disagree01.svg" height="20px" style="cursor:pointer;margin-left:-10px" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center;margin-left:-25px">disagree</div>
        </div>
      </div>


      <!-- NEUTRAL  -->

      <div fxLayout="column" fxFlex="1px" fxLayoutAlign="end center" style="position:relative;height:100%" >

        <div fxLayout="column" fxLayoutAlign="start center" style="height:40px">
          <img fxFlex="20px" src="assets/disagree02.svg" height="20px" style="cursor:pointer" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center">neutral</div>
        </div>
      </div>


      <!-- AGREE  -->

      <div fxLayout="column" fxFlex fxLayoutAlign="end end" style="position:relative;height:100%" *ngIf="agreementsByLevels['agree']">
        <div style="font-size:12px;text-align:right;padding-bottom:15px">
          <b>{{ (agreementsByLevels['agree'].length/totalAggregatedByPeople)*100 | number:'1.2-2'}}%</b> of <b>{{totalAggregatedByPeople}}</b> participants <br> <b>agreed</b>
        </div>
        <div class="chart-block"  fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['agree'].length/maxLevel)*100">
          <div fxLayout="row" style="position:relative;width:90%;height:100%">
              <div *ngFor="let agreement of agreementsByLevels['agree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" [matTooltip]="'Click here to see how this participant felt during the debate '" [ngClass]="{'hide': participantsDict[agreement.metadata.createdBy].selected === false}" (click)="toggleParticipant(agreement.metadata.createdBy)">

                <img class="avatar"  [ngClass]="{'red-circle': participantsDict[agreement.metadata.createdBy].selected === true}" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'" [style.background]="'rgba('+participantsDict[agreement.metadata.createdBy].background+','+(255-participantsDict[agreement.metadata.createdBy].background)+','+((participantsDict[agreement.metadata.createdBy].background+255)/2)+',0.5)'"/>
              </div>
              <div *ngFor="let agreement of selectedParticipantAgremeentsByLevels['agree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center">
                <img class="avatar" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'"/>
              </div>
          </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start center" style="height:40px;margin-right:-18px">
          <img fxFlex="20px" src="assets/agree01.svg" height="20px" style="cursor:pointer;" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center">agree</div>
        </div>
      </div>

      <!-- STRONGLY AGREE  -->

      <div fxLayout="column" fxFlex fxLayoutAlign="end end" style="position:relative;height:100%" *ngIf="agreementsByLevels['strongly_agree']">
        <div style="font-size:12px;text-align:right;padding-bottom:15px">
          <b>{{ (agreementsByLevels['strongly_agree'].length/totalAggregatedByPeople)*100 | number:'1.2-2'}}%</b> of <b>{{totalAggregatedByPeople}}</b> participants<br> <b>strongly agreed</b>
        </div>
        <div class="chart-block" fxLayout="row" fxLayoutAlign="start start" [style.height.%]="(agreementsByLevels['strongly_agree'].length/maxLevel)*100">
            <div fxLayout="row" style="position:relative;width:90%;height:100%">
              <div *ngFor="let agreement of agreementsByLevels['strongly_agree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" [matTooltip]="'Click here to see how this participant felt during the debate '" [ngClass]="{'hide': participantsDict[agreement.metadata.createdBy].selected === false}" (click)="toggleParticipant(agreement.metadata.createdBy)">

                <img class="avatar" [ngClass]="{'red-circle': participantsDict[agreement.metadata.createdBy].selected === true}" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'" [style.background]="'rgba('+participantsDict[agreement.metadata.createdBy].background+','+(255-participantsDict[agreement.metadata.createdBy].background)+','+((participantsDict[agreement.metadata.createdBy].background+255)/2)+',0.5)'"/>
              </div>

              <div *ngFor="let agreement of selectedParticipantAgremeentsByLevels['strongly_agree']; let i = index" class="avatar-container" [style.bottom.%]="getBottom(agreement, i)" [style.left.%]="agreement.offset*100" fxLayout="row" fxLayoutAlign="center center" >
                <img class="avatar" *ngIf="participantsDict[agreement.metadata.createdBy]" [src]="'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/avatars%2F'+participantsDict[agreement.metadata.createdBy].avatar+'.svg?alt=media'" />
              </div>
            </div>
        </div>
        <div fxLayout="column" fxLayoutAlign="start end" style="height:40px">
          <img fxFlex="20px" src="assets/agree02.svg" height="20px" style="cursor:pointer;" />
          <div fxFlex="5px"></div>
          <div  style="line-height: 1;text-align:center;">strongly<br>agree</div>
        </div>
      </div>
    </div>
    <div fxFlex="30px"></div>
    <app-agreements-participant-detail-box class="detail-box"  fxFlex="300px" style="height:100%" [participant]="participantsDict[selectedParticipant]" [agreements]="participantAgreements" (togglePositionEmitter)="openPosition($event)" *ngIf="selectedParticipant !== ''" [positions]="positions" [arguments]="arguments"></app-agreements-participant-detail-box>
    <!--  <div  fxLayout="column" fxLayoutAlign="start start" class="scroll-bar" style="border-left:1px solid #cecece; max-height:100%;padding:0px 30px;height:100%;overflow-y:scroll" fxFlex="300px">
        <b *ngIf="participantsDict[selectedParticipant].pseudo !== ''">{{participantsDict[selectedParticipant].pseudo}} felt:</b>
        <b *ngIf="participantsDict[selectedParticipant].pseudo === ''">This participant felt:</b>
        <div *ngFor="let agreement of participantAgreements" fxLayout="column" style="margin-top:10px">
            <div fxLayout="row" fxLayoutAlign="start center">
              <img class="avatar" [src]="getAgreementFace(agreement)" />
              <div fxFlex="5px"></div>
              <div fxFlex>in {{getAgreementLabel(agreement)}} with </div>
              <div fxFlex="10px"></div>
              <img class="avatar"  src="./assets/idea.png" [matTooltip]="getLabel(agreement.positionId)"/>
            </div>
        </div>
    </div> -->
    <div  fxFlex="300px" class="detail-box" *ngIf="selectedParticipant === ''" fxLayout="column" fxLayoutAlign="start start">
      <div class="detail-title">PARTICIPANT DETAILS</div>
      <div fxFlex="5px"></div>
      <div class="detail-text">  Click on one of the avatars to see more informations about the single participants</div>
    </div>
   </div>
  `,
  styles: [`

    .detail-box {
      border-left:solid 1px #D8D8D8;
      padding:20px;
      height:100%;
    }

    .detail-title {
      color:#003A5A;
      font-size:16px;
      font-weight:bold;
    }

    .detail-text {
      color:#838383;
      font-size:14px;
    }

    .chart-block {
        width:100%;
        position:relative;
        border-top:2px solid #cecece;
        background:#f0faff;
      }

      .avatar {
          width: 25px;
          border: 1px solid #919191;
          border-radius: 50px;
          height: 25px;
          background:white;
      }

      .avatar-container {
        position:absolute;
        width:25px;
        z-index:1000;
      }

      .hide {
        opacity:0.1;
      }

      .red-circle {
        border:3px solid red;

      }

      .label {
        position:absolute;
        background:#003A5A;
        padding:5px 10px;
        color:white;
        top:20px;
        left:0px;
        text-transform:uppercase;
      }
    `
  ]
})
export class AgreementPeopleChartComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() debate!:Debate;
  @Input() participants!:any;
  @Input() arguments!:any;
  @Input() positions!:any;
  @Input() participantTriggered!:any;
  @Output() averageLevelEmit:EventEmitter<number> = new EventEmitter<number>();
  @Output() togglePositionEmitter:EventEmitter<string> = new EventEmitter<string>();

  loading:boolean = true;

  argumentsAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  positionsAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  debateAgreements$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  allAgreements = <any>[];
  allAgreementsByUsers = <any>[];
  participantsDict = <any>{};

  maxLevel = 0;
  totalAggregatedByPeople:number = 0;
  agreementsByLevels:any;
  selectedParticipantAgremeentsByLevels:any;
  selectedParticipant:string = '';
  participantAgreements:any[] = [];

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog,
    private analyticsService: AnalyticsService
  ) {

  }

  async ngOnChanges(changes: SimpleChanges) {
    this.loading = true;
    if (this.participantTriggered) {
      this.toggleParticipant(this.participantTriggered)
    } else if (this.debate) {
      this.positionsAgreements$ = this.apiService.fetchPositionsAgreementsByDebate(this.debate);
      this.argumentsAgreements$ = this.apiService.fetchArgumentsAgreementsByDebate(this.debate);
      console.log(this.positions);
      this.processData();
    }

  }

  openPosition(positionId:string) {
    this.togglePositionEmitter.emit(positionId);
  }

  getNumberFromIndex(base:number, offset:number) {
    return Math.floor(Math.random() * base)+offset;
  }

  getBottom(agreement:any, index:number) {
    return ((Math.abs(agreement.level)+ index*10)%80 + 20);
  }

  getAvatar(agreement:any, index:number) {
    return (Math.trunc((agreement.level+ index*10)%70) +1);
  }

  getLabel(positionId:string) {
    let foundPosition = this.positions.find((p:Position)=>p.id == positionId);
    if (foundPosition) {
      return foundPosition.text
    } else {
      return '';
    }
  }

  getAgreementLabel(agreement:any) {
    return this.analyticsService.getAgreementsLabel(agreement.level);
  }

  getAgreementFace(agreement:any) {
    return './assets/'+this.analyticsService.getAgreementFace(agreement.level);
  }

  processData() {
    combineLatest([this.positionsAgreements$, this.argumentsAgreements$]).subscribe(results => {

      // results[0] is positions agreements
      // results[1] is arguments agreements

     this.allAgreements = this.analyticsService.getAllAgreements(results[0], results[1], this.arguments)
     this.allAgreementsByUsers = this.analyticsService.groupByUsers(this.allAgreements);

     this.participants = this.analyticsService.checkParticipants(this.participants,this.allAgreements);
     console.log("ALL AGREEMENTS ", this.allAgreements);
     console.log("ALL AGREEMENTS GROUP BY PEOPLE", this.allAgreementsByUsers);
     console.log("ALL PARTICIPANTS", this.participants);
      let counter = 0;
      for (let participant of this.participants) {
        if (!participant.avatar) {
          participant.avatar = counter%70;
        }

        participant.background = (counter*10+this.getNumberFromIndex(255,0))%200;
        participant.selected = null;
        this.participantsDict[participant.id] = participant;
        counter++;
      }

      this.agreementsByLevels = this.analyticsService.getAgreementsByLevel(this.allAgreementsByUsers);
      this.selectedParticipantAgremeentsByLevels = this.analyticsService.getAgreementsByLevel([]);

      this.maxLevel = Math.max(this.agreementsByLevels['strongly_disagree'].length, this.agreementsByLevels['disagree'].length, this.agreementsByLevels['neutral'].length,  this.agreementsByLevels['agree'].length, this.agreementsByLevels['strongly_agree'].length     )

      this.totalAggregatedByPeople = this.agreementsByLevels['strongly_disagree'].length + this.agreementsByLevels['disagree'].length + this.agreementsByLevels['neutral'].length + this.agreementsByLevels['agree'].length + this.agreementsByLevels['strongly_agree'].length

      this.loading = false;
      let sum = 0;
      for (let agreement of this.allAgreements) {
        if (!agreement.type) {
          agreement.type ='explicit';
        }

        if (! this.participantsDict[agreement.metadata.createdBy]) {
          this.participantsDict[agreement.metadata.createdBy] = {
            avatar: 5,
            debateId: this.debate.id,
            id: "0LGmychSYIbU5SMQJCcIIfMRdbG2",
            metadata: {createdBy: agreement.metadata.createdBy, createdDate: 1622688190343},
            pseudo: "",
            selected: true
          }
        }
        sum += agreement.level;
      }

      this.averageLevelEmit.emit(sum/this.allAgreements.length);

      this.resetData();
    });
  }

  toggleParticipant(id:string) {
    if (!this.participantsDict[id].selected) {
      this.participantsDict[id].selected = true;

      for (let participantId in this.participantsDict) {
        if (participantId !== id) {
          this.participantsDict[participantId].selected = false;
        }
      }

      // EXPLODE DATA ABOUT PARTICIPANT

      this.explodeParticipantData(id);
    } else {
      this.resetData();
    }
  }

  explodeParticipantData(id: string) {
    this.participantAgreements = this.allAgreements.filter((a:any) =>{
      if (a.metadata.createdBy === id) {
        return a;
      } else {
        return null;
      }
    })
    this.selectedParticipantAgremeentsByLevels = this.analyticsService.getAgreementsByLevel(this.participantAgreements);
    this.selectedParticipant = id;
    console.log(this.participantsDict[this.selectedParticipant]);
  }

  resetData() {
    this.participantAgreements = [];
    this.selectedParticipantAgremeentsByLevels = this.analyticsService.getAgreementsByLevel([]);
    for (let participantId in this.participantsDict) {
      this.participantsDict[participantId].selected = null;
    }

    this.selectedParticipant = '';
  }
}
