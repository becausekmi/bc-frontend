import { Component, OnInit, Inject, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-positions-widget',
  template: `
        <div style="position:absolute;cursor:pointer;right:10px;top:10px">
            <img src="assets/click.svg" style="height:20px;width:20px" />
        </div>
        <div fxLayout="row" fxLayoutAlign="start end">
        <div style="font-size:40px;line-height:1" *ngIf="positions">{{ positions.length}}</div>
        <div fxFlex="5px"></div>
        <div>positions</div>
        </div> 
        <div style="height:9px;width:100%;background: #f7f9fc;position:relative">
            <div style="position: absolute;height:100%;width:70%;background:#012e46"></div>
        </div>
        <div style="font-size:12px;color:#8f9bb3">Better than last week (70%)</div>
        <div fxFlex="20px"></div>
        <div fxLayout="row" fxLayoutAlign="start end">
        <div style="font-size:40px;line-height:1" *ngIf="arguments">{{ arguments.length}}</div>
        <div fxFlex="5px"></div>
        <div>arguments</div>
        </div> 
        <div style="height:9px;width:100%;background: #f7f9fc;position:relative">
            <div style="left:0px;position: absolute;height:100%;width:30%;background:#FE9329"></div>
            <div style="right:0px;position: absolute;height:100%;width:70%;background:#007FC1"></div>
        </div>
        <div style="font-size:12px;color:#8f9bb3;width:100%" fxLayout="row" fxLayoutAlign="space-between center">
        <div *ngIf="arguments">CONs ({{(pros/arguments.length)*100 | number: '1.2-2'}}%)</div>
        <div *ngIf="arguments">PROs ({{(cons/arguments.length)*100 | number: '1.2-2'}}%)</div>
    </div>
  `,
  styles: [

  ]
})
export class PositionWidgetComponent implements OnChanges {
  @Input() userData!:Identity | null;
  @Input() argument!:Argument;
  @Input() highlighted: boolean = false;
  @Input() participants!:any;
  @Input() arguments!:any;
  @Input() positions!:any;
  @Input() debate!:Debate;

  positionsCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  argumentsCounter$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  pros:number = 0;
  cons:number = 0;

  constructor(
    private apiService:ApiService,
    private dialog: MatDialog
  ) { }

  async ngOnChanges(changes: SimpleChanges) {

    if (this.arguments) {
      this.pros = 0;
      this.cons = 0;
      for (let argument of this.arguments) {
        if (argument.type === 'SUPPORTING') {
          this.pros++;
        } else {
          this.cons++;
        }
      }
    }
  }
}
