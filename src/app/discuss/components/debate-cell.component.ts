import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Debate } from 'src/app/core/models/debate';

@Component({
  selector: 'app-debate-cell',
  template: `
            <div fxLayout="column" fxFill fxLayoutAlign="start start" style="border:1px solid #d8d8d8;border-radius:25px;position:relative">
                <div matTooltip="The debate is visible only to users who have the URL or already contributed" *ngIf="debate.visibility === 'private'" style="position:absolute;left:15px;top:15px;color:red"><mat-icon>visibility_off</mat-icon></div>
                <div matTooltip="The debate is accessible only to users who joined the discussion group" *ngIf="debate.interactions === 'close'" style="position:absolute;right:15px;top:15px;color:red"><mat-icon>groups</mat-icon></div>
                
                <div class="debate-image" style="cursor:pointer;" [routerLink]="'/discuss/debate/'+debate.id+'/preview'"  [style.backgroundImage]="'url('+debate.image+')'">
                </div>
                <div fxLayout="column" style="padding:20px" class="width-100">
                    <div [routerLink]="'/discuss/debate/'+debate.id+'/preview'" style="cursor:pointer;text-align:left;font-size:1rem;font-weight: bold;color:black;height:100px;overflow:hidden">{{debate.title | slice:0:200 }}</div>
                    <div fxFlex="20px"></div>
                    <app-debate-stats [userData]="userData" [debate]="debate" (updateDebates)="emitNewData()"></app-debate-stats>
                </div>
            </div>
  `,
  styles: [`

    .debate-image {
        box-sizing: border-box;
        border-top-left-radius: 25px;
        border-top-right-radius: 25px;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        width:100%;height:200px;background-size:cover;background-color:white
    }


  `]
})
export class DebateCellComponent {
  @Input() debatesContributedByUser!:any;
  @Input() debate!:Debate;
  @Input() userData!: any;
  @Output() updateData = new EventEmitter();

  constructor() {
  }

  ngOnChanges() {
  }

  emitNewData() {
    this.updateData.emit(true);
  }
}
