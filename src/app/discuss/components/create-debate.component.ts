import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { ApiService } from 'src/app/core/services/api.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-create-debate',
  template: `
    <div mat-dialog-content  fxLayout="column" class="scroll-bar">
        <div fxLayout="row" fxLayoutAlign="space-between center">
          <div style="font-size:1rem;font-weight:bold" *ngIf="!alreadyExist">New Discussion</div>
          <div style="font-size:1rem;font-weight:bold" *ngIf="alreadyExist">Edit Discussion</div>
          <i class="material-icons" style="cursor:pointer" (click)="close(false)">close</i>
        </div>
        <div fxFlex="10px"></div>
        <div fxLayout="row"  class="margin-t-20 width-100">

          <div *ngIf="image && image!== ''" [style.backgroundImage]="'url('+image+')'" style="height:200px;width:200px;background-size:cover;border-radius:5px">

          </div>
          <div *ngIf="image && image!== ''" fxFlex="20px"></div>
          <div fxLayout="column" fxFlex>
            <b style="font-size:1rem;" [ngClass]="{'red':alert}"> Question*</b>
            <input class="input-box" rows="5" [ngClass]="{'red-border':alert}" [(ngModel)]="title" placeholder="Write the discussion question here" required/>
            <div fxFlex="20px"></div>
            <b style="font-size:1rem;"> Write more here</b>
            <textarea rows="5" class="input-box" [(ngModel)]="text" placeholder="Write more info here" required></textarea>
          </div>
        </div>
        <div fxFlex="20px"></div>
        <div fxLayout="column">
          <b style="font-size:1rem;">Discussion Cover</b>
          <div fxLayout="column" fxLayoutAlign="start start">

            
            <div *ngIf="image && image!== ''" style="width:20px">
            </div>
            <div fxLayout="column" fxLayoutAlign="center start" fxFlex>
                <label *ngIf="!uploadCoverPercent" [for]="'file-upload-cover'" class="label-button" fxLayout="row" fxLayoutAlign="center center">

                </label>
                <div *ngIf="uploadCoverPercent" class="label-button margin-t-10 width-100"  fxLayout="row" fxLayoutAlign="center center">
                    {{ uploadCoverPercent| async  | number:'1.2-2'}} %
                </div>
                <input [id]="'file-upload-cover'" accept=".jpg,.jpeg,.png" type="file" (change)="uploadCover($event)" />
            </div>
          </div>
          </div>
        <div fxFlex="20px"></div>
        <b style="font-size:1rem;" [ngClass]="{'red':alert}">Set the Discussion Visibility</b>
        <div style="font-size:1rem;"[ngClass]="{'red':alert}"> Do you want to show the discussion in the search list?</div>
        <div fxFlex="5px"></div>
        <mat-radio-group aria-label="Select an option" [(ngModel)]="visibility" fxLayout="row" fxLayoutAlign="start center">
            <mat-radio-button [value]="'public'">YES</mat-radio-button>
            <div fxFlex="20px"></div>
            <mat-radio-button [value]="'private'">NO</mat-radio-button>
        </mat-radio-group>
        
        <div fxFlex="20px"></div>
        <b style="font-size:1rem;" [ngClass]="{'red':alert}">Set the Discussion Interaction Mode</b>
        <div style="font-size:1rem;" [ngClass]="{'red':alert}"> How do you want to manage users interaction?</div>
        <div fxFlex="5px"></div>
        <mat-radio-group aria-label="Select an option" [(ngModel)]="interactions" fxLayout="column" fxLayoutAlign="start start">
            <mat-radio-button [value]="'open'">Everybody can join the discussion</mat-radio-button>
            <div fxFlex="10px"></div>
            <mat-radio-button [value]="'close'">Only users who belong to the discussion group can join the discussion</mat-radio-button>
        </mat-radio-group>
        <div *ngIf="interactions === 'close'" fxFlex="20px"></div>
        <div *ngIf="interactions === 'close'"  fxLayout="column" >
          <b style="font-size:1rem;" [ngClass]="{'red':alert}">Choose the Discussion Group</b>
          <div fxLayout="row" fxLayoutAlign="start center" style="width:100%">
            
            <select fxFlex class="input-box" [(ngModel)]="selectedGroup" >
              <option value="null">Select a group</option>
              <option [value]="group.id" *ngFor="let group of (groups$ | async)">{{group.title}}</option>
            </select>
            <div fxFlex="10px" *ngIf="!data.debate"></div>
            <span *ngIf="!data.debate">or</span>
            <div fxFlex="10px" *ngIf="!data.debate"></div>
            <button *ngIf="!data.debate" fxFlex="100px" mat-button class="button-blue" (click)="openDiscussionModal()" >Create</button>
          </div>
        </div>
        <div fxFlex="20px"></div>
        <button mat-button class="button-blue-negative" (click)="close({text,title,image, visibility, interactions, selectedGroup})" *ngIf="!alreadyExist">Create the Discussion</button>
        <button mat-button class="button-blue-negative" (click)="close({text,title,image, visibility, interactions,selectedGroup})" *ngIf="alreadyExist">Update Discussion</button>
    </div>
  `,
  styles: [
    `
      .red{
        color:red;
      }

      .red-border {
        border-color:red!important;
      }

      .input-box {
       border-radius:5px;border:1px solid rgba(0, 0, 0, 0.87);padding:10px;
      }
    `
  ]
})
export class NewDebateComponent implements OnInit {

  text:string = '';
  title:string = '';
  image:string = '';
  alert:boolean = false;
  visibility:string = 'public';
  interactions:string = 'open';
  uploadCoverPercent:any;
  alreadyExist:boolean = false;
  groupId:any = null;
  groups$!:BehaviorSubject<any[]>;
  selectedGroup:any = null;

  constructor(
    private storage: AngularFireStorage, // @todo replace this
    private apiService: ApiService,
    public dialogRef: MatDialogRef<NewDebateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 

    
    if (this.data && this.data.debate) {
      console.log(this.data);
      this.alreadyExist = true;
      this.text = this.data.debate.text;
      this.title = this.data.debate.title;
      this.image = this.data.debate.image;
      this.visibility = this.data.debate.visibility;
      this.interactions = this.data.debate.interactions;
      this.selectedGroup = this.data.debate.discussionGroup;
    }

    if (this.selectedGroup === undefined) {
      this.selectedGroup = null;
    }
    this.groups$ = this.apiService.fetchGroupsByAdmin(this.data.userData);
  }

  ngOnInit(): void {
  }


  close(save:any): void {
    if (save) {
      console.log(this.interactions, this.selectedGroup)
      if (this.title !== '' && (this.interactions === 'open' || (this.interactions === 'close' && this.selectedGroup !== null))) {
        if (this.interactions == 'open') {
          this.selectedGroup = null;
          save.selectedGroup = null;
        }

        save.discussionGroup = this.selectedGroup;
        delete save.selectedGroup;
        this.dialogRef.close(save);
      } else {
        this.alert = true;
      }
    } else {
      this.dialogRef.close(null);
    }
  }

  async uploadCover(event:any) {
    const file = event.target.files[0];
    const filePath = 'debates/covers/' + file.name;

    // @todo replace firebase storage
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    // observe percentage changes
    this.uploadCoverPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(async () => {
          this.uploadCoverPercent = null;
          this.image = await fileRef.getDownloadURL().toPromise();
        })
     )
    .subscribe();
  }

  openDiscussionModal(){
    this.dialogRef.close('discussion');
  }
}
