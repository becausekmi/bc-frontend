import { Component, OnInit, Input, ViewChild, ViewContainerRef, TemplateRef, SimpleChanges } from '@angular/core';
import { Debate } from 'src/app/core/models/debate';
import { Argument } from 'src/app/core/models/argument';
import { Position } from 'src/app/core/models/position';
import { DebateService } from 'src/app/core/services/debate.service';
import { MatDialog } from '@angular/material/dialog';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { AuthService } from 'src/app/auth/services/auth.service';
/* import { AngularFireAnalytics } from '@angular/fire/compat/analytics'; */
import { StatsService } from 'src/app/core/services/stats.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as RecordRTC from 'recordrtc';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Identity } from 'src/app/auth/models/Identity';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';
import { tap, take } from 'rxjs/operators';
import { PositionService } from 'src/app/core/services/position.service';

@Component({
  selector: 'app-debate-container',
  templateUrl: './debate.component.html',
  styleUrls: ['./debate.component.scss']
})
export class DebateContainerComponent implements OnInit {

  @ViewChild('debate-content') debateContent:any;
  @Input() debateKey: string|null = '';
  @Input() authenticated: boolean | null = false;
  @Input() lastPositionLoaded:number = 0;
  @ViewChild('positionsContainer', { read: ViewContainerRef }) container: ViewContainerRef | null = null;
  @ViewChild('position', { read: TemplateRef }) template: TemplateRef<any> | null = null;
  @ViewChild('emptyPosition', { read: TemplateRef }) emptyTemplate: TemplateRef<any> | null = null;

  userData!:any;

  currentMetaTags:any = null;
  recording:number = 0;
  record:any;
  url:any;
  error:any;
  loading:boolean = true;
  nowTS: number = new Date().getTime();
  public lastStartPosition:number = 0;
  public lastEndPosition:number = 0;
  public recSecondsCount:number= 0;
  public secsInterval:any;
  public showTopDots:boolean = true;
  public showBottomDots:boolean = false;
  public loadPositions:boolean = false;

  /* DATA VARIABLES */

  public debateData: Debate | null = null;
  public lastPositionId:string|null = null;
  public claimPlaceholder:string = 'Tell us why you are disagree';
  //public positionsArray: Position[] = [];
  //public argumentsArray: Argument[] = [];
  //public participantsArray: Participant[] = [];

  //public argumentsDict!:any;
  //public participantsDict!:any;
  //public positionsDict!:any;


  /* VIEW VARIABLES */
  public dataSubscription:any;
  public tempArguments:any = [];
  public currentNewPosition = '';
  public currentNewAudio = '';
  public lastPositionScrolled: string | null = null;
  /* public summaryData:any; */


  debate$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  summary$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private apiService:ApiService,
    private storage: AngularFireStorage,
    private domSanitizer: DomSanitizer,
    public dialog: MatDialog,
    public debateService:DebateService,
    public authService: AuthService,
    private statsService:StatsService,
    private positionService:PositionService) { }

  async ngOnChanges(changes: SimpleChanges) {
    if (this.debateKey) {
      this.debate$ = this.apiService.fetchDebate(this.debateKey)
      this.summary$ = this.apiService.fetchSummary(this.debateKey)
    }

    if (this.authenticated) {

      
      this.userData =  await (this.apiService.getIdentity().pipe(take(1)).toPromise());
      if (this.userData) {
        /* this.analytics.logEvent('visitor_user', {user: this.userData?.uid}); */
        this.updateContributionsData();
        if (this.debateService.participantsDict && !this.debateService.participantsDict[this.userData.uid]) {
          console.log("PUSH PARTICIPANT")
          await this.debateService.pushParticipant(this.userData);
        }
      } else {
        console.log("NO USER DATA")
        /* this.analytics.logEvent('visitor_lurker', {user: null}); */
      }
    } else {
      this.userData = null;
    }

    this.debateService.dataUpdatedObservable$.subscribe(time => {
      if (this.userData) {
        this.updateContributionsData();
      }
    })

  }



  updateContributionsData() {
    if (this.userData) {
      for (let position of this.debateService.positionsArray) {

        position.platformData.contributors = [];
        // SET LAST AGREEMENT POSITION VALUE
        if (this.debateService.agreementsByPositionsDict[position.id] && this.debateService.agreementsByPositionsDict[position.id][this.userData.uid]) {
          position.platformData.agreementLastValue = this.debateService.agreementsByPositionsDict[position.id][this.userData.uid].level;
          position.platformData.agreementLastTS = this.debateService.agreementsByPositionsDict[position.id][this.userData.uid].metadata.createdDate;
        }

        // PUSH AGREEMENTS CONTRIBUTORS

        let positionAgreements = this.debateService.agreementsByPositionsArray.filter((p:any)=>{
          if (p.positionId === position.id) {
            return p
          } else {
            return null;
          }
        })

        for (let agreement of positionAgreements) {
          if (position.platformData.contributors.indexOf(agreement.metadata.createdBy) === -1)
            position.platformData.contributors.push(agreement.metadata.createdBy);
        }

        // PUSH REFLECTION CONTRIBUTORS
        let positionReflections = this.debateService.reflectionsByPositionsArray.filter((p:any)=>{
          if (p.positionId === position.id) {
            return p
          } else {
            return null;
          }
        })

        for (let reflection of positionReflections) {
          if (position.platformData.contributors.indexOf(reflection.metadata.createdBy) === -1)
            position.platformData.contributors.push(reflection.metadata.createdBy);
        }

        position.platformData.reflectionsCounter = positionReflections.length;



        // PUSH HEART CONTRIBUTORS

        let positionHearts = this.debateService.heartsByPositionsArray.filter((p:any)=>{
          if (p.positionId === position.id) {
            return p
          } else {
            return null;
          }
        })


        position.platformData.heartsCounter = positionHearts.length;

        for (let argument of position.platformData.claims) {

          // SET LAST AGREEMENT CLAIM VALUE
          if (this.debateService.agreementsByArgumentsDict[argument.id] && this.debateService.agreementsByArgumentsDict[argument.id][this.userData.uid]) {
            argument.platformData.agreementLastValue = this.debateService.agreementsByArgumentsDict[argument.id][this.userData.uid].level;
            argument.platformData.agreementLastTS = this.debateService.agreementsByArgumentsDict[argument.id][this.userData.uid].metadata.createdDate;
          }

          // PUSH CLAIM AUTHOR AS CONTRIBUTOR
          if (position.platformData.contributors.indexOf(argument.metadata.createdBy) === -1)
            position.platformData.contributors.push(argument.metadata.createdBy);


          // PUSH AGREEMENTS CONTRIBUTORS

          let argumentAgreements = this.debateService.agreementsByArgumentsArray.filter((p:any)=>{
            if (p.argumentId === argument.id) {
              return p
            } else {
              return null;
            }
          })

          for (let agreement of argumentAgreements) {
            if (argument.platformData.contributors.indexOf(agreement.metadata.createdBy) === -1)
              argument.platformData.contributors.push(agreement.metadata.createdBy);
          }

          // PUSH REFLECTION CONTRIBUTORS

          let argumentReflections = this.debateService.reflectionsByArgumentsArray.filter((p:any)=>{
            if (p.argumentId === argument.id) {
              return p
            } else {
              return null;
            }
          })

          for (let reflection of argumentReflections) {
            if (argument.platformData.contributors.indexOf(reflection.metadata.createdBy) === -1)
              argument.platformData.contributors.push(reflection.metadata.createdBy);
          }

          argument.platformData.reflectionsCounter = argumentReflections.length;
          // PUSH HEART CONTRIBUTORS

          let argumentHearts = this.debateService.heartsByArgumentsArray.filter((p:any)=>{
            if (p.argumentId === argument.id) {
              return p
            } else {
              return null;
            }
          })

         /*  for (let heart of argumentHearts) {
            if (argument.platformData.contributors.indexOf(heart.metadata.createdBy) === -1)
              argument.platformData.contributors.push(heart.metadata.createdBy);
          } */

          argument.platformData.heartsCounter = argumentHearts.length;
        }
      }
    }
  }

  ngOnInit(): void {
    this.loading = true;

    /* SCROLL VIEW TO LAST POSITION */
    console.log("USER AGREEMENTS POSITION", this.debateService.agreementsByPositionsDict);
    console.log("USER AGREEMENTS ARGUMENTS", this.debateService.agreementsByArgumentsDict);
    this.buildData(this.debateService.positionsArray.length);

    this.positionService.selectedPositionObservable$.subscribe((id:any) => {
      let foundPosition = this.debateService.positionsArray.find((p:Position)=>p.id === id);
      let foundPositionIndex = this.debateService.positionsArray.findIndex((p:Position)=>p.id === id);
      if (foundPosition && foundPositionIndex > -1) {
        this.showNewData(foundPositionIndex,foundPositionIndex+10);
        setTimeout(()=>{
          let element = document.getElementById(id);
          if (element) {
            //element.scrollIntoView({block: "end"});
            element.scrollTop = element.scrollHeight;
          }
        },200)
      }
    })

    this.dataSubscription = this.debateService.areDataReady().subscribe(async data =>{

      if (data && this.userData) {
        this.updateContributionsData();
      }

      this.resetHighlightining(null);
    })

    this.debateService.getPositionPushed().subscribe(data =>{
      console.log(data);
      console.log("NEW POSITIONS DATA");
      if (this.container && this.template) {
        data.platformData.showOnScroll = true;
        this.container.createEmbeddedView(this.template, { position: data, show:true });
        console.log(this.container.length);
      }

    });

    this.debateService.getPositionDeleted().subscribe(data =>{
      console.log(" POSITIONS DELETED DATA AT INDEX ", data);
      if (this.container && this.template) {
        this.container.remove(data);
        console.log(this.container.length);
      }
    });


    const scrolled = 100;
    let eleBar = document.getElementById("scroll-bar");
    if (eleBar) {
      eleBar.style.height =  `${scrolled}%`;
    }

  }

  async checkIfUrl(text:any) {
    console.log(text);

    const regex = "([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?";

    if(new RegExp(regex).test(text) && text.indexOf('http') > -1) {
      let urlFound = text.match(new RegExp(regex))[0];

      let url = btoa(urlFound);
      if (!this.currentMetaTags) {
        let meta:any = await this.debateService.getMetaInformation(url);

        this.currentMetaTags = {
          url: urlFound,
          image: meta['og:image'],
          title: meta['title'],
          description: meta['description']
        }
      }
    } else {
      this.currentMetaTags = null;
    }
  }

  ngOnDestroy() {
    this.dataSubscription.unsubscribe();
    this.container = null;
  }

  moveDebate(event:any) {
    if (event && event.distance) {
      let percentage = (746+event.distance.x)/746*100;
      console.log(percentage)
    }
  }

  scrollDebate(event:any) {
    console.log(event);
  }

  onScroll(event:any) {

    let height = document.getElementById('debate-content')?.scrollHeight;
    let divHeight = document.getElementById('debate-content')?.clientHeight;
    let posTop = document.getElementById('debate-content')?.scrollTop;
    let cTop = document.getElementById('debate-content')?.clientTop;
    let currentPositionTop = posTop;

    /* if (height && divHeight && posTop) {
      const heightScroll = height - divHeight;
      const scrolled = (posTop / heightScroll) * 100 +100;
      let eleBar = document.getElementById("scroll-bar");
      if (eleBar) {
        eleBar.style.width =  `${scrolled}%`;
      }
    } */

    let counter = 1;


    let noPositionFound = true;

    for (let position of this.debateService.positionsArray) {
      position.platformData.treeCollapsed = true;
      if (position.id) {
        let element = document.getElementById(position.id);
        if (element) {
          let isVisible = this.isInViewport(element);
          if (isVisible && typeof counter != 'undefined' && counter !== undefined && position.platformData.showOnScroll) {

            let percentage = (counter/(this.debateService.positionsArray.length+this.debateService.argumentsArray.length)) * 100;
            let eleBar = document.getElementById("scroll-bar");
            if (eleBar) {
              eleBar.style.height =  `${percentage}%`;
            }
            noPositionFound = false;
            break;
          }

        }
        counter=counter+1+position.platformData.claims.length;
      }
    }


    // CHECK IF WHAT I'M SEEING IS ALREADY LOADED AND UPDATE THE CURSOR ON THE DISCUSSION MAP

    /* if (this.lastPositionScrolled) {
      for (let position of this.debateService.positionsArray) {
        position.platformData.treeCollapsed = true;
        if (position.id) {
          let element = document.getElementById(position.id);
          if (element) {
            let isVisible = this.isInViewport(element);

            if (isVisible && position.id !== this.lastPositionScrolled) {
              this.lastPositionScrolled = position.id;
              position.platformData.currentShown = true;
              position.platformData.treeCollapsed = false;
            } else if (!isVisible && position.platformData.currentShown) {
              position.platformData.currentShown = false;
            }
          }
        }

      }
    } */

    // TOP SCROLL REACHED
    //console.log(height, posTop, divHeight, cTop)
    if (height && posTop && divHeight &&  (height+posTop-divHeight) < 300) {
      this.showTopDots = false;
    } else {
      this.showTopDots = true;
    }

    if (height && posTop && divHeight && (height+posTop-divHeight) < 100 && this.lastPositionLoaded > 0) {
      this.loading = true;
      let start = this.lastPositionLoaded-5;
      let end = this.lastPositionLoaded;

      if (end > (this.debateService.positionsArray.length-1)) {
        end = this.debateService.positionsArray.length -1;
      }

      if (start<0) {
        start = 0;
      }

      this.showNewData(start,end);

      let element = document.getElementById('debate-content');
      if (element && currentPositionTop) {
        element.scrollTop = currentPositionTop;
      }
      this.lastStartPosition = start;
      this.lastEndPosition = end;
      this.lastPositionLoaded = start;
    } else {
      // LOAD PREVIOUS 5 POSITIONS
      /* if (this.lastStartPosition > 0) {

        let start = this.lastStartPosition-6;

        if (start <0) {
          start = 0;
        }

        let end = this.lastStartPosition-1;

        this.showNewData(start,end);
      } */

      // LOAD NEXT 5 POSITIONS

      /* if (this.lastEndPosition < this.debateService.positionsArray.length-1) {
        let start = this.lastEndPosition+1;
        let end = this.lastEndPosition+6;

        if (end > this.debateService.positionsArray.length-1) {
          end = this.debateService.positionsArray.length-1;
        }
        this.showNewData(start,end);
      }

      let element = document.getElementById('debate-content');
      if (element && currentPositionTop) {
        element.scrollTop = currentPositionTop;
      } */

    }

    // CHECK IF NEED TO UPLOAD MORE DATA



  }

  isInViewport(element:any) {
      const rect = element.getBoundingClientRect();
      return (
          rect.top >= 0 && rect.top < 300 &&
          rect.left >= 0 &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
          rect.right <= (window.innerWidth || document.documentElement.clientWidth)
      );
  }

  setValue(from:any,to:any) {
    if (from.target) {
      to = from.target.value;
    }
  }

  async pushPosition() {

    if (this.debateKey && this.userData) {

      let newPosition:Position = {
        debateId: this.debateKey,
        id: '',
        metadata: {
          createdBy: this.userData.uid,
          createdDate: new Date().getTime()
        },
        audio:this.currentNewAudio,
        text: this.currentNewPosition,
        type: "POSITION",
        parentId: null,
        attachedUrl:this.currentMetaTags,
        platformData: {
          agreements: [],
          agreementLastValue: 0,
          agreementAverageValue: 0,
          agreementLastTS: new Date().getTime(),
          claims: [],
          contributors:[],
          currentReplyPosition: null,
          opposingClaims: [],
          parentPosition: null,
          showAgreementAlert: false,
          showOnScroll:true,
          state:'Neutral',
          supportingClaims: [],
          treeCollapsed:true,
          dateSince: this.statsService.getTimeSince(new Date().getTime()),
          reflectionsCounter:0,
          heartsCounter:0,
        }
      }
      console.log(newPosition);
     /*  this.analytics.logEvent('push_position', {position: newPosition}); */
      let newId = await this.debateService.pushPosition(newPosition, this.userData);

      if (newId) {
        newPosition.id = newId;
      }

      this.resetHighlightining(null);
      setTimeout(()=>{
        let positionId = this.debateService.positionsArray[this.debateService.positionsArray.length-1].id;
        if (positionId) {
          let element = document.getElementById(positionId);
          if (element) {
            element.scrollIntoView({behavior:'smooth', block:'center'});
          }
        }
      }, 300)

      this.currentNewPosition = '';
      this.currentNewAudio = '';
      this.currentMetaTags = null;
      this.url = null;
      this.recording = 0;

    } else {
      this.dialog.open(SigninOrSignupComponent)
    }

  }

  stopPropagation(event:any) {
    event.stopPropagation();
  }

  /**
  * Start recording.
  */
  initiateRecording() {
    this.recSecondsCount = 0;
    this.recording = 1;
    let mediaConstraints = {
      video: false,
      audio: true
    };
    navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }

    /**
    * Will be called automatically.
    */
  successCallback(stream:any) {
    var options = {
      mimeType: "audio/wav",
      numberOfAudioChannels: 1,
      sampleRate: 48000,
    };
    //Start Actuall Recording
    var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
    this.secsInterval = setInterval(()=>{
      this.recSecondsCount++;
    },1000)
  }

    /**
    * Stop recording.
    */
  stopRecording() {
    this.recording = 2;
    this.record.stop(this.processRecording.bind(this));
    this.currentNewAudio = this.url;
    clearInterval(this.secsInterval);
  }


  deleteRecording() {
    this.recording = 0;
    this.recSecondsCount = 0;
    console.log(this.currentNewAudio);
    this.storage.refFromURL(this.currentNewAudio).delete();
    this.currentNewAudio = '';
    this.url = null;
  }

    /**
    * processRecording Do what ever you want with blob
    * @param  {any} blob Blog
    */
  processRecording(blob:any) {
    this.url = URL.createObjectURL(blob);

    let dateTS = new Date().getTime();

    var storageRef = this.storage.ref('debates/' + this.debateKey+'/'+this.userData?.uid+'/audio_'+dateTS+'.wav');

    var task = storageRef.put(blob);
    let self = this;
    task.then(ok => {
     self.setAudioUrl(storageRef)
    });
  }

  async setAudioUrl(storageRef:any) {
    let self = this;
    self.currentNewAudio = await storageRef.getDownloadURL().toPromise();
  }

    /**
    * Process Error.
    */
  errorCallback(error:any) {
    this.error = 'Can not play audio in your browser';
  }

  sanitize(url: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }

  async setFace(position:Position)  {

  }

  claimInputClick(event:any, position: Position) {
    event.stopPropagation();
  }


  openAuth(event:any) {
    event.stopPropagation();
    this.dialog.open(SigninOrSignupComponent)
  }


  setLastPosition(id:any) {
    this.lastPositionScrolled = id;
    
  }


  resetHighlightining(positionId:string | null) {
  }

  private buildData(length: number) {

    this.lastPositionLoaded = length-1-10;
    this.lastPositionId = null;
    let lastPosition:any = null;
    let show = false;
    setTimeout(()=>{

      for (let i=0; i< length; i++) {
        let position = this.debateService.positionsArray[i]
        if (i> length-1-10) {
          position.platformData.showOnScroll= true;

          if (!this.lastPositionId)
            this.lastPositionId = position.id;
        } else {
          position.platformData.showOnScroll = false;
        }

        const context = {
          position,
          isEven: i % 2 === 0,
          showLoader: false,
          show
        };

        if (this.container && this.template) {
          this.container.createEmbeddedView(this.template, context);
        }
        lastPosition = position;
      }
      this.lastStartPosition = this.lastPositionLoaded;
      this.lastEndPosition =  length-1;
      this.setLastPosition(this.lastPositionId);
      

     
        setTimeout(()=>{
          if (lastPosition && lastPosition.platformData.claims.length) {
            let element = document.getElementById(lastPosition.platformData.claims[0].id+'-anchor');
            if (element) {
              element.scrollIntoView({behavior: "smooth"});
            }
          }
        },300);
    },50)

    /* setTimeout(()=>{
      let element = document.getElementById('discussion-scroll-bar');
      if (element) {
        //element.scrollIntoView({block: "end"});
        element.scrollTop = element.scrollHeight;
      }
    },200) */

    this.loading = false;

  }

  showNewData(from:number,to:number) {
    for (let i=from; i<to;i++) {

      let position = this.debateService.positionsArray[i];
      if (position && !position.platformData.showOnScroll) {
        position.platformData.showOnScroll = true;
        let view = this.container?.get(i);
        if (view) {
          view.detectChanges();
        }
      }
    }
  }


}
