import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { Debate } from 'src/app/core/models/debate';
import { Identity } from 'src/app/auth/models/Identity';
import { NewDebateComponent } from '../../components/create-debate.component';
import { DebateService } from 'src/app/core/services/debate.service';
import { ApiService } from 'src/app/core/services/api.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DiscussionGroupComponent } from '../../components/groups/create-discussion-group.component';

@Component({
  selector: 'app-debate-grid',
  templateUrl: './debate-grid.component.html',
  styleUrls: ['./debate-grid.component.scss']
})
export class DebateGridComponent implements OnInit {

  debates$!: BehaviorSubject<Debate[]>;
  groups$!: BehaviorSubject<Debate[]>;
  term$ = new Subject<any>();
  userData:any;
  identity$: Observable<Identity | null>;
  debatesContributedByUser$!:BehaviorSubject<Debate[]>; 

  constructor(public dialog: MatDialog,
              private apiService: ApiService,
              private debateService: DebateService
  ) {
    
    this.identity$ = this.apiService.getIdentity();
    this.identity$.subscribe(data =>{
      console.log(data);
      this.userData = data;
      if (data) {
        this.debatesContributedByUser$ = this.apiService.fetchDebatesContributedByUser(data);
      }
    })
    
  }

  ngOnInit(): void {

    
    this.debates$ = this.apiService.fetchAllDebates();
    this.groups$ = this.apiService.fetchAllGroups();
    this.term$.pipe(
      debounceTime(800),
      distinctUntilChanged()
    ).subscribe(async (event: any) => {
      if (event.target.value) {
        /* this.debates$ = this.apiService.fetchDebatesBySearchKey(event.target.value); */
      } else {
        this.debates$ = this.apiService.fetchAllDebates();
      }

    });

  }

  


  search() {
    /* this.debates$ = this.apiService.fetchDebatesBySearchKey(''); */
  }

  // @todo remove this code
  // pushNewDebate() {
  //   const dialogRef = this.dialog.open(NewDebateComponent, {
  //     width: '600px'
  //   });

  //   dialogRef.afterClosed().subscribe(async newDebate => {
  //     if (newDebate) {
  //       newDebate.metadata = {
  //         createdBy: this.userData?.uid,
  //         createdDate: new Date().getTime()
  //       }

  //       if (newDebate.image === '')
  //         newDebate.image = 'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/discuss.svg?alt=media&token=b4000573-ff22-45a5-9605-fda14d9a6d67';

  //       await this.debateService.pushDebate(newDebate);
  //       this.debateService.fetchAllDebates();
  //       this.debates$ = this.debateService.debates$;
  //     }
  //   });
  // }

  updateData() {
    this.debates$ = this.apiService.fetchAllDebates();
  }
 
  pushNewDiscussionGroup() {
    const dialogRef = this.dialog.open(DiscussionGroupComponent, {
      width: '800px',
      data:{
        userData: this.userData
      }
    });

    dialogRef.afterClosed().subscribe(async newGroup => {
      
      if (newGroup) {
        // CHECK WITH RICCARDO
        this.identity$.subscribe(async (data: any) => {
          newGroup.metadata = {
            createdBy: data.uid,
            createdDate: new Date().getTime()
          }

          console.log(newGroup);
  
          if (newGroup.image === '')
            newGroup.image = 'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/discuss.svg?alt=media&token=b4000573-ff22-45a5-9605-fda14d9a6d67';
  
          await this.debateService.pushDiscussionGroup(newGroup);
        });

      }
    });
  }

  pushNewDebate() {
    const dialogRef = this.dialog.open(NewDebateComponent, {
      width:'800px',
      data: {
        userData:this.userData 
      }
    });


   /*  dialogRef.afterClosed()
      .pipe(
        mergeMap(formData => {
          console.log(formData);
          return of([])
        })
      );
 */
      dialogRef.afterClosed().subscribe(async newDebate => {
        if (newDebate === 'discussion') {
          this.pushNewDiscussionGroup();
        } else if (newDebate) {
          // CHECK WITH RICCARDO
          console.log(newDebate);
          this.identity$.subscribe(async (data: any) => {
            newDebate.metadata = {
              createdBy: data.uid,
              createdDate: new Date().getTime()
            }

            console.log(newDebate);
    
           if (newDebate.image === '')
              newDebate.image = 'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/discuss.svg?alt=media&token=b4000573-ff22-45a5-9605-fda14d9a6d67';
    
            await this.debateService.pushDebate(newDebate);
            this.debates$ = this.apiService.fetchAllDebates();
          });

        }
      });

    // dialogRef.afterClosed()
    //   .pipe(
    //     tap(formData => console.log('CLOSE DIALG', formData)), // DEBUG
    //     // mergeMap(formData => ),
    //     mergeMap(formData => {
    //       if (formData) {
    //         const newDebate = {
    //           ...formData,
    //           id: '',
    //           metadata: {
    //             createdBy: '', // @todo
    //             createdDate: new Date().getTime(),
    //           }
    //         }
    //         return this.apiService.addDebate(newDebate);
    //       }
    //       return of(null);
    //     })
    //   ).subscribe(() => {
    //     // @todo log?
    //   });

  }



}
