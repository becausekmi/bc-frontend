import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiscussLandingComponent  } from './pages/discuss-landing/discuss-landing.component';
import { DebateDetailComponent  } from './pages/debate-detail/debate-detail.component';
import { AnalyticsUserComponent } from './pages/analytics-user/analytics-user.component';
import { DebateLandingComponent } from './pages/debate-landing/debate-landing.component';
import { GroupLandingComponent } from './pages/group-landing/group-landing.component';

const routes: Routes = [
  { path: 'discuss', redirectTo: '/discuss/landing', pathMatch: 'full' },
  { path: 'discuss/landing', component: DiscussLandingComponent },
  { path: 'discuss/debate/:debateKey/preview', component: DebateLandingComponent },
  { path: 'discuss/debate/:debateKey', component: DebateDetailComponent },
  { path: 'discuss/debate/:debateKey/analytics/:userKey', component: AnalyticsUserComponent },
  { path: 'discuss/group/:groupKey', component: GroupLandingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscussRoutingModule {}
