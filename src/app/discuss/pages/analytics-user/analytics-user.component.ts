import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

/* SERVICES */
import { DebateService } from 'src/app/core/services/debate.service';
import { StatsService } from 'src/app/core/services/stats.service';

/* MODELS */
import { Debate } from 'src/app/core/models/debate';
import { Argument } from 'src/app/core/models/argument';
import { Position } from 'src/app/core/models/position';
import { Participant } from 'src/app/core/models/participant';

/* HIGHCHARTS */
import * as Highcharts from 'highcharts';
import Item from 'highcharts/modules/item-series';
import HighchartsMore from 'highcharts/highcharts-more';
import { LayoutService } from 'src/app/core/services/layout.service';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Identity } from 'src/app/auth/models/Identity';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';

Item(Highcharts);
HighchartsMore(Highcharts)


@Component({
  selector: 'app-analytics-user',
  templateUrl: './analytics-user.component.html',
  styleUrls: ['./analytics-user.component.scss']
})
export class AnalyticsUserComponent implements OnInit {

  @ViewChild('navWidgets', {read: DragScrollComponent}) ds!: DragScrollComponent;
  Highcharts: typeof Highcharts = Highcharts;
  error:string = '';
  loading:boolean = true;
  clickedWidget:string = 'agreements';
  public mode:string = 'all';
  public debateKey!:string|null;
  public userKey!:string|null;
  public participants$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
/* 
  public debateData: Debate | null = null;

  public positionsArray: Position[] = [];
  public argumentsArray: Argument[] = [];
  public participantsArray: Participant[] = [];

  public argumentsDict!:any;
  public participantsDict!:any;
  public positionsDict!:any; */

  /* CHART VARS */

/* 
  chartParliamentOptions: Highcharts.Options = {};
  chartScatterOptions: Highcharts.Options = {};
  chartRadarOptions: Highcharts.Options = {};
  chartRadarOptionsAll: Highcharts.Options = {};

  public selectedIdeas:boolean = true;
  public selectedPros:boolean = true;
  public selectedCons:boolean = true;

  public userPositions: any;
  public userClaims:any;

  radarChartData:any;
  radarChartDataAll:any;
 */
  agreementChart = [
    {
      label: 'strongly disagree',
      image: 'disagree01.svg',
      count: 0,
      min: -100,
      max: -83
    },
    {
      label: 'disagree',
      image: 'disagree01.svg',
      count: 0,
      min: -83,
      max: -49.5
    },
    {
      label: 'somewhat disagree',
      image: 'disagree02.svg',
      count: 0,
      min: -49.5,
      max: -16.5
    },
    {
      label: 'neutral',
      image: 'disagree02.svg',
      count: 0,
      min: -16.5,
      max: 16.5
    },
    {
      label: 'somewhat agree',
      image: 'agree01.svg',
      count: 0,
      min: 16.5,
      max: 49.5
    },
    {
      label: 'agree',
      image: 'agree01.svg',
      count: 0,
      min:49.5,
      max: 83
    },
    {
      label: 'strongly agree',
      image: 'agree02.svg',
      count: 0,
      min:83,
      max: 101
    }
  ]

  averageLevel:number = 0;
  positionTriggered:any = null;
  participantTriggered:any = null;

  profile$: Observable<Identity | null>;
  authenticated$: Observable<boolean>;
  debate$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  arguments$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  positions$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    private layout: LayoutService,
    private route: ActivatedRoute,
    public debateService:DebateService,
    private apiService:ApiService,
    private auth: AuthService,) {

      
    this.authenticated$ = this.auth.authenticated();
    this.profile$ = this.apiService.getIdentity();
    
    
    this.layout.disable();
    this.debateKey = this.route.snapshot.paramMap.get('debateKey');
    this.userKey = this.route.snapshot.paramMap.get('userKey');

    if (this.userKey === 'all') {
      this.userKey = null;
    }
  }

  async ngOnInit() {


    if (this.debateKey) {
      this.debate$ = this.apiService.fetchDebate(this.debateKey)
      this.participants$ = this.apiService.fetchParticipantsByDebate(this.debateKey);
      this.arguments$ = this.apiService.fetchArgumentsByDebate(this.debateKey)
      this.positions$ = this.apiService.fetchPositionsByDebate(this.debateKey)
      this.loading = false;

    } else {
      this.error = "No debate found";
    } 

  }


  ngOnDestroy() {
    this.debateService.unsubscribeDebate();
    this.layout.enable();
  }

}