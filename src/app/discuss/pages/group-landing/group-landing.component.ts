import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { GeneralDialogComponent } from 'src/app/shared/components/general-dialog.component';
import { DiscussionGroupComponent } from '../../components/groups/create-discussion-group.component';
import { DiscussionGroupModalComponent } from '../../components/groups/discussion-group-modal.component';
import { MemberModalComponent } from '../../components/groups/member-modal.component';

@Component({
  selector: 'app-debate-landing',
  template: `
  <div fxLayout="row" fxLayoutAlign="start stretch"  *ngIf="groupData">
      <div fxFlex="100" style="overflow-y:scroll;padding:40px" fxLayout="column" class="scroll-bar">
        <!-- <div fxLayout="row" fxLayoutAlign="space-between center">
            <button class="sea-button-negative" [routerLink]="'/discuss'" fxLayout="row" fxLayoutAlign="center center"><mat-icon>chevron_left</mat-icon> Back</button>
        </div> -->
        <!-- <div fxFlex="30px"></div> -->
        <app-discussion-group-header [userData]="identity$ | async" [groupData]="groupData"> </app-discussion-group-header>
        <div fxFlex="10px"></div>
        <div fxLayout="row">

          <div style="border-radius:4px;height:170px;width:170px;background-size:cover;background-position:center" [style.backgroundImage]="'url('+groupData.image+')'"></div>
          <div fxFlex="20px"></div>
          <div fxLayout="column">
            <div class="title"> {{groupData.title }}</div>
            <div fxFlex="10px"></div>
            <div class="text"> {{ groupData.text }}</div>
          </div>
        </div>
        <div fxFlex="30px"></div>
        <div *ngIf="(debates$ | async).length !== 0 " fxLayout="row" fxLayoutAlign="space-between center" style="border-bottom:1px solid rgb(216, 216, 216);padding-bottom:10px;width:100%;">
            <div style="color:#0073b6;font-size:24px;font-weight: 500">Discussions</div>
            <!-- <mat-icon  *ngIf="(identity$ | async) !== null"
                style="color:#0073b6;cursor:pointer"
                (click)="pushNewDebate()">add_circle</mat-icon> -->
        </div>
        <div fxFlex="20px"></div>
        <div fxLayout="column"   >
            <mat-grid-list [cols]="breakpoint" rowHeight="300px" style="width:100%" gutterSize="40px"   (window:resize)="onResize($event)">

                <mat-grid-tile *ngFor="let debate of (debates$ | async)" >
                    <app-debate-cell (updateData)="emitNewData()" [debatesContributedByUser]="debatesContributedByUser$ | async" [debate]="debate" [userData]="userData" fxFill></app-debate-cell>
                </mat-grid-tile>

            </mat-grid-list>
            <!-- <div *ngIf="!loading && (debates$ | async).length === 0" style="font-size:24px;font-weight:500">
                There are no discussions
            </div> -->
        </div>
        <div fxFlex="40px"></div>
      </div>

  </div>
  `,
  styles: [`
    h1 {
      color:#0073b6 !important;
      font-size:2.5rem;
      font-weight:bold;
    }

    .title {
      font-size:2rem;
      font-weight:bold;
      line-height:1;
    }

    .subtitle {
      font-size:1.5rem;
      font-weight:bold;
    }

    .text {
      font-size:1rem;
      line-height:1.5;
    }

    button {
      height:45px;
    }

    .sea-button-negative {
      width:150px;
    }

    .sea-button {
      width:250px;
    }
  `
  ]
})
export class GroupLandingComponent implements OnInit {

  debates$ = new BehaviorSubject<any>([]);
  discussionGroup: any;
  userData:any;
  groupData:any;
  identity$: Observable<Identity | null>;
  debatesContributedByUser$!:BehaviorSubject<Debate[]>;
  groupKey:any;
  breakpoint:number=4;
  loading:boolean = true;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private auth:AuthService
    ) {

    this.groupKey = this.route.snapshot.paramMap.get('groupKey');
    this.identity$ = this.apiService.getIdentity();
    this.identity$.subscribe(data =>{
      if (data) {
        console.log("IDENTITY CHANGED")
        this.userData = data;
        this.debates$ = this.apiService.fetchDebatesByGroup(this.groupKey)
        this.debatesContributedByUser$ = this.apiService.fetchDebatesContributedByUser(data);
      }
      this.loading = false;
    })

  }

  async ngOnInit() {
    this.userData = await (this.apiService.getIdentity().pipe(take(1)).toPromise());
    if (this.userData) {
      this.debates$ = this.apiService.fetchDebatesByGroup(this.groupKey)
      this.debatesContributedByUser$ = this.apiService.fetchDebatesContributedByUser(this.userData);
    }
    if (this.groupKey) {
        this.groupData = await (this.apiService.fetchGroup(this.groupKey).pipe(take(1)).toPromise());
    }

    this.loading = false;
    this.setBreakpoint(window.innerWidth);
  }

  onResize(event:any) {
    this.setBreakpoint(event.target.innerWidth);
  }

  setBreakpoint(width:number) {
    if (width <=400) {
      this.breakpoint= 1;
    } else if (width > 400 && width <= 800) {
      this.breakpoint= 3;
    } else if (width > 800 && width < 1500) {
      this.breakpoint= 4;
    } else {
      this.breakpoint= 6;
    }
  }

  emitNewData() {

  }


}
