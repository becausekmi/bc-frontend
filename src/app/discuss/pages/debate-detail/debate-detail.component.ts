import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from 'src/app/core/services/layout.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Identity } from 'src/app/auth/models/Identity';
import { ApiService } from 'src/app/core/services/api.service';
import { tap, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DiscussionGroupModalComponent } from '../../components/groups/discussion-group-modal.component';

@Component({
  selector: 'app-debate-detail',
  templateUrl: './debate-detail.component.html',
  styleUrls: ['./debate-detail.component.scss']
})
export class DebateDetailComponent implements OnInit {

  error:string = '';
  loading:boolean = true;
  lightLoading:boolean = false;
  public debateKey!:string|null;
  authenticated$: Observable<boolean>;
  lastPositionLoaded:any = null;
  summary$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  debate$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  discussionGroup: any;
  identity$: Observable<Identity | null>;
  userData:any;
  debateData:any;

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private layout: LayoutService,
    private auth: AuthService,
    private debateService: DebateService,
    private apiService: ApiService,
    private router:Router
  ) {
    
    this.authenticated$ = this.auth.authenticated();
    
    this.identity$ = this.apiService.getIdentity();
    
    this.identity$.subscribe(data =>{
      console.log(data);
      this.userData = data;
    })
  }

  async ngOnInit() {

    this.layout.disable();
    this.debateKey = this.route.snapshot.paramMap.get('debateKey');
    if (this.debateKey) {
      await this.debateService.subscribeDebate(this.debateKey);
      this.debate$ = this.apiService.fetchDebate(this.debateKey)
      this.summary$ = this.apiService.fetchSummary(this.debateKey);
      this.loading = false;
        
      this.debateData = await this.debate$.pipe(take(1)).toPromise();

      if (this.debateData && this.debateData.discussionGroup) {
        this.discussionGroup = await (this.apiService.fetchGroup(this.debateData.discussionGroup).pipe(take(1)).toPromise());
        console.log(this.discussionGroup);

      }
      this.checkDiscussionGroup(this.userData);
    } else {
      this.error = "No debate found";
    }

  }

  setLoader(evt:any) {
    console.log(evt);
    this.lightLoading = evt;
  }

  ngOnDestroy() {
    this.layout.enable();
  }

  checkDiscussionGroup(userData:any) {
    if (this.debateData.interactions === 'close' && (!userData || !this.discussionGroup || (this.discussionGroup && userData && (this.discussionGroup.users.indexOf(userData.email) === -1 && this.discussionGroup.admins.indexOf(userData.email) === -1)))) {

      const dialogRef = this.dialog.open(DiscussionGroupModalComponent, {
        disableClose:true,
        data:{
          userData: userData,
          debateData: this.debateData,
          discussionGroup: this.discussionGroup
        }
      });
  
      dialogRef.afterClosed().subscribe(async newGroup => {
      })
    }
  }

}
