import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { DiscussionGroupComponent } from '../../components/groups/create-discussion-group.component';
import { DiscussionGroupModalComponent } from '../../components/groups/discussion-group-modal.component';

@Component({
  selector: 'app-debate-landing',
  template: `
  <div fxLayout="row" fxLayoutAlign="start stretch" fxFill>
      <div fxFlex="50" style="background-size:cover;" [style.backgroundImage]="'url('+(debate$ | async)?.image+')'"></div>
      <div fxFlex="50" style="padding:60px;" fxLayout="column">
        <button class="sea-button-negative" [routerLink]="'/discuss/landing'" fxLayout="row" fxLayoutAlign="center center"><mat-icon>chevron_left</mat-icon> Back</button>
        <div fxFlex="30px"></div>
        <div class="title"> {{ (debate$ | async)?.title }}</div>
        <div fxFlex="20px"></div>
        <div fxFlex fxLayout="column" style="overflow-y:scroll" class="scroll-bar">
          <div class="text"> {{ (debate$ | async)?.text }}</div>
          <div fxFlex="20px"></div>
          <div class="subtitle"> DYNAMIC SUMMARY</div>
          <div fxFlex="10px"></div>
          <div class="text">{{ (summary$ | async)?.generic.current.Synopsis }}</div>
        </div>
        <div fxFlex="30px"></div>
        <button class="sea-button" *ngIf="(debate$ | async)?.interactions === 'open'" [routerLink]="'/discuss/debate/'+(debate$ | async)?.id" fxLayout="row" fxLayoutAlign="center center">Go to the discussion</button>
        <button class="sea-button" *ngIf="(debate$ | async)?.interactions === 'close'" (click)="openDialog()" fxLayout="row" fxLayoutAlign="center center">Go to the discussion</button>
        <div fxFlex="50px"></div>
      </div>
  </div>
  `,
  styles: [`
    h1 {
      color:#0073b6 !important;
      font-size:2.5rem;
      font-weight:bold;
    }

    .title {
      font-size:36px;
      font-weight:bold;
      line-height:1.4;
    }

    .subtitle {
      font-size:20px;
      font-weight:bold;
    }

    .text {
      font-size:16px;
      line-height:1.5;
    }

    button {
      height:45px;
    }

    .sea-button-negative {
      width:150px;
    }

    .sea-button {
      width:250px;
    }
  `
  ]
})
export class DebateLandingComponent implements OnInit {

  summary$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  debate$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  discussionGroup: any;
  userData:any;
  debateData:any;
  identity$: Observable<Identity | null>;
  debatesContributedByUser$!:BehaviorSubject<Debate[]>;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService) {

    let debateKey = this.route.snapshot.paramMap.get('debateKey');
    if (debateKey) {
      this.debate$ = this.apiService.fetchDebate(debateKey)
      this.summary$ = this.apiService.fetchSummary(debateKey);

    }

    this.identity$ = this.apiService.getIdentity();
    this.identity$.subscribe(data =>{
      this.userData = data;
    })

  }

  async ngOnInit() {

    this.debateData = await this.debate$.pipe(take(1)).toPromise();

    if (this.debateData && this.debateData.discussionGroup) {
      this.discussionGroup = await (this.apiService.fetchGroup(this.debateData.discussionGroup).pipe(take(1)).toPromise());
      console.log(this.discussionGroup);
    }
  }

  openDialog() {
    if (this.discussionGroup && this.userData && (this.discussionGroup.users.indexOf(this.userData.email) > -1 || this.discussionGroup.admins.indexOf(this.userData.email) > -1)) {

      this.router.navigate(['/discuss/debate/'+this.debateData.id]);
    } else {
      console.log(this.userData);
      if (this.userData) {

        const dialogRef = this.dialog.open(DiscussionGroupModalComponent, {
          data:{
            userData: this.userData,
            debateData: this.debateData,
            discussionGroup: this.discussionGroup
          }
        });
  
        dialogRef.afterClosed().subscribe(async newGroup => {
          if (newGroup) {
            this.router.navigate(['/discuss/debate/'+this.debateData.id]);
          }
        })
      } else {
        this.openLoginModal();
        
      }
    }
  }

  openLoginModal() {
    const dialogRef = this.dialog.open(SigninOrSignupComponent);
    dialogRef.afterClosed().subscribe(async newGroup => {
      this.identity$ = this.apiService.getIdentity();
      this.identity$.subscribe(data =>{
        this.userData = data;
        if (this.userData) {
          this.openDialog();
        }
      })
    })
  }

}
