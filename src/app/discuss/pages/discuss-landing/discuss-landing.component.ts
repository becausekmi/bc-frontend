import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-discuss-landing',
  template: `

    
    <div fxLayout="column" fxFill fxLayoutAlign="start center" >
      <app-landing-box
      class="landing-container"
      title="Discuss"
      text=" Engage in a journey of personal and collective inquiry to better inform your decision and actions. <br>
      With <b>BCAUSE</b>, groups can co-create solutions to complex problems by openly discussing them with others.
      Individuals and groups can launch group discussions around specific questions or challenges. The discussions
      are then structured in a way to enable people to contribute ideas, arguments, and appraise evidence in favour
      and against any given idea."
      [image]="{ path: 'assets/discuss_landing.svg', alt: 'Discuss' }"
      [destination]="{ route: '/discuss', label: 'Discuss' }"
      variant="reverse">
      </app-landing-box>
      <app-debate-grid  style="width:100%;"></app-debate-grid>
      <app-group-grid  style="width:100%;"></app-group-grid>
    </div>
  `,
  styles: [`
    h1 {
      color:#0073b6 !important;
      font-size:2.5rem;
      font-weight:bold;
    }
  `
  ]
})
export class DiscussLandingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
