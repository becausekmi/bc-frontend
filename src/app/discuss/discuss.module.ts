import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DebateLeftBoxComponent } from '../shared/components/debate-left-box/debate-left-box.component';
import { DebateRightBoxComponent } from '../shared/components/debate-right-box/debate-right-box.component';
import { SharedModule } from '../shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';
import { DragDropModule} from '@angular/cdk/drag-drop'

// ROUTING
import { MaterialModule } from '../shared/material.module';
import { DiscussRoutingModule } from './discuss-routing.module';

// COMPONENTS
import { LoaderComponent } from '../shared/components/loader/loader.component';
import { PositionReplyComponent } from './components/positions/position-reply.component';
import { ClaimReplyComponent } from './components/claims/claim-reply.component';
import { ReflectionPositionComponent } from './components/positions/reflection-position.component';

// CONTAINERS
import { DebateContainerComponent } from './containers/debate-container/debate.component';

// PAGES
import { DiscussLandingComponent } from './pages/discuss-landing/discuss-landing.component';
import { DebateDetailComponent } from './pages/debate-detail/debate-detail.component';
import { AnalyticsUserComponent } from './pages/analytics-user/analytics-user.component';
import { DebateHeaderComponent } from './components/debate-header.component';
import { NewDebateComponent } from './components/create-debate.component';
import { AgreementSliderComponent } from './components/positions/agreement-slider.component';
import { AddClaimsComponent } from './components/claims/add-claims.component';
import { DebateGridComponent } from './containers/debate-grid/debate-grid.component';
import { ClaimsHeaderComponent } from './components/claims/claims-header.component';
import { AgreementChartComponent } from './components/agreement-chart.component';
import { PositionBoxComponent } from './components/positions/position-box.component';
import { DebateLandingComponent } from './pages/debate-landing/debate-landing.component';
import { ReflectionArgumentComponent } from './components/claims/claim-reflection.component';
import { AudioEqualizerComponent } from './components/audio-equalizer.component';
import { AgreementSliderArgumentComponent } from './components/claims/agreement-slider-claim.component';
import { ContributorsComponent } from './components/contributors.component';
import { UrlPreviewComponent } from './components/url-preview.component';
import { DebateStatsComponent } from './components/debate-stats.component';
import { PositionWidgetComponent } from './components/analytics/positions-widget.component';
import { ArgumentWidgetComponent } from './components/analytics/arguments-widget.component';
import { AgreementWidgetComponent } from './components/analytics/agreements-widget.component';
import { ReflectionWidgetComponent } from './components/analytics/reflections-widget.component';
import { ParticipantWidgetComponent } from './components/analytics/participants-widget.component';
import { AgreementPeopleChartComponent } from './components/analytics/agreements-people-chart.component';
import { AgreementPositionChartComponent } from './components/analytics/agreements-position-chart.component';
import { AgreementParticipantDetailComponent } from './components/analytics/agreements-participant-detail-box.component';
import { AgreementPositionDetailComponent } from './components/analytics/agreements-position-detail-box.component';
import { DiscussionGroupComponent } from './components/groups/create-discussion-group.component';
import { ClaimBoxComponent } from './components/claims/claim-box.component';
import { ClaimHighlightsComponent } from './components/claims/claim.highlights';
import { HeartArgumentComponent } from './components/claims/claim-hearts';
import { ReflectionRadarChartPositionComponent } from './components/positions/reflection-radar-chart.component';
import { PositionHeaderComponent } from './components/positions/position-header.component';
import { PositionContentComponent } from './components/positions/position-content.component';
import { PositionNodeComponent } from '../shared/components/debate-right-box/components/position-node.component';
import { ClaimNodeComponent } from '../shared/components/debate-right-box/components/claim-node.component';
import { DebateCellComponent } from './components/debate-cell.component';
import { DebateGridListComponent } from './components/debate-grid.component';
import { RecommendationSelectorComponent } from './components/claims/recommendation-selection.component';
import { RecommendationItem } from './components/claims/recommendation-item.component';
import { DiscussionGroupModalComponent } from './components/groups/discussion-group-modal.component';
import { GroupsGridListComponent } from './components/groups/groups-grid.component';
import { GroupLandingComponent } from './pages/group-landing/group-landing.component';
import { MemberDetailComponent } from './components/groups/member-detail.component';
import { MemberListComponent } from './components/groups/member-list.component';
import { MemberModalComponent } from './components/groups/member-modal.component';
import { DiscussionGroupHeaderComponent } from './components/groups/discussion-group-header.component';

@NgModule({
  declarations: [
    LoaderComponent,

    DiscussLandingComponent,
    DebateLandingComponent,
    DebateDetailComponent,

    DebateContainerComponent,
    DebateRightBoxComponent,
    DebateLeftBoxComponent,
    DebateHeaderComponent,
    AgreementSliderComponent,
    AddClaimsComponent,

    PositionReplyComponent,
    ClaimReplyComponent,
    NewDebateComponent,
    ReflectionPositionComponent,
    ReflectionArgumentComponent,
    AnalyticsUserComponent,
    DebateGridComponent,
    ClaimsHeaderComponent,
    AgreementChartComponent,
    PositionBoxComponent,
    AudioEqualizerComponent,
    AgreementSliderArgumentComponent,
    UrlPreviewComponent,
    ContributorsComponent,
    DebateStatsComponent,
    PositionWidgetComponent,
    ArgumentWidgetComponent,
    AgreementWidgetComponent,
    ReflectionWidgetComponent,
    ParticipantWidgetComponent,
    AgreementPeopleChartComponent,
    AgreementPositionChartComponent,
    AgreementParticipantDetailComponent,
    AgreementPositionDetailComponent,
    DiscussionGroupComponent,
    ClaimBoxComponent,
    ClaimHighlightsComponent,
    HeartArgumentComponent,
    ReflectionRadarChartPositionComponent,
    PositionHeaderComponent,
    PositionContentComponent,
    PositionNodeComponent,
    ClaimNodeComponent,
    DebateCellComponent,
    DebateGridListComponent,
    RecommendationSelectorComponent,
    RecommendationItem,
    DiscussionGroupModalComponent,
    GroupsGridListComponent,
    GroupLandingComponent,
    MemberDetailComponent,
    MemberListComponent,
    MemberModalComponent,
    DiscussionGroupHeaderComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HighchartsChartModule,
    SharedModule,
    DiscussRoutingModule,
    DragDropModule
  ],
  exports: [
    NewDebateComponent
  ]
})
export class DiscussModule { }
