import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-explore-landing',
  template: `
    <div class="landing-container" fxLayout="column" fxLayoutAlign="start center" >

      <app-landing-box
          title="Reflect"
          text="Think critically in all areas of public deliberation (in the enterprise, your local community, and in which
            organisations you work, learn or advocate).<br>
            BCAUSE provides an advanced Visual Analytics Dashboard build on top of your group discussion data to help
            you reflect on emerging patterns, discussion processes, unexpected results or unwanted social dynamics (such
            as group thinks, unbalanced debates or heated discussion)."
          [image]="{ path: 'assets/reflect.svg', alt: 'Reflect' }"
          [destination]="{ route: '/reflect', label: 'Reflect' }"
          variant="reverse"
      ></app-landing-box>
      <div fxLayout="row" fxLayoutAlign="center center">
        <img src="assets/work-in-progress.png" width="150px"/>
        <div fxFlex="50px"></div>
        <div class="warning">This page is under<br>active development.<br>New features <b>coming soon!</b></div>
      </div>

      <div style="height:50px"></div>
    </div>

    
    <div fxLayout="row" fxLayoutAlign="center center">
        <img src="assets/work-in-progress.png" width="150px"/>
        <div fxFlex="50px"></div>
        <div class="warning">This page is under<br>active development.<br>New features <b>coming soon!</b></div>
      </div>

      <div style="height:50px"></div>
  `,
  styles: [`
    h1 {
      color:#0073b6 !important;
      font-size:2.5rem;
      font-weight:bold;
    }

    .warning {
      margin-top:30px;
      font-family: "Inter";
      line-height:1.2;
      font-size:1.5rem;
    }
  `
  ]
})
export class ReflectLandingComponent implements OnInit {
  debates$: BehaviorSubject<Debate[]>;
  identity$: Observable<Identity | null>;

  constructor(private apiService: ApiService) { 
    this.identity$ = this.apiService.getIdentity();
    this.debates$ = this.apiService.fetchAllDebates();
  }

  ngOnInit(): void {
  }

}

