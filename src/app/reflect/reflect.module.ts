import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';

// ROUTING
import { MaterialModule } from '../shared/material.module';
import { ReflectRoutingModule } from './reflect-routing.module';

// PAGES
import { ReflectLandingComponent } from './pages/reflect-landing/reflect-landing.component';
@NgModule({
  declarations: [
    ReflectLandingComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HighchartsChartModule,
    SharedModule,
    ReflectRoutingModule
  ]
})
export class ReflectModule { }
