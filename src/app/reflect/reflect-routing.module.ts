import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReflectLandingComponent } from './pages/reflect-landing/reflect-landing.component';

const routes: Routes = [
  { path: 'reflect', redirectTo: '/reflect/landing', pathMatch: 'full' },
  { path: 'reflect/landing', component: ReflectLandingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReflectRoutingModule {}
