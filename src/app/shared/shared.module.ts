import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ScrollingModule } from '@angular/cdk/scrolling';

// Components
import { BrandComponent } from "./components/brand.component";
import { LandingBoxComponent } from "./components/lading-box.component";
import { DialogYesNoComponent } from "./components/dialog-yesno.component";
import { SummaryComponent } from "./components/summary/summary.component";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "./material.module";
import { GeneralDialogComponent } from "./components/general-dialog.component";

@NgModule({
  declarations: [
    BrandComponent,
    LandingBoxComponent,
    DialogYesNoComponent,
    SummaryComponent,
    GeneralDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FlexLayoutModule,
    ScrollingModule,
    MaterialModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    ScrollingModule,
    // --- Public omponents --- //
    BrandComponent,
    LandingBoxComponent,
    DialogYesNoComponent,
    SummaryComponent,
    GeneralDialogComponent
  ]
})
export class SharedModule { }
