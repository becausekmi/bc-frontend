import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-landing-box',
  template: `
    <div class="box" fxLayout="row" fxLayoutAlign="start center">
      <div class="content" fxLayout="column">
        <h2 >{{ title }}</h2>
        <div fxFlex="10px"></div>
        <p [innerHTML]="text"></p>
      </div>
      <div fxFlex="100px"></div>
      <img height="330px" [src]="image.path" [alt]="image.alt" />
    </div>
  `,
  styles: [`
      .box {
         padding:60px 0px;
      }

      h2 {
        color: #007FC1;
        font-size: 48px;
        font-weight:bold;
        font-family: 'Inter';
        letter-spacing: -1px;
      }

      p {
        color:#2b2b2b;
        font-size:15px;
        font-family: 'Inter';
        line-height:1.5;
      }

      .front {
        background: #F8F8F8;
      }

      button {
        background:#007FC1;
        color:white;
        border-radius:5px;
        border:none;
        padding:20px 10px;
        width:150px;
      }
  `]
})
export class LandingBoxComponent {
  @Input() title: string = '';
  @Input() text: string = '';
  @Input() image = {
    path: '',
    alt: ''
  };
  @Input() button = {};
  @Input() destination = {
    label:'',
    route:'',
  };
  @Input() variant: string = '';

  constructor() {

      console.log(this.title);
  }

}
