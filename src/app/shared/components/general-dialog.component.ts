import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-yes-no',
  template: `
    <div mat-dialog-content fxLayout="column" style="height:110px">
        <div style="font-size:1.2rem;font-weight:bold" *ngIf="data?.title">
            {{data?.title}}
        </div>

        <div fxFlex="10px"></div>
        <div style="font-size:1rem;" *ngIf="data?.text">
            {{data?.text}}
        </div>
        <div fxFlex="20px" ></div>
        <div fxLayout="row" fxLayoutAlign="space-between center" *ngIf="data?.yesno">
            <button mat-button style="background:white;border:1px black solid;color:black" fxFlex (click)="close(true)">YES</button>
            <div fxFlex="20px"></div>
            <button mat-button style="background:black;border:1px black solid;color:white" fxFlex (click)="close(false)">NO</button>
        </div>
        <div fxLayout="row" fxLayoutAlign="space-between center" *ngIf="!data?.yesno">
            <button mat-button style="background:white;border:1px black solid;color:black" fxFlex (click)="close(false)">CLOSE</button>
        </div>
    
    </div>
  `,
  styles: [`
    
  `]
})
export class GeneralDialogComponent {

  constructor(public dialogRef: MatDialogRef<GeneralDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    console.log(JSON.parse(JSON.stringify(data)))
  }

  ngOnInit(): void {
  }

  
  close(save:boolean): void {
    this.dialogRef.close(save);
  }
}
