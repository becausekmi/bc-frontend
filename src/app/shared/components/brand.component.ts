import { Component } from '@angular/core';

/*
<span>
  <span class="brand">bcause</span>
  <span class="motive">reasoning for change</span>
</span>
 */
@Component({
  selector: 'app-brand',
  template: `
    <a class="appBrand" [routerLink]="['/home']">
      <img src="/assets/logo_white.svg" alt="Bcause - Reasoning for change">
    </a>
  `,
  styles: [`
    .appBrand {
      display: flex;
      align-items: center;
      font-family: "Helvetica Neue";
      text-decoration: none;
      text-transform: uppercase;
      color: white;
      * {
        // border: solid black 1px; // DEBUG
        // display: block;
      }
      & > * { margin-left: 15px; }
      img { height: 48px; }
      .brand {
        font-size: 20px;
        // font-size: 1.7em;
        font-weight: bold;
      }
      .motive {
        font-size: 10px;
        // font-size: 0.9em;
        // font-weight: normal;
      }
    }
  `]
})
export class BrandComponent {}
