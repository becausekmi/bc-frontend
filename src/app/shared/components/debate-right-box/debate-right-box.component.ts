import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewContainerRef, OnChanges } from '@angular/core';
import { Argument } from 'src/app/core/models/argument';
import { Position } from 'src/app/core/models/position';
import { DebateService } from 'src/app/core/services/debate.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { ApiService } from 'src/app/core/services/api.service';
import { PositionService } from 'src/app/core/services/position.service';
import { tap, take } from 'rxjs/operators';

@Component({
  selector: 'app-debate-right-box',
  templateUrl: './debate-right-box.component.html',
  styleUrls: ['./debate-right-box.component.scss']
})
export class DebateRightBoxComponent implements OnChanges {

  @Input() debateKey: string | null = null;
  @Input() summary: any;
  @Input() userData:any;
  @Input() page: string | null = null;
  @Input() authenticated: boolean | null = false;
  @ViewChild('positionsContainer', { read: ViewContainerRef }) container: ViewContainerRef | null = null;
  @Output() loadPositionsEmitter:EventEmitter<any> = new EventEmitter<any>();
  @Output() logoutEmit:EventEmitter<any> = new EventEmitter<any>();
  @Output() showLoaderEmitter:EventEmitter<boolean> = new EventEmitter<boolean>();

  loading:boolean = true;
  now:number =  new Date().getTime();

  positionsSubscription!: Subscription;
  public positionsArray!:Position[];

  claimsSubscription!: Subscription;
  public claimsArray!:Argument[];
  
  positions$!: BehaviorSubject<any[]>;
  arguments$!: BehaviorSubject<any[]>;
  //summary$: BehaviorSubject<any> = new BehaviorSubject<null>(null);

  constructor(
    public dialog: MatDialog,
    public debateService:DebateService,
    public authService: AuthService,
    public apiService: ApiService,
    public positionService: PositionService
    ) {

    
  }

  async ngOnInit() {

    /* this.positionsArray = this.debateService.positionsArray;
    this.claimsArray = this.debateService.argumentsArray;
  */
    this.arguments$ = this.apiService.fetchArgumentsByDebate(this.debateKey)
    this.positions$ = this.apiService.fetchPositionsByDebate(this.debateKey)

    this.positions$.subscribe(data =>{
      this.positionsArray = data;
    })

    this.arguments$.subscribe(data =>{
      this.claimsArray = data;
    });


  }

  async ngOnChanges() {
  }

  logout() {
    this.authService.logout();
    this.logoutEmit.emit(true);
  }



  login() {
    this.dialog.open(SigninOrSignupComponent)
  }

  showLoader(value:boolean) {
    this.showLoaderEmitter.emit(value);
  }
}
