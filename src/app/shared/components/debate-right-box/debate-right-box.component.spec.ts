import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebateRightBoxComponent } from './debate-right-box.component';

describe('DebateRightBoxComponent', () => {
  let component: DebateRightBoxComponent;
  let fixture: ComponentFixture<DebateRightBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebateRightBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebateRightBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
