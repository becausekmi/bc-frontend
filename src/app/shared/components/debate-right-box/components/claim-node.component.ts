import { Component, OnInit, Inject, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AnyNaptrRecord } from 'dns';
import { Argument } from 'src/app/core/models/argument';
import { Position } from 'src/app/core/models/position';
import { DebateService } from 'src/app/core/services/debate.service';
import { PositionService } from 'src/app/core/services/position.service';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';

@Component({
  selector: 'app-claim-node',
  template: `
    <div style="position:relative" [matTooltip]="claim.text" matTooltipPosition="right">
        <div style="position:absolute;left:8px;top:-10px;height:8px;border:1px solid white"></div>

        <div *ngIf="checkIfClaimNotHighlights() && claim.type === 'SUPPORTING'">
            <img src="assets/plus-grey.svg" style="cursor:pointer" *ngIf="!highlighted" (click)="scrollClaimTo($event)"   />
            <img src="assets/plus-active.svg" height="18px" *ngIf="highlighted" style="cursor:pointer" (click)="scrollClaimTo($event)"/>
        </div>
        
        <div *ngIf="checkIfClaimNotHighlights()  && claim.type === 'OPPOSING'">
            <img src="assets/minus-grey.svg" style="cursor:pointer" *ngIf="!highlighted" (click)="scrollClaimTo($event)"   />
            <img src="assets/minus-active.svg" height="18px" *ngIf="highlighted" style="cursor:pointer" (click)="scrollClaimTo($event)"/>
        </div>

        <!-- HIGHLIGHTS -->

        <div style="width:18px;height:24px;overflow:hidden;" *ngIf="!checkIfClaimNotHighlights()" >
            <div class="claim-icon most-opposed-bg" fxLayout="row" fxLayoutAlign="center center"  *ngIf="summary.generic.current.Opposed_position.target_node === claim.id">
                <mat-icon style="font-size:12px;height:12px;width:12px">pan_tool</mat-icon>
            </div>

            <div class="claim-icon most-contested-bg" fxLayout="row" fxLayoutAlign="center center"  *ngIf="summary.generic.current.Contested_position.target_node === claim.id">
                <mat-icon style="font-size:12px;height:12px;width:12px">thumbs_up_downs</mat-icon>
            </div>
            <div class="claim-icon relevant-bg" fxLayout="row" fxLayoutAlign="center center"  *ngIf="summary.generic.current.Needs_attention.target_node === claim.id">
                <mat-icon style="font-size:12px;height:12px;width:12px">visibility</mat-icon>
            </div>
        </div>
    </div>
  `,
  styles: [
    `
        .text {
        margin-top:10px;
        line-height: 1.5;
        }

        .menu {
        border-right:1px solid #005581;
        }

        .lamp-yellow {
            background-image:url('../../../../../assets/lamp-yellow.svg');
            background-size: cover;
            width:20px;height:20px;cursor:pointer;
        }

        .lamp-white {
            background-image:url('../../../../../assets/lamp-white.svg');
            background-size: cover;
            width:20px;height:20px;cursor:pointer;
        }

        .position-icon {
            width:20px;height:20px;cursor:pointer;
        }

        .claim-icon {
            border-radius:20px;
            width:18px;height:18px;cursor:pointer;
        }

        .needs-attention-bg {

         background:#1EC1AB;
        }

        .most-contested-bg {

            background:#DECF00;
        }

        .most-opposed-bg {

            background:#F28222;
        }

    `
  ]
})
export class ClaimNodeComponent implements OnChanges {

    @Input() position!:any;
    @Input() claim!:any;
    @Input() summary!:any;
    @Input() userData!:any;
    @Output() resetEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() clickEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() showClaimEmitter:EventEmitter<any> = new EventEmitter<any>();

    labels:any[] = [];
    highlighted:boolean = false;
    currentShown:boolean = false;

    constructor(
        public dialog: MatDialog,
        public debateService:DebateService,
        public positionService: PositionService) { 

            this.positionService.selectedClaimObservable$.subscribe(id => {
                if(this.claim && id !== '' && id === this.claim.id){
                  this.highlighted = true;
                } else {
                  this.highlighted = false;
                }
            })
        }

    ngOnChanges(): void {

    }
    

        
    checkIfClaimNotHighlights() {
        let notSummary = (!this.summary || !this.summary.generic || !this.summary.generic.current || !this.summary.generic.current.Opposed_position || !this.summary.generic.current.Contested_position || !this.summary.generic.current.Needs_attention);

        if (notSummary) {
            return true;
        } else if(this.summary.generic.current.Opposed_position.target_node !== this.claim.id && this.summary.generic.current.Contested_position.target_node !== this.claim.id && this.summary.generic.current.Needs_attention.target_node !== this.claim.id) {
            return true;
        } else {
            return false;
        }
    }


    scrollClaimTo(event:any) {

        event.stopPropagation();
        this.highlighted =  !this.highlighted;
        if (this.highlighted) {
            /* let element = document.getElementById(this.claim.id);
                if (element) {
                    element.scrollIntoView({block:'center',behavior: "smooth"});
                }
            */
            this.showClaimEmitter.emit(this.claim);
        }

        if (this.highlighted) {
            this.positionService.notifyCollapsedPosition(this.position);
            this.positionService.notifySelectedClaim(this.claim.id);
        } else {
            this.positionService.notifySelectedClaim('');
        }
    }

    
    resetHighlightining() {
        this.highlighted = false;
        this.currentShown = false;
    }
}
