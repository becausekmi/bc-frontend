import { Component, OnInit, Inject, Input, OnChanges, EventEmitter, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AnyNaptrRecord } from 'dns';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Argument } from 'src/app/core/models/argument';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { PositionService } from 'src/app/core/services/position.service';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';

@Component({
  selector: 'app-position-node',
  template: `
        <div style="position:relative;padding-left:20px;padding-top:10px;padding-bottom:10px;cursor: pointer;"  fxLayout="column" [id]="'discussion-map-'+position.id" (click)="scrollPositionTo(position)">
            <!-- POSITION INFO -->
            <div fxLayout="row" fxLayoutAlign="start center" [matTooltip]="position.text" matTooltipPosition="left" >
              <div style="position:absolute;left:5px;top:10px;" *ngIf="currentShown">
              <img src="assets/cursor-white.svg" height="20px" />
            </div>

            <div style="position:absolute;left:20px;top:0px;height:100%;border:1px solid white"></div>

            <div fxFlex="20px" style="border:1px solid white"></div>

            <div class="lamp-white" *ngIf="checkIfNotHighlights(position)"   ></div>

            <!-- HIGHLIGHTS -->

            <div style="width:20px;height:20px;overflow:hidden" *ngIf="!checkIfNotHighlights(position)" >
              <div class="position-icon most-opposed-bg" fxLayout="row" fxLayoutAlign="center center"  *ngIf="summary.generic.current.Opposed_position.target_node === position.id">
                  <mat-icon style="font-size:12px;height:12px;width:12px">pan_tool</mat-icon>
              </div>

              <div class="position-icon most-contested-bg" fxLayout="row" fxLayoutAlign="center center"  *ngIf="summary.generic.current.Contested_position.target_node === position.id">
                  <mat-icon style="font-size:12px;height:12px;width:12px">thumbs_up_downs</mat-icon>
              </div>

              <div class="position-icon needs-attention-bg" fxLayout="row" fxLayoutAlign="center center"  *ngIf="summary.generic.current.Needs_attention.target_node === position.id">
                  <mat-icon style="font-size:12px;height:12px;width:12px">visibility</mat-icon>
              </div>
            </div>

            <!-- MINUS SECTION -->

            <!-- <div fxFlex="5px"></div> -->

            <div fxFlex="82px" style="border:1px solid white" *ngIf="claimsOpposing.length && claimsSupporting.length"></div>
            <div fxFlex="40px" style="border:1px solid white" *ngIf="claimsOpposing.length && !claimsSupporting.length"></div>
            <div fxFlex="82px" style="border:1px solid white" *ngIf="!claimsOpposing.length && claimsSupporting.length"></div>
          
            </div>

            <!-- CLAIMS -->
            <div  fxLayout="row" style="margin-top:-10px" fxLayoutAlign="start start">
              <div fxFlex="70px"></div>
              <div fxLayout="column" fxLayoutAlign="start start">
              <div fxFlex="5px"></div>

              <!-- CONs -->
              <div *ngFor="let claim of claimsOpposing" style="margin-top:5px;position:relative"
              [matTooltip]="claim.text" matTooltipPosition="right">
                <app-claim-node (showClaimEmitter)="scrollClaimTo(claim)"  [position]="position" [claim]="claim" [summary]="summary" [userData]="userData"></app-claim-node>
              
              </div>


              <div *ngIf="!claimsOpposing.length" style="width:18px"></div>
            </div>

            <div fxFlex="25px"  ></div>

            <div fxLayout="column" fxLayoutAlign="start start">
              <div fxFlex="5px"></div>

              <!-- PROs -->
              <div *ngFor="let claim of claimsSupporting" style="margin-top:5px;position:relative"
              [matTooltip]="claim.text" matTooltipPosition="right">
                  <app-claim-node (showClaimEmitter)="scrollClaimTo(claim)"  [position]="position" [claim]="claim" [summary]="summary" [userData]="userData"></app-claim-node>
              </div>
            </div>
            </div>
        </div>
  `,
  styles: [
    `
        .text {
        margin-top:10px;
        line-height: 1.5;
        }

        .menu {
        border-right:1px solid #005581;
        }

        .lamp-yellow {
            background-image:url('../../../../../assets/lamp-yellow.svg');
            background-size: cover;
            width:20px;height:20px;cursor:pointer;
        }

        .lamp-white {
            background-image:url('../../../../../assets/lamp-white.svg');
            background-size: cover;
            width:20px;height:20px;cursor:pointer;
        }

        .position-icon {
            width:20px;height:20px;cursor:pointer;
        }

        .claim-icon {
            border-radius:20px;
            width:18px;height:18px;cursor:pointer;
        }

        .needs-attention-bg {

         background:#1EC1AB;
        }

        .most-contested-bg {

            background:#DECF00;
        }

        .most-opposed-bg {

            background:#F28222;
        }

    `
  ]
})
export class PositionNodeComponent implements OnChanges {

    @ViewChild('positionsContainer', { read: ViewContainerRef }) container: ViewContainerRef | null = null;
    @Input() position!:any;
    @Input() claims!:any;
    @Input() summary!:any;
    @Input() userData!:any;
    @Output() resetEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() clickEmitter:EventEmitter<any> = new EventEmitter<any>();
    @Output() showLoaderEmitter:EventEmitter<boolean> = new EventEmitter<boolean>();

    claimsSupporting:any[]=[];
    claimsOpposing:any[]=[];
    labels:any[] = [];
    highlighted:boolean = false;
    currentShown:boolean = false;
    positionsArray:any[] = [];
    

    constructor(
        public dialog: MatDialog,
        public debateService:DebateService,
        public positionService: PositionService,
        public apiService:ApiService
        ) { }

    ngOnChanges(): void {
      this.positionsArray = this.debateService.positionsArray;
      this.claimsSupporting = this.claims.filter((claim:any) => claim.positionId === this.position.id && claim.type === 'SUPPORTING' );
      this.claimsOpposing = this.claims.filter((claim:any) => claim.positionId === this.position.id &&  claim.type === 'OPPOSING' );
     
    }

    ngOnInit(){
        
      this.positionService.selectedClaimObservable$.subscribe(id => {
        this.resetHighlightining();
        if(id !== ''){
            if (this.position.id === id) {
                this.highlighted = true;
            }
        } 
      })
  
      this.positionService.selectedPositionObservable$.subscribe(id => {
        this.resetHighlightining();
        if(id !== ''){
          if (this.position.id === id) {
            this.highlighted = true;
            this.currentShown = true;
          }
        } 
      })
    }

    /* scrollPositionTo(targetId:string) {
      let position = this.debateService.positionsDict[targetId];
      console.log(position);
      if (position) {
        this.resetHighlightining();
        position.platformData.treeCollapsed = false;
        this.checkIfPositionIsShown(position, null);
        this.positionService.notifySelectedPosition(position.id);
      } else {
        let claim = this.debateService.argumentsDict[targetId];
        if (claim) {
          this.scrollClaimTo(claim)
        }
      }
    } */
    
        
    scrollPositionTo(position:Position) {

      console.log(position);
      this.highlighted = !this.highlighted;
      this.currentShown = !this.currentShown;

      let positionObj = this.debateService.positionsArray.find((p:Position)=>p.id === position.id);
      if (positionObj) {
        positionObj.platformData.treeCollapsed = false;
        this.checkIfPositionIsShown(position,null);
      }

      if (this.highlighted) {
        this.positionService.notifyCollapsedPosition(this.position);
        this.positionService.notifySelectedPosition(this.position.id);
      } else {
        this.positionService.notifySelectedPosition('');
      }
    }

    scrollClaimTo(claim: Argument) {
      this.resetHighlightining();
      let findPosition = this.positionsArray.find(p => p.id === claim.positionId);
      this.checkIfPositionIsShown(findPosition, claim);
      this.positionService.notifySelectedClaim(claim.id);
    }
  
    checkIfPositionIsShown(position:any, claim:any) {
      let firstTimer = 200;
      let secondTimer = 400;
      let positionObj = this.debateService.positionsArray.find((p:Position)=>p.id === position.id);
      if (positionObj && !positionObj.platformData.showOnScroll) {
        let index = this.positionsArray.findIndex(p => p.id === position.id);
        let start = index-5;
        let end = index+5;
        if (end > (this.positionsArray.length-1)) {
          end = this.positionsArray.length -1;
        }
        if (start<0) {
          start = 0;
        }
        this.showNewData(start,end);
        firstTimer = 1000;
        secondTimer = 2000;
      }

      if (firstTimer === 1000)
        this.showLoaderEmitter.emit(true);
  
      setTimeout(()=>{
        if (claim) {
          let element = document.getElementById(claim.id+'-anchor');
          if (element) {
            element.scrollIntoView({behavior: "smooth"});
          }
  
        } else {
          let element = document.getElementById(position.id);
          console.log(element);
          if (element) {
            element.scrollIntoView({behavior: "smooth", block: "start"});
          }
        }
        if (firstTimer === 1000)
          this.showLoaderEmitter.emit(false);
      },firstTimer)
    }

    
    checkIfNotHighlights(position:Position) {
        let notSummary = (!this.summary || !this.summary.generic || !this.summary.generic.current || !this.summary.generic.current.Opposed_position || !this.summary.generic.current.Contested_position || !this.summary.generic.current.Needs_attention);
        if (notSummary) {
        return true;
        } else if(this.summary.generic.current.Opposed_position.target_node !== position.id && this.summary.generic.current.Contested_position.target_node !== position.id && this.summary.generic.current.Needs_attention.target_node !== position.id) {
        return true;
        } else {
        return false;
        }

    }

    showNewData(from:number,to:number) {
      for (let i=from; i<to;i++) {
  
        let position = this.debateService.positionsArray[i];
        position.platformData.showOnScroll = true;
        let view = this.container?.get(i);
        if (view) {
          view.detectChanges();
        }
      }
    }

        
    resetHighlightining() {
      this.highlighted = false;
      this.currentShown = false;
    }

}
