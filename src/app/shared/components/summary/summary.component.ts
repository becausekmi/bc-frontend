import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Argument } from 'src/app/core/models/argument';
import { Position } from 'src/app/core/models/position';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { PositionService } from 'src/app/core/services/position.service';

@Component({
  selector: 'app-summary',
  template: `
   <div class="title" style="padding:20px 20px;padding-bottom:0px;font-weight: bold;font-size:1.1rem">SUMMARY</div>
    <div style="padding:20px">
      <div class="image" [style.backgroundImage]="'url('+(debate$ | async)?.image+')'"  ></div>
    </div>
    <div fxFlex style="overflow-y:scroll;width:100%;" class="scroll-bar" *ngIf="(summary$ | async) as summary" >
      <div fxLayout="column">
        <!-- 1. SYNOPSIS -->
        <div class="p-title">
            <!-- {{ summaryData.generic.current.summary_sections.Description.headline }} -->
            Synopsis
        </div>
        <div class="p-content-synopsis">
          {{ filteredSummary }}
          <div *ngIf="isSummaryShort" (click)="seeAllSummary()">
          <button 
          style="margin:8px 3px; color: #007fc1;cursor: pointer;border: 1px solid #007fc1 !important;padding: 5px 5px !important;height: 30px;border-radius: 5px;"
          >
          See all</button>
          </div>
        </div>

        <!-- 2. NUGGETS -->
        <div class="p-title">
          Sensemaking nuggets
        </div>
        <!-- MOST CONTESTED -->
        <div style="margin-top:5px" class="subtitle-bar most-contested-bg nuggets-title" fxLayout="row" fxLayoutAlign="space-between center" (click)="showMostContested = !showMostContested">
          <div fxLayout="row" fxLayoutAlign="start center">
            <mat-icon style="font-size:16px;height:16px">thumbs_up_down</mat-icon>
            <div fxFlex="5px"></div>
            <div>Most contested position</div>
          </div>
          <mat-icon  *ngIf="showMostContested" >expand_less</mat-icon>
          <mat-icon  *ngIf="!showMostContested" >expand_more</mat-icon>
        </div>
        <div  class="p-content" *ngIf="showMostContested"  (click)="scrollPositionTo( summary.generic.current.Contested_position.target_node)">
          {{ (summary$ | async)?.generic.current.Contested_position.position_text }}
        </div>        
        
        <div class="p-arg-content" *ngIf="summary.generic.current.Contested_position.argument_text && showMostContested">
            <p style="text-decoration: underline;">Pro/Con argument summary:</p>
            <p>{{summary.generic.current.Contested_position.argument_text}}</p>  
        </div>

        <!-- MOST opposed -->
        <div class="subtitle-bar most-opposed-bg nuggets-title" fxLayout="row" fxLayoutAlign="space-between center" (click)="showMostOpposed = !showMostOpposed">
          <div fxLayout="row" fxLayoutAlign="start center">
            <mat-icon style="font-size:16px;height:16px">pan_tool</mat-icon>
            <div fxFlex="5px"></div>
            <div>Most opposed position</div>
          </div>
          <mat-icon *ngIf="showMostOpposed" >expand_less</mat-icon>
          <mat-icon *ngIf="!showMostOpposed" >expand_more</mat-icon>
        </div>
        <div class="p-content" *ngIf="showMostOpposed" (click)="scrollPositionTo(summary.generic.current.Opposed_position.target_node)">
          {{ (summary$ | async)?.generic.current.Opposed_position.position_text }}  
        </div>
        <div class="p-arg-content" *ngIf="summary.generic.current.Opposed_position.argument_text && showMostOpposed">
          <p style="text-decoration: underline;">Pro/Con argument summary:</p>
          <p>{{summary.generic.current.Opposed_position.argument_text}}</p>
        </div>

        <!-- Needs attention -->
        <div class="subtitle-bar relevant-bg nuggets-title" fxLayout="row" fxLayoutAlign="space-between center" (click)="showRelevantPoint = !showRelevantPoint">
          <div fxLayout="row" fxLayoutAlign="start center">
            <mat-icon style="font-size:16px;height:16px">visibility</mat-icon>
            <div fxFlex="5px"></div>
            <div>Needs attention</div>
          </div>
          <mat-icon *ngIf="showRelevantPoint">expand_less</mat-icon>
          <mat-icon *ngIf="!showRelevantPoint" >expand_more</mat-icon>
        </div>
        <div class="p-content" *ngIf="showRelevantPoint" (click)="scrollPositionTo(summary.generic.current.Needs_attention.target_node)">
           {{ (summary$ | async)?.generic.current.Needs_attention.chunk_text }}
        </div>
      </div>

    </div>
  `,
  styles: [`
  
      .text {
        margin-top:10px;
        line-height: 1.5;
      }

      .menu {
        border-right:1px solid #005581;
      }

      .p-title {
        font-weight: 500;
        font-size:1.1rem;
        padding:10px 20px 5px 20px;
      }

      .chunk {
        padding:20px;
        font-size:0.9rem;
      }


      .image {
        background-size:cover;
        box-sizing: border-box;
        height:150px;
        width:100%;
        background-repeat:no-repeat;
        border-radius: 10px;
      }

      .p-content {
        padding:10px 20px;
        &:hover {
          cursor:pointer;
          text-decoration: underline;
        }
      }

      .p-arg-content {
        padding:10px 20px;
      }

      .p-content-synopsis {
        padding:10px 20px;
      }

      .nuggets-title {
        padding:5px 10px;
        color:#012e46;
        font-weight: bold;
        line-height: 1;
        cursor:pointer;
        font-size:12px;
        margin-bottom:2px;
      }`
  ]
})
export class SummaryComponent implements OnChanges {

  @Input() debateKey: string | null = null;
  @Output() loadPositionsEmitter:EventEmitter<any> = new EventEmitter<any>();
  @Output() showLoaderEmitter:EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('positionsContainer', { read: ViewContainerRef }) container: ViewContainerRef | null = null;
  showMostContested:boolean = false;
  showMostOpposed:boolean = false;
  showRelevantPoint:boolean = false;
  
  debate$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  summary$: BehaviorSubject<any> = new BehaviorSubject<null>(null);
  summary_text: string = "";
  filteredSummary: any;
  isSummaryShort = false;
  analytics:AngularFireAnalytics;
  authState:any;

  public positionsArray!:Position[];

  constructor(
    private apiService: ApiService,
    private debateService: DebateService,
    private firebaseAuth: AngularFireAuth,
    private positionService: PositionService,
    analytics: AngularFireAnalytics,) {

      this.analytics = analytics;
      this.firebaseAuth.authState.subscribe(authState => {
          this.authState = authState;          
      });

    }

  ngOnInit() {
    if (this.debateKey) {
      this.debate$ = this.apiService.fetchDebate(this.debateKey)
      this.summary$ = this.apiService.fetchSummary(this.debateKey);
      this.positionsArray = this.debateService.positionsArray;
    }

    this.summary$.subscribe((data)=>{
      
      let current_text = data.generic.current.Synopsis;
      
      this.summary_text = current_text;
      
      this.filteredSummary = this.shortenText(current_text);
    });
  }

  shortenText(text:string) {

    if (text.length > 500) {
      this.isSummaryShort = true;
      text = text.substring(0, 650);
      if (text.split(" ").length > 100) {
        text = text.split(" ").slice(0,99).join(" ")
      }
      text = text + " ...";
    }
    
    return text;
  }

  seeAllSummary() {
    this.filteredSummary = this.summary_text;
    this.isSummaryShort = false;
  }

  ngOnChanges(): void {
    
  }

  scrollPositionTo(targetId:string) {
    let position = this.debateService.positionsDict[targetId];

    if (position) {
      this.resetHighlightining();
      position.platformData.treeCollapsed = false;
      this.checkIfPositionIsShown(position, null);
      this.positionService.notifySelectedPosition(position.id);
    } else {
      let claim = this.debateService.argumentsDict[targetId];
      console.log(claim);
      if (claim) {
        this.scrollClaimTo(claim)
      }
    }
  }

  scrollClaimTo(claim: Argument) {
    this.resetHighlightining();
    let findPosition = this.positionsArray.find(p => p.id === claim.positionId);
    this.checkIfPositionIsShown(findPosition, claim);
    this.positionService.notifySelectedClaim(claim.id);
  }

  checkIfPositionIsShown(position:any, claim:any) {
    
    let firstTimer = 400;
    let secondTimer = 800;
    if (position && position.platformData.showOnScroll) {
      position.platformData.collapsed = false;
    } else if (position && !position.platformData.showOnScroll) {
      let index = this.positionsArray.findIndex(p => p.id === position.id);
      let start = index-5;
      let end = index+5;
      if (end > (this.positionsArray.length-1)) {
        end = this.positionsArray.length -1;
      }
      if (start<0) {
        start = 0;
      }
      this.showNewData(start,end);
      this.loadPositionsEmitter.emit(index);
      firstTimer = 1000;
      secondTimer = 2000;
    }

    if (firstTimer === 1000)
      this.showLoaderEmitter.emit(true);

    setTimeout(()=>{
      if (claim) {
        let element = document.getElementById(claim.id+'-anchor');
        console.log(element);
        if (element) {
          element.scrollIntoView({behavior: "smooth"});
        }

      } else {
        let element = document.getElementById(position.id);
        if (element) {
          element.scrollIntoView({behavior: "smooth", block: "start"});
        }
      }
      setTimeout(()=>{
        if (claim) {
          let element2 = document.getElementById('discussion-map-'+claim.positionId);
            if (element2) {
              element2.scrollIntoView({behavior: "smooth"});
            }
        } else {
          let element2 = document.getElementById('discussion-map-'+position.id);
          if (element2) {
            element2.scrollIntoView({behavior: "smooth"});
          }
        }
        if (firstTimer === 1000)
          this.showLoaderEmitter.emit(false);
      },secondTimer);
    },firstTimer)
  }

  resetHighlightining() {
    this.positionService.notifySelectedPosition("");
    this.positionService.notifySelectedClaim("");
  }

  showNewData(from:number,to:number) {
    for (let i=from; i<to;i++) {

      let position = this.debateService.positionsArray[i];
      position.platformData.showOnScroll = true;
      let view = this.container?.get(i);
      if (view) {
        view.detectChanges();
      }
    }
  }
  logExpandNugget(type:string, showMostContested:boolean) {
    setTimeout(()=>{
      let element = document.getElementById(type+"_content");
      if (element) {
        element.scrollIntoView({behavior: "smooth"});
      }
    },200);
    let event_name = 'sensemaking_nugget_'+type+"_";
    if (showMostContested) {
      event_name += "expanded";
    }else {
      event_name += "closed";
    }
            this.analytics.logEvent(event_name, {
            "user": this.authState.uid
        });

  }
}
