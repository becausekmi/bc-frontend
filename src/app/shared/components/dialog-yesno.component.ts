import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-yes-no',
  template: `
    <div mat-dialog-content fxLayout="column" style="height:110px">
        <div style="font-size:1.2rem;font-weight:bold">
            {{data?.title}}
        </div>

        <div fxFlex="10px"></div>
        <div style="font-size:1rem;">
            {{data?.text}}
        </div>
        <div fxFlex="20px"></div>
        <div fxLayout="row" fxLayoutAlign="space-between center">
            <button mat-button style="background:white;border:1px black solid;color:black" fxFlex (click)="close(true)">YES</button>
            <div fxFlex="20px"></div>
            <button mat-button style="background:black;border:1px black solid;color:white" fxFlex (click)="close(false)">NO</button>
        </div>
    
    </div>
  `,
  styles: [`
    
  `]
})
export class DialogYesNoComponent {

  constructor(public dialogRef: MatDialogRef<DialogYesNoComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    console.log(JSON.parse(JSON.stringify(data)))
  }

  ngOnInit(): void {
  }

  
  close(save:boolean): void {
    this.dialogRef.close(save);
  }
}
