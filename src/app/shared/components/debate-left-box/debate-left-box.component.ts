import { Component, OnInit,Input, ViewChild, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { SigninOrSignupComponent } from 'src/app/auth/containers/signin-or-signup.component';
import { Identity } from 'src/app/auth/models/Identity';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Argument } from 'src/app/core/models/argument';
import { Debate } from 'src/app/core/models/debate';
import { Position } from 'src/app/core/models/position';
import { DebateService } from 'src/app/core/services/debate.service';

@Component({
  selector: 'app-debate-left-box',
  templateUrl: './debate-left-box.component.html',
  styleUrls: ['./debate-left-box.component.scss']
})
export class DebateLeftBoxComponent implements OnInit {

  @Input() debateKey: string | null = null;
  @Input() page: string | null = null;
  @Input() userData: Identity | null = null;
  loading: boolean = true;
  @ViewChild('positionsContainer', { read: ViewContainerRef }) container: ViewContainerRef | null = null;
  @Output() loadPositionsEmitter:EventEmitter<any> = new EventEmitter<any>();
  @Output() showLoaderEmitter:EventEmitter<boolean> = new EventEmitter<boolean>();

  public summaryData: any | null = null;
  public debateData: Debate | null = null;

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public authService: AuthService,
    public debateService: DebateService) {

    this.debateKey = this.route.snapshot.paramMap.get('debateKey');
  }

  ngOnInit(): void {

  }

  updateLastPositionId(index:number) {
    this.loadPositionsEmitter.emit(index);
  }

  showLoader(value:boolean) {
    this.showLoaderEmitter.emit(value);
  }
}
