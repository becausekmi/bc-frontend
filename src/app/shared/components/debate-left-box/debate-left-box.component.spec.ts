import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebateLeftBoxComponent } from './debate-left-box.component';

describe('DebateLeftBoxComponent', () => {
  let component: DebateLeftBoxComponent;
  let fixture: ComponentFixture<DebateLeftBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebateLeftBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebateLeftBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
