import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupComponent } from './pages/signup.component';
import { LoginComponent } from './pages/login.component';
import { PasswordRecoveryComponent } from './pages/password-recovery.component';
import { SandboxComponent } from './pages/sandbox.component';

const routes: Routes = [
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'auth/password-recovery', component: PasswordRecoveryComponent },
  { path: 'auth/sandbox', component: SandboxComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AuthRoutingModule { }
