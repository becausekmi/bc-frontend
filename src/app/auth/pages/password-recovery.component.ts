import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SignupForm } from '../models/SignupForm';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-password-recovery',
  template: `
    <div class="appForgot">
    <div style="width:300px">
      <h2>Recover your password</h2>

      <app-auth-recoveryPasswordForm
        (onSubmit)="handleRecovery($event)"
      ></app-auth-recoveryPasswordForm>

      <div class="formBottom">
        <a mat-button
          [routerLink]="['/auth/login']"
          >Back to Sign in</a>
      </div>

    </div>
  </div>
  `,
  styles: [`
    .appForgot {
      display: flex;
      justify-content: center;

      & > div {
        margin: 20px 20px;
        text-align: center;
      }

      .formBottom {
        padding: 10px 0px;
        a { padding: 0px; }
      }
    }
  `]
})
export class PasswordRecoveryComponent {
  constructor(
    private router: Router,
    private auth: AuthService
  ) { }

  handleRecovery(values: SignupForm) {
    console.log(values);
  }
}
