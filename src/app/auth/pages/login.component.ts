import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';
import { Identity } from '../models/Identity';

import { LoginForm } from '../models/LoginForm';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-auth-login',
  template: `
    <div class="appLogin" *ngIf="!(identity$ | async)">
      <div>
        <h2>Sign In</h2>

        <app-auth-loginForm
          (onSubmit)="handleLogin($event)"
          ></app-auth-loginForm>

        <div
          class="formBottom"
          fxLayout="row"
          fxLayoutGap="20px"
        >
          <!-- <a mat-button
            [routerLink]="['/auth/password-recovery']"
            >Forgot password?</a> -->
          <a mat-button
            [routerLink]="['/auth/signup']"
            >Don't have an account? Sign Up</a>
        </div>

      </div>
    </div>
    <div fxFill fxLayout="row" fxLayoutAlign="center center" *ngIf="(identity$ | async)" style="font-size:20px">
      You're already LOGGED IN
    </div>
  `,
  styles: [`
    .appLogin {
      display: flex;
      justify-content: center;

      & > div {
        margin: 20px 20px;
        text-align: center;
      }

      .formBottom {
        padding: 10px 0px;
        a { padding: 0px; }
      }
    }
  `]
})
export class LoginComponent {

  identity$: Observable<Identity | null>;

  constructor(
    private router: Router,
    private auth: AuthService,
    private apiService: ApiService
  ) { 

    this.identity$ = this.apiService.getIdentity();

  }

  handleLogin(credentials: LoginForm) {
    this.auth.login(credentials).subscribe(
      res => {
        //this.router.navigate(['profile/dashboard']);
      },
      error => {
        // console.log('SignupComponent.error', error);
        // @todo handle server error
        //  - error.message
      }
    );
  }

}
