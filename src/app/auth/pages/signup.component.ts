import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';
import { Identity } from '../models/Identity';

import { SignupForm } from '../models/SignupForm';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-auth-signup',
  template: `
    <div class="appSignup" *ngIf="!(identity$ | async)" >
      <div>
        <h2>Sign Up</h2>

        <app-auth-signupForm
          (onSubmit)="handleSignup($event)"
          ></app-auth-signupForm>

        <div class="formBottom">
          <a mat-button
            [routerLink]="['/auth/login']"
            >Already have an account? Sign in</a>
        </div>

      </div>
    </div>
    <div fxFill fxLayout="row" fxLayoutAlign="center center" *ngIf="(identity$ | async)" style="font-size:20px">
      You're already LOGGED IN
    </div>
  `,
  styles: [`
    .appSignup {
      display: flex;
      justify-content: center;

      & > div {
        margin: 20px 20px;
        text-align: center;
      }

      .formBottom {
        padding: 10px 0px;
        text-align: right;
        a { padding: 0px; }
      }
    }
  `]
})
export class SignupComponent {
  identity$: Observable<Identity | null>;

  constructor(
    private router: Router,
    private auth: AuthService,
    private apiService: ApiService
  ) { 

    this.identity$ = this.apiService.getIdentity();
  }

  handleSignup(values: SignupForm) {
    this.auth.emailSignup(values).subscribe(
      res => {
        //this.router.navigate(['profile/dashboard']);
      },
      error => {
        // console.log('SignupComponent.error', error);
        // @todo handle server error
        //  - error.message
      }
    );
  }

}
