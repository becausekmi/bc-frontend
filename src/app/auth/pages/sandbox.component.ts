import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { SigninOrSignupComponent } from '../containers/signin-or-signup.component';

@Component({
  selector: 'app-auth-sandbox',
  template: `
    <div class="appAuthSandbox">
      <div>
        <h1>Auth - Sandbox</h1>

        <h2>Trigger</h2>
        <button
          mat-raised-button
          color="primary"
          (click)="openDialog()"
          >Open Login Dialog</button>
        <br /><br />

        <!--
        <h2>Preview</h2>
        <app-auth-signinOrSignup></app-auth-signinOrSignup>
         -->

      </div>
    </div>
  `,
  styles: [`
    .appAuthSandbox {
      display: flex;
      justify-content: center;

      & > div {
        margin: 20px 20px;
        text-align: center;
      }

    }
  `]
})
export class SandboxComponent {

  constructor(private dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(SigninOrSignupComponent);
  }

}
