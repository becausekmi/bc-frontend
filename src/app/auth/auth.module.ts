import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFireAuthModule } from "@angular/fire/compat/auth";

import { MaterialModule } from '../shared/material.module';
import { SharedModule } from '../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';

// Components
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { RecoveryPasswordFormComponent } from './components/recovery-password-form/recovery-password-form.component';
import { AvatarComponent } from './components/avatar.component';

// Containers
import { AuthContextComponent } from './containers/auth-context.component';
import { SigninOrSignupComponent } from './containers/signin-or-signup.component';

// Pages
import { SignupComponent } from './pages/signup.component';
import { LoginComponent } from './pages/login.component';
import { PasswordRecoveryComponent } from './pages/password-recovery.component';
import { SandboxComponent } from './pages/sandbox.component';

// Services
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [
    // --- Components --- //
    SignupFormComponent,
    LoginFormComponent,
    RecoveryPasswordFormComponent,
    AvatarComponent, // @todo move in profile module ?

    // --- Containers --- //
    AuthContextComponent,
    SigninOrSignupComponent,

    // --- Pages --- //
    SignupComponent,
    LoginComponent,
    PasswordRecoveryComponent,
    SandboxComponent,
  ],
  imports: [
    CommonModule,
    AngularFireAuthModule, // @see https://codesource.io/firebase-authentication-in-angular-using-angularfire
    MaterialModule,
    SharedModule,
    AuthRoutingModule,
  ],
  exports: [
    // --- Public components --- //
    AuthContextComponent,
  ],
  providers: [ AuthService ],
})
export class AuthModule { }
