export interface Identity {
  // --- firebase props
  uid: string;
  displayName: string;
  email: string;
  emailVerified: boolean;
  isAnonymous: boolean;
  phoneNumber: string | null;
  photoURL: string | null;
  // --- extended props
  firstName: string;
  lastName: string;
  defaultPseudo: string;
  pseudo: string; // @todo ??? ask to Alberto
}
