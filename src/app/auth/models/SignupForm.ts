export interface SignupForm {
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  pseudo:string
}
