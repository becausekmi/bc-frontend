import { Component, Input, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTabGroup } from '@angular/material/tabs';

import { LoginForm } from '../models/LoginForm';
import { SignupForm } from '../models/SignupForm';
import { AuthService } from '../services/auth.service';

// <button mat-icon-button class="close-button" [mat-dialog-close]="true">
//   <mat-icon class="close-icon" color="warn">close</mat-icon>
// </button>

@Component({
  selector: 'app-auth-signinOrSignup',
  template: `

    <h2 mat-dialog-title>Login required</h2>
    <mat-dialog-content>
      <mat-tab-group mat-align-tabs="center" [selectedIndex]="selectedIndex">
        <mat-tab label="Sign in">

          <div class="tabFrame">
            <app-auth-loginForm
              (onSubmit)="handleLogin($event)"
              ></app-auth-loginForm>
              <div *ngIf="loginError" style="margin-top:10px;color:red">{{loginError}}</div>
              <div style="margin-top:10px">New to <b>Bcause?</b> <span (click)="switchToSignup()" style="cursor:pointer;margin-left:5px;text-decoration:underline;color:#0073b6">Create an account.</span></div>
          </div>

        </mat-tab>
        <mat-tab label="Sign up">

          <div class="tabFrame">
            <app-auth-signupForm
              (onSubmit)="handleSignup($event)"
              ></app-auth-signupForm>
              <div *ngIf="signupError" style="margin-top:10px;color:red">{{signupError}}</div>
              <div style="margin-top:10px">Already have an account? <span (click)="switchToSignin()" style="cursor:pointer;margin-left:5px;text-decoration:underline;color:#0073b6">Sign in</span></div>
          
          </div>
        </mat-tab>
      </mat-tab-group>
    </mat-dialog-content>

  `,
  styles: [`
    .tabFrame {
      padding-top: 20px;
      width: 400px;
    }

    
  `]
})
export class SigninOrSignupComponent {

  // @Input() hero: Hero;
  @ViewChild(MatTabGroup) tabGroup!: MatTabGroup;

  loginError: string | null = null;
  signupError: string | null = null;
  selectedIndex:number = 0;

  constructor(
    private auth: AuthService,
    public dialogRef: MatDialogRef<SigninOrSignupComponent>,
  ) { }

  handleSignup(values: SignupForm) {
    this.auth.emailSignup(values).subscribe(
      res => {
        this.dialogRef.close();
      },
      error => {
        this.signupError = error.message;
        // console.log('SignupComponent.error', error);
        // @todo handle server error
        //  - error.message
      }
    );
  }

  handleLogin(credentials: LoginForm) {
    this.auth.login(credentials).subscribe(
      res => {
        this.dialogRef.close();
      },
      error => {
        this.loginError = error.message;
        // console.log('SignupComponent.error', error);
        // @todo handle server error
        //  - error.message
      }
    );
  }

  switchToSignup() {
    this.selectedIndex = 1;
  }

  
  switchToSignin() {
    this.selectedIndex = 0;
  }
}
