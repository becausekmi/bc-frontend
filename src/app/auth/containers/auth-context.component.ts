import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';

import { Identity } from '../models/Identity';
import { AuthService } from '../services/auth.service';
import { SigninOrSignupComponent } from './signin-or-signup.component';

@Component({
  selector: 'app-auth-context',
  // @todo get routes from the constants file
  // @todo trasform the login page in a dialog or floating tooltip
  template: `
    <div class="authContext">
      <span *ngIf="(authenticated$ | async); else loggedOut">
        <!-- @todo create a dropdown menu -->
        <div fxLayout="row" fxLayoutGap="15px">
          <app-auth-avatar [user]="profile$ | async"></app-auth-avatar>
          <button mat-flat-button (click)="logout()">Sign out</button>
        </div>
        <!-- ---------------------------- -->
      </span>

      <ng-template #loggedOut>
        <a mat-flat-button
          (click)="login()"
          >Sign in</a>
      </ng-template>
    </div>
  `,
  styles: [`
    .authContext {
      .mat-flat-button {
        background: white;
        color: rgb(0, 115, 182); // @todo theme
      }
    }
  `]
})
export class AuthContextComponent {

  profile$: Observable<Identity | null>;
  authenticated$: Observable<boolean>;

  constructor(private auth: AuthService, public dialog: MatDialog, public apiService:ApiService) {
    this.authenticated$ = this.auth.authenticated();
    this.profile$ = this.apiService.getIdentity();
  }

  logout() {
    this.auth.logout();
  }

  
  login() {
    this.dialog.open(SigninOrSignupComponent)
  }

}
