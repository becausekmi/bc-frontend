import { Component, Input } from '@angular/core';

import { Identity } from '../models/Identity';

//
// @todo INCOMPLETE
//

@Component({
  selector: 'app-auth-avatar',
  template: `
    <ng-container *ngIf="user" >
      <div class="user" routerLink="/profile/dashboard" style="cursor:pointer" >
        {{ user?.firstName }} {{ user?.lastName }} <br />
        {{ user?.email }}
      </div>
    </ng-container>
  `,
  styles: [`
    .user {
      // border: solid black 1px; // DEBUG
      line-height: 15px;
      font-size: 12px;
    }
  `]
})
export class AvatarComponent {

  @Input()
  user!: Identity | null;

}
