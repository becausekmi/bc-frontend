import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-loginForm',
  templateUrl: './login-form.component.html',
  // styleUrls: ['./login-form.component.scss'],
  styles: [`
    .appAuthLoginForm {
      form { width: 100%; } // IE fix
      mat-form-field { width: 100%; }
      button  { width: 100%;cursor:pointer }
    }
  `],
})
export class LoginFormComponent {

  @Output() onSubmit = new EventEmitter();

  passwordHide = true;
  loginForm: FormGroup = this.fb.group({
    email: ['', [
      Validators.required,
      Validators.email,
    ]],
    password: ['', Validators.required],
  });

  constructor(private fb: FormBuilder) { }

  handleSubmit(form: FormGroup) {
    this.onSubmit.emit(form.value);
  }

}
