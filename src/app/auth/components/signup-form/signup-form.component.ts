import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-signupForm',
  templateUrl: './signup-form.component.html',
  // styleUrls: ['./signup-form.component.scss'],
  styles: [`
    .appAuthSignupForm {
      form { width: 100%; } // IE fix
      mat-form-field { width: 100%; }
      button { width: 100%; color:white;cursor:pointer}
    }
  `],
})
export class SignupFormComponent {

  @Output() onSubmit = new EventEmitter();

  passwordHide: boolean = true;
  signupForm: FormGroup = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    defaultPseudo: [''],
    email: ['', [
      Validators.required,
      Validators.email,
    ]],
    password: ['', Validators.required],
  });

  constructor(private fb: FormBuilder) { }

  handleSubmit(form: FormGroup) {
    if (form.controls.defaultPseudo.value === '') {
      form.controls.defaultPseudo.setValue(form.controls.firstName.value + ' ' + form.controls.lastName.value);
    }
    this.onSubmit.emit(form.value);
  }

}
