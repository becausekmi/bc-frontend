import { Component, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-recoveryPasswordForm',
  templateUrl: './recovery-password-form.component.html',
  styles: [`
    .appAuthSignupForm {
      form { width: 100%; } // IE fix
      mat-form-field { width: 100%; }
      button { width: 100%; color:white}
    }
  `],
})
export class RecoveryPasswordFormComponent {

  @Output() onSubmit = new EventEmitter();
  
  checkEmail: ValidatorFn = (form: AbstractControl):  ValidationErrors | null => { 
   
    let pass = form.get('email')?.value;
    let confirmEmail = form.get('confirmEmail')?.value
    return pass === confirmEmail ? null : { notSame: true }
    
  }

  passwordHide: boolean = true;
  signupForm: FormGroup = this.fb.group({
    email: ['', [
      Validators.required,
      Validators.email,
    ]],
    emailConfirm: ['', [
      Validators.required,
      Validators.email,
    ]],
  }, { validators: this.checkEmail });

  constructor(private fb: FormBuilder) { }

  handleSubmit(form: FormGroup) {
    /* if (form.controls.defaultPseudo.value === '') {
      form.controls.defaultPseudo.setValue(form.controls.firstName.value + ' ' + form.controls.lastName.value);
    }
    this.onSubmit.emit(form.value); */
  }

  

}
