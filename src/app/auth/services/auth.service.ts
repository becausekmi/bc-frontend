import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Observable, from, of } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { get as _get, pick as _pick, omit as _omit, merge as _merge } from 'lodash';

import { SignupForm } from '../models/SignupForm';
import { LoginForm } from '../models/LoginForm';
import { userInfo } from 'os';
import { Identity } from '../models/Identity';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // profile$!: Observable<Profile | null>;

  constructor(
    private db: AngularFireDatabase,
    private auth: AngularFireAuth
  ) { }

  /**
   * @@deprecated
   */
  authenticated(): Observable<boolean> {
    return this.auth.user.pipe(
      map((user: any | null) => user !== null) // @todo fix user type
    );
  }

  // export function createUserWithEmailAndPassword(newUser) {
  //   return auth.createUserWithEmailAndPassword(
  //     newUser.email,
  //     newUser.password
  //   ).then(res => {
  //     const uid = res.user.uid;
  //     const userData = _.omit(newUser, ['email', 'password']);
  //     return db.ref('users').child(uid).set(userData);
  //   });
  // }
  emailSignup(newUser: SignupForm) {
    return from(
      this.auth.createUserWithEmailAndPassword(newUser.email,
                                               newUser.password)
    ).pipe(
      mergeMap((res: any) => {
        const uid = _get(res, 'user.uid', null); // @todo check what happen if uid is null
        const userData = _omit(newUser, [ 'password']);
        return this.db.object(`users/${uid}`).set(userData);
      }),
    );
  }

  // export function signInWithEmailAndPassword({ email, password }) {
  //   return auth
  //     .signInWithEmailAndPassword(email, password)
  //     .then(user => serializeUser(user))
  //   ;
  // }
  login(credentials: LoginForm) {
    return from(
      this.auth.signInWithEmailAndPassword(credentials.email,
                                           credentials.password)
    );
  }

  logout() {
    this.auth.signOut();
  }

  async updateUserData(userData: Identity) {
    console.log(userData);
    console.log('users/'+userData.uid);

    await this.db.object('users/'+userData.uid).update(userData);
  }

}
