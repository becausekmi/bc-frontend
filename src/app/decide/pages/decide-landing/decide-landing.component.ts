import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-explore-landing',
  template: `
    <div class="landing-container" fxLayout="column" fxLayoutAlign="start center">
      <app-landing-box
      title="Decide"
      text="Make decisions that are consulted, reflected upon and critically assessed by all.<br>
      BCAUSE unleashes the power of collective reasoning and sense making to enable groups to make sounder judgments, and take wiser decision and actions. With BCUASE, group can collectively prioritise and choose options based on the assessment of rational arguments and sound evidence, rather than manipulation. Co-constructing these decisions with others is done with a stage-based process of open ideation, structured dialogue and collective deliberation.
      "
      [image]="{ path: 'assets/decide.svg', alt: 'Decide' }"
      [destination]="{ route: '/decide', label: 'Decide' }"
      variant="reverse">
      </app-landing-box>
    </div>

    
    <div fxLayout="row" fxLayoutAlign="center center">
        <img src="assets/work-in-progress.png" width="150px"/>
        <div fxFlex="50px"></div>
        <div class="warning">This page is under<br>active development.<br>New features <b>coming soon!</b></div>
      </div>

      <div style="height:50px"></div>
  `,
  styles: [`
    h1 {
      color:#0073b6 !important;
      font-size:2.5rem;
      font-weight:bold;
    }

    .warning {
      margin-top:30px;
      font-family: "Inter";
      line-height:1.2;
      font-size:1.5rem;
    }
  `
  ]
})
export class DecideLandingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

