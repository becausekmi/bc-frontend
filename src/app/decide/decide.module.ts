import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';

// ROUTING
import { MaterialModule } from '../shared/material.module';
import { DecideRoutingModule } from './decide-routing.module';

// PAGES
import { DecideLandingComponent } from './pages/decide-landing/decide-landing.component';

@NgModule({
  declarations: [
    DecideLandingComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HighchartsChartModule,
    SharedModule,
    DecideRoutingModule,
  ]
})
export class DecideModule { }
