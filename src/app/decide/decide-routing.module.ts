import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DecideLandingComponent } from './pages/decide-landing/decide-landing.component';

const routes: Routes = [
  { path: 'decide', redirectTo: '/decide/landing', pathMatch: 'full' },
  { path: 'decide/landing', component: DecideLandingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DecideRoutingModule {}
