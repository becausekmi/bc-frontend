import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Identity } from 'src/app/auth/models/Identity';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { StatsService } from 'src/app/core/services/stats.service';

@Component({
  selector: 'app-profile-dashboard',
  template: `
    <div fxLayout="column" style="padding:40px 80px;min-height:90%;" >
        <div fxLayout="row" style="width:100%" fxLayoutAlign="start center" *ngIf="(identity$ | async);">
          <mat-icon *ngIf="!(identity$ | async)?.photoURL" style="font-size:60px;height:60px;width:60px">account_circle</mat-icon>
          <div *ngIf="(identity$ | async)?.photoURL" style="background-position:center;background-size:cover;height:60px;width:60px;border-radius:30px" [style.backgroundImage]="'url('+(identity$ | async)?.photoURL+')'"></div>
          <div fxFlex="10px"></div>
          <div fxLayout="column">
            <div style="font-size:20px;font-weight:bold">{{(identity$ | async)?.firstName}} {{(identity$ | async)?.lastName}}</div>
            <div fxFlex="5px"></div>
            <div style="font-size:16px">{{(identity$ | async)?.email}}</div>
          </div>
        </div>
        <div fxFlex="30px"></div>
        <mat-tab-group  *ngIf="(identity$ | async);">
          <mat-tab label="My profile" >
            <app-profile-form [userData]="(identity$ | async)"></app-profile-form>
          </mat-tab>
          <mat-tab label="My contributions">
            <app-contributions-tab [userContributions]="(userContributionsStats$ | async)" [userData]="(identity$ | async)" [debates]="(contributedDebates$ | async)"></app-contributions-tab>
          </mat-tab>
          <mat-tab label="My debates">
            <app-debate-list (onNewDebate)="reloadDebates()" [debates]="(debates$ | async)" [userData]="(identity$ | async)"></app-debate-list>
          </mat-tab>
          <mat-tab label="My discussion groups">
            <app-group-list [groups]="(groups$ | async)" [userData]="(identity$ | async)"></app-group-list>
          </mat-tab>
        </mat-tab-group>
    </div>
  `,
  styles: [`
    .profile-image {
      width:100px;
      height:100px;
      border-radius:50px;
      border:1px solid black;
    }
  `]
})
export class DashboardComponent {

  userContributionsStats$ = new BehaviorSubject<any>(null);
  identity$: Observable<Identity | null>;
  debates$ = new BehaviorSubject<any>(null);
  groups$ = new BehaviorSubject<any>(null);
  contributedDebates$ = new BehaviorSubject<any>(null);
  identityData!:Identity;

  constructor(
    private apiService:ApiService
  ) {
      
    this.identity$ = this.apiService.getIdentity();
    // CHECK WITH RICCARDO
    this.identity$.subscribe(async (data: any) => {
      console.log(data);
      if (data) {
        this.identityData = data;
        this.debates$ = this.apiService.fetchDebatesByUser(data);
        this.groups$ = this.apiService.fetchGroupsByUser(data);
        this.contributedDebates$ = this.apiService.fetchDebatesContributedByUser(data);
        this.userContributionsStats$ = this.apiService.fetchParticipantContributionsStatsAllDebates(data);
      } else {
        
        this.identityData = data;
      }
    })

      
  }

  async reloadDebates() {
    
  }

  ngOnDestroy() {
  }

}