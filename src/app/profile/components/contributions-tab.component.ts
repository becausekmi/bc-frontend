import { Component, OnInit, Inject, Input, OnChanges } from '@angular/core';
import { Identity } from 'src/app/auth/models/Identity';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Debate } from 'src/app/core/models/debate';
import { StatsService } from 'src/app/core/services/stats.service';

@Component({
  selector: 'app-contributions-tab',
  template: `
    <div fxLayout="column" style="padding:40px">
        <div style="font-size:18px;font-weight:bold">My contributions</div>
        <div fxFlex="30px"></div>
        <div fxLayout="row" fxLayoutAlign="space-between start">
            <div fxLayout="column">
                <mat-form-field appearance="fill">
                    <mat-label>Filter data for</mat-label>
                    <mat-select>
                        <mat-option value="all">All Debates</mat-option>
                        <mat-option [value]="debate.id" *ngFor="let debate of debates">{{debate.title}}</mat-option>
                    </mat-select>
                </mat-form-field>
            </div>
            <div fxLayout="column" *ngIf="userContributions">
                <div>My contributions on all debates</div>
                <div fxFlex="20px"></div>
                <div fxLayout="row" >
                    <div class="box" fxLayout="column" fxLayoutAlign="center center">
                        <div class="box-title" *ngIf="userContributions.positions">{{ userContributions.positions }}</div>
                        <div class="box-title" *ngIf="!userContributions.positions">0</div>
                        <div class="box-text">Positions</div>
                    </div>
                    <div fxFlex="20px"></div>
                    <div class="box" fxLayout="column" fxLayoutAlign="center center">
                        <div class="box-title" *ngIf="userContributions.pros">{{ userContributions.pros }}</div>
                        <div class="box-title" *ngIf="!userContributions.pros">0</div>
                        <div class="box-text">PRO</div>
                    </div>
                    <div fxFlex="20px"></div>
                    <div class="box" fxLayout="column" fxLayoutAlign="center center">
                        <div class="box-title" *ngIf="userContributions.cons">{{ userContributions.cons }}</div>
                        <div class="box-title" *ngIf="!userContributions.cons">0</div>
                        <div class="box-text">CON</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  `,
  styles:  [`
    .box {
        width:130px;
        height:130px;
        border:1px solid #CBCBCB;
        border-radius:3px;
    }

    .box-title {
        font-size:50px;
        font-weight:bold;
        line-height:1;
    }

    .box-text {
        font-size:12px;
    }
  `]
})
export class ContributionsTabComponent implements OnChanges {

  @Input() debates: Debate[] | null = null;
  @Input() userData: Identity | null = null;
  @Input() userContributions!: any;

  constructor(public statsService:StatsService, public authService:AuthService) { 
      
  }

  ngOnInit(): void {

  }

  ngOnChanges(){
      console.log(this.debates)
      console.log(this.userContributions);
  }

}