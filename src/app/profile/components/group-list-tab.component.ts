import { Component, OnInit, Inject, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Identity } from 'src/app/auth/models/Identity';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Debate } from 'src/app/core/models/debate';
import { StatsService } from 'src/app/core/services/stats.service';

@Component({
  selector: 'app-group-list',
  template: `
    <div fxLayout="column" style="padding:40px">
        <div style="font-size:18px;font-weight:bold">My discussion groups</div>
        <div fxFlex="30px"></div>
        <div fxLayout="row" fxLayoutAlign="space-between start">
          <div *ngIf="!groups || groups.length === 0">
              You have no discussions groups yet! Why don't you try to create one?
          </div>
          <app-group-row (updateGroup)="updateGroup()" [group]="group" *ngFor="let group of groups" style="width:100%"></app-group-row>
           
        </div>
    </div>
  `,
  styles:  [`
  `]
})
export class GroupListTabComponent implements OnChanges {

  @Input() groups: any | null = null;
  @Input() userData: Identity | null = null;
  @Output() emitUpdateGroup = new EventEmitter();

  constructor(public statsService:StatsService, public authService:AuthService) { 
      
  }

  ngOnInit(): void {

  }

  ngOnChanges(){
      console.log(this.groups)
  }

  updateGroup() {
    this.emitUpdateGroup.emit();
  }

}