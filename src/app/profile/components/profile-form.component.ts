import { Component, OnInit, Inject, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Identity } from 'src/app/auth/models/Identity';

@Component({
  selector: 'app-profile-form',
  template: `
    <form [formGroup]="profileForm" fxLayout="row" fxLayoutAlign="center center" *ngIf="profileForm" style="padding:40px">

        <div fxLayout="column" >

            <div fxLayout="column" fxLayoutAlign="start center">
                <mat-icon *ngIf="!userData?.photoURL" style="font-size:60px;height:60px;width:60px">account_circle</mat-icon>
                <div *ngIf="userData?.photoURL" style="background-position:center;background-size:cover;height:60px;width:60px;border-radius:30px" [style.backgroundImage]="'url('+userData?.photoURL+')'"></div>
                <div fxLayout="column" fxLayoutAlign="center center" class="width-100">
                    <div fxFlex="10px"></div>
                        <label *ngIf="!uploadCoverPercent" [for]="'file-upload-cover'" style="font-size:12px" fxLayout="row" fxLayoutAlign="center center">
                               <b> Change Profile Image</b>
                        </label>
                        <div *ngIf="uploadCoverPercent" class="label-button margin-t-10 width-100"  fxLayout="row" fxLayoutAlign="center center">
                            {{ uploadCoverPercent| async  | number:'1.2-2'}} %
                        </div>
                        <input [id]="'file-upload-cover'" accept=".jpg,.jpeg,.png" type="file" style="display:none" (change)="uploadCover($event)" />
                </div>
            </div>
            <div fxFlex="20px"></div>
            <div fxLayout="row">
                <div fxLayout="column">
                  <mat-form-field appearance="outline">
                      <mat-label>Your name</mat-label>
                      <input matInput
                      formControlName="firstName"
                      placeholder="Name"
                      required
                      />
                  </mat-form-field>


                  <mat-form-field appearance="outline">
                      <mat-label>Your surname</mat-label>
                      <input matInput
                      formControlName="lastName"
                      placeholder="Surname"
                      required
                      />
                  </mat-form-field>
                </div>
                <div fxFlex="40px"></div>
                <div fxLayout="column">

                  <mat-form-field appearance="outline">
                    <mat-label>Email</mat-label>
                    <input matInput
                    formControlName="email"
                    placeholder="Surname"
                    required
                      />
                  </mat-form-field>

                  <mat-form-field appearance="outline">
                      <mat-label>Pseudo</mat-label>
                      <input matInput
                      formControlName="pseudo"
                      placeholder="Pseudo"
                      required
                      />
                  </mat-form-field>
                </div>
            </div>

        </div>
    </form>

    <div fxLayout="row" fxLayoutAlign="center center" style="width:100%">
      <button mat-raised-button
        fxFlex="150px"
        type="submit"
        color="primary"
        (click)="saveData()"
      >Save</button>
    </div>
  `,
  styles:  ['']
})
export class ProfileFormComponent implements OnChanges {
    @Input() userData: Identity | null = null;

    profileForm!: FormGroup;
    uploadCoverPercent:any;

  constructor(private storage: AngularFireStorage,private fb: FormBuilder, private auth:AuthService) {


  }

  ngOnChanges(): void {
      console.log(this.userData);
      this.profileForm = this.fb.group({
        email: [this.userData?.email, [
          Validators.required,
          Validators.email,
        ]],
        firstName: [this.userData?.firstName, [
            Validators.required,
          ]],
        lastName: [this.userData?.lastName, [
            Validators.required,
          ]],
        pseudo: [this.userData?.defaultPseudo, [
            Validators.required,
        ]],
    });
  }

  async uploadCover(event:any) {
    const file = event.target.files[0];
    const filePath = 'users/' + this.userData?.uid;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    // observe percentage changes
    this.uploadCoverPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(async () => {
          if (this.userData) {
            this.uploadCoverPercent = null;
            this.userData.photoURL = await fileRef.getDownloadURL().toPromise();
            await this.saveData();
          }
        })
     )
    .subscribe();
  }

  async saveData() {
    if (this.userData) {
      this.userData.firstName = this.profileForm.get(['firstName'])?.value;
      this.userData.lastName = this.profileForm.get(['lastName'])?.value;
      this.userData.email = this.profileForm.get(['email'])?.value;
      this.userData.defaultPseudo = this.profileForm.get(['pseudo'])?.value;
      await this.auth.updateUserData(this.userData); // @todo fix it
    }
  }
}