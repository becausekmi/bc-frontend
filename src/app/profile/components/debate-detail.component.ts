import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable } from 'rxjs';
import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { NewDebateComponent } from 'src/app/discuss/components/create-debate.component';
import { DialogYesNoComponent } from 'src/app/shared/components/dialog-yesno.component';

@Component({
  selector: 'app-debate-detail',
  template: `
    <div style="cursor:pointer;width:100%;margin-bottom:20px" fxLayout="row stretch" fxLayoutAlign="start start" >
        <div style="width:100px;height:80px;background-size:cover;background-position:center;border:1px solid black; border-radius:5px" [style.backgroundImage]="'url('+debate.image+')'" [routerLink]="'/discuss/debate/'+debate.id">
        </div>
        <div fxFlex="20px"></div>
        <div fxLayout="column" fxLayoutAlign="space-between start"  style="height:100%" fxFlex>

            <div style="font-size:18px"><b>{{debate.title}}</b></div>
            <div fxFlex="5px"></div>
            <div style="color:#868686;font-size:15px">Created on {{ debate.metadata.createdDate| date: 'dd/MM/yyyy' }}</div>

            <div fxFlex="10px"></div>
            <div fxLayout="row" fxLayoutAlign="start center">
                <div>Positions ({{(positions$ | async)?.length}})</div>
                <div fxFlex="50px"></div>
                <div>Arguments ({{(arguments$ | async)?.length}})</div>
            </div>
        </div>

        <div *ngIf="(identity$ | async) && debate.metadata.createdBy === (identity$ | async)?.uid" fxLayout="row">
            <div class="update-button"  >
                <mat-icon (click)="updateDebate(debate)">edit</mat-icon>
            </div>
            <div fxFlex="5px"></div>
            <!-- DELETE BUTTON -->
            <div class="delete-button" *ngIf="!(positions$ | async)?.length && !(arguments$ | async)?.length && !(agreements$ | async)?.length" >
                <mat-icon (click)="deleteDebate(debate)">delete_outline</mat-icon>
            </div>
            
        </div>
    </div>
        
    <div class="line"></div>
  `,
  styles: [`

        .delete-button {
            flex-direction: row;
            cursor:pointer
        }

        .update-button {
            flex-direction: row;
            cursor:pointer
        }
  `]
})
export class DebateDetailComponent {
  @Input() debate!: Debate;
  @Output() updateDebates = new EventEmitter();
  
  positions$!: BehaviorSubject<any[]>;
  arguments$!: BehaviorSubject<any[]>;
  agreements$!:BehaviorSubject<any[]>;
  identity$!: Observable<Identity | null>;
  userData!:any;

  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private debateService: DebateService
    ) {
  }

  ngOnChanges() {

    this.identity$ = this.apiService.getIdentity();
    this.positions$ = this.apiService.fetchPositionsByDebate(this.debate.id);
    this.arguments$ = this.apiService.fetchArgumentsByDebate(this.debate.id);
    this.agreements$ = this.apiService.fetchAgreementsByDebate(this.debate.id);

    this.identity$.subscribe(data =>{
      this.userData = data;
    })
  }

  
  deleteDebate(debate:any) {

    const dialogRef = this.dialog.open(DialogYesNoComponent, {
      width: '600px',
      data: {
        title:'DELETE DISCUSSION',
        text:'Are you sure you want to delete this discussion?'
      }
    });

    dialogRef.afterClosed().subscribe(async yes => {
      if (yes) {

      }
    })
  }

  updateDebate(debate:any) {
    const dialogRef = this.dialog.open(NewDebateComponent, {
        width:'800px',
        data: {
            debate,
            userData: this.userData
        }
    });

    dialogRef.afterClosed().subscribe(async save => {
       
        if (save) {
          this.debate.text = save.text;
          this.debate.title = save.title;
          this.debate.image = save.image;
          this.debate.interactions = save.interactions;
          this.debate.visibility = save.visibility;
          this.debate.discussionGroup = save.selectedGroup;
          await this.debateService.updateDebate(this.debate);
          this.updateDebates.emit(true);
        }
      });
  }

}
