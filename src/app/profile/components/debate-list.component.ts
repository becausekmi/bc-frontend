import { Component, OnInit, Inject, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from 'src/app/core/models/debate';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { NewDebateComponent } from 'src/app/discuss/components/create-debate.component';

@Component({
  selector: 'app-debate-list',
  template: `
  <div fxLayout="column" style="padding:40px 0px">
      <div style="font-size:18px;font-weight:bold">My discussion</div>
      <div class="line"></div>
      <div *ngIf="!debates || debates.length === 0">
          You have no discussions yet! Why don't you try to create one?
      </div>
      <app-debate-detail [debate]="debate" *ngFor="let debate of debates" style="width:100%"></app-debate-detail>

      <div fxFlex="20px"></div>
      <div fxLayout="row" fxLayoutAlign="start center" style="width:100%">
        <button mat-raised-button
          fxFlex="150px"
          type="submit"
          color="primary"
          (click)="createDebate()"
        >Create</button>
      </div>
  </div>
  `,
  styles:  [`
      .line {
        border:1px solid #e0e0e0;
        width:100%;
        margin-top:20px;
        margin-bottom:20px;
      }
  `]
})
export class DebateListTabComponent implements OnChanges {

  @Input() debates: Debate[] | null = null;
  @Input() userData: Identity | null = null;
  @Output() onNewDebate = new EventEmitter();

  constructor(public dialog: MatDialog,private apiService: ApiService) {
  }

  ngOnChanges(): void {
    console.log(this.debates);
  }

  createDebate() {
      const dialogRef = this.dialog.open(NewDebateComponent, {
        
        width:'800px',
        data: {
          userData: this.userData
        }
      });

      let subscription = dialogRef.afterClosed().subscribe(async newDebate => {
        console.log(newDebate, this.userData)
        subscription.unsubscribe();
        if (newDebate && this.userData) {
          newDebate.metadata = {
            createdBy: this.userData?.uid,
            createdDate: new Date().getTime()
          }

          if (newDebate.image === '')
            newDebate.image = 'https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/discuss.svg?alt=media&token=b4000573-ff22-45a5-9605-fda14d9a6d67';

          await this.apiService.addDebate(newDebate);
          this.newDebateEvent();
        }
      });
  }


  newDebateEvent() {
    this.onNewDebate.emit(true);
  }

}
