import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable } from 'rxjs';
import { Identity } from 'src/app/auth/models/Identity';
import { ApiService } from 'src/app/core/services/api.service';
import { DebateService } from 'src/app/core/services/debate.service';
import { DiscussionGroupComponent } from 'src/app/discuss/components/groups/create-discussion-group.component';

@Component({
  selector: 'app-group-row',
  template: `
    <div style="cursor:pointer;width:100%;margin-bottom:20px" fxLayout="row stretch" fxLayoutAlign="start start" >
        <div style="width:100px;height:80px;background-size:cover;background-position:center;border:1px solid black; border-radius:5px" [style.backgroundImage]="'url('+group.image+')'" [routerLink]="'/profile/group/'+group.id">
        </div>
        <div fxFlex="20px"></div>
        <div fxLayout="column" fxLayoutAlign="space-between start"  style="height:100%" fxFlex>

            <div style="font-size:18px"><b>{{group.title}}</b></div>
            <div fxFlex="5px"></div>
            <div style="color:#868686;font-size:15px">Created on {{ group.metadata.createdDate| date: 'dd/MM/yyyy' }}</div>

            <div fxFlex="10px"></div>
            <div fxLayout="row" fxLayoutAlign="start center">
                <div>{{group.admins.length}} Admins</div>
                <div fxFlex="50px"></div>
                <div>{{group.users.length - group.admins.length}} Users</div>
            </div>
        </div>

        <div *ngIf="(identity$ | async) && group.admins.indexOf((identity$ | async)?.email) > -1" fxLayout="row">
            <div class="update-button"  >
                <mat-icon (click)="updateDiscussionGroup(group)">edit</mat-icon>
            </div>
            <div fxFlex="30px"></div>
            <!-- DELETE BUTTON -->
           <!--  <div class="delete-button" *ngIf="!(positions$ | async)?.length && !(arguments$ | async)?.length && !(agreements$ | async)?.length" >
                <mat-icon (click)="deleteDebate(debate)">delete_outline</mat-icon>
            </div> -->
            
        </div>
    </div>
        
    <div class="line"></div>
  `,
  styles: [`

        .delete-button {
            flex-direction: row;
            cursor:pointer
        }

        .update-button {
            flex-direction: row;
            cursor:pointer
        }
  `]
})
export class GroupRowComponent {
  @Input() group!: any;
  @Output() updateGroup = new EventEmitter();
  
  agreements$!:BehaviorSubject<any[]>;
  identity$!: Observable<Identity | null>;

  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private debateService: DebateService
    ) {
  }

  ngOnChanges() {
    this.identity$ = this.apiService.getIdentity();
  }


  updateDiscussionGroup(group:any) {
    const dialogRef = this.dialog.open(DiscussionGroupComponent, {
        width: '800px',
        data: {
            group
        }
    });

    dialogRef.afterClosed().subscribe(async save => {
       
        if (save) {
            this.group.text = save.text;
            this.group.title = save.title;
            this.group.image = save.image;
            this.group.users = save.users;
            this.group.open = save.open;
            this.group.admins = save.admins;
            await this.apiService.updateDiscussionGroup(this.group);
            this.updateGroup.emit(true);
        }
      });
  }

}
