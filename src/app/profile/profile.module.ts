import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Shared modules
import { MaterialModule } from '../shared/material.module';
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';

// Routing module
import { ProfileRoutingModule } from './profile-routing,module';

// Pages
import { DashboardComponent } from './pages/dashboard.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ProfileFormComponent } from './components/profile-form.component';
import { DebateListTabComponent } from './components/debate-list.component';
import { ContributionsTabComponent } from './components/contributions-tab.component';
import { DebateDetailComponent } from './components/debate-detail.component';
import { GroupListTabComponent } from './components/group-list-tab.component';
import { GroupRowComponent } from './components/group-row.component';

@NgModule({
  declarations: [
    // --- Components --- //
    ProfileFormComponent,
    DebateListTabComponent,
    ContributionsTabComponent,
    DebateDetailComponent,
    GroupListTabComponent,
    GroupRowComponent,
    

    // --- Containers --- //

    // --- Pages --- //
    DashboardComponent,

  ],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    AuthModule,
    ProfileRoutingModule,
  ],
  exports: [],
  providers: []
})
export class ProfileModule { }
