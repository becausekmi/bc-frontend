import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';

// ROUTING
import { MaterialModule } from '../shared/material.module';
import { ExploreRoutingModule } from './explore-routing.module';

// PAGES
import { ExploreLandingComponent } from './pages/explore-landing/explore-landing.component';

@NgModule({
  declarations: [
    ExploreLandingComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HighchartsChartModule,
    SharedModule,
    ExploreRoutingModule,
  ]
})
export class ExploreModule { }
