import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExploreLandingComponent } from './pages/explore-landing/explore-landing.component';

const routes: Routes = [
  { path: 'explore', redirectTo: '/explore/landing', pathMatch: 'full' },
  { path: 'explore/landing', component: ExploreLandingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExploreRoutingModule {}
