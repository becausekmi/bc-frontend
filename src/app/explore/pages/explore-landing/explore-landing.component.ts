import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-explore-landing',
  template: `
    <div class="landing-container" fxLayout="column" fxLayoutAlign="start center">
      <app-landing-box
      title="Explore"
      text=" Explore existing public debates or take a personal learning journey into new topics or questions you’d like to examine.<br>
      The BCAUSE Explorer helps you gather pertinent information and evidence, recognise stated arguments and reflect on unstated assumptions (including your own); thus, helping you reflect and make sense of complex issues. 
      "
      [image]="{ path: 'assets/explore.svg', alt: 'Explore' }"
      [destination]="{ route: '/explore', label: 'Explore' }"
      variant="reverse">
      </app-landing-box>
    </div>
    
    <div fxLayout="row" fxLayoutAlign="center center">
        <img src="assets/work-in-progress.png" width="150px"/>
        <div fxFlex="50px"></div>
        <div class="warning">This page is under<br>active development.<br>New features <b>coming soon!</b></div>
      </div>

      <div style="height:50px"></div>

    
    <div fxLayout="row" fxLayoutAlign="center center">
        <img src="assets/work-in-progress.png" width="150px"/>
        <div fxFlex="50px"></div>
        <div class="warning">This page is under<br>active development.<br>New features <b>coming soon!</b></div>
      </div>

      <div style="height:50px"></div>

  `,
  styles: [`
    h1 {
      color:#0073b6 !important;
      font-size:2.5rem;
      font-weight:bold;
    }

    .warning {
      margin-top:30px;
      font-family: "Inter";
      line-height:1.2;
      font-size:1.5rem;
    }
  `
  ]
})
export class ExploreLandingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

