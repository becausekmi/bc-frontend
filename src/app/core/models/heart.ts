export interface Heart {
    claimId: string,
    debateId: string,
    positionid: string,
    metadata: {
      createdBy: string,
      createdDate: string
    },
    value: number
  }
  