export interface Participant {
  debateId: string,
  id: string,
  metadata: {
    createdBy: string,
    createdDate: string
  },
  pseudo: string
}
