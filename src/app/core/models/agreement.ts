export interface PositionAgreement {
    debateId: string,
    metadata: {
      createdBy: string,
      createdDate: string
    },
    level: number,
    positionId: string
  }

export interface ArgumentAgreement {
    debateId: string,
    metadata: {
      createdBy: string,
      createdDate: string
    },
    level: number,
    positionId: string,
    claimId: string
  }
  