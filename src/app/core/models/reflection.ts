export interface ReflectionPosition {
    debateId: string,
    positionId: string,
    levels: {
        agree: number,
        // polarized: number, -> polarization
        // unclear: number, -> trust
        // unpolarized: number -> prioritization
        prioritization: number,
        trust: number,
        polarization: number
    }
    metadata: {
      createdBy: string,
      createdDate: number
    }
  }

  export interface ReflectionArgument {
    claimId: string,
    debateId: string,
    positionId: string,
    levels: {
        agree: number,
        // polarized: number, -> polarization
        // unclear: number, -> trust
        evidences: number,
        trust: number,
        polarization: number,
    }
    metadata: {
      createdBy: string,
      createdDate: number
    }
  }
  