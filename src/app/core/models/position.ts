import { Argument } from "./argument";

export interface Position {
  debateId: string,
  parentId: string|null,
  id: string,
  metadata: {
    createdBy: string,
    createdDate: number
  },
  text: string,
  audio: string,
  type: string,
  attachedUrl:any,
  platformData: {
    agreements: any[],
    agreementLastValue: number | null,
    agreementAverageValue: number,
    agreementLastTS: number | null,
    claims: Argument[],
    currentReplyPosition:any,
    contributors:any[],
    opposingClaims: Argument[],
    parentPosition:any|null,
    showAgreementAlert: boolean,
    showOnScroll:boolean,
    state: string,
    supportingClaims: Argument[],
    treeCollapsed: boolean,
    dateSince: any,
    reflectionsCounter:number,
    heartsCounter:number
  }
}

export interface PositionAngular extends Position {

}