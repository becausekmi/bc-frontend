import { Recommendation } from 'src/app/core/models/recommendation-query';
export interface Argument {
  debateId: string,
  id: string,
  metadata: {
    createdBy: string,
    createdDate: number
  },
  parentId: string | null,
  positionAgreement: number | null,
  positionId: string,
  attachedUrl: any,
  text: string,
  type: string,
  platformData: {
    currentReplyClaim: any | null,
    dateSince: any,
    state: string,
    agreements: any[],
    agreementLastValue: number | null,
    agreementAverageValue: number,
    agreementLastTS: number | null,
    reflectionsCounter: number,
    heartsCounter: number,
    contributors: any[],
  },
  externalClaimsAttached: Recommendation[] | null

}
