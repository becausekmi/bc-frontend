export interface Debate {
  id: string,
  image: string,
  title: string,
  text: string,
  visibility: string,
  interactions: string,
  discussionGroup:string,
  metadata: {
    createdBy: string,
    createdDate: string
  }
}
