export interface RecommendationRoot {
    query: Query
    response: Response
  }
  
export interface Query {
    argument_id: string
    argument_text: string
    debate_id: string
    position_id: string
    position_text: string
}

export interface Response {
    data: Data
}

export interface Data {
    recommendations: Recommendation[]
}

export interface Recommendation {
    article_authors: ArticleAuthor[]
    article_id: number
    article_title: string
    article_yearPublished: number
    scope: string
    sentence: string
    type: string
}

export interface ArticleAuthor {
    name: string
}
  