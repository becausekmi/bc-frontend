import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-hero></app-hero>
    <div fxLayout="column" fxLayoutAlign="start center" >
      <app-home-section
        title="Explore"
        text="Explore existing public debates or take a personal learning journey into new topics or questions you’d like to
        examine.<br>
        The BCAUSE Explorer helps you gather pertinent information and evidence, recognise stated arguments and
        reflect on unstated assumptions (including your own); thus, helping you reflect and make sense of complex
        issues."
        [image]="{ path: 'assets/explore.svg', alt: 'Explore' }"
        [destination]="{ route: '/explore', label: 'Explore' }"
        variant="reverse"
        ></app-home-section>

        <app-home-section
        title="Discuss"
        text="Engage in a journey of personal and collective inquiry to better inform your decision and actions.<br>
        With BCAUSE, groups can co-create solutions to complex problems by openly discussing them with others.
        Individuals and groups can launch group discussions around specific questions or challenges. The discussions
        are then structured in a way to enable people to contribute ideas, arguments, and appraise evidence in favour
        and against any given idea."
        [image]="{ path: 'assets/discuss_landing.svg', alt: 'discuss' }"
        [destination]="{ route: '/discuss', label: 'Discuss' }"
        variant="front"
        ></app-home-section>


        <app-home-section
        title="Reflect"
        text="Think critically in all areas of public deliberation (in the enterprise, your local community, and in which
          organisations you work, learn or advocate).<br>
          BCAUSE provides an advanced Visual Analytics Dashboard build on top of your group discussion data to help
          you reflect on emerging patterns, discussion processes, unexpected results or unwanted social dynamics (such
          as group thinks, unbalanced debates or heated discussion)."
        [image]="{ path: 'assets/reflect.svg', alt: 'Reflect' }"
        [destination]="{ route: '/reflect', label: 'Reflect' }"
        variant="reverse"
        ></app-home-section>


        <app-home-section
        title="Decide"
        text="Make decisions that are consulted, reflected upon and critically assessed by all.<br>
        BCAUSE unleashes the power of collective reasoning and sense making to enable groups to make sounder
        judgments, and take wiser decision and actions.
        With BCUASE, group can collectively prioritise and choose options based on the assessment of rational
        arguments and sound evidence, rather than manipulation. Co-constructing these decisions with others is done
        with a stage-based process of open ideation, structured dialogue and collective deliberation."
        [image]="{ path: 'assets/decide.svg', alt: 'Decide' }"
        [destination]="{ route: '/decide', label: 'Decide' }"
        variant="front"
        ></app-home-section>
    </div>
  `,
  styles: [`
      .home-section {
        padding:60px;
      }
  `]
})
export class HomeComponent {}
