import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-home-section',
  template: `
    <div class="box" fxLayout="row" fxLayoutAlign="start center" *ngIf="variant === 'reverse'">
      <div fxLayout="row" class="padding-content">
        <div class="content" fxLayout="column" fxLayoutAlign="space-between start">
          <h2 >{{ title }}</h2>
          <div fxFlex="10px"></div>
          <p [innerHTML]="text"></p>
          <div fxFlex="40px"></div>
          <button
            fxLayout="row" fxLayoutAlign="center center"
            [routerLink]="[destination.route]"
            alt=""
            >{{ destination.label }}</button>
        </div>
        <div fxFlex="100px"></div>
        <img height="330px" [src]="image.path" [alt]="image.alt" />
      </div>
    </div>

    <div class="box front" fxLayout="row" fxLayoutAlign="start center" *ngIf="variant === 'front'">
      <div fxLayout="row" class="padding-content">
        <img height="330px" [src]="image.path" [alt]="image.alt" />
        <div fxFlex="100px"></div>
        <div class="content" fxLayout="column" fxLayoutAlign="space-between start">
            <h2 >{{ title }}</h2>
            <div fxFlex="10px"></div>
            <p [innerHTML]="text"></p>
            <div fxFlex="40px"></div>
            <button fxLayout="row" fxLayoutAlign="center center"
              [routerLink]="[destination.route]"
              alt=""
              >{{ destination.label }}</button>
        </div>
      </div>
    </div>
  `,
  styles: [`
      .box {
         padding:0px;
      }

      .padding-content {
        padding:80px 150px;
      }

      h2 {
        color: #007FC1;
        font-size: 48px;
        font-weight:bold;
        font-family: 'Inter';
        letter-spacing: -1px;
      }

      p {
        color:#2b2b2b;
        font-size:15px;
        font-family: 'Inter';
        line-height:1.5;
      }

      .front {
        background: #F8F8F8;
      }

      button {
        background:#007FC1;
        color:white;
        border-radius:5px;
        border:none;
        padding:20px 10px;
        width:150px;
      }
  `]
})
export class HomeSectionComponent {
  @Input() title: string = '';
  @Input() text: string = '';
  @Input() image = {
    path: '',
    alt: ''
  };
  @Input() button = {};
  @Input() destination = {
    label:'',
    route:'',
  };
  @Input() variant: string = '';

  constructor() {

      console.log(this.title);
  }

}
