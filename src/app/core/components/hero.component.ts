import { Component } from '@angular/core';

// @todo take motive from constant file ?
@Component({
  selector: 'app-hero',
  template: `
    <div class="appHero">
      <span>
        <h2>reasoning for change</h2>
        <h3>A structured and decentralised Discussion System for Distributed Decision Making.</h3>
      </span>
    </div>
  `,
  styles: [`
    .appHero {
      background-image: url(/assets/images/home/hero.png);
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center;
      color: white;

      display: flex;
      align-items: flex-end;
      justify-content: center;
      height: 450px;
      & > span {
        display: block;
        margin-bottom: 20px;
        h2 {
          font-size: 64px;
          font-weight: bold;
          text-transform: capitalize;
          // line-height: 70px;
        }
        h3 {
          font-size: 16px;
          font-weight: bold;
          text-align: center;
        }
      }
    }
  `]
})
export class HeroComponent { }
