
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  enabled$: BehaviorSubject<boolean>;

  constructor() {
    this.enabled$ = new BehaviorSubject<boolean>(true);
  }

  enable(): void {
    this.enabled$.next(true);
  }

  disable(): void {
    this.enabled$.next(false);
  }

}
