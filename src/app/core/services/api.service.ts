import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { get as _get, pick as _pick, omit as _omit, merge as _merge } from 'lodash';

import { Identity } from 'src/app/auth/models/Identity';
import { FirebaseService } from './apis/firebase.service';
import { DClientService } from './apis/dclient.service';
import { Api } from './api.interface';

import { environment } from 'src/environments/environment';
import { Position } from '../models/position';
import { Argument } from '../models/argument';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private api: Api;
  private identity$!: Observable<Identity | null>;

  // private debates$: Observable<Debate[]>;

  constructor(
    private firebaseService: FirebaseService,
    private dClientService: DClientService,
  ) {
    switch (environment.api) {
      case 'FIREBASE':
        this.api = this.firebaseService;
        break;
      case 'DCLIENT':
        this.api = this.dClientService;
        break;
      default:
        console.warn("Environment api not recognized, you set VARIABLE in env.json");
        this.api = this.firebaseService;
    }
  }



  getIdentity(): Observable<Identity | null> {
    // if (this.profile$ === null) {
    // }
    // return this.profile$;
    return this.firebaseService.getIdentity(); // temporary function
    // return this.api.getIdentity(); // @todo
  }


  // DEBATES

  fetchDebate(debateId:string) {
    return this.api.fetchDebate(debateId)
  }

  fetchDebatesByUser(user:any) {
    return this.api.fetchDebatesByUser(user.uid)
  }

  
  fetchDebatesByGroup(groupId:any) {
    return this.api.fetchDebatesByGroup(groupId)
  }

  fetchDebatesContributedByUser(user:any) {
    return this.api.fetchDebatesContributedByUser(user)
  }

  fetchAllDebates() {
    return this.api.fetchAllDebates();
  }

  fetchPublicDebates() {
    return this.api.fetchPublicDebates();
  }

  addDebate(newDebate: any) { // @todo fix type
    return this.api.addDebate(newDebate);
  }

  // SUMMARY

  fetchSummary(debateId:string) {
    return this.api.fetchSummary(debateId)
  }

  // POSITIONS 

  createPosition(position:Position) {
    return this.api.createPosition(position);
  }

  deletePosition(position:Position) {
    return this.api.deletePosition(position);
  }
  
  fetchPositionsByDebate(debateId:any) {
    return this.api.fetchPositionsByDebate(debateId);
  }

  // ARGUMENTS / CLAIMS

  fetchArgumentsByDebate(debateId:any) {
    return this.api.fetchArgumentsByDebate(debateId);
  }

  fetchArgumentsByPosition(debateId:string, positionId:string) {
    return this.api.fetchArgumentsByPosition(debateId, positionId)
  }

  createArgument(argument:Argument) {
    return this.api.createArgument(argument);
  }

  deleteArgument(argument:Argument) {
    return this.api.deleteArgument(argument);
  }
  
  // PARTICIPANTS

  fetchParticipantsByDebate(debateKey:any) {
    return this.api.fetchParticipantsByDebate(debateKey);
  }

  fetchParticipant(debateKey:any, participantKey:any) {
    return this.api.fetchParticipant(debateKey, participantKey);
  }

  createParticipant(participant:any) {
    return this.api.createParticipant(participant);
  }

  fetchParticipantContributionsStatsByDebate(user:any, debate:any) {
    return this.api.fetchParticipantContributionsStatsByDebate(user, debate)
  }

  fetchParticipantContributionsStatsAllDebates(userId:string) {
    return this.api.fetchParticipantContributionsStatsAllDebates(userId);
  }

  // HEARTS

  fetchHeartsByPosition(position:any) {
    return this.api.fetchHeartsByPosition(position);
  }

  fetchHeartsByPositionAndUser(position:any, user:any) {
    return this.api.fetchHeartsByPositionAndUser(position,user)
  }

  fetchHeartsByArgumentAndUser(argument:any, user:any) {
    return this.api.fetchHeartsByArgumentAndUser(argument,user)
  }

  fetchHeartsByArgument(argument:any) {
    return this.api.fetchHeartsByArgument(argument);
  }

  createHeartOnPosition(heart:any, position: any, user:any) {
    return this.api.createHeartOnPosition(heart, position, user);
  }

  createHeartOnArgument(heart:any,argument: any, user:any) {
    return this.api.createHeartOnArgument(heart,argument, user);
  }


  deleteHeartOnPosition(position: any, user:any) {
    return this.api.deleteHeartOnPosition(position, user);
  }
  deleteHeartOnArgument(argument: any, user:any) {
    return this.api.deleteHeartOnArgument(argument, user);
  }

  // REFLECTIONS

  fetchReflectionsCounterByPosition(position:any) {
    return this.api.fetchReflectionsCounterByPosition(position);
  }

  
  // AGREEMENTS

  fetchAgreementsByDebate(debate:any) {
    return this.api.fetchAgreementsByDebate(debate);
  }

  fetchArgumentsAgreementsByDebate(debate:any) {
    return this.api.fetchArgumentsAgreementsByDebate(debate);
  }

  
  fetchPositionsAgreementsByDebate(debate:any) {
    return this.api.fetchPositionsAgreementsByDebate(debate);
  }

  
  fetchAgreementsByPosition(position:any) {
    return this.api.fetchAgreementsByPosition(position);
  }

  fetchLastUserAgreementByPosition(position:any, uid:string) {
    return this.api.fetchLastUserAgreementByPosition(position, uid);
  }

  
  fetchLastUserAgreementByArgument(argument:any, uid:string) {
    return this.api.fetchLastUserAgreementByArgument(argument, uid)
  }

  // DISCUSSION GROUPS
  
  fetchAllGroups() {
    return this.api.fetchAllGroups();
  }

  fetchGroup(id:any) {
    return this.api.fetchGroup(id);
  }

  fetchGroupsByAdmin(userData:any) {
    return this.api.fetchGroupsByAdmin(userData);
  }

  fetchGroupsByUser(userData:any) {
    return this.api.fetchGroupsByUser(userData);
  }

  updateDiscussionGroup(group:any) {
    return this.api.updateDiscussionGroup(group);
  }

  // USERS 
  fetchUserByEmail(email:string) {
    return this.api.fetchUserByEmail(email);
  }
}