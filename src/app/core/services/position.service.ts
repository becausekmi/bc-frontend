import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/compat/database';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PositionService {

    public selectedPosition = new BehaviorSubject<any>('');
    selectedPositionObservable$ = this.selectedPosition.asObservable();

    
    public collapsedPosition = new BehaviorSubject<any>('');
    collapsedPositionObservable$ = this.collapsedPosition.asObservable();

    public selectedClaim = new BehaviorSubject<any>('');
    selectedClaimObservable$ = this.selectedClaim.asObservable();

    constructor(private db: AngularFireDatabase) { }

    public notifySelectedPosition(data: any) {
        if (data) {
            this.selectedClaim.next('');
            this.selectedPosition.next(data);
        } else {
            this.selectedPosition.next('');
        }
    }

    public notifySelectedClaim(data: any) {
        if (data) {
            this.selectedPosition.next('');
            this.selectedClaim.next(data);
        } else {
            this.selectedClaim.next('');
        }
    }

    public notifyCollapsedPosition(data: any) {
        if (data) {
            this.collapsedPosition.next(data);
        } else {
            this.collapsedPosition.next('');
        }
    }

}