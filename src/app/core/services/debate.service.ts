
import { Injectable, Output } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/compat/database';
import { Observable, Subject } from 'rxjs';
import { Argument } from '../models/argument';
import { Debate } from '../models/debate';
import { Position } from '../models/position';
import { Participant } from '../models/participant';
import { Heart } from '../models/heart';
import { ReflectionPosition, ReflectionArgument } from '../models/reflection';
import { BehaviorSubject } from 'rxjs';
import { ArgumentAgreement, PositionAgreement } from '../models/agreement';
import { StatsService } from './stats.service';
import { HttpClient } from '@angular/common/http';
import { Identity } from 'src/app/auth/models/Identity';
import { Recommendation } from 'src/app/core/models/recommendation-query';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DebateService {

  dataReady = new BehaviorSubject<boolean>(false);

  /* DEBATE VARs */
  debateRef!: AngularFireObject<Debate>;
  debate = new Subject<any>();
  debateObj: Debate| null = null;
  debateKey: string | null = null;
  debates$ = new BehaviorSubject<any>(null);



  /* summary VARs */
  /* summarySubscription: any;
  summaryRef!: AngularFireObject<Debate>;
  summary = new Subject<any>();
  summaryObj: any| null = null;
  summary$= new BehaviorSubject<any>(null); */

  /* POSITIONS VARs */
  positionsSubscription: any;
  positionsRef!: AngularFireList<Position>;
  positions = new Subject<any>();
  positionPushed = new Subject<any>();
  positionDeleted = new Subject<any>();
  positionsArray:Position[] = [];
  positionsDict:any = {};

  /* ARGUMENTS VARs */
  argumentsSubscription: any;
  argumentsRef!: AngularFireList<Argument>;
  arguments = new Subject<any>();
  argumentsArray:Argument[] = [];
  argumentsDict:any = {};

  /* PARTICIPANTS VARs */
  participantsSubscription: any;
  participantsRef!: AngularFireObject<Participant>;
  participants = new Subject<any>();
  participantsArray:Participant[] = [];
  participantsDict:any = {};

  /* POSITION HEARTS VARs */

  heartsByPositionsSubscription: any;
  heartsByPositionsRef!: AngularFireList<Argument>;
  heartsByPositions = new Subject<any>();
  heartsByPositionsArray: Heart[] = [];
  heartsByPositionsDict:any = {};
  positionsHeartsByUsersDict:any = {};
  heartsStatsByPositionsDict:any = {};

  /* ARGUMENT HEARTS VARs */

  heartsByArgumentsSubscription: any;
  heartsByArgumentsRef!: AngularFireList<Argument>;
  heartsByArguments = new Subject<any>();
  heartsByArgumentsArray: Heart[] = [];
  heartsByArgumentsDict:any = {};
  argumentsHeartsByUsersDict:any = {};
  heartsStatsByArgumentsDict:any = {};

  /* POSITION REFLECTIONS VARs */

  reflectionsByPositionsSubscription: any;
  reflectionsByPositionsRef!: AngularFireList<ReflectionPosition>;
  reflectionsByPositions = new Subject<any>();
  reflectionsByPositionsArray: ReflectionPosition[] = [];
  reflectionsByPositionsDict:any = {};
  positionsReflectionsByUsersDict:any = {};
  reflectionsStatsByPositionsDict:any = {};

  /* ARGUMENT REFLECTIONS VARs */

  reflectionsByArgumentsSubscription: any;
  reflectionsByArgumentsRef!: AngularFireList<ReflectionArgument>;
  reflectionsByArguments = new Subject<any>();
  reflectionsByArgumentsArray: ReflectionArgument[] = [];
  reflectionsByArgumentsDict:any = {};
  argumentsReflectionsByUsersDict:any = {};
  reflectionsStatsByArgumentsDict:any = {};

  /* POSITION AGREEMENTS VARs */

  agreementsByPositionsRef!: AngularFireList<PositionAgreement>;
  agreementsByPositionsSubscription: any;
  agreementsByPositions = new Subject<any>();
  agreementsByPositionsArray: PositionAgreement[] = [];
  agreementsByPositionsDict:any = {};
  positionsAgreementsByUsersDict:any = {};
  agreementsStatsByPositionsDict:any = {};

  /* ARGUMENT AGREEMENTS VARs */

  agreementsByArgumentsRef!: AngularFireList<ArgumentAgreement>;
  agreementsByArgumentsSubscription: any;
  agreementsByArguments = new Subject<any>();
  agreementsByArgumentsArray: PositionAgreement[] = [];
  agreementsByArgumentsDict:any = {};
  argumentsAgreementsByUsersDict:any = {};
  agreementsStatsByArgumentsDict:any = {}

  
  public dataUpdated= new BehaviorSubject<any>(0);
  dataUpdatedObservable$ = this.dataUpdated.asObservable();

  constructor(private db: AngularFireDatabase, private statsService: StatsService, private http:HttpClient) {

    this.debates$ = new BehaviorSubject<null>(null);
  }

  async subscribeDebate(key: string) {
    try {

      this.debateKey = key;
      this.positions.next(null);
      this.arguments.next(null);
      this.participants.next(null);
      /* GET DEBATE DATA  */

      /* this.debateRef = this.db.object('debates/' + key);
      let promiseDebate = new Promise((resolve,reject) => {
        this.debateSubscription = this.debateRef.snapshotChanges().subscribe(data=>{
          console.log("DEBATE DATA DOWLOADED");
          this.debate.next(data.payload.val());
          this.debateObj = data.payload.val();
          resolve(true);
        });
      }) */


      /* GET AND PROCESS SUMMARY  */

      /* this.summaryRef = this.db.object('summary/' + key);
      let promiseSummary = new Promise((resolve,reject) => {
        this.agreementsByArgumentsSubscription = this.summaryRef.snapshotChanges().subscribe(data=>{
          this.summary.next(data.payload.val());
          this.summaryObj = data.payload.val();
          resolve(true);
        });
      }); */

      /* GET AND PROCESS POSITIONS OF THE DEBATE */

      this.positionsRef = this.db.list('positions/' + key);
      this.positionsDict = {}
      this.positionsArray = [];
      let promisePositions = new Promise((resolve,reject) => {
        this.positionsSubscription = this.positionsRef.snapshotChanges().subscribe(data=>{
          this.processPositions(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

      /* GET AND PROCESS ARGUMENTS OF THE DEBATE */

      this.argumentsRef = this.db.list('claims/' + key);
      this.argumentsDict = {}
      this.argumentsArray = [];
      let promiseArguments = new Promise((resolve,reject) => {
        this.argumentsSubscription = this.argumentsRef.snapshotChanges().subscribe(data=>{
          this.processArguments(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

      /* GET AND PROCESS PARTICIPANTS OF THE DEBATE */

      this.participantsRef = this.db.object('participants/' + key);
      this.participantsDict = {};
      this.participantsArray = [];
      let promiseParticipants = new Promise((resolve,reject) => {
        this.participantsSubscription = this.participantsRef.snapshotChanges().subscribe(data=>{
          this.processParticipants(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

      /* GET AND PROCESS HEARTS OF THE POSITIONS */

      this.heartsByPositionsRef = this.db.list('positionHearts/' + key);
      this.heartsByPositionsArray = [];
      this.heartsByPositionsDict = {};
      let promiseHeartByPositions = new Promise((resolve,reject) => {
        this.heartsByPositionsSubscription = this.heartsByPositionsRef.snapshotChanges().subscribe(data=>{
          this.processHeartsByPositions(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

      /* GET AND PROCESS HEARTS OF THE ARGUMENTS */

      this.heartsByArgumentsRef = this.db.list('claimHearts/' + key);
      this.heartsByArgumentsDict ={};
      this.heartsByArgumentsArray = [];
      let promiseHeartByArguments = new Promise((resolve,reject) => {
        this.heartsByArgumentsSubscription = this.heartsByArgumentsRef.snapshotChanges().subscribe(data=>{
          this.processHeartsByArguments(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

      /* GET AND PROCESS REFLECTIONS OF THE POSITIONS */

      this.reflectionsByPositionsRef = this.db.list('positionDimensions/' + key);
      this.reflectionsByPositionsDict = {};
      this.reflectionsByPositionsArray = [];
      let promiseReflectionsByPositions = new Promise((resolve,reject) => {
        this.reflectionsByPositionsSubscription = this.reflectionsByPositionsRef.snapshotChanges().subscribe(data=>{
          this.processReflectionsByPositions(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

      /* GET AND PROCESS REFLECTIONS OF THE ARGUMENTS */

      this.reflectionsByArgumentsRef = this.db.list('claimDimensions/' + key);
      this.reflectionsByArgumentsDict = {};
      this.reflectionsByArgumentsArray = [];
      let promiseReflectionsByArguments = new Promise((resolve,reject) => {
        this.reflectionsByArgumentsSubscription = this.reflectionsByArgumentsRef.snapshotChanges().subscribe(data=>{
          this.processReflectionsByArguments(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

      /* GET AND PROCESS AGREEMENTS OF THE POSITIONS */

      this.agreementsByPositionsRef = this.db.list('positionAgreements/' + key);
      this.agreementsByPositionsArray = [];
      this.agreementsByPositionsDict = {};
      this.agreementsByPositions.next(null);
      let promiseAgreementsByPositions = new Promise((resolve,reject) => {
        this.agreementsByPositionsSubscription = this.agreementsByPositionsRef.snapshotChanges().subscribe(data=>{
          this.processAgreementsByPositions(data);
          this.dataUpdated.next(new Date().getTime());
          resolve(true);
        });
      });

       /* GET AND PROCESS AGREEMENTS OF THE ARGUMENTS  */

       this.agreementsByArgumentsRef = this.db.list('claimAgreements/' + key);
       this.agreementsByArgumentsDict = {};
       this.agreementsByArgumentsArray = [];
       let promiseAgreementsByArguments = new Promise((resolve,reject) => {
         this.agreementsByArgumentsSubscription = this.agreementsByArgumentsRef.snapshotChanges().subscribe(data=>{
           this.processAgreementsByArguments(data);
           this.dataUpdated.next(new Date().getTime());
           resolve(true);
         });
       });


      return Promise.all([promisePositions,promiseArguments, promiseParticipants, promiseHeartByPositions, promiseHeartByArguments, promiseReflectionsByArguments, promiseReflectionsByPositions, promiseAgreementsByPositions, promiseAgreementsByArguments]).then(()=>{
        this.dataReady.next(true);
        return true;
      });


    } catch(err) {
      console.log(err);
      return err;
    }
  }

  async unsubscribeDebate() {
    this.debateKey = null;
    this.argumentsSubscription.unsubscribe();
    this.positionsSubscription.unsubscribe();
    this.participantsSubscription.unsubscribe();
    this.heartsByPositionsSubscription.unsubscribe();
    this.heartsByArgumentsSubscription.unsubscribe();
    this.reflectionsByArgumentsSubscription.unsubscribe();
    this.reflectionsByPositionsSubscription.unsubscribe();
    this.agreementsByPositionsSubscription.unsubscribe();
    this.agreementsByArgumentsSubscription.unsubscribe();
    this.debate.next(null);
  }


  getPositions(): Observable<Position[]> {
    return this.positions.asObservable();
  }

  getPositionPushed(): Observable<Position> {
    return this.positionPushed.asObservable();
  }

  getPositionDeleted() : Observable<number> {
    return this.positionDeleted.asObservable();
  }

  getArguments(): Observable<Argument[]> {
    return this.arguments.asObservable();
  }

  getParticipants(): Observable<Participant[]> {
    return this.participants.asObservable();
  }

  areDataReady(): Observable<boolean> {
    return this.dataReady.asObservable();
  }

  getPositionsAgreements(): Observable<PositionAgreement[]> {
    return this.agreementsByPositions.asObservable();
  }

  getUserDebates(uid:string): Promise<Debate[]> {
    let userDebates = this.db.list('debates/',ref=>ref.orderByChild('metadata/createdBy').equalTo(uid));
    return new Promise((resolve,reject) => {
      let sub = userDebates.snapshotChanges().subscribe(data=>{
        sub.unsubscribe();
        let output: any = [];
        data.forEach(snap =>{
          output.push(snap.payload.val());
        })
        resolve(output);
      });
    });

  }

  getUserContributedDebates(uid:string): Promise<Debate[]> {
    let userDebates = this.db.list('debates/',ref=>ref.orderByChild('metadata/createdBy').equalTo(uid));
    return new Promise((resolve,reject) => {
      let sub = userDebates.snapshotChanges().subscribe(data=>{
        sub.unsubscribe();
        let output: any = [];
        data.forEach(snap =>{
          output.push(snap.payload.val());
        })
        resolve(output);
      });
    });

  }

  async updateDebate(debate: any) {
    let dataToSave = JSON.parse(JSON.stringify(debate));
    this.db.object('debates/'+debate.id).update(dataToSave)
    return;
  }

  async pushDebate(debate: any) {
    let dataToSave = JSON.parse(JSON.stringify(debate));
    console.log(dataToSave);
    dataToSave.type = 'ISSUE';
    let newDebate = this.db.list('debates/').push(dataToSave);
    this.db.object('debates/'+newDebate.key).update({id:newDebate.key})
    return newDebate.key;
  }

  async pushDiscussionGroup(debate: any) {
    let dataToSave = JSON.parse(JSON.stringify(debate));
    let newDebate = this.db.list('discussionGroups/').push(dataToSave);
    this.db.object('discussionGroups/'+newDebate.key).update({id:newDebate.key})
    return newDebate.key;
  }

  async pushPosition(position: Position, user: Identity) {
    let dataToSave = JSON.parse(JSON.stringify(position));
    delete dataToSave.platformData;
    console.log(dataToSave);
    let newPosition = await this.positionsRef.push(dataToSave);
    this.db.object('positions/'+this.debateKey+'/'+ newPosition.key).update({id:newPosition.key});
    return newPosition.key;
  }

  async deletePosition(positionId: string) {
    let positionsClaims = this.argumentsArray.filter((a:Argument)=>a.positionId === positionId);

    await this.db.object('positions/'+this.debateKey+'/'+ positionId).remove();

    for (let argument of positionsClaims) {
      await this.deleteArgument(argument.id, argument.positionId);
    }

    this.positions.next(this.positionsArray);
  }

  async deleteDebate(debateId: string) {

    await this.db.object('debates/'+debateId).remove();
  }

  async deleteArgument(argumentId: string, positionId:string) {
    await this.db.object('claims/'+this.debateKey+'/'+ positionId+'/'+ argumentId).remove();
  }

  async pushArgument(argument: Argument, user: Identity) {
    let dataToSave = JSON.parse(JSON.stringify(argument));
    delete dataToSave.platformData;
    let newArgument = await this.db.list('claims/'+this.debateKey+'/'+ argument.positionId).push(dataToSave);
    await this.db.object('claims/'+this.debateKey+'/'+ argument.positionId + '/' + newArgument.key).update({id:newArgument.key});
    return newArgument.key;
  }

  async pushPositionHeart(position:Position, user:Identity) {

    await this.db.object('positionHearts/'+this.debateKey+'/'+ position.id + '/users/' + user.uid).set({
      debateId: this.debateKey,
      metadata: {
        createdBy: user.uid,
        createdDate: new Date().getTime()
      },
      positionId: position.id,
      value: 1
    });

    let newTotal = 1;

    if (this.heartsStatsByPositionsDict[position.id]) {
      newTotal = this.heartsStatsByPositionsDict[position.id]+1;
    }

    await this.db.object('positionHearts/'+this.debateKey+'/'+ position.id + '/total/').set(newTotal);
  }

  async popPositionHeart(position:Position, user:Identity) {

    let newTotal = 0;
    if (this.heartsStatsByPositionsDict[position.id]) {
      newTotal = this.heartsStatsByPositionsDict[position.id]-1;
    }

    await this.db.object('positionHearts/'+this.debateKey+'/'+ position.id + '/users/' + user.uid).remove();

    if (newTotal <= 0) {
      await this.db.object('positionHearts/'+this.debateKey+'/'+ position.id ).remove();
    } else {
      await this.db.object('positionHearts/'+this.debateKey+'/'+ position.id + '/total/').set(newTotal);
    }
  }

  async pushArgumentHeart(argument:Argument, user:Identity) {
    await this.db.object('claimHearts/'+this.debateKey+'/'+ argument.positionId+'/'+ argument.id + '/users/' + user.uid).set({
      debateId: this.debateKey,
      metadata: {
        createdBy: user.uid,
        createdDate: new Date().getTime()
      },
      claimId: argument.id,
      positionId: argument.positionId,
      value: 1
    });
    let newTotal = 1;
    if (this.heartsStatsByArgumentsDict[argument.id]) {
      newTotal = this.heartsStatsByArgumentsDict[argument.id]+1;
    }
    await this.db.object('claimHearts/'+this.debateKey+'/'+ argument.positionId+'/'+ argument.id + '/total/').set(newTotal);
  }

  async popArgumentHeart(argument:Argument, user:Identity) {

    let newTotal = 0;
    if (this.heartsStatsByArgumentsDict[argument.id]) {
      newTotal = this.heartsStatsByArgumentsDict[argument.id]-1;
    }

    await this.db.object('claimHearts/'+this.debateKey+'/'+ argument.positionId+'/'+ argument.id + '/users/' + user.uid).remove();

    if (newTotal <= 0) {
      newTotal = 0
      await this.db.object('claimHearts/'+this.debateKey+'/'+ argument.positionId+'/'+ argument.id ).remove();
    } else {
      await this.db.object('claimHearts/'+this.debateKey+'/'+ argument.positionId+'/'+ argument.id + '/total/').set(newTotal);
    }
  }

  async pushParticipant(user:Identity) {
    let pseudo = ''

    if (user.defaultPseudo) {
      pseudo = user.defaultPseudo;
    }

    await this.db.object('participants/'+this.debateKey+'/'+ user.uid).set({
      debateId: this.debateKey,
      id: user.uid,
      metadata: {
        createdBy: user.uid,
        createdDate: new Date().getTime(),
      },
      pseudo
    });
  }

  async pushPositionAgreement(position:Position, user:Identity) {
    await this.db.object('positionAgreements/'+this.debateKey+'/'+ position.id+'/users/'+user.uid).set({
      debateId: this.debateKey,
      metadata: {
        createdBy: user.uid,
        createdDate: new Date().getTime(),
      },
      positionId: position.id,
      level: position.platformData.agreementLastValue
    });

    /* @todo  DA ELIMINARE E OTTIMIZZARE IN POSITIONAGREEMENTS */

    await this.db.list('userPositionAgreements/'+this.debateKey+'/'+ user.uid+'/'+position.id).push({
      debateId: this.debateKey,
      metadata: {
        createdBy: user.uid,
        createdDate: new Date().getTime(),
      },
      positionId: position.id,
      level: position.platformData.agreementLastValue
    });
  }

  async pushArgumentAgreement(argument:Argument, user:Identity) {
    await this.db.object('claimAgreements/'+this.debateKey+'/'+ argument.positionId+'/'+argument.id+'/users/'+user.uid).set({
      debateId: this.debateKey,
      metadata: {
        createdBy: user.uid,
        createdDate: new Date().getTime(),
      },
      positionId: argument.id,
      level: argument.platformData.agreementLastValue
    });

    /* @todo DA ELIMINARE E OTTIMIZZARE IN ARGUMENT AGREEMENTS */

    await this.db.list('userArgumentAgreements/'+this.debateKey+'/'+ user.uid+'/'+argument.id).push({
      debateId: this.debateKey,
      metadata: {
        createdBy: user.uid,
        createdDate: new Date().getTime(),
      },
      positionId: argument.positionId,
      claimId: argument.id,
      level: argument.platformData.agreementLastValue
    });
  }

  /* PROCESS DATA FROM RTDB */

  processPositions(snapshotData: any) {
    if (!this.positionsDict) {
      this.positionsDict = {}
    }
    let positions: Position[] =[];
    snapshotData.forEach((snap: { payload: { val: () => Position; }; }) =>{
      positions.push(snap.payload.val())
    })

    console.log(this.positionsArray, positions)

    /* CHECK IF THERE'S A NEW POSITION */

    positions.forEach((p:Position)=>{
      if (p.id !== '' && !this.positionsDict[p.id]) {
        p = this.setPositionDefaultPlatformData(p);
        this.positionsDict[p.id] = p;
        this.positionsArray.push(p);
        this.positionPushed.next(p);
      }
    })

    /* CHECK IF A POSITION HAS BEEN DELETED */

    this.positionsArray.forEach((pLocal:Position, index, object)=>{

      let stillExist = positions.findIndex((pDB:Position)=> pDB.id === pLocal.id );

      if (stillExist === -1) {
        delete this.positionsDict[pLocal.id];
        object.splice(index,1);
        console.log("REMOVE POSITION", pLocal.id);
        this.positionDeleted.next(index);
      }

    })

    console.log(this.positionsArray)

    for (let pos of positions) {
      if (pos.parentId && pos.platformData) {
        pos.platformData.parentPosition = positions.find((p:Position)=>p.id === pos.parentId);
      }
    }

    this.positionsArray = this.positionsArray.sort(this.sortByCreatedDate);

    this.positions.next(this.positionsArray);
    console.log("POSITIONS ARRAY", this.positionsArray)
  }

  sortByCreatedDate( a:Position, b:Position ) {
    if ( a.metadata.createdDate < b.metadata.createdDate ){
      return -1;
    }
    if ( a.metadata.createdDate > b.metadata.createdDate ){
      return 1;
    }
    return 0;
  }
  

  processArguments(snapshotData: any) {
    let claims: Argument[]= [];

    let argumentsByPosition: any = [];
    snapshotData.forEach((snap: { payload: { val: () => any; }; }) =>{
      argumentsByPosition.push(snap.payload.val())
    })

    for (let position of argumentsByPosition) {
      for (let argumentKey in position) {
        let singleArgument = position[argumentKey];
        singleArgument = this.setArgumentDefaultPlatformData(singleArgument);
        claims.push(singleArgument);
      }
    }

    if (this.argumentsArray.length === 0) {

      this.argumentsArray = claims;

      this.argumentsArray.forEach((a:Argument)=>{
        this.argumentsDict[a.id] = a;
      })

      for (let position of this.positionsArray) {

        let findClaims = this.argumentsArray.filter((d: { positionId: any; }) => d.positionId === position.id);

        if (findClaims) {
          position.platformData.claims = findClaims;
          position.platformData.opposingClaims = findClaims.filter((c: { type: string; }) => c.type === 'OPPOSING');
          position.platformData.supportingClaims = findClaims.filter((c: { type: string; }) => c.type === 'SUPPORTING');
        }

      }
    } else {
      /* CHECK IF THERE'S A NEW ARGUMENT */

      claims.forEach((c:Argument)=>{
        if (c.id !== '' && !this.argumentsDict[c.id]) {
          console.log(c);
          c = this.setArgumentDefaultPlatformData(c);
          this.argumentsDict[c.id] = c;
          this.argumentsArray.push(c);
          console.log("PUSH NEW ARGUMENT");

          /* PUSH ARGUMENT INSIDE THE POSITION PLATFORM DATA */
          let position = this.positionsArray.find((p:Position)=>p.id === c.positionId);
          if (position) {
            position.platformData.claims.push(c);

            if (position && c.type.toUpperCase() === 'OPPOSING') {
              position.platformData.opposingClaims.push(c);
            } else {
              position.platformData.supportingClaims.push(c);
            }

            this.positionsDict[position.id] = position;
          }

        }

      })

      this.argumentsArray = this.argumentsArray.filter((a:Argument)=>a.id !== '');

      /* CHECK IF AN ARGUMENT HAS BEEN DELETED */
      console.log(this.argumentsArray);
      console.log(claims);
      this.argumentsArray.forEach((cLocal:Argument)=>{

        let stillExist = claims.findIndex((cDB:Argument)=> cDB.id === cLocal.id);

        if (stillExist === -1) {
          delete this.argumentsDict[cLocal.id];
          this.argumentsArray.splice(stillExist,1);
          console.log("REMOVE ARGUMENT", cLocal.id);

          /* REMOVE ARGUMENT FROM THE POSITION PLATFORM DATA */

          let position =  this.positionsDict[cLocal.positionId]

          if (position) {

            let findIndex = position.platformData.claims.findIndex((c:Argument)=>c.id === cLocal.id);
            console.log(findIndex);
            if (findIndex > -1) {
              position.platformData.claims.splice(findIndex,1)
            }

            if (cLocal.type === 'opposing') {
              let findIndexOpposing = position.platformData.opposingClaims.findIndex((c:Argument)=>c.id === cLocal.id);

              if (findIndex > -1) {
                position.platformData.opposingClaims.splice(findIndexOpposing,1)
              }
            } else {
              let findIndexSupporting = position.platformData.supportingClaims.findIndex((c:Argument)=>c.id === cLocal.id);

              if (findIndexSupporting > -1) {
                position.platformData.supportingClaims.splice(findIndexSupporting,1)
              }
            }

            let positionFound = this.positionsArray.find((p:Position)=>p.id === position.id);
            positionFound = position;
          }

        }

      })
    }
    this.arguments.next(claims);
  }

  processParticipants(snapshotData: any) {

    let participants: Participant[]= [];
    this.participantsDict = snapshotData.payload.val();
    let i = 0;
    for (let partKey in this.participantsDict) {
      this.participantsDict[partKey].avatar = (i+1)%50;
      participants.push(this.participantsDict[partKey]);
      i++;
    }

    this.participantsArray = participants;
    this.participants.next(participants);

    if (!this.participantsDict) {
      this.participantsDict = {};
    }
    console.log(this.participantsDict, this.participantsArray)
  }

  processHeartsByPositions(snapshotData: any) {
    let hearts: Heart[]= [];

    let heartsByPosition: any = [];
    snapshotData.forEach((snap: { payload: { val: () => any, key:string }; }) =>{

      let obj = {
        data: snap.payload.val(),
        positionKey: snap.payload.key
      }
      heartsByPosition.push(obj)
    })

    for (let heart of heartsByPosition) {
      this.heartsStatsByPositionsDict[heart.positionKey] = heart.data.total
      this.heartsByPositionsDict[heart.positionKey] = {}
      for (let userKey in heart.data.users) {
        let singleHeart = heart.data.users[userKey];
        this.heartsByPositionsDict[heart.positionKey][userKey] = true;
        // CHECK IF THE HEART IS LEFT FROM THE USER
        hearts.push(singleHeart);
      }
    }

    if (this.heartsByPositionsArray.length === 0) {
      this.heartsByPositionsArray = hearts;
    } else {
      /* CHECK IF THERE'S A NEW LIKE */
    }

  }

  processHeartsByArguments(snapshotData: any) {
    let hearts: Heart[]= [];

    let heartsByPosition: any = [];
    snapshotData.forEach((snap: { payload: { val: () => any, key:string }; }) =>{

      let obj = {
        data: snap.payload.val(),
        positionKey: snap.payload.key
      }
      heartsByPosition.push(obj)
    })

    for (let ele of heartsByPosition) {

      for (let argumentKey in ele.data) {
        this.heartsStatsByArgumentsDict[argumentKey] = ele.data[argumentKey].total

        this.heartsByArgumentsDict[argumentKey] = {}
        for (let userKey in ele.data[argumentKey].users) {
          let singleHeart = ele.data[argumentKey].users[userKey];
          singleHeart.argumentId = argumentKey;
          this.heartsByArgumentsDict[argumentKey][userKey] = true;
          hearts.push(singleHeart);
        }
      }
    }

    this.heartsByArgumentsArray = hearts;
  }

  processReflectionsByPositions(snapshotData: any) {
    console.log("PROCESS REFLECTIONS")
    let reflections: ReflectionPosition[]= [];
    this.reflectionsByPositionsArray = [];
    let reflectionsByPosition: any = [];
    snapshotData.forEach((snap: { payload: { val: () => any, key:string }; }) =>{

      let obj = {
        data: snap.payload.val(),
        positionKey: snap.payload.key
      }
      reflectionsByPosition.push(obj)
    })

    for (let reflection of reflectionsByPosition) {
      this.reflectionsStatsByPositionsDict[reflection.positionKey] = reflection.data.total
      this.reflectionsByPositionsDict[reflection.positionKey] = {}
      for (let userKey in reflection.data.users) {
        let singleReflection = reflection.data.users[userKey];
        this.reflectionsByPositionsDict[reflection.positionKey][userKey] = singleReflection;
        // CHECK IF THE HEART IS LEFT FROM THE USER
        reflections.push(singleReflection);
      }
    }

    if (this.reflectionsByPositionsArray.length === 0) {
      this.reflectionsByPositionsArray = reflections;
    } else {
      /* CHECK IF THERE'S A NEW LIKE */
    }

    /* console.log("REFLECTIONS POSITIONS")

    console.log(this.reflectionsByPositionsArray);
    console.log(this.reflectionsByPositionsDict); */

    if (!this.reflectionsByPositionsDict) {
      this.reflectionsByPositionsDict = {};
    }

  }

  processReflectionsByArguments(snapshotData: any) {
    let reflections: ReflectionArgument[]= [];

    let reflectionsByPosition: any = [];
    snapshotData.forEach((snap: { payload: { val: () => any, key:string }; }) =>{

      let obj = {
        data: snap.payload.val(),
        positionKey: snap.payload.key
      }
      reflectionsByPosition.push(obj)
    })

    for (let ele of reflectionsByPosition) {

      for (let argumentKey in ele.data) {
        this.reflectionsStatsByArgumentsDict[argumentKey] = ele.data[argumentKey].total

        this.reflectionsByArgumentsDict[argumentKey] = {}
        for (let userKey in ele.data[argumentKey].users) {
          let singleReflection = ele.data[argumentKey].users[userKey];
          singleReflection.argumentId = argumentKey;
          this.reflectionsByArgumentsDict[argumentKey][userKey] = singleReflection;
          reflections.push(singleReflection);
        }
      }
    }

    this.reflectionsByArgumentsArray = reflections;

    /* console.log("REFLECTIONS ARGUMENTS");

    console.log(this.reflectionsByArgumentsArray)
    console.log(this.reflectionsByArgumentsDict)
    console.log(this.reflectionsStatsByArgumentsDict) */
    /* for (let heart of heartsByPosition) {
      this.heartsStatsByPositionsDict[heart.positionKey] = heart.data.total
      this.heartsByPositionsDict[heart.positionKey] = {}
      for (let userKey in heart.data.users) {
        let singleHeart = heart.data.users[userKey];
        this.heartsByPositionsDict[heart.positionKey][userKey] = true;
        hearts.push(singleHeart);
      }
    }

    if (this.heartsByPositionsArray.length === 0) {
      this.heartsByPositionsArray = hearts;
    } else {
    } */
  }

  processAgreementsByPositions(snapshotData: any) {
    let agreements: PositionAgreement[]= [];

    let agreementsByPosition: any = [];
    snapshotData.forEach((snap: { payload: { val: () => any, key:string }; }) =>{

      let obj = {
        data: snap.payload.val(),
        positionKey: snap.payload.key
      }
      agreementsByPosition.push(obj)
    })

    for (let agreement of agreementsByPosition) {
      this.agreementsStatsByPositionsDict[agreement.positionKey] = agreement.data
      this.agreementsByPositionsDict[agreement.positionKey] = {}
      for (let userKey in agreement.data.users) {
        let singleReflection = agreement.data.users[userKey];
        this.agreementsByPositionsDict[agreement.positionKey][userKey] = singleReflection;
        // CHECK IF THE HEART IS LEFT FROM THE USER
        agreements.push(singleReflection);
      }
    }

    if (this.agreementsByPositionsArray.length === 0) {
      this.agreementsByPositionsArray = agreements;
    } else {
      /* CHECK IF THERE'S A NEW LIKE */
    }

    console.log("agreements POSITIONS")

    console.log(this.agreementsByPositionsArray);
    console.log(this.agreementsByPositionsDict);
    console.log(this.agreementsStatsByPositionsDict);

    this.agreementsByPositions.next(this.agreementsByPositionsArray);

  }

  processAgreementsByArguments(snapshotData: any) {
    let agreements: ArgumentAgreement[]= [];

    let agreementsByPosition: any = [];
    snapshotData.forEach((snap: { payload: { val: () => any, key:string }; }) =>{

      let obj = {
        data: snap.payload.val(),
        positionKey: snap.payload.key
      }
      agreementsByPosition.push(obj)
    })

    for (let ele of agreementsByPosition) {

      for (let argumentKey in ele.data) {
        this.agreementsStatsByArgumentsDict[argumentKey] = ele.data[argumentKey].total

        this.agreementsByArgumentsDict[argumentKey] = {}
        for (let userKey in ele.data[argumentKey].users) {
          let singleAgreement = ele.data[argumentKey].users[userKey];
          singleAgreement.argumentId = argumentKey;
          this.agreementsByArgumentsDict[argumentKey][userKey] = singleAgreement;
          agreements.push(singleAgreement);
        }
      }
    }

    this.agreementsByArgumentsArray = agreements;

    console.log("agreements ARGUMENTS");

    console.log(this.agreementsByArgumentsArray)
    console.log(this.agreementsByArgumentsDict)
    console.log(this.agreementsStatsByArgumentsDict)
  }

  async pushReflectionPosition(data:ReflectionPosition) {
    await this.db.object('positionDimensions/'+this.debateKey+'/'+ data.positionId + '/users/' + data.metadata.createdBy).set(data);
  }

  async pushReflectionArgument(data:ReflectionArgument) {
    console.log(data);
    console.log('claimDimensions/'+this.debateKey+'/'+ data.positionId + '/' + data.claimId +'/users/');
    await this.db.object('claimDimensions/'+this.debateKey+'/'+ data.positionId + '/' + data.claimId +'/users/'+ data.metadata.createdBy).set(data);
  }

  setPositionDefaultPlatformData(position:Position) {
    position.platformData= {
      agreements: [],
      agreementLastValue: null,
      agreementAverageValue: 0,
      agreementLastTS: new Date().getTime(),
      claims: [],
      contributors:[],
      currentReplyPosition: undefined,
      opposingClaims: [],
      parentPosition: null,
      showAgreementAlert: true,
      showOnScroll:false,
      state:'Neutral',
      supportingClaims: [],
      treeCollapsed:true,
      dateSince: this.statsService.getTimeSince(position.metadata.createdDate),
      reflectionsCounter:0,
      heartsCounter:0,
    }

    if (!position.platformData.agreementLastValue) {
      position.platformData.agreementAverageValue = 0;
    }

    return position;
  }

  setArgumentDefaultPlatformData(argument:Argument) {

    argument.platformData= {
      currentReplyClaim: null,
      dateSince: this.statsService.getTimeSince(argument.metadata.createdDate),
      state:'Neutral',
      agreements: [],
      agreementLastValue: null,
      agreementAverageValue: 0,
      agreementLastTS: new Date().getTime(),
      reflectionsCounter:0,
      heartsCounter:0,
      contributors:[]
    }

    return argument;
  }

  async getMetaInformation(url:string) {
    return await this.http.get('https://europe-west1-'+environment.firebase.projectId+'.cloudfunctions.net/scrapeURL?url='+url).toPromise();
  }


  async pushExternalClaimAttached(data:Recommendation, argument:Argument) {

    let dataToSave = JSON.parse(JSON.stringify(data));
    let newEvidence = this.db.list(
      'claims/'+this.debateKey+'/'+argument.positionId+'/'+argument.id+'/externalClaimsAttached/')
      .push(dataToSave);
    // this.db.object('discussionGroups/'+newDebate.key).update({id:newDebate.key})
    return newEvidence.key;
  }

}
