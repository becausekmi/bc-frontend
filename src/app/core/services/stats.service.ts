import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/compat/database';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private db: AngularFireDatabase) { }

  cards = {
    agree: {
      label: 'Most people would agree with this',
      key: 'agree'
    },
    polarized: {
      label: 'This is a very polarized position',
      key: 'polarized'
    },
    unclear: {
      label: 'It is clear I can trust this',
      key: 'clear'
    },
    unpolarized: {
      label: 'This should be prioritized',
      key: 'unpolarized'
    }
  }

  cardsArray = [
     {
      label: 'Most people would agree with this',
      key: 'agree'
    }, {
      label: 'This is a very polarized position',
      key: 'polarized'
    }, {
      label: 'It is clear I can trust this',
      key: 'clear'
    }, {
      label: 'This should be prioritized',
      key: 'unpolarized'
    }
  ]

  // USER CONTRIBUTIONS

  

  userContributionSubscription: any;
  userStatsRef!: AngularFireObject<any>;

  getArrayFromDict(data:any) {
    let output = [];
    for (const key in data) {
      output.push(data[key]);
    }
    return output;
  }

  getTimeSince(date:any) {

    var seconds = Math.floor((new Date().getTime() - date) / 1000);
  
    var interval = seconds / 31536000;
  
    if (interval > 1) {
      return Math.floor(interval) + " years";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
      return Math.floor(interval) + " months";
    }
    interval = seconds / 86400;
    if (interval > 1) {
      return Math.floor(interval) + " days";
    }
    interval = seconds / 3600;
    if (interval > 1) {
      return Math.floor(interval) + " hours";
    }
    interval = seconds / 60;
    if (interval > 1) {
      return Math.floor(interval) + " minutes";
    }
    return Math.floor(seconds) + " seconds";
  }

  getUserContributions(uid:string) {
    
    this.userStatsRef = this.db.object('usersContributions/' + uid + '/stats');
    let promiseUserContributions = new Promise((resolve,reject) => {
      this.userContributionSubscription = this.userStatsRef.snapshotChanges().subscribe(data=>{
        resolve(data.payload.val());
      });
    });

    return promiseUserContributions.then((ok:any) =>{
      if (ok) {
        if (!ok.arguments) {
          ok.arguments = 0;
        }
        if (!ok.positions) {
          ok.positions = 0;
        }
        if (!ok.cons) {
          ok.cons = 0;
        }
        if (!ok.pros) {
          ok.pros = 0;
        }
        return ok;
      } else {
        return null;
      }
    })
  }

  getUserContributionsByDebate(uid:string, debateId:string) {
    
    this.userStatsRef = this.db.object('usersContributions/' + uid + '/debates/' + debateId + '/stats');
    let promiseUserContributions = new Promise((resolve,reject) => {
      this.userContributionSubscription = this.userStatsRef.snapshotChanges().subscribe(data=>{
        resolve(data.payload.val());
      });
    });

    return promiseUserContributions.then((ok:any) =>{
      if (ok) {
        if (!ok.arguments) {
          ok.arguments = 0;
        }
        if (!ok.positions) {
          ok.positions = 0;
        }
        if (!ok.cons) {
          ok.cons = 0;
        }
        if (!ok.pros) {
          ok.pros = 0;
        }
        return ok;
      } else {
        return null;
      }
    })
  }

  async unsubscribeStats() {
    this.userContributionSubscription.unsubscribe();
  }
}
