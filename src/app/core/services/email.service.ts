import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/compat/database';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(
    private http:HttpClient,
    private db: AngularFireDatabase) { }

  async sendEmail(recipients:string,subject:string,html:string) {
    try {

      let res =  await this.http.post('https://europe-west1-'+environment.firebase.projectId+'.cloudfunctions.net/sendEmail', {
          recipients,
          html,
          subject
      }).toPromise();
      return res;
    } catch(err) {
      return err;
    }
  }

  getDiscussionGroupRequestEmailTemplate(user:any, group:any) {
   
    let text = `<br>Hello, <br><br> The following person has requested to join the group  <a href="https://bcause.app/profile/dashboard" target="blank"><b>`+group.title+`</b></a> for which you are an administrator:<br><br>
                <b>`+user.firstName+` `+user.lastName+` - `+user.email+`</b><br><br>

                NOTE: All group administrators for this group will have been emailed with this new join request. However, please make sure you check the request to join the group has been dealt with, and if not, please deal with it in a timely fashion.<br><br>
    
               `;

    let output = this.getTemplate(text);

    return output;
  }

  getDiscussionGroupRequestConfirmEmailTemplate(user:any, group:any) {
   
    let text = `<br>Dear `+user.firstName+` `+user.lastName+`, <br><br>Your request to join the group <b>`+group.title+`</b> on Bcause App has been approved.</b><br><br>
    We look forward to seeing you active in this group soon. Thank you for your interest.<br><br>`;

    let output = this.getTemplate(text);

    return output;
  }


  getTemplate(text:string) {

    return `<link rel="preconnect" href="https://fonts.googleapis.com">
              <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
              <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
      
              <body style="font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-style: normal;">
      
          <table style="width:600px">
              <tr>
                  <td colspan="2" style="background:#007FC1;padding:10px">
                      <a href="https://bcause.app" target="blank" >
                          <img width="200px" src="https://firebasestorage.googleapis.com/v0/b/bcause-alpha01.appspot.com/o/logo_white.png?alt=media&token=5623ca49-8ae5-4d53-9a40-b46786df486d" />
                      </a>
                  </td>
              </tr>
      
              <tr>
                  <td colspan="2">
                      <div style="font-size: medium;">
                          `+text+`
                      </div>
                  </td>
              </tr>
              <tr>
                  <td colspan="2">
                      <div style="font-size: medium;padding-top:10px;padding-bottom:10px;border-top:solid 1px #00476b;border-bottom:solid 1px #00476b;margin-top:20px;line-height:1.5">
                          The Bcause App Team<br>
                          <a href="https://bcause.app" target="blank">https://bcause.app</a>
                      </div>
                  </td>
              </tr>
      
              <tr>
                  <td colspan="2">
                      <div style="font-size: medium;padding-bottom:10px;line-height:1.5">
                        The Open University is incorporated by Royal Charter (RC 000391), an exempt charity in England & Wales and a charity registered in Scotland (SC 038302).
                      </div>
                  </td>
              </tr>
      
          </table>
      </body>`
  }
}
