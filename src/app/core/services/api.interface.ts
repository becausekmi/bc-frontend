import { Observable } from "rxjs";
import { Identity } from "src/app/auth/models/Identity";

export interface Api {

  getIdentity(): Observable<Identity | null>;

  // DEBATES

  fetchAllDebates(): any; // @todo fix type
  fetchPublicDebates(): any; // @todo fix type
  fetchDebate(debateId:string): any; // @todo fix type
  fetchDebatesByUser(user:any):any // @todo fix type
  fetchDebatesByGroup(groupId:any):any // @todo fix type
  fetchDebatesContributedByUser(user:any):any // @todo fix type
  addDebate(newDebate: any): any; // @todo fix type

  // SUMMARY

  fetchSummary(debateId:string): any; // @todo fix type

  // POSITIONS

  fetchPositionsByDebate(debateId:string):any; // @todo fix type
  fetchPositionsByUser(debateId:string, userId:string):any // @todo fix type
  fetchPosition(positionId: any):any // @todo fix type
  createPosition(newPosition: any):any // @todo fix type
  updatePosition(position:any):any // @todo fix type
  deletePosition(position:any):any // @todo fix type

  // ARGUMENTS

  fetchArgumentsByDebate(debateId:string):any // @todo fix type
  fetchArgumentsByUser(debateId:string, userId:string):any // @todo fix type
  fetchArgumentsByPosition(debateId:string, positionId:string):any // @todo fix type
  fetchArgument(argumentId: any):any // @todo fix type
  createArgument(newArgument: any):any // @todo fix type
  updateArgument(argument:any):any // @todo fix type
  deleteArgument(argument:any):any // @todo fix type

  // REFLECTIONS

  fetchReflectionsByDebate(debateId:string):any // @todo fix type
  fetchReflectionsByUser(debateId:string, userId:string):any // @todo fix type
  fetchReflectionsByPosition(debateId:string, positionId:string):any // @todo fix type
  fetchReflectionsByArgument(debateId:string, argumentId: any):any // @todo fix type
  createReflection(reflection: any):any // @todo fix type
  updateReflection(reflection:any):any // @todo fix type
  deleteReflection(reflection:any):any // @todo fix type

  // AGREEMENTS

  fetchArgumentsAgreementsByDebate(debate:any):any // @todo fix type
  fetchPositionsAgreementsByDebate(debate:any):any // @todo fix type
  fetchAgreementsByDebate(debate:any):any // @todo fix type
  fetchAgreementsByUser(user:string):any // @todo fix type
  fetchAgreementsByPosition(position:any):any // @todo fix type
  fetchAgreementsByArgument(debateId:string, argumentId: any):any // @todo fix type
  createAgreement(agreement: any):any // @todo fix type
  updateAgreement(heart:any):any // @todo fix type
  deleteAgreement(heart:any):any // @todo fix type
  fetchLastUserAgreementByPosition(position:any, uid:string) :any
  fetchLastUserAgreementByArgument(position:any, uid:string) :any

  // PARTICIPANTS
  
  fetchParticipantsByDebate(debateId:any):any // @todo fix type
  fetchParticipant(debateId:any, participantId:any):any // @todo fix type
  fetchParticipantContributionsStatsAllDebates(user:any):any // @todo fix type
  fetchParticipantContributionsStatsByDebate(user:any, debate:any):any // @todo fix type
  createParticipant(participant: any):any // @todo fix type
  updateParticipant(participant:any):any // @todo fix type
  deleteParticipant(participant:any):any // @todo fix type

  // HEARTS

  fetchHeartsByDebate(debate:any):any // @todo fix type
  fetchHeartsByPositionAndUser(position:any, user:any):any // @todo fix type
  fetchHeartsByArgumentAndUser(argument:any, user:any):any // @todo fix type
  fetchHeartsByPosition(position:any):any // @todo fix type
  fetchHeartsByArgument(argument:any):any // @todo fix type
  createHeartOnPosition(heart:any, position: any, user:any):any // @todo fix type
  createHeartOnArgument(heart:any,argument: any, user:any):any // @todo fix type
  deleteHeartOnPosition(position: any, user:any):any // @todo fix type
  deleteHeartOnArgument(argument: any, user:any):any // @todo fix type

  // REFLECTIONS

  fetchReflectionsCounterByPosition(position:any):any;

  // DISCUSSION GROUPS

  fetchAllGroups():any;
  fetchGroup(id:any):any;
  fetchGroupsByAdmin(userData:any):any;
  fetchGroupsByUser(userData:any):any;
  updateDiscussionGroup(group:any):any;

  // USERS

  fetchUserByEmail(email:string):any;
}