import { Injectable } from '@angular/core';
import { from, Observable, Observer, of, zip } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { create } from 'ipfs-http-client';
import Web3 from 'web3';
// import { BlockHeader, Block } from 'web3-eth' // ex. package types

// import { get as _get, pick as _pick, omit as _omit, merge as _merge } from 'lodash';

import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from '../../models/debate';
import { Api } from '../api.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostgresService implements Api {


  // private identity$!: Observable<Identity | null>;

  // private debates$: Observable<Debate[]>;

  constructor() {
  }

  getIdentity(): Observable<Identity | null> {
    throw new Error('Method not implemented.');
  }

  fetchAllDebates() {
    throw new Error('Method not implemented.');
  }

  fetchPublicDebates() {
    throw new Error('Method not implemented.');
  }

  fetchDebate() {
    throw new Error('Method not implemented.');
  }

  fetchDebatesByUser() {
    throw new Error('Method not implemented.');
  }
  
  fetchDebatesByGroup() {
    throw new Error('Method not implemented.');
  }
  
  fetchDiscussionGroupsByUser() {
    throw new Error('Method not implemented.');
  }

  fetchDebatesContributedByUser(user: any) {
    throw new Error('Method not implemented.');
  }

  fetchSummary() {
    throw new Error('Method not implemented.');
  }

  addDebate(newDebate: any): any { // @todo fix type
  }

  // POSITIONS
  // POSITIONS
  // POSITIONS

  fetchPositionsByDebate(debateId:string) {
    throw new Error('Method not implemented.');
  }

  fetchPositionsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchPosition(positionId: any) {
    throw new Error('Method not implemented.');
  }

  createPosition(newPosition: any) {
    throw new Error('Method not implemented.');
  }

  updatePosition(position:any) {
    throw new Error('Method not implemented.');
  }

  deletePosition(position:any) {
    throw new Error('Method not implemented.');
  }

  // ARGUMENTS
  // ARGUMENTS
  // ARGUMENTS

  fetchArgumentsByDebate(debateId:any) {
    throw new Error('Method not implemented.');
  }

  fetchArgumentsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchArgumentsByPosition(debateId:string, positionId:string) {
    throw new Error('Method not implemented.');
  }

  fetchArgument(argumentId: any) {
    throw new Error('Method not implemented.');
  }

  createArgument(newArgument: any) {
    throw new Error('Method not implemented.');
  }

  updateArgument(argument:any) {
    throw new Error('Method not implemented.');
  }

  deleteArgument(argument:any) {
    throw new Error('Method not implemented.');
  }

  // REFLECTIONS
  // REFLECTIONS
  // REFLECTIONS

  fetchReflectionsByDebate(debateId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByPosition(debateId:string, positionId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByArgument(debateId:string, argumentId: any) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsCounterByPosition(position:any) {
    throw new Error('Method not implemented.');
  }

  createReflection(reflection: any) {
    throw new Error('Method not implemented.');
  }

  updateReflection(reflection:any) {
    throw new Error('Method not implemented.');
  }

  deleteReflection(reflection:any) {
    throw new Error('Method not implemented.');
  }

  // AGREEMENTS
  // AGREEMENTS
  // AGREEMENTS

  fetchAgreementsCounterByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }
  
  fetchLastUserAgreementByPosition(position:any, uid:string) {
    throw new Error('Method not implemented.');
  }

  
  fetchLastUserAgreementByArgument(argument:any, uid:string) {
    throw new Error('Method not implemented')
  }

  fetchArgumentsAgreementsByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchPositionsAgreementsByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByDebate(debateId:string) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByUser(user:any) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByPosition(position:any) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByArgument(debateId:string, argumentId: any) {
    throw new Error('Method not implemented.');
  }

  createAgreement(agreement: any) {
    throw new Error('Method not implemented.');
  }

  updateAgreement(heart:any) {
    throw new Error('Method not implemented.');
  }

  deleteAgreement(heart:any) {
    throw new Error('Method not implemented.');
  }

  // PARTICIPANTS
  // PARTICIPANTS
  // PARTICIPANTS


  fetchParticipantsByDebate(debateId:any) {
    throw new Error('Method not implemented.');
  }

  fetchParticipant(debateId:any, participantId:any) {
    throw new Error('Method not implemented.');
  }

  fetchParticipantContributionsStatsByDebate(user:any, debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchParticipantContributionsStatsAllDebates(user:any) {
    throw new Error('Method not implemented.');
  }

  createParticipant(participant: any) {
    throw new Error('Method not implemented.');
  }

  updateParticipant(participant:any) {
    throw new Error('Method not implemented.');
  }

  deleteParticipant(participant:any) {
    throw new Error('Method not implemented.');
  }

  // HEARTS
  // HEARTS
  // HEARTS

  fetchHeartsByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByPositionAndUser(position:any, user:any) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByArgumentAndUser(argument:string, user:any) {
    throw new Error('Method not implemented.');
  }


  fetchHeartsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByPosition(position:any) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByArgument(argument:any) {
    throw new Error('Method not implemented.');
  }

  createHeartOnPosition(heart:any, position:any, userData:any) {
    throw new Error('Method not implemented.');
  }

  createHeartOnArgument(heart:any, argument:any, userData:any) {
    throw new Error('Method not implemented.');
  }

  deleteHeartOnPosition(position:any, user:any) {
    throw new Error('Method not implemented.');
  }

  deleteHeartOnArgument(argument:any, user:any) {
    throw new Error('Method not implemented.');
  }

  
  fetchGroupsByAdmin(userData:any) { 
    throw new Error('Method not implemented.');
  }

  fetchGroupsByUser(userData:any) {
    throw new Error('Method not implemented.');
  }

  updateDiscussionGroup(group:any) {
    throw new Error('Method not implemented.');
  }

  
  fetchGroup(id:any) {
    throw new Error('Method not implemented.');
  }
  
  fetchAllGroups() {
    throw new Error('Method not implemented.');
  }

  // USERS

  fetchUserByEmail(email:string) {
    throw new Error('Method not implemented');
  }

}