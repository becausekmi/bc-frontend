import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Database, ref, onValue, objectVal, listVal, query } from '@angular/fire/database';
import { Observable, of } from 'rxjs';
import { map, mergeMap, take, tap } from 'rxjs/operators';
import { get as _get, pick as _pick, omit as _omit, merge as _merge } from 'lodash';

import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from '../../models/debate';
import { Api } from '../api.interface';
import { equalTo, orderByChild } from 'firebase/database';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService implements Api {

  // private identity$!: Observable<Identity | null>;s

  // private debates$: Observable<Debate[]>;

  constructor(
    private rtdb: Database,
    private db: AngularFireDatabase,
    private auth: AngularFireAuth,
  ) { }


  fetchCollection(reference:string):Observable<any> {
    const doc = ref(this.rtdb, reference);
    let collection$ = listVal(doc)
    return collection$;
  }

  fetchCollectionOrderBy(reference:string, key:string):Observable<any> {
    const doc = ref(this.rtdb, reference);
    const q = query(doc, orderByChild(key));
    let collection$ = listVal(q)
    return collection$;
  }
  

  fetchCollectionWithFilterEqualTo(reference:string, key:string, value:any):Observable<any> {
    const doc = ref(this.rtdb, reference);
    const q = query(doc, orderByChild(key), equalTo(value));
    let collection$ = listVal(q)
    return collection$;
  }
  
  fetchDocumentRealTime(reference:string):Observable<any>{
    const doc = ref(this.rtdb, reference);
    return new Observable(
      (observer)=>{
        onValue(doc, (snap) =>{
          observer.next(snap.val())
        })
      }
    );
  }

  fetchDocument(reference:string):Observable<any>{
    const doc = ref(this.rtdb, reference);
    let document$ = objectVal(doc);
    return document$;
  }

  

  getIdentity(): Observable<Identity | null> {
    // if (this.profile$ === null) {
    // }
    // return this.profile$;
    return this.auth.user.pipe(
      // tap(fireUser => console.log(fireUser)), // DEBUG
      mergeMap((fireUser: any) => this.mapFireUserToIdentity(fireUser))
    );
  }

  private mapFireUserToIdentity(fireUser: any): Observable<Identity | null> {
    if (fireUser) {
      
      return this.db.object(`users/${fireUser.uid}`).valueChanges()
        .pipe(
          take(1),
          map((fireUserExtention: any) => {
            //console.log("MAP FIRE USER IDENTITY", fireUserExtention)
            if (fireUserExtention) {
              if (!fireUserExtention.defaultPseudo && fireUserExtention.pseudo) {
                fireUserExtention.defaultPseudo = fireUserExtention.pseudo;
              } else if (!fireUserExtention.defaultPseudo) {
                fireUserExtention.defaultPseudo = fireUserExtention.firstName + fireUserExtention.lastName;
              }
  
              const fireUserReduced = _pick(fireUser, [
                'uid', 'displayName',
                'email', 'emailVerified', 'defaultPseudo', 'pseudo',
                'isAnonymous','phoneNumber', 'photoURL',
              ]);
  
              return _merge(fireUserReduced, fireUserExtention);

            }
          })
        );
    }
    return of(null);
  }

  // DEBATEs
  // DEBATEs
  // DEBATEs

  fetchAllDebates() {
    return this.fetchCollection('debates')
  }
  
  fetchPublicDebates() {
    return this.fetchCollectionWithFilterEqualTo('debates', 'visibility', 'public')
  }

  fetchDebatesByUser(user: any) {
    console.log(user);
    return this.fetchCollectionWithFilterEqualTo('debates', 'metadata/createdBy', user);
  }

  
  fetchDebatesByGroup(groupId: any) {
    return this.fetchCollectionWithFilterEqualTo('debates', 'discussionGroup', groupId);
  }
  

  fetchDebatesContributedByUser(user: any) {
    return this.fetchDocument('usersContributions/'+user.uid+'/debates')
  }

  fetchDebate(debateId:string) {
    return this.fetchDocument('debates/'+debateId)
  }

  addDebate(newDebate: any) {
    newDebate.type = 'ISSUE';
    let ref = this.db.list('debates/').push(newDebate);
    this.db.object('debates/'+ref.key).update({id:ref.key})
    return ref.key;
  }  

  // SUMMARY
  // SUMMARY
  // SUMMARY

  fetchSummary(debateId:string) {
    return this.fetchDocument('summary/'+debateId)
  }

  // POSITIONS
  // POSITIONS
  // POSITIONS

  fetchPositionsByDebate(debateId:string) {
    return this.fetchCollectionOrderBy('positions/' + debateId + '/','metadata/createdDate')
    .pipe(
      map(res => {
        const array :any[]= [];
        res.forEach((d:any) => array.push(d));
        return array;
      }),
      tap((res)=> console.log('positions' , res)),
    )
  }

  fetchPositionsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchPosition(positionId: any) {
    throw new Error('Method not implemented.');
  }

  async createPosition(position: any) {
    let dataToSave = JSON.parse(JSON.stringify(position));
    delete dataToSave.platformData;
    console.log(dataToSave);
    let ref = await this.db.list('positions/').push(position);
    this.db.object('positions/'+position.debateId+'/'+ ref.key).update({id:ref.key});
    return ref.key;
  }

  updatePosition(position:any) {
    throw new Error('Method not implemented.');
  }

  async deletePosition(position:any) {
    await this.db.object('positions/'+position.debateId+'/'+ position.id).remove();
  }

  // ARGUMENTS
  // ARGUMENTS
  // ARGUMENTS

  fetchArgumentsByDebate(debateId:any) {
    
    return this.fetchCollection('claims/' + debateId + '/')
    .pipe(
      map(res => {
        const array :any[]= [];
        res.forEach((d:any) => array.push(d));
        return array;
      }),
      map((res)=>{
        const array :any[]= [];
        res.forEach((d:any) =>
        {
          for (let key in d) {
            array.push(d[key]);
          }
        }
        );
        return array;
      }),
      tap((res)=> console.log('arguments' , res)),
    )
  }

  fetchArgumentsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchArgumentsByPosition(debateId:string, positionId:string) {
    return this.fetchCollection('claims/'+debateId+'/'+positionId)
  }


  fetchArgument(argumentId: any) {
    throw new Error('Method not implemented.');
  }

  async createArgument(argument: any) {
    let dataToSave = JSON.parse(JSON.stringify(argument));
    delete dataToSave.platformData;
    let newArgument = await this.db.list('claims/'+argument.debateId+'/'+ argument.positionId).push(dataToSave);
    await this.db.object('claims/'+argument.debateId+'/'+ argument.positionId + '/' + newArgument.key).update({id:newArgument.key});
    return newArgument.key;
  }

  updateArgument(argument:any) {
    throw new Error('Method not implemented.');
  }

  async deleteArgument(argument:any) {
    await this.db.object('claims/'+argument.debateId+'/'+ argument.positionId+'/'+argument.id).remove();
  }

  // REFLECTIONS
  // REFLECTIONS
  // REFLECTIONS

  fetchReflectionsByDebate(debateId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByPosition(debateId:string, argumentId: any) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByArgument(debateId:string, argumentId: any) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsCounterByPosition(position:any) {
    return this.fetchCollection('positionDimensions/' + position.debateId + '/' + position.id+'/users/').pipe(map(res => res.length))
  }

  fetchReflectionsCounterByDebate(debate:any) {
    return this.fetchCollection('positionDimensions/' + debate.id + '/').pipe(map(res => res.length))
  }

  createReflection(reflection: any) {
    throw new Error('Method not implemented.');
  }

  updateReflection(reflection:any) {
    throw new Error('Method not implemented.');
  }

  deleteReflection(reflection:any) {
    throw new Error('Method not implemented.');
  }

  // AGREEMENTS
  // AGREEMENTS
  // AGREEMENTS

  fetchAgreementsByDebate(debate:any) {
    return this.fetchCollection('positionAgreements/' + debate.id + '/')
    .pipe(
      map((res)=>{
        const array :any[]= [];
        res.forEach((d:any) =>
        {
          for (let userKey in d.users) {
            array.push(d.users[userKey]);
          }
        }
        );
        return array;
      }),
    )
  }

  
  fetchLastUserAgreementByPosition(position:any, uid:string) {
    console.log('positionAgreements/'+position.debateId+'/'+position.id+'/users/'+uid);
    return this.fetchDocument('positionAgreements/'+position.debateId+'/'+position.id+'/users/'+uid);
  }

  
  fetchLastUserAgreementByArgument(argument:any, uid:string){
    return this.fetchDocument('claimAgreements/'+argument.debateId+'/'+argument.positionId+'/'+argument.id+'/users/'+uid);
  }

  fetchAgreementsByUser(user:any) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByPosition(position:any) {
    throw new Error('Method not implemented.');
  }

  fetchArgumentsAgreementsByDebate(debate:any) {
    return this.fetchCollection('claimAgreements/' + debate.id + '/')
    .pipe(
      map(res => {
        const array :any[]= [];
        res.forEach((d:any) => array.push(d));
        return array;
      }),
      map((res)=>{
        const array :any[]= [];
        res.forEach((d:any) =>
        {
          for (let key in d) {
            for (let userKey in d[key].users) {
              array.push(d[key].users[userKey]);
            }
          }
        }
        );
        return array;
      }),
    )
  }

  fetchPositionsAgreementsByDebate(debate:any) {
    return this.fetchCollection('positionAgreements/' + debate.id + '/')
    .pipe(
      map((res)=>{
        const array :any[]= [];
        res.forEach((d:any) =>
        {
          for (let userKey in d.users) {
            array.push(d.users[userKey]);
          }
        }
        );
        return array;
      }),
    )
  }

  fetchAgreementsByArgument(debateId:string, argumentId: any) {
    throw new Error('Method not implemented.');
  }

  createAgreement(agreement: any) {
    throw new Error('Method not implemented.');
  }

  updateAgreement(heart:any) {
    throw new Error('Method not implemented.');
  }

  deleteAgreement(heart:any) {
    throw new Error('Method not implemented.');
  }

  // PARTICIPANTS
  // PARTICIPANTS
  // PARTICIPANTS

  fetchParticipantsByDebate(debateId:any) {
    return this.fetchCollection('participants/'+debateId);
  }

  fetchParticipantContributionsStatsByDebate(user:any, debate:any) {
    return this.fetchDocument('usersContributions/'+user.uid+'/debates/'+debate.id+'/stats');
  }

  fetchParticipant(debateId:string, id:string) {
    return this.fetchDocument('participants/'+debateId+'/'+id);
  }

  fetchParticipantContributionsStatsAllDebates(user:any) {
    return this.fetchDocument('usersContributions/'+user.uid+'/stats');
  }

  async createParticipant(participant: any) {
    await this.db.object('participants/'+participant.debateId+'/'+ participant.id).set(participant);
  }

  updateParticipant(participant:any) {
    throw new Error('Method not implemented.');
  }

  deleteParticipant(participant:any) {
    throw new Error('Method not implemented.');
  }

  // HEARTS
  // HEARTS
  // HEARTS

  fetchHeartsByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByPositionAndUser(position:any, user:any) {
    return this.fetchDocument('positionHearts/'+position.debateId+'/'+ position.id + '/users/'+user.uid)
  }
  
  fetchHeartsByArgumentAndUser(argument:any, user:any) {
    return this.fetchDocument('claimHearts/' + argument.debateId + '/' + argument.positionId+'/'+ argument.id + '/users/'+user.uid)
  }

  fetchHeartsByPosition(position:any) {
    return this.fetchCollection('positionHearts/'+position.debateId+'/'+ position.id+'/users').pipe(map(res => res.length))
  }

  fetchHeartsByArgument(argument:any) {
    return this.fetchCollection('claimHearts/' + argument.debateId + '/' + argument.positionId+'/'+ argument.id + '/users/').pipe(map(res => res.length))
  }

  // RICCARDO -> come strutturare il salvataggio dei dati?
  async createHeartOnPosition(heart:any, position: any, user:any) {
    await this.db.object('positionHearts/'+position.debateId+'/'+ position.id + '/users/' + user.uid).set(heart);
  }

  async createHeartOnArgument(heart:any, argument: any, user:any) {
    await this.db.object('claimHearts/'+argument.debateId+'/'+ argument.positionId+'/'+ argument.id + '/users/' + user.uid).set(heart);
  }

  async deleteHeartOnPosition(position:any, user:any) {
    await this.db.object('positionHearts/'+position.debateId+'/'+ position.id + '/users/' + user.uid).remove();
  }

  async deleteHeartOnArgument(argument:any, user:any) {
    await this.db.object('claimHearts/'+argument.debateId+'/'+ argument.positionId+'/'+ argument.id + '/users/' + user.uid).remove();
  }

  // DISCUSSION GROUPS

  fetchGroupsByAdmin(userData:any) {
    
    return this.fetchCollection('discussionGroups').pipe(
      map((res)=>{
        const array :any[]= [];
        res.forEach((d:any) =>
        {
          for (let user of d.admins) {
            if (user === userData.uid || user === userData.email) {
              array.push(d)
            }
          }
        }
        );
        return array;
      }),
    )
  }

  fetchGroupsByUser(userData:any) {
    return this.fetchCollection('discussionGroups').pipe(
      map((res)=>{
        const array :any[]= [];
        res.forEach((d:any) =>
        {
          if (d && d.users) {
            for (let user of d.users) {
              if (user === userData.uid || user === userData.email) {
                array.push(d)
              }
            }
          }
        }
        );
        return array;
      }),
    )
  }

  
  fetchDiscussionGroupsByUser(user: any) {
    return this.fetchCollectionWithFilterEqualTo('discussionGroups', 'metadata/createdBy', user);
  }

  async updateDiscussionGroup(group: any) {
    let data = await this.db.object('discussionGroups/'+group.id).update(group);
    return data;
  }

  
  fetchGroup(id:any) {
    return this.fetchDocument('discussionGroups/'+id)
  }
  
  fetchAllGroups() {
    return this.fetchCollection('discussionGroups')
  }

  
  // USERS

  fetchUserByEmail(email:string) {
    return this.fetchCollectionWithFilterEqualTo('users', 'email', email);
  }
  

}