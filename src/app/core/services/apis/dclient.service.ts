import { Injectable } from '@angular/core';
import { from, Observable, Observer, of, zip } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { create } from 'ipfs-http-client';
import Web3 from 'web3';
// import { BlockHeader, Block } from 'web3-eth' // ex. package types

// import { get as _get, pick as _pick, omit as _omit, merge as _merge } from 'lodash';

import { Identity } from 'src/app/auth/models/Identity';
import { Debate } from '../../models/debate';
import { Api } from '../api.interface';

import debateContractJson from 'src/contracts/Debates.json';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DClientService implements Api {

  private ipfs: any;
  private web3: any;
  private debateContract: any;
  private debateContractConf: any = debateContractJson;
  private networkId: string = environment.eth.networkId;

  // private identity$!: Observable<Identity | null>;

  // private debates$: Observable<Debate[]>;

  constructor() {
    // @see https://github.com/Layla-Koenig/angular-web3
    // npm install crypto-browserify stream-browserify assert stream-http https-browserify os-browserify
    // Add in tsconfig.ts under compilerOptions
    // "paths": {
    //   "crypto": ["./node_modules/crypto-browserify"],
    //   "stream": ["./node_modules/stream-browserify"],
    //   "assert": ["./node_modules/assert"],
    //   "http": ["./node_modules/stream-http"],
    //   "https": ["./node_modules/https-browserify"],
    //   "os": ["./node_modules/os-browserify"]
    // },


    // Init IPFS client
    // const ipfsUrl = 'http://127.0.0.1:5001';
    // const ipfsUrl = new URL('http://127.0.0.1:5001');
    // this.ipfs = create(ipfsUrl);
    // const ipfsOpts = { port: 5001 };
    // this.ipfs = create(ipfsOpts);
    /*
    ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin "[\"example.com\"]"
    ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods "[\"PUT\", \"GET\", \"POST\"]"
    ipfs config --json API.HTTPHeaders.Access-Control-Allow-Credentials "[\"true\"]"
    */
    this.ipfs = create();

    // Init WEB3 client
    // @see https://github.com/Layla-Koenig/angular-web3
    // Append in polyfills.ts the follow lines
    // (window as any).global = window;
    // import { Buffer } from 'buffer';
    // global.Buffer = Buffer;
    // global.process = {
    //   env: { DEBUG: undefined },
    //   version: '',
    //   nextTick: require('next-tick'),
    // } as any;

    // const providerUrl = 'http://127.0.0.1:8545';
    // this.web3 = new Web3(new Web3.providers.HttpProvider(providerUrl));
    const providerUrl = 'ws://127.0.0.1:8546';
    this.web3 = new Web3(providerUrl);
    // this.web3 = new Web3(new Web3.providers.WebsocketProvider(providerUrl));

    // const accounts = this.web3.eth.getAccounts();
    // console.log('eth accounts', accounts);
    // this.web3.eth.defaultAccount = '0x29B0249014d751048689bD3DE253bBCab2732fbb'; // @todo temp, take from identity manager
    this.getAccount(0).subscribe((account: string) => {
      this.web3.eth.defaultAccount = account;
    });

    this.debateContract = new this.web3.eth.Contract(
      this.debateContractConf.abi,
      '0x4d937a4b08D6f410F8Ac88CFE7c4935266577fAB'
      //this.debateContractConf.networks[this.networkId].address
    );
  }

  getIdentity(): Observable<Identity | null> {
    throw new Error('Method not implemented.');
  }

  /*
    Observable.create(
    observer => firebase
        .auth()
        .onAuthStateChanged((user) => {
          observer.next(!!user)
        });
    );
  */
  fetchAllDebates() {
  // fetchAllDebates(): Observable<Debate[]> {
    return this.getAccount(0)
      .pipe(
        tap(res => console.log('account', res)),
        // switchMap((account: string) => from(
        //   this.debateContract.methods
        //     .getChunk(0, 10)
        //     // .call()
        //     .call({ from: account })
        // )),
        tap(res => console.log('fetchAllDebates()', res)),
        map(() => [])
      );

    //   from(
    //     this.debateContract.methods
    //       .getDebates()
    //       .call()
    //     // .call({ from: account })

  // this.debateContract.events
  //   .DebateAdded()
  //   .on('data', (event: any) => console.log('DebateAdded.data', event))
  //   .on('changed', (changed: any) => console.log('DebateAdded.changed', changed))
  //   .on('error', (err: any) => console.log('DebateAdded.error', err))
  //   .on('connected', (str: any) => console.log('DebateAdded.connected', str))
  // ;

  return of([]);

    // return new Observable(observer => this.debateContract.events
    //   .DebateAdded()
    //   .on('data', (event: any) => {
    //     console.log('aaaaaa', event);
    //     // observer.next(!!user) // @todo
    //     observer.next([]);
    //   })
    //   // .on('error', console.error)
    // );
  }

  fetchPublicDebates() {
    // fetchAllDebates(): Observable<Debate[]> {
      return this.getAccount(0)
        .pipe(
          tap(res => console.log('account', res)),
          // switchMap((account: string) => from(
          //   this.debateContract.methods
          //     .getChunk(0, 10)
          //     // .call()
          //     .call({ from: account })
          // )),
          tap(res => console.log('fetchAllDebates()', res)),
          map(() => [])
        );
  
      //   from(
      //     this.debateContract.methods
      //       .getDebates()
      //       .call()
      //     // .call({ from: account })
  
    // this.debateContract.events
    //   .DebateAdded()
    //   .on('data', (event: any) => console.log('DebateAdded.data', event))
    //   .on('changed', (changed: any) => console.log('DebateAdded.changed', changed))
    //   .on('error', (err: any) => console.log('DebateAdded.error', err))
    //   .on('connected', (str: any) => console.log('DebateAdded.connected', str))
    // ;
  
    return of([]);
  
      // return new Observable(observer => this.debateContract.events
      //   .DebateAdded()
      //   .on('data', (event: any) => {
      //     console.log('aaaaaa', event);
      //     // observer.next(!!user) // @todo
      //     observer.next([]);
      //   })
      //   // .on('error', console.error)
      // );

  }

  fetchDebate() {
    throw new Error('Method not implemented.');
  }

  fetchDebatesByUser() {
    throw new Error('Method not implemented.');
  }

  fetchDebatesByGroup() {
    throw new Error('Method not implemented.');
  }
  
  fetchDiscussionGroupsByUser() {
    throw new Error('Method not implemented.');
  }

  fetchDebatesContributedByUser(user: any) {
    throw new Error('Method not implemented.');
  }

  fetchSummary() {
    throw new Error('Method not implemented.');
  }

  // const debatesCollection = Object.entries(data.debates);
  // for (const [debateId, aDebate] of debatesCollection) {
  //   await client.files.write(
  //     `/bcause/debates/${debateId}`,
  //     JSON.stringify(aDebate), {
  //       create: true,
  //       parents: true,
  //     })
  // }
  /*
  ADDRESS
  0xa5be72dDd283EeAbF5BEbbE9669798f74d906f12
  */

  addDebate(newDebate: any): any { // @todo fix type
    return this.addResourceOnIPFS(newDebate)
      .pipe(
        // tap(res => console.log('dclient.addDebate():', 'pushed debate in IPFS', res)), // DEBUG
        switchMap(() => zip(
          this.getCID(newDebate.mfs),
          this.getAccount(0) // @todo SELECT account of the logged user
        )),
        switchMap(([cid, account]) => from(
          this.debateContract.methods.add(
            newDebate.id,
            cid,
            newDebate.mfs,
            newDebate.metadata.createdBy
          // ).send()
          ).send({ from: account })
          // ).send({ from: this.web3.eth.defaultAccount })
        )),
        tap(res => console.log('dclient.addDebate():', 'sent debate on chain', res)), // DEBUG
      );
  }

  private getAccount(index: number): Observable<string> {
    return from(this.web3.eth.getAccounts())
      .pipe(
        map((accounts: any) => accounts[index])
      );
  }

  private getCID(mfsPath: string): Observable<string> {
    return from(this.ipfs.files.stat(mfsPath))
      .pipe(
        map((res: any) => res.cid.toString())
      );
  }

  private addResourceOnIPFS(resource: any) { // @todo type HasMFS or HasPath or HasFQDN
    return from(
      this.ipfs.files.write(
        resource.mfs,
        JSON.stringify(resource), {
          create: true,
          parents: true,
        }
      )
    );
  }

  // POSITIONS
  // POSITIONS
  // POSITIONS

  fetchPositionsByDebate(debateId:string) {
    throw new Error('Method not implemented.');
  }

  fetchPositionsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchPosition(positionId: any) {
    throw new Error('Method not implemented.');
  }

  createPosition(newPosition: any) {
    throw new Error('Method not implemented.');
  }

  updatePosition(position:any) {
    throw new Error('Method not implemented.');
  }

  deletePosition(position:any) {
    throw new Error('Method not implemented.');
  }

  // ARGUMENTS
  // ARGUMENTS
  // ARGUMENTS

  fetchArgumentsByDebate(debateId:any) {
    throw new Error('Method not implemented.');
  }

  fetchArgumentsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchArgumentsByPosition(debateId:string, positionId:string) {
    throw new Error('Method not implemented.');
  }

  fetchArgument(argumentId: any) {
    throw new Error('Method not implemented.');
  }

  createArgument(newArgument: any) {
    throw new Error('Method not implemented.');
  }

  updateArgument(argument:any) {
    throw new Error('Method not implemented.');
  }

  deleteArgument(argument:any) {
    throw new Error('Method not implemented.');
  }

  // REFLECTIONS
  // REFLECTIONS
  // REFLECTIONS

  fetchReflectionsByDebate(debateId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByPosition(debateId:string, positionId:string) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsByArgument(debateId:string, argumentId: any) {
    throw new Error('Method not implemented.');
  }

  fetchReflectionsCounterByPosition(position:any) {
    throw new Error('Method not implemented.');
  }

  createReflection(reflection: any) {
    throw new Error('Method not implemented.');
  }

  updateReflection(reflection:any) {
    throw new Error('Method not implemented.');
  }

  deleteReflection(reflection:any) {
    throw new Error('Method not implemented.');
  }

  // AGREEMENTS
  // AGREEMENTS
  // AGREEMENTS

  fetchAgreementsCounterByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }
  
  fetchLastUserAgreementByPosition(position:any, uid:string) {
    throw new Error('Method not implemented.');
  }

  
  fetchLastUserAgreementByArgument(argument:any, uid:string) {
    throw new Error('Method not implemented')
  }

  fetchArgumentsAgreementsByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchPositionsAgreementsByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByDebate(debateId:string) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByUser(user:any) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByPosition(position:any) {
    throw new Error('Method not implemented.');
  }

  fetchAgreementsByArgument(debateId:string, argumentId: any) {
    throw new Error('Method not implemented.');
  }

  createAgreement(agreement: any) {
    throw new Error('Method not implemented.');
  }

  updateAgreement(heart:any) {
    throw new Error('Method not implemented.');
  }

  deleteAgreement(heart:any) {
    throw new Error('Method not implemented.');
  }

  // PARTICIPANTS
  // PARTICIPANTS
  // PARTICIPANTS


  fetchParticipantsByDebate(debateId:any) {
    throw new Error('Method not implemented.');
  }

  fetchParticipant(debateId:any, participantId:any) {
    throw new Error('Method not implemented.');
  }

  fetchParticipantContributionsStatsByDebate(user:any, debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchParticipantContributionsStatsAllDebates(user:any) {
    throw new Error('Method not implemented.');
  }

  createParticipant(participant: any) {
    throw new Error('Method not implemented.');
  }

  updateParticipant(participant:any) {
    throw new Error('Method not implemented.');
  }

  deleteParticipant(participant:any) {
    throw new Error('Method not implemented.');
  }

  // HEARTS
  // HEARTS
  // HEARTS

  fetchHeartsByDebate(debate:any) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByPositionAndUser(position:any, user:any) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByArgumentAndUser(argument:string, user:any) {
    throw new Error('Method not implemented.');
  }


  fetchHeartsByUser(debateId:string, userId:string) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByPosition(position:any) {
    throw new Error('Method not implemented.');
  }

  fetchHeartsByArgument(argument:any) {
    throw new Error('Method not implemented.');
  }

  createHeartOnPosition(heart:any, position:any, userData:any) {
    throw new Error('Method not implemented.');
  }

  createHeartOnArgument(heart:any, argument:any, userData:any) {
    throw new Error('Method not implemented.');
  }

  deleteHeartOnPosition(position:any, user:any) {
    throw new Error('Method not implemented.');
  }

  deleteHeartOnArgument(argument:any, user:any) {
    throw new Error('Method not implemented.');
  }

  
  fetchGroupsByAdmin(userData:any) { 
    throw new Error('Method not implemented.');
  }

  fetchGroupsByUser(userData:any) {
    throw new Error('Method not implemented.');
  }

  updateDiscussionGroup(group:any) {
    throw new Error('Method not implemented.');
  }
  
  fetchGroup(id:any) {
    throw new Error('Method not implemented.');
  }

  fetchAllGroups() {
    throw new Error('Method not implemented.');
  }

  // USERS

  fetchUserByEmail(email:string) {
    throw new Error('Method not implemented');
  }
}