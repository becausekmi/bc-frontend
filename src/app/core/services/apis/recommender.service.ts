// import { Injectable } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/compat/auth';
// import { AngularFireDatabase } from '@angular/fire/compat/database';
// import { Database, ref, onValue, objectVal, listVal, query } from '@angular/fire/database';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ArticleAuthor, Data, Query, Recommendation, Response, RecommendationRoot } from '../../models/recommendation-query';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RecommenderService {

  baseurl = 'https://master03.ou.bcause.app:5001';// recommender endpoint in GCE VM

  constructor(private http: HttpClient) {

  }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  // GET
  query(environment_name: string,
    debate_id: string,
    position_id: string,
    argument_id: string) {
    let apiURL = `${this.baseurl}/query/${environment_name}/${debate_id}/${position_id}/${argument_id}`;
    return this.http.get(apiURL).toPromise();
  }
  //   : Observable<RecommendationRoot> {
  //   console.log("querying with", environment_name, debate_id, position_id, argument_id);
  //   let r = this.http
  //     .get<RecommendationRoot>(this.baseurl + '/query/' + environment_name + "/" + debate_id + "/" + position_id + "/" + argument_id)
  //     .pipe(retry(1), catchError(this.errorHandl));
  //   return r
  // }


  // Error handling
  errorHandl(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}