import { Injectable } from '@angular/core';
import { Argument } from '../models/argument';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  agreementsByLevels = {
    'strongly_disagree':<any>[],
    'disagree':<any>[],
    'neutral':<any>[],
    'agree':<any>[],
    'strongly_agree':<any>[]
  }

  constructor() { }

  getAgreementsByLevel(allAgreements:any[]) {
    let output = JSON.parse(JSON.stringify(this.agreementsByLevels));

    for (let agreement of allAgreements) {
      if (agreement.level >= -100 && agreement.level < -50) {
        let interval = 50;
        let valuePosition= Math.abs((agreement.level+100))
        agreement.offset = Math.trunc((valuePosition/interval)*100)/100;
        output['strongly_disagree'].push(agreement);
      } else if (agreement.level >= -50 && agreement.level < 0) {
        let interval = 50;
        let valuePosition= Math.abs((agreement.level+50))
        agreement.offset = Math.trunc((valuePosition/interval)*100)/100;
        output['disagree'].push(agreement);
      }  else if (agreement.level >= 0 && agreement.level <= 50) {
        let interval = 51;
        let valuePosition= Math.abs((agreement.level-0))
        agreement.offset = Math.trunc((valuePosition/interval)*100)/100;
        output['agree'].push(agreement);
      } else if (agreement.level > 50 && agreement.level <= 100) {
        let interval = 51;
        let valuePosition= Math.abs((agreement.level-50))
        agreement.offset = Math.trunc((valuePosition/interval)*100)/100;
        output['strongly_agree'].push(agreement);
      } else {
        agreement.offset = 0
        output['neutral'].push(agreement);
      }
    }

    return output;
  }

  getAgreementsLabel(level:any) {

      if (level >= -100 && level < -50) {
       return 'strong disagreement'
      } else if (level >= -50 && level < 0) {
        return 'disagreement'
      }  else if (level >= 0 && level <= 50) {
        return 'agreement'
      } else if (level > 50 && level <= 100) {
        return 'strong agreement'
      } else {
        return 'neutral'
      }

  }

  getAgreementFace(level:any) {

    if (level >= -100 && level < -50) {
     return 'disagree02.svg'
    } else if (level >= -50 && level < 0) {
      return 'disagree01.svg'
    }  else if (level >= 0 && level <= 50) {
      return 'agree01.svg'
    } else if (level > 50 && level <= 100) {
      return 'agree02.svg'
    } else {
      return 'neutral.svg'
    }

}

  getAllAgreements(positionsAgreements:any[], argumentsAgreements:any[], argumentsArray:Argument[]) {
    let agreements = [];
    let output = [];
    agreements = positionsAgreements.concat(argumentsAgreements);
    
    for (let argument of argumentsArray) {
      if (argument.type === 'SUPPORTING') {
        agreements.push({
          argumentId: argument.id,
          debateId: argument.debateId,
          level: 50,
          metadata: argument.metadata,
          offset: 0,
          positionId: argument.positionId,
          type:'implicit',
          mode:'supporting'
        })
      } else {
        agreements.push({
          argumentId: argument.id,
          debateId: argument.debateId,
          level: -50,
          metadata: argument.metadata,
          offset: 0,
          positionId: argument.positionId,
          type:'implicit',
          mode:'opposing'
        })
      }
    }

    for (let agreement of agreements) {

      if (!isNaN(agreement.level)) {

        let interval = 200;
        let valuePosition= Math.abs((agreement.level+100))
        agreement.absoluteOffset = Math.trunc((valuePosition/interval)*100)/100;
        output.push(agreement);
      }
    }
    return output;
  }

  groupByUsers(allAgreements:any[]) {
    let users = <any>{};
    let output = [];

    for (let agreement of allAgreements) {
      if (!users[agreement.metadata.createdBy]) {
        users[agreement.metadata.createdBy] = {
          average: 0,
          agreements:[]
        }
      }

      users[agreement.metadata.createdBy].agreements.push(agreement);
      users[agreement.metadata.createdBy].average += Math.trunc((agreement.level-users[agreement.metadata.createdBy].average)/users[agreement.metadata.createdBy].agreements.length*100)/100;
    }

    for (let userKey in users) {
      let interval = 200;
      let valuePosition= Math.abs((users[userKey].average+100))
      let absoluteOffset = Math.trunc((valuePosition/interval)*100)/100;
      let obj ={
        level: users[userKey].average,
        metadata: {
          createdBy: userKey
        },
        offset: 0,
        absoluteOffset
      }

      output.push(obj);
    }

    return output;
  }

  groupByPositions(allAgreements:any[], positionsDict:any) {
    let positions = <any>{};
    let output = [];

    for (let agreement of allAgreements) {
      if (agreement.positionId && positionsDict[agreement.positionId]) {
        if (!positions[agreement.positionId]) {
          positions[agreement.positionId] = {
            average: 0,
            agreements:[],
            positionId:agreement.positionId
          }
        }
  
        positions[agreement.positionId].agreements.push(agreement);
        positions[agreement.positionId].average += Math.trunc((agreement.level-positions[agreement.positionId].average)/positions[agreement.positionId].agreements.length*100)/100;
      }
    }

    for (let positionId in positions) {
      let interval = 200;
      let valuePosition= Math.abs((positions[positionId].average+100))
      let absoluteOffset = Math.trunc((valuePosition/interval)*100)/100;
      let obj ={
        level: positions[positionId].average,
        metadata: {
          createdBy: positionId
        },
        offset: 0,
        absoluteOffset,
        positionId:positionId
      }

      output.push(obj);
    }

    return output;
  }

  checkParticipants(participants:any[],array:any[]) {
    let counter = 1;
    for (let contribution of array) {
      let findParticipant = participants.find((p)=>contribution.metadata.createdBy === p.id)

      if (!findParticipant) {
        participants.push({
          pseudo:'',
          avatar:counter%70,
          id: contribution.metadata.createdBy,
          metadata: {
            createdBy: contribution.metadata.createdBy
          }
        })
      }
    }

    return participants;
  }
  
}