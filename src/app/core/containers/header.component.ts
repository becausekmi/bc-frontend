import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <mat-toolbar class="appHeader">
      <app-brand></app-brand>
      <span class="spacer"></span>
      <nav class="mainNav">
        <ng-container *ngFor="let link of links">
          <a mat-button
            [routerLink]="[link.route]"
            >{{ link.label }}</a>
        </ng-container>
      </nav>
      <span class="spacer"></span>
      <app-auth-context></app-auth-context>
    </mat-toolbar>
  `,
  styles: [`
    .appHeader {
      padding-left:10%;
      padding-right:10%;
      background: rgb(0, 115, 182) !important; // @todo theme
      color: white;
      // .mainNav {}
      .spacer { flex: 1 1 auto; }
    }
  `],
})
export class HeaderComponent {

  // @todo move routes in a file of costants
  links = [
    { route: '/explore', label: 'Explore' },
    { route: '/reflect', label: 'Reflect' },
    { route: '/discuss', label: 'Discuss' },
    { route: '/decide',  label: 'Decide'  },
  ];

}
