import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { LayoutService } from '../services/layout.service';

@Component({
  selector: 'app-layout',
  template: `
    <app-header *ngIf="layoutOn$ | async"></app-header>
    <div style="overflow-y:scroll" class="scroll-bar" fxFill>
      <ng-content></ng-content>
      <!-- <app-footer *ngIf="layoutOn$ | async"></app-footer> -->
    </div>
  `,
  styles: []
})
export class LayoutComponent {

  layoutOn$: BehaviorSubject<boolean>;

  constructor(private layout: LayoutService) {
    this.layoutOn$ = this.layout.enabled$;
  }

}
