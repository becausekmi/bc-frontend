import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <div class="appFooter">
      Designed by <b>KMi</b>
    </div>
  `,
  styles: [`
    .appFooter {
      background: rgb(0, 115, 182); // @todo theme
      color: white;
      text-align: center;
      padding: 20px;
    }
  `]
})
export class FooterComponent { }
