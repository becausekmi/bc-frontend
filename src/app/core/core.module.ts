import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { provideDatabase,getDatabase } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAnalyticsModule } from '@angular/fire/compat/analytics';


// Shared modules
import { MaterialModule } from '../shared/material.module';
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';

// Routing module
import { CoreRoutingModule } from './core-routing.module';

// Components
import { HeroComponent } from './components/hero.component';

// Containers
import { HeaderComponent } from './containers/header.component';
import { FooterComponent } from './containers/footer.component';
import { LayoutComponent } from './containers/layout.component';

// Pages
import { HomeComponent } from './pages/home.component';

// Services
import { LayoutService } from './services/layout.service';
import { DebateService } from './services/debate.service'; // @todo remove
import { AnalyticsService } from './services/analytics.service'; // @todo remove
import { FirebaseService } from './services/apis/firebase.service';
import { DClientService } from './services/apis/dclient.service';
import { ApiService } from './services/api.service';

// Config
import { environment } from '../../environments/environment';
import { HomeSectionComponent } from './components/home-section.component';
import { PositionService } from './services/position.service';
import { EmailService } from './services/email.service';

@NgModule({
  declarations: [
    // --- Components
    HeroComponent,
    HomeSectionComponent,
    // --- Containers
    HeaderComponent,
    FooterComponent,
    //
    HomeComponent, // @todo WHY!!!
    // --- Main Container
    LayoutComponent,
  ],
  imports: [
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideDatabase(() => getDatabase()),
    AngularFireModule.initializeApp(environment.firebase),
    CommonModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    /* AngularFireAnalyticsModule, */
    MaterialModule,
    SharedModule,
    AuthModule,
    CoreRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule
  ],
  exports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // --- Public Components
    LayoutComponent,
  ],
  providers: [
    LayoutService,
    DebateService,
    FirebaseService,
    DClientService,
    ApiService,
    AnalyticsService,
    PositionService,
    EmailService
  ]
})
export class CoreModule { }
