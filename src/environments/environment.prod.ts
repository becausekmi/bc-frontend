import firebase from '../../firebase.json';
import env from '../../env.json';
import pack from '../../package.json';

export const environment = {
  production: true,
  firebase: firebase.app,
  api: env.API,
  eth: {
    networkId: env.ETH_NETWORK_ID,
    providerUrl: env.ETH_PROVIDER_URL
  }
};
