#!/bin/sh

echo "Switching to production environment (bcause-alpha01)"

cp config_files/firebase.bcause-alpha01.json firebase.json
cp firebase.json functions/firebase.json 
cp config_files/.firebaserc.bcause-alpha01 .firebaserc
cp config_files/bcause-alpha01-firebase-adminsdk-gxffx-6e7cc4f31a.json functions/service_account.json