const functions = require("firebase-functions");
const admin = require("firebase-admin");

const nodemailer = require('nodemailer')
const cors = require('cors')({origin: true});
const urlMetadata = require('url-metadata');
const axios = require('axios')

const serviceAccount = require("./service_account.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://bcause-alpha01-default-rtdb.europe-west1.firebasedatabase.app"
  });

  
const mailTransport = nodemailer.createTransport({
    host: 'smtps.aruba.it',
    port: 465 ,
    secure: true, // true for 465, false for other ports
    auth: {
        user: 'bcause@composingstudio.com', // generated ethereal user
        pass: 'Testest2020!@'  // generated ethereal password
    },
    tls: {
        rejectUnauthorized:false
    }
});

// -------------------- TRIGGERS -------------------- //
// -------------------- TRIGGERS -------------------- //
// -------------------- TRIGGERS -------------------- //

// POSITION TRIGGER

exports.positionTriggers = functions.database.ref('/positions/{debateId}/{positionId}')
    .onWrite(async (change, context) => {
        try {
            // CONTEXT VALUES -> context.params.debateId
            // PREV VALUES -> change.before
            // NEXT VALUES -> change.after

            let debateId = context.params.debateId;
            let positionId = context.params.positionId;
            axios.get('https://master03.ou.bcause.app/summary/summary/'+ serviceAccount.project_id +'/'+debateId)
            .then(resp=>{
                console.log(JSON.stringify(resp.data));
            })
            

            if (!change.after.exists()) {
                // DATA DELETED
                let oldPosition = change.before.val();

                // update global stats on positions
                // users -> uid -> stats

                await removePositionDataFromUserContributions(oldPosition);

                return true;
            } else if (change.before.exists()) {
                // DATA UPDATED

                return true;
            } else {
                // DATA CREATED

                // Grab the current value of what was written to the Realtime Database.
                const newPosition = change.after.val();
                await addPositionDataToUserContributions(newPosition);
                return true;
            }

        } catch(err) {
            console.log("ERROR");
            console.log(JSON.stringify(err));
            return err;
        }

    });

exports.argumentTriggers = functions.database.ref('/claims/{debateId}/{positionId}/{argumentId}')
    .onWrite(async (change, context) => {
        try {

            // CONTEXT VALUES -> context.params.debateId
            // PREV VALUES -> change.before
            // NEXT VALUES -> change.after

            let debateId = context.params.debateId;
            let positionId = context.params.positionId;
            let argumentId = context.params.argumentId;

            axios.get('https://master03.ou.bcause.app/summary/summary/'+ serviceAccount.project_id +'/'+debateId)
            .then(resp => {
                console.log(JSON.stringify(resp.data));
            })

            if (!change.after.exists()) {
                // DATA DELETED
                let oldArgument = change.before.val();

                await removeArgumentDataFromUserContributions(oldArgument);

                return true;
            } else if (change.before.exists()) {
                // DATA UPDATED

                return true;
            } else {
                // DATA CREATED

                // Grab the current value of what was written to the Realtime Database.
                const newArgument = change.after.val();
                await addArgumentDataToUserContributions(newArgument);

                return true;
            }

        } catch(err) {
            console.log("ERROR");
            console.log(JSON.stringify(err));
            return err;
        }


    });


// -------------------- HTTP REQS -------------------- //
// -------------------- HTTP REQS -------------------- //
// -------------------- HTTP REQS -------------------- //

exports.alignPositionData = functions.region('europe-west1').https.onRequest(async (request, response) => {
    let allPositions = [];
    let byDebate = [];
    let usersAlreadyProcessed = []
    admin.database().ref('positions').once('value', (snapshot)=>{
        snapshot.forEach(snap =>{
            byDebate.push(snap.val());
        })

        for (let debateData of byDebate) {
            for (let positionKey in debateData) {
                allPositions.push(debateData[positionKey]);
            }
        }

        let promises = [];

        for (let position of allPositions) {
            if (position.metadata) {
                promises.push(resetUserPositionsStats(position));
                usersAlreadyProcessed.push(position.metadata.createdBy);
            }
        }

       

        Promise.all(promises).then(ok=>{
            let promises2 = [];
            for (let position of allPositions) {
                promises2.push(addPositionDataToUserContributions(position));
            }
            Promise.all(promises2).then(async ok2=>{
                response.send({positions: allPositions});
            })
        })
    })
})

exports.alignArgumentData = functions.region('europe-west1').https.onRequest(async (request, response) => {
    let allArguments = [];
    let byDebate = [];
    let byPosition = [];
    let debateId = '';
    let usersAlreadyProcessed = [];
    admin.database().ref('claims').once('value', (snapshot)=>{
        snapshot.forEach(snap =>{
            byDebate.push(snap.val());
        })

        for (let debateData of byDebate) {
            for (let positionKey in debateData) {
                byPosition.push(debateData[positionKey]);
            }
        }

        for (let positionData of byPosition) {
            for (let argumentKey in positionData) {
                allArguments.push(positionData[argumentKey]);
            }
        }

        let promises = [];


        for (let argument of allArguments) {
            /* if (usersAlreadyProcessed.indexOf(argument.metadata.createdBy+'_'+argument.debateId) === -1) { */
                promises.push(resetUserArgumentsStats(argument));
                usersAlreadyProcessed.push(argument.metadata.createdBy+'_'+argument.debateId);
           /*  } */
        }


        Promise.all(promises).then(ok=>{
            let promises2 = [];
            for (let argument of allArguments) {
                promises2.push(addArgumentDataToUserContributions(argument));
            }
            Promise.all(promises2).then(async ok=>{
                console.log(debateId);
                /* console.log(resp.data); */
                response.send({arguments: allArguments});
            })
        })
    })
})

exports.alignPositionDimensionsData = functions.region('europe-west1').https.onRequest(async (request, response) => {
    let allPositions = [];
    let byDebate = [];
    let usersAlreadyProcessed = []
    admin.database().ref('positionDimensions').once('value', (snapshot)=>{
        snapshot.forEach(snap =>{
            byDebate.push(snap.val());
        })

        for (let debateData of byDebate) {
            for (let positionKey in debateData) {
                for (let userKey in debateData[positionKey].users)
                    allPositions.push(debateData[positionKey].users[userKey]);
            }
        }

        let promises = [];

        for (let position of allPositions) {
            if (position.metadata) {
                promises.push(updatePositionDimensionData(position));
            }
        }

        Promise.all(promises).then(ok=>{
            response.send({positions: allPositions});
        })
    })
})

exports.alignClaimDimensionsData = functions.region('europe-west1').https.onRequest(async (request, response) => {
    let allClaims = [];
    let byDebate = [];
    let usersAlreadyProcessed = []
    admin.database().ref('claimDimensions').once('value', (snapshot)=>{
        snapshot.forEach(snap =>{
            byDebate.push(snap.val());
        })

        for (let debateData of byDebate) {
            for (let positionKey in debateData) {
                for (let argumentKey in debateData[positionKey]) {
                    for (let userKey in debateData[positionKey][argumentKey].users)
                        allClaims.push(debateData[positionKey][argumentKey].users[userKey]);
                }
            }
        }

        let promises = [];

        for (let claim of allClaims) {
            if (claim.metadata) {
                promises.push(updateClaimDimensionData(claim));
            }
        }

        Promise.all(promises).then(ok=>{
            response.send({claims: allClaims});
        })
    })
})

exports.scrapeURL = functions.region('europe-west1').https.onRequest(async (request, response) => {

  cors(request, response, async () => {
      let url = request.query.url;
      url = decodeSafeUrl(url);
      console.log(url);
      urlMetadata(url).then(
        resp => {
            response.send(resp);
        },
        error => {
            response.send(error).status(500);
        }
      );
  })
});

exports.sendEmail = functions.region('europe-west1').https.onRequest(async (request, response) => {

    cors(request, response, async () => {
        try {
            let html = request.body.html;
            let subject = request.body.subject;
            let recipients = request.body.recipients;

            console.log(subject);
            console.log(recipients);
            
            const mailOptionsCustomer = {
                from: 'Bcause <bcause@composingstudio.com>',
                to: recipients,
                subject: subject,
                html: html
            }

            let sent1 = await mailTransport.sendMail(mailOptionsCustomer);
            console.log(JSON.stringify(sent1));
            response.status(200).json({message:sent1});
        } catch(err) {
            console.log(err);
            response.status(400).send(err);
        }
    })
  });

  
// -------------------- ORBIS HTTP REQS -------------------- //
// -------------------- ORBIS HTTP REQS -------------------- //
// -------------------- ORBIS HTTP REQS -------------------- //


exports.getDiscussionsV2 = functions.region('europe-west1').https.onRequest(async (request,response)=>{
    cors(request, response, async () => {
        try {
            await orbis_isAuthorized(request);
            let debates = await orbis_getDiscussions();
            response.status(200).json(debates);
        } catch(err) {
            console.log(err);
            response.status(400).send(err);
        }
    });
})

exports.getContributionsV2 = functions.region('europe-west1').https.onRequest(async (request,response)=>{
    cors(request, response, async () => {
        try {
            await orbis_isAuthorized(request);
            let debateKey = request.query.debateId;
            if (!debateKey) {
                throw('no-debate-key-found');
            } else {
                output = [];
                positions = [];
                claims = [];
                let argumentsDB = await orbis_getDiscussionArguments(debateKey);
                let positionsDB = await orbis_getDiscussionPositions(debateKey);
                let positionsHearts = await orbis_getPositionsHearts(debateKey);
                let argumentsHearts = await orbis_getClaimsHearts(debateKey);
                for (let positionKey in argumentsDB) {
                    let argumentsKeys = argumentsDB[positionKey];
                    for (let argumentKey in argumentsKeys) {
                        let argumentToParse = argumentsKeys[argumentKey];
                        claims.push(argumentToParse);
                    }
                }

                for (let argument of claims) {
                    let arg = await orbis_parseArgument(argument,claims,argumentsHearts);
                    output.push(arg);
                }

                for (let position of positionsDB) {
                   let pos = await orbis_parsePosition(position,claims,positionsHearts);
                   output.push(pos);
                }
                
                response.status(200).json(output);
            }
        } catch(err) {
            console.log(err);
            response.status(400).send(err);
        }
    });
})

exports.getParticipantsV2 = functions.region('europe-west1').https.onRequest(async (request,response)=>{
    cors(request, response, async () => {
        try {
            await orbis_isAuthorized(request);
            let debateKey = request.query.debateId;
            if (!debateKey) {
                throw('no-debate-key-found');
            } else {
                let participantsDB = await orbis_getDiscussionParticipants(debateKey);
                response.status(200).json(participantsDB);
            }
        } catch(err) {
            console.log(err);
            response.status(400).send(err);
        }
    });
})



// -------------------- FUNCTIONS -------------------- //
// -------------------- FUNCTIONS -------------------- //
// -------------------- FUNCTIONS -------------------- //

function decodeSafeUrl(value) {
  const valueBase64 = decodeURI(value);
  return Buffer.from(valueBase64, 'base64').toString('utf8');
}

async function resetUserArgumentsStats(argument) {

    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/arguments').set(0);
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/cons').set(0);
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/pros').set(0);
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/arguments').set(0);
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/cons').set(0);
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/pros').set(0);
    return true;
}

function resetUserPositionsStats(position) {
    return new Promise(async (resolve,reject)=>{
        if (position.metadata) {
            await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/positions').set(null);
            await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/stats/positions').set(0);
            await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/debates/'+position.debateId+'/stats/positions').set(0);
        }
        resolve(true);
    })
}

async function addPositionDataToUserContributions(position) {
    return new Promise(async (resolve,reject)=>{
        if (position.metadata) {
            
            await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/stats/positions').set(admin.database.ServerValue.increment(1));

            // update debate stats on positions
            // users -> uid -> debates -> debateId -> stats
            await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/debates/'+position.debateId+'/stats/positions').set(admin.database.ServerValue.increment(1));

            // push position inside the users/positions/debateId collection
            await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/positions/'+position.debateId+'/'+position.id).set(position);
        }
        resolve(true);
        
    })
}

async function updatePositionDimensionData(reflection) {
    return new Promise(async (resolve,reject)=>{
        if (reflection.metadata) {
            
            await admin.database().ref('positionDimensions/'+reflection.debateId+'/'+reflection.positionId+'/users/'+reflection.metadata.createdBy+'/levels/polarization').set(reflection.levels.polarized);

            // if before 11/05 the swap value
            
            if (reflection.metadata.createdDate < 1652227200000)
                await admin.database().ref('positionDimensions/'+reflection.debateId+'/'+reflection.positionId+'/users/'+reflection.metadata.createdBy+'/levels/trust').set(100-reflection.levels.unclear);
            else
                await admin.database().ref('positionDimensions/'+reflection.debateId+'/'+reflection.positionId+'/users/'+reflection.metadata.createdBy+'/levels/trust').set(reflection.levels.unclear);

            
            await admin.database().ref('positionDimensions/'+reflection.debateId+'/'+reflection.positionId+'/users/'+reflection.metadata.createdBy+'/levels/prioritization').set(reflection.levels.unpolarized);

        }
        resolve(true);
        
    })
}

async function updateClaimDimensionData(reflection) {
    return new Promise(async (resolve,reject)=>{
        if (reflection.metadata) {
            
            await admin.database().ref('claimDimensions/'+reflection.debateId+'/'+reflection.positionId+'/'+reflection.claimId+'/users/'+reflection.metadata.createdBy+'/levels/polarization').set(reflection.levels.polarized);

            // if before 11/05 the swap value
            
            if (reflection.metadata.createdDate < 1652227200000)
                await admin.database().ref('claimDimensions/'+reflection.debateId+'/'+reflection.positionId+'/'+reflection.claimId+'/users/'+reflection.metadata.createdBy+'/levels/trust').set(100-reflection.levels.unclear);
            else
                await admin.database().ref('claimDimensions/'+reflection.debateId+'/'+reflection.positionId+'/'+reflection.claimId+'/users/'+reflection.metadata.createdBy+'/levels/trust').set(reflection.levels.unclear);

        }
        resolve(true);
        
    })
}

async function addArgumentDataToUserContributions(argument) {
    return new Promise(async (resolve,reject)=>{

        // update global stats on arguments
        // users -> uid -> stats
        await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/arguments').set(admin.database.ServerValue.increment(1));

        // update debate stats on arguments
        // users -> uid -> debates -> debateId -> stats
        await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/arguments').set(admin.database.ServerValue.increment(1));

        // push position inside the users/arguments/debateId collection
        await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/arguments/'+argument.debateId+'/'+argument.positionId+'/'+argument.id).set(argument);

        if (argument.type === 'OPPOSING') {
            await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/cons').set(admin.database.ServerValue.increment(1));
            await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/cons').set(admin.database.ServerValue.increment(1));
        } else {
            await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/pros').set(admin.database.ServerValue.increment(1));
            await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/pros').set(admin.database.ServerValue.increment(1));
        }

        resolve(true);
    })
}

async function removePositionDataFromUserContributions(position) {
    await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/stats/positions').set(admin.database.ServerValue.increment(-1));

    // update debate stats on positions
    // users -> uid -> debates -> debateId -> stats
    await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/debates/'+position.debateId+'/stats/positions').set(admin.database.ServerValue.increment(-1));

    // remove position from users positions
    await admin.database().ref('usersContributions/'+position.metadata.createdBy+'/positions/'+position.debateId+'/'+position.id).remove();

    // remove positions arguments and subcollections (hearts, reflections)

    // ----------------> ALERT - THIS WILL CALL ANOTHER TRIGGER!! <-----------------
    //await admin.database().ref('claims/'+debateId+'/'+positionId).remove();

    // remove positions subcollections
    await admin.database().ref('positionAgreements/'+debateId+'/'+positionId).remove();
    await admin.database().ref('positionDimensions/'+debateId+'/'+positionId).remove();
    await admin.database().ref('positionHearts/'+debateId+'/'+positionId).remove();

    return null;
}

async function removeArgumentDataFromUserContributions(argument) {
    // update global stats on positions
    // users -> uid -> stats
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/arguments').set(admin.database.ServerValue.increment(-1));

    // update debate stats on positions
    // users -> uid -> debates -> debateId -> stats
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/arguments').set(admin.database.ServerValue.increment(-1));

    if (argument.type === 'OPPOSING') {
        await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/cons').set(admin.database.ServerValue.increment(-1));
        await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/cons').set(admin.database.ServerValue.increment(-1));
    } else {
        await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/stats/pros').set(admin.database.ServerValue.increment(-1));
        await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/debates/'+argument.debateId+'/stats/pros').set(admin.database.ServerValue.increment(-1));
    }

    // remove argument from users arguments
    await admin.database().ref('usersContributions/'+argument.metadata.createdBy+'/arguments/'+argument.debateId+'/'+argument.positionId+'/'+argument.id).remove();

    // remove argument subcollections (hearts, reflections)

    await admin.database().ref('claimAgreements/'+debateId+'/'+positionId+'/'+argumentId).remove();
    await admin.database().ref('claimDimensions/'+debateId+'/'+positionId+'/'+argumentId).remove();
    await admin.database().ref('claimHearts/'+debateId+'/'+positionId+'/'+argumentId).remove();
    return null;
}


// ORBIS FUNCTIONS
// ORBIS FUNCTIONS
// ORBIS FUNCTIONS
// ORBIS FUNCTIONS

async function orbis_isAuthorized(request) {
    try {
        if (request.headers.authorization && request.headers.authorization === '7fdcbaf2562c9dab2a12fa740fab6e176d3d6f16') {
        } else {
            throw('no-authorization')
        }
    } catch(err) {
        throw(err);
    }
}

async function orbis_getDiscussions() {
    return new Promise(async (resolve,reject)=>{
        let debates = [];
        admin.database().ref('debates').once('value', (snapshot)=>{
            snapshot.forEach(snap =>{
                let debate = orbis_parseDiscussion(snap.val());
                debates.push(debate);
            })

            resolve(debates);
        });
    });
}

function orbis_parseDiscussion(discussion) {
    let output= {
        id: discussion.id,
        author_id: discussion.metadata.createdBy,
        creation_timestamp: discussion.metadata.createdDate,
        last_update_timestamp: discussion.metadata.createdDate,
        title: discussion.title,
        tagline: discussion.text,
        image: discussion.image
    }

    return output;
}

async function orbis_getDiscussionPositions(debateKey) {
    return new Promise(async (resolve,reject)=>{
        let positions = [];
        admin.database().ref('positions/'+debateKey).once('value', async (snapshot)=>{
            snapshot.forEach(snap =>{
                positions.push(snap.val());
            })
            resolve(positions);
        });
    });
}

async function orbis_getDiscussionArguments(debateKey) {
    return new Promise(async (resolve,reject)=>{
        let arguments = [];
        admin.database().ref('claims/'+debateKey).once('value', async (snapshot)=>{
            snapshot.forEach(snap =>{
                arguments.push(snap.val());
            })
            resolve(arguments);
        });
    });
}

async function orbis_getDiscussionParticipants(debateKey) {
    return new Promise(async (resolve,reject)=>{
        let participants = [];
        admin.database().ref('participants/'+debateKey).once('value', async (snapshot)=>{
            snapshot.forEach(snap =>{
                let obj = {
                    id: snap.val().id,
                    pseudo: snap.val().pseudo,
                    creation_timestamp: snap.val().metadata.createdDate,
                    last_update_timestamp: snap.val().metadata.createdDate,
                }
                participants.push(obj);
            })
            resolve(participants);
        });
    });
}

async function orbis_parseArgument(argument, arguments, hearts) {

    return new Promise(async (resolve,reject)=>{

        let linked_parent_item = "";
        linked_parent_item = argument.positionId;

        let discussion_type = 'argument_against';

        if (argument.type == 'SUPPORTING') {
            discussion_type = 'argument_for';
        }

        let output= {
            id: argument.id,
            author_id: argument.metadata.createdBy,
            text: argument.text,
            discussion_type:discussion_type,
            linked_parent_item,
            linked_children_items:[],
            upvotes:0,
            downvotes:0,
            creation_timestamp: argument.metadata.createdDate,
            last_update_timestamp: argument.metadata.createdDate,
            //originalData: argument
        }
        
        let children = arguments.filter((a)=>a.parentId === argument.id);
        for (let c of children) {
            output.linked_children_items.push(c.id);
        }
        let foundHeart = hearts.find((h)=>h.argumentId === argument.id);
        if (foundHeart) {
            output.upvotes = foundHeart.total;
        }
        resolve(output);
    });  
}

async function orbis_parsePosition(position, arguments, hearts) {
    return new Promise(async (resolve,reject)=>{

        let linked_parent_item = "";
        if (position.parentId) {
            linked_parent_item = position.parentId;
        }
        let output= {
            id: position.id,
            author_id: position.metadata.createdBy,
            text: position.text,
            discussion_type: 'position',
            linked_parent_item,
            linked_children_items:[],
            upvotes:0,
            downvotes:0,
            creation_timestamp: position.metadata.createdDate,
            last_update_timestamp: position.metadata.createdDate,
            //originalData: position
        }

        let children = arguments.filter((a)=>a.positionId === position.id);
        for (let c of children) {
            output.linked_children_items.push(c.id);
        }
        let foundHeart = hearts.find((h)=>h.positionId === position.id);
        if (foundHeart) {
            output.upvotes = foundHeart.total;
        }

        resolve(output);
    });  
}

async function orbis_getPositionsHearts(debateKey) {
    return new Promise(async (resolve,reject)=>{
        let hearts = [];
        admin.database().ref('positionHearts/'+debateKey).once('value', async (snapshot)=>{
            snapshot.forEach(snap =>{
                hearts.push({
                    positionId: snap.key,
                    total: snap.val().total
                });
            })
            resolve(hearts);
        });
    });
}

async function orbis_getClaimsHearts(debateKey) {
    return new Promise(async (resolve,reject)=>{
        let hearts = [];
        admin.database().ref('claimHearts/'+debateKey).once('value', async (snapshot)=>{
            snapshot.forEach(snap =>{
                hearts.push({
                    argumentId: snap.key,
                    total: snap.val().total
                });
            })
            resolve(hearts);
        });
    });
}